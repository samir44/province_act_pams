<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LocalLevelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('local_levels')->insert(
            [
                [
                    "name"=>"सप्तकोशी  नगरपालिका ",
                    "code"=>80215407,
                    "district_id"=>15,
                    "status"=>1
                ],
                [
                    "name"=>"मनरा गाउँपालिका",
                    "code"=>80218407,
                    "district_id"=>18,
                    "status"=>1
                ],
                [
                    "name"=>"भँगाहा गाउँपालिका",
                    "code"=>80218405,
                    "district_id"=>18,
                    "status"=>1
                ],
                [
                    "name"=>"बलवा गाउँपालिका",
                    "code"=>80218404,
                    "district_id"=>18,
                    "status"=>1
                ],
                [
                    "name"=>"रंगेली नगरपालिका ",
                    "code"=>80113404,
                    "district_id"=>13,
                    "status"=>1
                ],
                [
                    "name"=>"औरही गाउँपालिका",
                    "code"=>80216502,
                    "district_id"=>16,
                    "status"=>1
                ],
                [
                    "name"=>"भगवानपुर गाउँपालिका",
                    "code"=>80216506,
                    "district_id"=>16,
                    "status"=>1
                ],
                [
                    "name"=>"दुधकौशिका गाउँपालिका",
                    "code"=>80109503,
                    "district_id"=>9,
                    "status"=>1                ],
                [
                    "name"=>"मुसिकोट नगरपालिका ",
                    "code"=>80552401 ,
                    "district_id"=>52,
                    "status"=>1
                ],
                [
                    "name"=>"मलंगवा  नगरपालिका ",
                    "code"=>80219407 ,
                    "district_id"=>19,
                    "status"=>1
                ],
                [
                    "name"=>"कालीगण्डकी गाउँपालिका",
                    "code"=>80552502,
                    "district_id"=>52,
                    "status"=>1
                ],
                [
                    "name"=>"व्यास नगरपालिका ",
                    "code"=>80438403,
                    "district_id"=>38,
                    "status"=>1
                ],
                [
                    "name"=>"गुल्मीदरबार गाउँपालिका",
                    "code"=>80552503,
                    "district_id"=>52,
                    "status"=>1
                ],
                [
                    "name"=>"नीलकण्ठ नगरपालिका ",
                    "code"=>80330402,
                    "district_id"=>30,
                    "status"=>1
                ],
                [
                    "name"=>"गजुरी गाउँपालिका",
                    "code"=>80330503,
                    "district_id"=>30,
                    "status"=>1
                ],
                [
                    "name"=>"महाङ्काल गाउँपालिका",
                    "code"=>80334502,
                    "district_id"=>34,
                    "status"=>1
                ],
                [
                    "name"=>"वरेङ गाउँपालिका",
                    "code"=>80445506,
                    "district_id"=>45,
                    "status"=>1
                ],
                [
                    "name"=>"हरिवन नगरपालिका ",
                    "code"=>80219411,
                    "district_id"=>19,
                    "status"=>1
                ],
                [
                    "name"=>"बलरा नगरपालिका ",
                    "code"=>80219405,
                    "district_id"=>19,
                    "status"=>1
                ],
                [
                    "name"=>"फाल्गुनन्द गाउँपालिका",
                    "code"=>80102503,
                    "district_id"=>2,
                    "status"=>1                ],
                [
                    "name"=>"चम्पादेवी गाउँपालिका",
                    "code"=>80110502,
                    "district_id"=>10,
                    "status"=>1
                ],
                [
                    "name"=>"जनकपुर उपमहानगरपालिका",
                    "code"=>80217301 ,
                    "district_id"=>17,
                    "status"=>1
                ],
                [
                    "name"=>"कुम्मायक गाउँपालिका",
                    "code"=>80102501,
                    "district_id"=>2,
                    "status"=>1                ],
                [
                    "name"=>"चिशंखुगढी गाउँपालिका",
                    "code"=>80110503,
                    "district_id"=>10,
                    "status"=>1
                ],
                [
                    "name"=>"चुलाचुली गाउँपालिका",
                    "code"=>80103501,
                    "district_id"=>3,
                    "status"=>1                ],
                [
                    "name"=>"रुपाकोट मझुवागढी  नगरपालिका ",
                    "code"=>80108506,
                    "district_id"=>8,
                    "status"=>1                ],
                [
                    "name"=>"नेत्रावती गाउँपालिका",
                    "code"=>80330508,
                    "district_id"=>30,
                    "status"=>1
                ],
                [
                    "name"=>"त्रिपुरासुन्दरी गाउँपालिका",
                    "code"=>80330506,
                    "district_id"=>30,
                    "status"=>1
                ],
                [
                    "name"=>"थाहा नगरपालिका ",
                    "code"=>80332401,
                    "district_id"=>32,
                    "status"=>1
                ],
                [
                    "name"=>"कटहरिया गाउँपालिका",
                    "code"=>80220402,
                    "district_id"=>20,
                    "status"=>1
                ],
                [
                    "name"=>"मौलापुर गाउँपालिका",
                    "code"=>80220414,
                    "district_id"=>20,
                    "status"=>1
                ],
                [
                    "name"=>"फतुवाविजयपुर गाउँपालिका",
                    "code"=>80220410,
                    "district_id"=>20,
                    "status"=>1
                ],
                [
                    "name"=>"कलैया उपमहानगरपालिका",
                    "code"=>80221301,
                    "district_id"=>21,
                    "status"=>1
                ],
                [
                    "name"=>"बारागढी गाउँपालिका",
                    "code"=>80221507,
                    "district_id"=>21,
                    "status"=>1
                ],
                [
                    "name"=>"धोबीनी गाउँपालिका",
                    "code"=>80222506,
                    "district_id"=>22,
                    "status"=>1
                ],
                [
                    "name"=>"NULL",
                    "code"=>80222509,
                    "district_id"=>22,
                    "status"=>1
                ],
                [
                    "name"=>"पर्सागढी गाउँपालिका",
                    "code"=>80222401,
                    "district_id"=>22,
                    "status"=>1
                ],
                [
                    "name"=>"सखुवा प्रसौनी गाउँपालिका",
                    "code"=>80222510,
                    "district_id"=>22,
                    "status"=>1
                ],
                [
                    "name"=>"पटेर्वा सुगौली गाउँपालिका",
                    "code"=>80222508,
                    "district_id"=>22,
                    "status"=>1
                ],
                [
                    "name"=>"भरतपुर महानगरपालिका",
                    "code"=>80331201,
                    "district_id"=>31,
                    "status"=>1
                ],
                [
                    "name"=>"खैरहनी नगरपालिका ",
                    "code"=>80331402,
                    "district_id"=>31,
                    "status"=>1
                ],
                [
                    "name"=>"माडी नगरपालिका ",
                    "code"=>80331403,
                    "district_id"=>31,
                    "status"=>1
                ],
                [
                    "name"=>"रत्ननगर नगरपालिका ",
                    "code"=>80331404,
                    "district_id"=>31,
                    "status"=>1
                ],
                [
                    "name"=>"मष्टा गाउँपालिका ",
                    "code"=>80770508,
                    "district_id"=>70,
                    "status"=>1
                ],
                [
                    "name"=>"धुर्कोट गाउँपालिका",
                    "code"=>80552506 ,
                    "district_id"=>52,
                    "status"=>1
                ],
                [
                    "name"=>"मालिका गाउँपालिका",
                    "code"=>80552508,
                    "district_id"=>52,
                    "status"=>1
                ],
                [
                    "name"=>"सूर्मा गाउँपालिका ",
                    "code"=>80770510,
                    "district_id"=>70,
                    "status"=>1
                ],
                [
                    "name"=>"प्यूठान नगरपालिका ",
                    "code"=>80555401,
                    "district_id"=>55,
                    "status"=>1
                ],
                [
                    "name"=>"मंगलसेन नगरपालिका ",
                    "code"=>80772403,
                    "district_id"=>72,
                    "status"=>1
                ],
                [
                    "name"=>"बुद्धभुमी नगरपालिका ",
                    "code"=>80549403,
                    "district_id"=>49,
                    "status"=>1
                ],
                [
                    "name"=>"रामारोशन गाउँपालिका",
                    "code"=>80772506,
                    "district_id"=>72,
                    "status"=>1
                ],
                [
                    "name"=>"मल्लरानी गाउँपालिका",
                    "code"=>80555505,
                    "district_id"=>55,
                    "status"=>1
                ],
                [
                    "name"=>"ढकारी गाउँपालिका",
                    "code"=>80772502,
                    "district_id"=>72,
                    "status"=>1
                ],
                [
                    "name"=>"नौबहिनी गाउँपालिका",
                    "code"=>80555504,
                    "district_id"=>55,
                    "status"=>1
                ],
                [
                    "name"=>"छत्रदेव गाउँपालिका",
                    "code"=>80551501,
                    "district_id"=>51,
                    "status"=>1
                ],
                [
                    "name"=>"के.आई.सिं. गाउँपालिका",
                    "code"=>80771502,
                    "district_id"=>71,
                    "status"=>1
                ],
                [
                    "name"=>"चौकुने गाउँपालिका",
                    "code"=>80668502,
                    "district_id"=>68,
                    "status"=>1
                ],
                [
                    "name"=>"ठाँटीकाँध गाउँपालिका",
                    "code"=>80667502,
                    "district_id"=>67,
                    "status"=>1
                ],
                [
                    "name"=>"बर्दगोरिया गाउँपालिका",
                    "code"=>80777505,
                    "district_id"=>77,
                    "status"=>1
                ],
                [
                    "name"=>"त्रिवेणी गाउँपालिका",
                    "code"=>80659501,
                    "district_id"=>59,
                    "status"=>1
                ],
                [
                    "name"=>"मोहन्याल गाउँपालिका",
                    "code"=>80777506,
                    "district_id"=>77,
                    "status"=>1
                ],
                [
                    "name"=>"कैलारी गाउँपालिका",
                    "code"=>80777501,
                    "district_id"=>77,
                    "status"=>1
                ],
                [
                    "name"=>"शारदा नगरपालिका ",
                    "code"=>80660403,
                    "district_id"=>60,
                    "status"=>1
                ],
                [
                    "name"=>"जुनीचाँदे गाउँपालिका",
                    "code"=>80666502,
                    "district_id"=>66,
                    "status"=>1
                ],
                [
                    "name"=>"बेलौरी नगरपालिका ",
                    "code"=>80776404,
                    "district_id"=>76,
                    "status"=>1
                ],
                [
                    "name"=>"मुड्केचुला गाउँपालिका",
                    "code"=>80661505,
                    "district_id"=>61,
                    "status"=>1
                ],
                [
                    "name"=>"कनकासुन्दरी गाउँपालिका",
                    "code"=>80662501,
                    "district_id"=>62,
                    "status"=>1
                ],
                [
                    "name"=>"अमरगढी नगरपालिका ",
                    "code"=>80775401,
                    "district_id"=>75,
                    "status"=>1
                ],
                [
                    "name"=>"गढवा गाउँपालिका",
                    "code"=>80556501,
                    "district_id"=>56,
                    "status"=>1
                ],
                [
                    "name"=>"पाटन नगरपालिका ",
                    "code"=>80774403,
                    "district_id"=>74,
                    "status"=>1
                ],
                [
                    "name"=>"पुर्चौडी नगरपालिका ",
                    "code"=>80774402,
                    "district_id"=>74,
                    "status"=>1
                ],
                [
                    "name"=>"सुर्नया गाउँपालिका",
                    "code"=>80774505,
                    "district_id"=>74,
                    "status"=>1
                ],
                [
                    "name"=>"अजयमेरु गाउँपालिका",
                    "code"=>80775501,
                    "district_id"=>75,
                    "status"=>1
                ],
                [
                    "name"=>"खाँडाचक्र नगरपालिका ",
                    "code"=>80665401,
                    "district_id"=>65,
                    "status"=>1
                ],
                [
                    "name"=>"तिलागुफा नगरपालिका ",
                    "code"=>80665402,
                    "district_id"=>65,
                    "status"=>1
                ],
                [
                    "name"=>"सिगास गाउँपालिका",
                    "code"=>80774506,
                    "district_id"=>74,
                    "status"=>1
                ],
                [
                    "name"=>"पचालझरना गाउँपलिका",
                    "code"=>80665503,
                    "district_id"=>65,
                    "status"=>1
                ],
                [
                    "name"=>"बैजनाथ गाउँपालिका",
                    "code"=>80557505 ,
                    "district_id"=>57,
                    "status"=>1
                ],
                [
                    "name"=>"नरहरिनाथ गाउँपालिका",
                    "code"=>80665502,
                    "district_id"=>65,
                    "status"=>1
                ],
                [
                    "name"=>"गुलरिया नगरपालिका ",
                    "code"=>80558401,
                    "district_id"=>58,
                    "status"=>1
                ],
                [
                    "name"=>"दुहुँ गाउँपालिका",
                    "code"=>80773502,
                    "district_id"=>73,
                    "status"=>1
                ],
                [
                    "name"=>"छायाँनाथ रारा नगरपालिका ",
                    "code"=>80663401,
                    "district_id"=>63,
                    "status"=>1
                ],
                [
                    "name"=>"नौगाड गाउँपालिका",
                    "code"=>80773503,
                    "district_id"=>73,
                    "status"=>1
                ],
                [
                    "name"=>"मधुवन नगरपालिका ",
                    "code"=>80558405,
                    "district_id"=>58,
                    "status"=>1
                ],
                [
                    "name"=>"मुगुमकार्मारोंग गाउँपालिका",
                    "code"=>80663502,
                    "district_id"=>63,
                    "status"=>1
                ],
                [
                    "name"=>"महाकाली नगरपालिका ",
                    "code"=>80773504,
                    "district_id"=>73,
                    "status"=>1
                ],
                [
                    "name"=>"राजापुर नगरपालिका ",
                    "code"=>80558406,
                    "district_id"=>58,
                    "status"=>1
                ],
                [
                    "name"=>"लेकम गाउँपालिका",
                    "code"=>80773506,
                    "district_id"=>73,
                    "status"=>1
                ],
                [
                    "name"=>"व्याँस गाउँपालिका",
                    "code"=>80773507,
                    "district_id"=>73,
                    "status"=>1
                ],
                [
                    "name"=>"खत्याड गाउँपालिका",
                    "code"=>80663501,
                    "district_id"=>63,
                    "status"=>1
                ],
                [
                    "name"=>"बाँसगढी नगरपालिका",
                    "code"=>80558404 ,
                    "district_id"=>58,
                    "status"=>1
                ],
                [
                    "name"=>"नाम्खा गाउँपालिका",
                    "code"=>80664505 ,
                    "district_id"=>64,
                    "status"=>1
                ],
                [
                    "name"=>"बारबर्दिया नगरपालिका ",
                    "code"=>80558403,
                    "district_id"=>58,
                    "status"=>1
                ],
                [
                    "name"=>"खार्पुनाथ गाउँपालिका",
                    "code"=>80664502,
                    "district_id"=>64,
                    "status"=>1
                ],
                [
                    "name"=>"बढैयाताल गाउँपालिका",
                    "code"=>80558502,
                    "district_id"=>58,
                    "status"=>1
                ],
                [
                    "name"=>"सर्केगाड गाउँपालिका",
                    "code"=>80664506,
                    "district_id"=>64,
                    "status"=>1
                ],
                [
                    "name"=>"चंखेली गाउँपालिका",
                    "code"=>80664503,
                    "district_id"=>64,
                    "status"=>1
                ],
                [
                    "name"=>"पाण्डव गुफा गाउँपालिका",
                    "code"=>80769503,
                    "district_id"=>69,
                    "status"=>1
                ],
                [
                    "name"=>"ईलाम नगरपालिका ",
                    "code"=>80103401,
                    "district_id"=>3,
                    "status"=>1                ],
                [
                    "name"=>"सुनकोशी गाउँपालिका",
                    "code"=>80110507,
                    "district_id"=>10,
                    "status"=>1
                ],
                [
                    "name"=>"सुर्योदय नगरपालिका ",
                    "code"=>80103404,
                    "district_id"=>3,
                    "status"=>1                ],
                [
                    "name"=>"मिथिला नगरपालिका ",
                    "code"=>80217406,
                    "district_id"=>17,
                    "status"=>1
                ],
                [
                    "name"=>"विदुर नगरपालिका ",
                    "code"=>80329402,
                    "district_id"=>29,
                    "status"=>1
                ],
                [
                    "name"=>"बेलकोटगढी नगरपालिका ",
                    "code"=>80329401,
                    "district_id"=>29,
                    "status"=>1
                ],
                [
                    "name"=>"ककनी गाउँपालिका",
                    "code"=>80329501,
                    "district_id"=>29,
                    "status"=>1
                ],
                [
                    "name"=>"तादीगाउँ गाउँपालिका",
                    "code"=>80329503,
                    "district_id"=>29,
                    "status"=>1
                ],
                [
                    "name"=>"दुप्चेश्वर गाउँपालिका",
                    "code"=>80329505,
                    "district_id"=>29,
                    "status"=>1
                ],
                [
                    "name"=>"पञ्चकन्या गाउँपालिका",
                    "code"=>80329506,
                    "district_id"=>29,
                    "status"=>1
                ],
                [
                    "name"=>"लिखु गाउँपालिका",
                    "code"=>80329508,
                    "district_id"=>29,
                    "status"=>1
                ],
                [
                    "name"=>"मेघाङ गाउँपालिका",
                    "code"=>80329507,
                    "district_id"=>29,
                    "status"=>1
                ],
                [
                    "name"=>"शिवपुरी गाउँपालिका",
                    "code"=>80329509 ,
                    "district_id"=>29,
                    "status"=>1
                ],
                [
                    "name"=>"सुर्यगढी गाउँपालिका",
                    "code"=>80329510,
                    "district_id"=>29,
                    "status"=>1
                ],
                [
                    "name"=>"देवानगञ्ज गाउँपालिका",
                    "code"=>80114503,
                    "district_id"=>14,
                    "status"=>1
                ],
                [
                    "name"=>"गढी गाउँपालिका",
                    "code"=>80114502,
                    "district_id"=>14,
                    "status"=>1
                ],
                [
                    "name"=>"भिमफेदी गाउँपालिका",
                    "code"=>80332504,
                    "district_id"=>32,
                    "status"=>1
                ],
                [
                    "name"=>"राक्सिराङ्ग गाउँपालिका",
                    "code"=>80332507,
                    "district_id"=>32,
                    "status"=>1
                ],
                [
                    "name"=>"गौर नगरपालिका ",
                    "code"=>80220406,
                    "district_id"=>20,
                    "status"=>1
                ],
                [
                    "name"=>"बौधीमाई गाउँपालिका",
                    "code"=>80220412,
                    "district_id"=>20,
                    "status"=>1
                ],
                [
                    "name"=>"बृन्दावन गाउँपालिका",
                    "code"=>80220411,
                    "district_id"=>20,
                    "status"=>1
                ],
                [
                    "name"=>"राजपुर गाउँपालिका",
                    "code"=>80220503,
                    "district_id"=>20,
                    "status"=>1
                ],
                [
                    "name"=>"निजगढ नगरपालिका ",
                    "code"=>80221402,
                    "district_id"=>21,
                    "status"=>1
                ],
                [
                    "name"=>"करैयामाई गाउँपालिका",
                    "code"=>80221502,
                    "district_id"=>21,
                    "status"=>1
                ],
                [
                    "name"=>"प्रसौनी गाउँपालिका",
                    "code"=>80221505,
                    "district_id"=>21,
                    "status"=>1
                ],
                [
                    "name"=>"सुवर्ण गाँउपालिका",
                    "code"=>80221509,
                    "district_id"=>21,
                    "status"=>1
                ],
                [
                    "name"=>"विरगंज उपमहानगरपालिका",
                    "code"=>80222201,
                    "district_id"=>22,
                    "status"=>1
                ],
                [
                    "name"=>"पोखरिया नगरपालिका ",
                    "code"=>80222402,
                    "district_id"=>22,
                    "status"=>1
                ],
                [
                    "name"=>"सुवर्णपुर गाउँपालिका",
                    "code"=>80222505,
                    "district_id"=>22,
                    "status"=>1
                ],
                [
                    "name"=>"जगरनाथपुर गाउँपालिका",
                    "code"=>80222503,
                    "district_id"=>22,
                    "status"=>1
                ],
                [
                    "name"=>"छिपहरमाई गाउँपालिका",
                    "code"=>80222502,
                    "district_id"=>22,
                    "status"=>1
                ],
                [
                    "name"=>"बिन्दबासिनी गाउँपालिका",
                    "code"=>80222509,
                    "district_id"=>22,
                    "status"=>1
                ],
                [
                    "name"=>"बहुदरमाई गाउँपालिका",
                    "code"=>80222403,
                    "district_id"=>22,
                    "status"=>1
                ],
                [
                    "name"=>"राप्ती नगरपालिका ",
                    "code"=>80331405,
                    "district_id"=>31,
                    "status"=>1
                ],
                [
                    "name"=>"सुलीकोट गाउँपालिका",
                    "code"=>80436508,
                    "district_id"=>36,
                    "status"=>1
                ],
                [
                    "name"=>"सिरानचोक गाउँपालिका",
                    "code"=>80436509,
                    "district_id"=>36,
                    "status"=>1
                ],
                [
                    "name"=>"अजिरकोट गाउँपालिका",
                    "code"=>80436501,
                    "district_id"=>36,
                    "status"=>1
                ],
                [
                    "name"=>"आरूघाट गाउँपालिका",
                    "code"=>80436502,
                    "district_id"=>36,
                    "status"=>1
                ],
                [
                    "name"=>"गण्डकी गाउँपालिका",
                    "code"=>80436503,
                    "district_id"=>36,
                    "status"=>1
                ],
                [
                    "name"=>"भिमसेन गाउँपालिका",
                    "code"=>80436506,
                    "district_id"=>36,
                    "status"=>1
                ],
                [
                    "name"=>"शहिद लखन गाउँपालिका",
                    "code"=>80436507,
                    "district_id"=>36,
                    "status"=>1
                ],
                [
                    "name"=>"जयपृथ्वी नगरपालिका ",
                    "code"=>80770401,
                    "district_id"=>70,
                    "status"=>1
                ],
                [
                    "name"=>"बुंगल नगरपालिका ",
                    "code"=>80770402 ,
                    "district_id"=>70,
                    "status"=>1
                ],
                [
                    "name"=>"तलकोट गाँउपालिका ",
                    "code"=>80770505,
                    "district_id"=>70,
                    "status"=>1
                ],
                [
                    "name"=>"मदाने गाउँपालिका",
                    "code"=>80552507,
                    "district_id"=>52,
                    "status"=>1
                ],
                [
                    "name"=>"थलारा गाँउपालिका ",
                    "code"=>80770506,
                    "district_id"=>70,
                    "status"=>1
                ],
                [
                    "name"=>"वित्थडचिर गाँउपालिका ",
                    "code"=>80770509,
                    "district_id"=>70,
                    "status"=>1
                ],
                [
                    "name"=>"छबिसपाथिभेरा गाँउपालिका ",
                    "code"=>80770504,
                    "district_id"=>70,
                    "status"=>1
                ],
                [
                    "name"=>"दुर्गाथली गाँउपालिका ",
                    "code"=>80770507,
                    "district_id"=>70,
                    "status"=>1
                ],
                [
                    "name"=>"केदारस्युँ गाँउपालिका ",
                    "code"=>80770501,
                    "district_id"=>70,
                    "status"=>1
                ],
                [
                    "name"=>"साँफेवगर नगरपालिका ",
                    "code"=>80772404,
                    "district_id"=>72,
                    "status"=>1
                ],
                [
                    "name"=>"शुद्धोधन गाउँपालिका",
                    "code"=>80549504,
                    "district_id"=>49,
                    "status"=>1
                ],
                [
                    "name"=>"विजयनगर गाउँपालिका",
                    "code"=>80549503,
                    "district_id"=>49,
                    "status"=>1
                ],
                [
                    "name"=>"बडीकेदार गाउँपालिका",
                    "code"=>80771505,
                    "district_id"=>71,
                    "status"=>1
                ],
                [
                    "name"=>"लुङग्री गाउँपालिका",
                    "code"=>80554506,
                    "district_id"=>54,
                    "status"=>1
                ],
                [
                    "name"=>"सुकिदह गाउँपालिका",
                    "code"=>80554507,
                    "district_id"=>54,
                    "status"=>1
                ],
                [
                    "name"=>"दुल्लु नगरपालिका ",
                    "code"=>80667403,
                    "district_id"=>67,
                    "status"=>1
                ],
                [
                    "name"=>"महाबु गाउँपालिका",
                    "code"=>80667507,
                    "district_id"=>67,
                    "status"=>1
                ],
                [
                    "name"=>"भेरी नगरपालिका ",
                    "code"=>80666403,
                    "district_id"=>66,
                    "status"=>1
                ],
                [
                    "name"=>"सानीभेरी गाउँपालिका",
                    "code"=>80659503,
                    "district_id"=>59,
                    "status"=>1
                ],
                [
                    "name"=>"शिवालय गाउँपालिका",
                    "code"=>80666504,
                    "district_id"=>66,
                    "status"=>1
                ],
                [
                    "name"=>"पुनर्वास नगरपालिका ",
                    "code"=>80776402,
                    "district_id"=>76,
                    "status"=>1
                ],
                [
                    "name"=>"त्रिवेणी गाउँपालिका",
                    "code"=>80660506,
                    "district_id"=>60,
                    "status"=>1
                ],
                [
                    "name"=>"ठूलीभेरीनगरपालिका ",
                    "code"=>80661401,
                    "district_id"=>61,
                    "status"=>1
                ],
                [
                    "name"=>"बेदकोट नगरपालिका ",
                    "code"=>80776403,
                    "district_id"=>76,
                    "status"=>1
                ],
                [
                    "name"=>"छत्रेश्वरी गाउँपालिका",
                    "code"=>80660504,
                    "district_id"=>60,
                    "status"=>1
                ],
                [
                    "name"=>"डोल्पो बुद्ध गाउँपालिका",
                    "code"=>80661504,
                    "district_id"=>61,
                    "status"=>1
                ],
                [
                    "name"=>"तुल्सीपुर उपमहानगरपालिका",
                    "code"=>80556302,
                    "district_id"=>56,
                    "status"=>1
                ],
                [
                    "name"=>"छार्का ताङसोङ गाउँपालिका",
                    "code"=>80661502,
                    "district_id"=>61,
                    "status"=>1
                ],
                [
                    "name"=>"घोराही उपमहानगरपालिका",
                    "code"=>80556301,
                    "district_id"=>56,
                    "status"=>1
                ],
                [
                    "name"=>"लमही नगरपालिका ",
                    "code"=>80556401,
                    "district_id"=>56,
                    "status"=>1
                ],
                [
                    "name"=>"दशरथचन्द नगरपालिका ",
                    "code"=>80774401,
                    "district_id"=>74,
                    "status"=>1
                ],
                [
                    "name"=>"बेलडाँडी गाउँपालिका",
                    "code"=>80776501,
                    "district_id"=>76,
                    "status"=>1
                ],
                [
                    "name"=>"सिंजा गाउँपालिका",
                    "code"=>80662506,
                    "district_id"=>62,
                    "status"=>1
                ],
                [
                    "name"=>"बंगलाचुली गाउँपालिका",
                    "code"=>80556503,
                    "district_id"=>56,
                    "status"=>1
                ],
                [
                    "name"=>"तिला गाउँपालिका",
                    "code"=>80662504,
                    "district_id"=>62,
                    "status"=>1
                ],
                [
                    "name"=>"गुठिचौर गाउँपालिका",
                    "code"=>80662502,
                    "district_id"=>62,
                    "status"=>1
                ],
                [
                    "name"=>"शान्तिनगर गाउँपालिका",
                    "code"=>80556507,
                    "district_id"=>56,
                    "status"=>1
                ],
                [
                    "name"=>"आलिताल गाउँपालिका",
                    "code"=>80775502,
                    "district_id"=>75,
                    "status"=>1
                ],
                [
                    "name"=>"भागेश्वर गाउँपालिका",
                    "code"=>80775505,
                    "district_id"=>75,
                    "status"=>1
                ],
                [
                    "name"=>"कालिकामाई गाउँपालिका",
                    "code"=>80222501,
                    "district_id"=>22,
                    "status"=>1
                ],
                [
                    "name"=>"जिराभवानी गाउँपालिका",
                    "code"=>80222504,
                    "district_id"=>22,
                    "status"=>1
                ],
                [
                    "name"=>"बलान-विहुल गाउँपालिका",
                    "code"=>80215506,
                    "district_id"=>15,
                    "status"=>1
                ],
                [
                    "name"=>"सिदिङ्वा गाउँपालिका",
                    "code"=>80101507,
                    "district_id"=>1,
                    "status"=>1                ],
                [
                    "name"=>"रैनादेवी छहरा गाउँपालिका",
                    "code"=>80550506,
                    "district_id"=>50,
                    "status"=>1
                ],
                [
                    "name"=>"गैंडाकोट नगरपालिका ",
                    "code"=>80446402,
                    "district_id"=>46,
                    "status"=>1
                ],
                [
                    "name"=>"चन्द्रपुर नगरपालिका ",
                    "code"=>80220407 ,
                    "district_id"=>20,
                    "status"=>1
                ],
                [
                    "name"=>"गरुडा नगरपालिका ",
                    "code"=>80220405,
                    "district_id"=>20,
                    "status"=>1
                ],
                [
                    "name"=>"माधव नारायण गाउँपालिका",
                    "code"=>80220413,
                    "district_id"=>20,
                    "status"=>1
                ],
                [
                    "name"=>"परोहा गाउँपालिका",
                    "code"=>80220409,
                    "district_id"=>20,
                    "status"=>1
                ],
                [
                    "name"=>"जीतपुर सिमरा उपमहानगरपालिका",
                    "code"=>80221302,
                    "district_id"=>21,
                    "status"=>1
                ],
                [
                    "name"=>"छत्रकोट गाउँपालिका",
                    "code"=>80552505,
                    "district_id"=>52,
                    "status"=>1
                ],
                [
                    "name"=>"सिम्रौनगढ नगरपालिका ",
                    "code"=>80221405,
                    "district_id"=>21,
                    "status"=>1
                ],
                [
                    "name"=>"पकाहा मैनपुर गाउँपालिका",
                    "code"=>80222507,
                    "district_id"=>22,
                    "status"=>1
                ],
                [
                    "name"=>"कालिका नगरपालिका ",
                    "code"=>80331401 ,
                    "district_id"=>31,
                    "status"=>1
                ],
                [
                    "name"=>"चुमनुव्री गाउँपालिका",
                    "code"=>80436504,
                    "district_id"=>36,
                    "status"=>1
                ],
                [
                    "name"=>"स्वर्गद्वारी नगरपालिका ",
                    "code"=>80555402,
                    "district_id"=>55,
                    "status"=>1
                ],
                [
                    "name"=>"कपिलवस्तु नगरपालिका ",
                    "code"=>80549401,
                    "district_id"=>49,
                    "status"=>1
                ],
                [
                    "name"=>"कमलबजार नगरपालिका ",
                    "code"=>80772401,
                    "district_id"=>72,
                    "status"=>1
                ],
                [
                    "name"=>"चौरपाटी गाउँपालिका",
                    "code"=>80772501,
                    "district_id"=>72,
                    "status"=>1
                ],
                [
                    "name"=>"कृष्णनगर नगरपालिका ",
                    "code"=>80549402,
                    "district_id"=>49,
                    "status"=>1
                ],
                [
                    "name"=>"दिपायल सिलगढी नगरपालिका ",
                    "code"=>80771401,
                    "district_id"=>71,
                    "status"=>1
                ],
                [
                    "name"=>"ऐरावती गाउँपालिका",
                    "code"=>80555501,
                    "district_id"=>55,
                    "status"=>1
                ],
                [
                    "name"=>"भुमिकास्थान नगरपालिका ",
                    "code"=>80551401,
                    "district_id"=>51,
                    "status"=>1
                ],
                [
                    "name"=>"त्रिवेणी गाउँपालिका",
                    "code"=>80554501,
                    "district_id"=>54,
                    "status"=>1
                ],
                [
                    "name"=>"दुईखोली गाउँपालिका",
                    "code"=>80554503,
                    "district_id"=>54,
                    "status"=>1
                ],
                [
                    "name"=>"माडी गाउँपालिका",
                    "code"=>80554504,
                    "district_id"=>54,
                    "status"=>1
                ],
                [
                    "name"=>"पञ्चपुरी नगरपालिका ",
                    "code"=>80668402,
                    "district_id"=>68,
                    "status"=>1
                ],
                [
                    "name"=>"रुन्टीगढी गाउँपालिका",
                    "code"=>80554505,
                    "district_id"=>54,
                    "status"=>1
                ],
                [
                    "name"=>"टिकापुर नगरपालिका ",
                    "code"=>80777404,
                    "district_id"=>77,
                    "status"=>1
                ],
                [
                    "name"=>"सिम्ता गाउँपालिका",
                    "code"=>80668504,
                    "district_id"=>68,
                    "status"=>1
                ],
                [
                    "name"=>"नारायण नगरपालिका ",
                    "code"=>80667404,
                    "district_id"=>67,
                    "status"=>1
                ],
                [
                    "name"=>"नौमुले गाउँपालिका",
                    "code"=>80667504,
                    "district_id"=>67,
                    "status"=>1
                ],
                [
                    "name"=>"भजनी नगरपालिका ",
                    "code"=>80777405,
                    "district_id"=>77,
                    "status"=>1
                ],
                [
                    "name"=>"गोदावरी नगरपालिका ",
                    "code"=>80777401,
                    "district_id"=>77,
                    "status"=>1
                ],
                [
                    "name"=>"पुथा उत्तरगंगा गाउँपालिका",
                    "code"=>80553501,
                    "district_id"=>53,
                    "status"=>1
                ],
                [
                    "name"=>"गौरीगंगा नगरपालिका ",
                    "code"=>80777402,
                    "district_id"=>77,
                    "status"=>1
                ],
                [
                    "name"=>"भूमे गाउँपालिका",
                    "code"=>80553502,
                    "district_id"=>53,
                    "status"=>1
                ],
                [
                    "name"=>"त्रिवेणी नलगाड नगरपालिका ",
                    "code"=>80666402,
                    "district_id"=>66,
                    "status"=>1
                ],
                [
                    "name"=>"कुसे गाउँपालिका",
                    "code"=>80666501,
                    "district_id"=>66,
                    "status"=>1
                ],
                [
                    "name"=>"भिमदत्त नगरपालिका ",
                    "code"=>80776405,
                    "district_id"=>76,
                    "status"=>1
                ],
                [
                    "name"=>"ढोरचौर गाउँपालिका",
                    "code"=>80660505,
                    "district_id"=>60,
                    "status"=>1
                ],
                [
                    "name"=>"दार्मा गाउँपालिका",
                    "code"=>80660507,
                    "district_id"=>60,
                    "status"=>1
                ],
                [
                    "name"=>"जगदुल्ला गाउँपालिका",
                    "code"=>80661503,
                    "district_id"=>61,
                    "status"=>1
                ],
                [
                    "name"=>"कृष्णपुर नगरपालिका ",
                    "code"=>80776401,
                    "district_id"=>76,
                    "status"=>1
                ],
                [
                    "name"=>"काईके गाउँपालिका",
                    "code"=>80661501,
                    "district_id"=>61,
                    "status"=>1
                ],
                [
                    "name"=>"चन्दननाथ नगरपालिका",
                    "code"=>80662401,
                    "district_id"=>62,
                    "status"=>1
                ],
                [
                    "name"=>"लालझाडी गाउँपालिका",
                    "code"=>80776502,
                    "district_id"=>76,
                    "status"=>1
                ],
                [
                    "name"=>"हिमा गाउँपालिका",
                    "code"=>80662507,
                    "district_id"=>62,
                    "status"=>1
                ],
                [
                    "name"=>"दंगीशरण गाउँपालिका",
                    "code"=>80556502,
                    "district_id"=>56,
                    "status"=>1
                ],
                [
                    "name"=>"राजपुर गाउँपालिका",
                    "code"=>80556505,
                    "district_id"=>56,
                    "status"=>1
                ],
                [
                    "name"=>"परशुराम नगरपालिका ",
                    "code"=>80775402,
                    "district_id"=>75,
                    "status"=>1
                ],
                [
                    "name"=>"राप्ती गाउँपालिका",
                    "code"=>80556506,
                    "district_id"=>56,
                    "status"=>1
                ],
                [
                    "name"=>"मेलौली नगरपालिका ",
                    "code"=>80774404 ,
                    "district_id"=>74,
                    "status"=>1
                ],
                [
                    "name"=>"खुम्वु पासाङल्हामु गाउँपालिका",
                    "code"=>80109501,
                    "district_id"=>9,

"status"=>1                ],
                [
                    "name"=>"रामग्राम नगरपालिका ",
                    "code"=>80547402,
                    "district_id"=>47,
                    "status"=>1
                ],
                [
                    "name"=>"वाह्रविसे नगरपालिका ",
                    "code"=>80327403,
                    "district_id"=>27,
                    "status"=>1
                ],
                [
                    "name"=>"मेन्छयायेम गाउँपालिका",
                    "code"=>80105504,
                    "district_id"=>5,

"status"=>1                ],
                [
                    "name"=>"चैनपुर नगरपालिका ",
                    "code"=>80104402,
                    "district_id"=>4,

"status"=>1                ],
                [
                    "name"=>"अन्नपूर्ण गाउँपालिका",
                    "code"=>80444501,
                    "district_id"=>44,
                    "status"=>1
                ],
                [
                    "name"=>"ललितपुर महानगरपालिका",
                    "code"=>80334201,
                    "district_id"=>34,
                    "status"=>1
                ],
                [
                    "name"=>"गोदावरी नगरपालिका ",
                    "code"=>80334401,
                    "district_id"=>34,
                    "status"=>1
                ],
                [
                    "name"=>"बाग्मती गाउँपालिका",
                    "code"=>80334503,
                    "district_id"=>34,
                    "status"=>1
                ],
                [
                    "name"=>"ढोरपाटन नगरपालिका ",
                    "code"=>80445403,
                    "district_id"=>45,
                    "status"=>1
                ],
                [
                    "name"=>"तमानखोला गाउँपालिका",
                    "code"=>80445502,
                    "district_id"=>45,
                    "status"=>1
                ],
                [
                    "name"=>"भक्तपुर नगरपालिका ",
                    "code"=>80333402,
                    "district_id"=>33,
                    "status"=>1
                ],
                [
                    "name"=>"ताराखोला गाउँपालिका",
                    "code"=>80445503,
                    "district_id"=>45,
                    "status"=>1
                ],
                [
                    "name"=>"ब्रह्मपुरी गाउँपालिका ",
                    "code"=>80219404,
                    "district_id"=>19,
                    "status"=>1
                ],
                [
                    "name"=>"निसीखोला गाउँपालिका",
                    "code"=>80445504,
                    "district_id"=>45,
                    "status"=>1
                ],
                [
                    "name"=>"बुढानिलकण्ठ नगरपालिका ",
                    "code"=>80335409,
                    "district_id"=>35,
                    "status"=>1
                ],
                [
                    "name"=>"उमाकुण्ड गाउँपालिका",
                    "code"=>80324501,
                    "district_id"=>24,
                    "status"=>1
                ],
                [
                    "name"=>"कालिन्चोक गाउँपालिका",
                    "code"=>80323501,
                    "district_id"=>23,
                    "status"=>1
                ],
                [
                    "name"=>"ईन्द्रावती गाउँपालिका",
                    "code"=>80327501,
                    "district_id"=>27,
                    "status"=>1
                ],
                [
                    "name"=>"बेसीशहर नगरपालिका ",
                    "code"=>80437401,
                    "district_id"=>37,
                    "status"=>1
                ],
                [
                    "name"=>"दोर्दी गाउँपालिका",
                    "code"=>80437503,
                    "district_id"=>37,
                    "status"=>1
                ],
                [
                    "name"=>"नमोबुद्ध नगरपालिका ",
                    "code"=>80326402,
                    "district_id"=>26,
                    "status"=>1
                ],
                [
                    "name"=>"थासाङ गाउँपालिका",
                    "code"=>80441502,
                    "district_id"=>41,
                    "status"=>1
                ],
                [
                    "name"=>"खानीखोला गाउँपालिका",
                    "code"=>80326501,
                    "district_id"=>26,
                    "status"=>1
                ],
                [
                    "name"=>"रुपनी गाउँपालिका",
                    "code"=>80215509 ,
                    "district_id"=>15,
                    "status"=>1
                ],
                [
                    "name"=>"बेल्ही चपेना गाउँपालिका",
                    "code"=>80215505,
                    "district_id"=>15,
                    "status"=>1
                ],
                [
                    "name"=>"बुढीगंगा गाउँपालिका",
                    "code"=>80113507,
                    "district_id"=>13,
                    "status"=>1
                ],
                [
                    "name"=>"अन्नपूर्ण गाउँपालिका",
                    "code"=>80439501,
                    "district_id"=>39,
                    "status"=>1
                ],
                [
                    "name"=>"देवदह नगरपालिका ",
                    "code"=>80548402,
                    "district_id"=>48,
                    "status"=>1
                ],
                [
                    "name"=>"कन्चन गाउँपालिका",
                    "code"=>80548502,
                    "district_id"=>48,
                    "status"=>1
                ],
                [
                    "name"=>"कोटहीमाई गाउँपालिका",
                    "code"=>80548503,
                    "district_id"=>48,
                    "status"=>1
                ],
                [
                    "name"=>"बर्जु गाउँपालिका",
                    "code"=>80114504,
                    "district_id"=>14,
                    "status"=>1
                ],
                [
                    "name"=>"भोक्राहा गाउँपालिका",
                    "code"=>80114505,
                    "district_id"=>14,
                    "status"=>1
                ],
                [
                    "name"=>"पाख्रिवास नगरपालिका ",
                    "code"=>80106402,
                    "district_id"=>6,

"status"=>1                ],
                [
                    "name"=>"तारकेश्वर नगरपालिका ",
                    "code"=>80335406 ,
                    "district_id"=>35,
                    "status"=>1
                ],
                [
                    "name"=>"मुखियापट्टि मुसहरमिया गाउँपालिका",
                    "code"=>80217505,
                    "district_id"=>17,
                    "status"=>1
                ],
                [
                    "name"=>"पिपरा गाउँपालिका",
                    "code"=>80218503,
                    "district_id"=>18,
                    "status"=>1
                ],
                [
                    "name"=>"रतुवामाई नगरपालिका ",
                    "code"=>80113405,
                    "district_id"=>13,
                    "status"=>1
                ],
                [
                    "name"=>"ग्रामथान गाउँपालिका",
                    "code"=>80113504,
                    "district_id"=>13,
                    "status"=>1
                ],
                [
                    "name"=>"धनकुटा नगरपालिका ",
                    "code"=>80106401,
                    "district_id"=>6,

"status"=>1                ],
                [
                    "name"=>"चाँगुनारायण नगरपालिका ",
                    "code"=>80333401,
                    "district_id"=>33,
                    "status"=>1
                ],
                [
                    "name"=>"शंखरापुर नगरपालिका ",
                    "code"=>80335410,
                    "district_id"=>35,
                    "status"=>1
                ],
                [
                    "name"=>"गोलन्जोर गाउँपालिका",
                    "code"=>80325501,
                    "district_id"=>25,
                    "status"=>1
                ],
                [
                    "name"=>"मन्थली नगरपालिका ",
                    "code"=>80324401,
                    "district_id"=>24,
                    "status"=>1
                ],
                [
                    "name"=>"गोकुलगङ्गा गाउँपालिका",
                    "code"=>80324503,
                    "district_id"=>24,
                    "status"=>1
                ],
                [
                    "name"=>"सुनापति गाउँपालिका",
                    "code"=>80324506,
                    "district_id"=>24,
                    "status"=>1
                ],
                [
                    "name"=>"वडिगाड गाउँपालिका",
                    "code"=>80445505,
                    "district_id"=>45,
                    "status"=>1
                ],
                [
                    "name"=>"मेलुङ्ग गाउँपालिका",
                    "code"=>80323504,
                    "district_id"=>23,
                    "status"=>1
                ],
                [
                    "name"=>"मेलम्ची नगरपालिका ",
                    "code"=>80327402,
                    "district_id"=>27,
                    "status"=>1
                ],
                [
                    "name"=>"जुगल गाउँपालिका",
                    "code"=>80327502,
                    "district_id"=>27,
                    "status"=>1
                ],
                [
                    "name"=>"पाँचपोखरी थाङपाल गाउँपालिका",
                    "code"=>80327504,
                    "district_id"=>27,
                    "status"=>1
                ],
                [
                    "name"=>"मध्यनेपाल नगरपालिका ",
                    "code"=>80437402,
                    "district_id"=>37,
                    "status"=>1
                ],
                [
                    "name"=>"लिसंखुपाखर गाउँपालिका",
                    "code"=>80327507,
                    "district_id"=>27,
                    "status"=>1
                ],
                [
                    "name"=>"सुनकोशी गाउँपालिका",
                    "code"=>80327508,
                    "district_id"=>27,
                    "status"=>1
                ],
                [
                    "name"=>"चापाकोट नगरपालिका ",
                    "code"=>80443402,
                    "district_id"=>43,
                    "status"=>1
                ],
                [
                    "name"=>"दालोमे गाउँपालिका",
                    "code"=>80441503,
                    "district_id"=>41,
                    "status"=>1
                ],
                [
                    "name"=>"लोमन्थाङ गाउँपालिका",
                    "code"=>80441505,
                    "district_id"=>41,
                    "status"=>1
                ],
                [
                    "name"=>"विश्रामपुर गाउँपालिका",
                    "code"=>80221508,
                    "district_id"=>21,
                    "status"=>1
                ],
                [
                    "name"=>"हलेसीतुवाचुङ नगरपालिका ",
                    "code"=>80108402,
                    "district_id"=>8,

"status"=>1                ],
                [
                    "name"=>"देउमाई नगरपालिका ",
                    "code"=>80103402,
                    "district_id"=>3,

"status"=>1                ],
                [
                    "name"=>"धनुषाधाम नगरपालिका ",
                    "code"=>80217404,
                    "district_id"=>17,
                    "status"=>1
                ],
                [
                    "name"=>"वराहपोखरी गाउँपालिका",
                    "code"=>80108507 ,
                    "district_id"=>8,

"status"=>1                ],
                [
                    "name"=>"माईजोगमाई गाउँपालिका",
                    "code"=>80103503,
                    "district_id"=>3,

"status"=>1                ],
                [
                    "name"=>"शहिदनगर नगरपालिका ",
                    "code"=>80217408,
                    "district_id"=>17,
                    "status"=>1
                ],
                [
                    "name"=>"जनकनन्दिनी गाउँपालिका",
                    "code"=>80217502,
                    "district_id"=>17,
                    "status"=>1
                ],
                [
                    "name"=>"सन्दकपुर गाउँपालिका",
                    "code"=>80103506,
                    "district_id"=>3,

"status"=>1                ],
                [
                    "name"=>"बर्दिबास नगरपालिका ",
                    "code"=>80218403,
                    "district_id"=>18,
                    "status"=>1
                ],
                [
                    "name"=>"कचनकवल गाउँपालिका",
                    "code"=>80112501,
                    "district_id"=>12,
                    "status"=>1
                ],
                [
                    "name"=>"विराटनगर उपमहानगरपालिका",
                    "code"=>80113201,
                    "district_id"=>13,
                    "status"=>1
                ],
                [
                    "name"=>"लोहरपट्टी गाउँपालिका",
                    "code"=>80218409,
                    "district_id"=>18,
                    "status"=>1
                ],
                [
                    "name"=>"रामगोपालपुर गाउँपालिका",
                    "code"=>80218408,
                    "district_id"=>18,
                    "status"=>1
                ],
                [
                    "name"=>"पथरी शनिश्चरे नगरपालिका ",
                    "code"=>80113402,
                    "district_id"=>13,
                    "status"=>1
                ],
                [
                    "name"=>"तिरहुत गाउंपालिका",
                    "code"=>80215503,
                    "district_id"=>15,
                    "status"=>1
                ],
                [
                    "name"=>"औरही गाउँपालिका",
                    "code"=>80218502 ,
                    "district_id"=>18,
                    "status"=>1
                ],
                [
                    "name"=>"सुनवर्षी नगरपालिका ",
                    "code"=>80113408,
                    "district_id"=>13,
                    "status"=>1
                ],
                [
                    "name"=>"विष्णुपुर गाउँपालिका",
                    "code"=>80216406,
                    "district_id"=>16,
                    "status"=>1
                ],
                [
                    "name"=>"धरान उप-महानगरपालिका",
                    "code"=>80114302,
                    "district_id"=>14,
                    "status"=>1
                ],
                [
                    "name"=>"इटहरी उप-महानगरपालिका",
                    "code"=>80114401,
                    "district_id"=>14,
                    "status"=>1
                ],
                [
                    "name"=>"कोशी गाउँपालिका",
                    "code"=>80114501,
                    "district_id"=>14,
                    "status"=>1
                ],
                [
                    "name"=>"आठराई गाउँपालिका",
                    "code"=>80105501,
                    "district_id"=>5,
                    "status"=>1                ],
                [
                    "name"=>"मादी नगरपालिका ",
                    "code"=>80104405,
                    "district_id"=>4,
                    "status"=>1
                ],
                [
                    "name"=>"मकालु गाउँपालिका",
                    "code"=>80104503,
                    "district_id"=>4,
                    "status"=>1                ],
                [
                    "name"=>"षडानन्द नगरपालिका ",
                    "code"=>80107402,
                    "district_id"=>7,
                    "status"=>1                ],
                [
                    "name"=>"ट्याम्केमैयुम गाउँपालिका",
                    "code"=>80107503 ,
                    "district_id"=>7,
                    "status"=>1                ],
                [
                    "name"=>"अरुण गाउँपालिका",
                    "code"=>80107501 ,
                    "district_id"=>7,
                    "status"=>1
                ],
                [
                    "name"=>"साल्पासिलिछो गाउँपालिका",
                    "code"=>80107506,
                    "district_id"=>7,
                    "status"=>1
                ],
                [
                    "name"=>"जैमिनी नगरपालिका ",
                    "code"=>80445402,
                    "district_id"=>45,
                    "status"=>1
                ],
                [
                    "name"=>"काठेखोला गाउँपालिका",
                    "code"=>80445501,
                    "district_id"=>45,
                    "status"=>1
                ],
                [
                    "name"=>"गोडैटा नगरपालिका ",
                    "code"=>80219403,
                    "district_id"=>19,
                    "status"=>1
                ],
                [
                    "name"=>"काठमाण्डौं महानगरपालिका",
                    "code"=>80335201,
                    "district_id"=>35,
                    "status"=>1
                ],
                [
                    "name"=>"कविलासी गाउँपालिका",
                    "code"=>80219402,
                    "district_id"=>19,
                    "status"=>1
                ],
                [
                    "name"=>"कागेश्वरी–मनोहरा नगरपालिका ",
                    "code"=>80335401,
                    "district_id"=>35,
                    "status"=>1
                ],
                [
                    "name"=>"चक्रघट्टा गाउँपालिका",
                    "code"=>80219502,
                    "district_id"=>19,
                    "status"=>1
                ],
                [
                    "name"=>"चन्द्रागिरी नगरपालिका ",
                    "code"=>80335404,
                    "district_id"=>35,
                    "status"=>1
                ],
                [
                    "name"=>"टोखा नगरपालिका ",
                    "code"=>80335405,
                    "district_id"=>35,
                    "status"=>1
                ],
                [
                    "name"=>"कालिका गाउँपालिका",
                    "code"=>80328502,
                    "district_id"=>28,
                    "status"=>1
                ],
                [
                    "name"=>"गोसाईंकुण्ड गाउँपालिका",
                    "code"=>80328503,
                    "district_id"=>28,
                    "status"=>1
                ],
                [
                    "name"=>"घ्याङलेख गाउँपालिका",
                    "code"=>80325502,
                    "district_id"=>25,
                    "status"=>1
                ],
                [
                    "name"=>"मरिण गाउँपालिका",
                    "code"=>80325505,
                    "district_id"=>25,
                    "status"=>1
                ],
                [
                    "name"=>"हरिहरपुरगढी गाउँपालिका",
                    "code"=>80325507,
                    "district_id"=>25,
                    "status"=>1
                ],
                [
                    "name"=>"रामेछाप नगरपालिका ",
                    "code"=>80324402,
                    "district_id"=>24,
                    "status"=>1
                ],
                [
                    "name"=>"खाँडादेवी गाउँपालिका",
                    "code"=>80324502,
                    "district_id"=>24,
                    "status"=>1
                ],
                [
                    "name"=>"तामाकोशी गाउँपालिका",
                    "code"=>80323503,
                    "district_id"=>23,
                    "status"=>1
                ],
                [
                    "name"=>"बलेफी गाउँपालिका",
                    "code"=>80327505,
                    "district_id"=>27,
                    "status"=>1
                ],
                [
                    "name"=>"भोटेकोशी गाउँपालिका",
                    "code"=>80327506,
                    "district_id"=>27,
                    "status"=>1
                ],
                [
                    "name"=>"धुलिखेल नगरपालिका ",
                    "code"=>80326401,
                    "district_id"=>26,
                    "status"=>1
                ],
                [
                    "name"=>"पांचखाल नगरपालिका ",
                    "code"=>80326404,
                    "district_id"=>26,
                    "status"=>1
                ],
                [
                    "name"=>"चौंरीदेउराली गाउँपालिका",
                    "code"=>80326502,
                    "district_id"=>26,
                    "status"=>1
                ],
                [
                    "name"=>"बेथानचोक गाउँपालिका",
                    "code"=>80326504,
                    "district_id"=>26,
                    "status"=>1
                ],
                [
                    "name"=>"महाभारत गाउँपालिका",
                    "code"=>80326506,
                    "district_id"=>26,
                    "status"=>1
                ],
                [
                    "name"=>"स्वामीकार्तिक गाउँपालिका",
                    "code"=>80769504,
                    "district_id"=>69,
                    "status"=>1
                ],
                [
                    "name"=>"कौडेना गाउँपालिका",
                    "code"=>80219501,
                    "district_id"=>19,
                    "status"=>1
                ],
                [
                    "name"=>"पर्सा गाउँपालिका",
                    "code"=>80219505,
                    "district_id"=>19,
                    "status"=>1
                ],
                [
                    "name"=>"बसबरीया गाउँपालिका",
                    "code"=>80219507,
                    "district_id"=>19,
                    "status"=>1
                ],
                [
                    "name"=>"हिलिहाङ गाउँपालिका",
                    "code"=>80102507,
                    "district_id"=>2,
                    "status"=>1
                ],
                [
                    "name"=>"लामिडाँडा गाउँपालिका",
                    "code"=>80108401,
                    "district_id"=>8,
                    "status"=>1
                ],
                [
                    "name"=>"माङसेबुङ गाउँपालिका",
                    "code"=>80103504,
                    "district_id"=>3,
                    "status"=>1
                ],
                [
                    "name"=>"सबैला नगरपालिका ",
                    "code"=>80217409,
                    "district_id"=>17,
                    "status"=>1
                ],
                [
                    "name"=>"किस्पाङ गाउँपालिका",
                    "code"=>80329502,
                    "district_id"=>29,
                    "status"=>1
                ],
                [
                    "name"=>"बटेश्वर गाउँपालिका",
                    "code"=>80217504,
                    "district_id"=>17,
                    "status"=>1
                ],
                [
                    "name"=>"मंगला गाउँपालिका",
                    "code"=>80444503,
                    "district_id"=>44,
                    "status"=>1
                ],
                [
                    "name"=>"बरियारपट्टी गाउँपालिका",
                    "code"=>80216505,
                    "district_id"=>16,
                    "status"=>1
                ],
                [
                    "name"=>"धुनीबेंसी नगरपालिका ",
                    "code"=>80330401,
                    "district_id"=>30,
                    "status"=>1
                ],
                [
                    "name"=>"खनियाबास गाउँपालिका",
                    "code"=>80330501,
                    "district_id"=>30,
                    "status"=>1
                ],
                [
                    "name"=>"बागमती नगरपालिका ",
                    "code"=>80219406,
                    "district_id"=>19,
                    "status"=>1
                ],
                [
                    "name"=>"बरहथवा नगरपालिका ",
                    "code"=>80219506,
                    "district_id"=>19,
                    "status"=>1
                ],
                [
                    "name"=>"रामनगर गाउँपालिका",
                    "code"=>80219508,
                    "district_id"=>19,
                    "status"=>1
                ],
                [
                    "name"=>"विष्णु गाउँपालिका",
                    "code"=>80219509,
                    "district_id"=>19,
                    "status"=>1
                ],
                [
                    "name"=>"नौकुण्ड गाउँपालिका",
                    "code"=>80328504,
                    "district_id"=>28,
                    "status"=>1
                ],
                [
                    "name"=>"सुनकोशी गाउँपालिका",
                    "code"=>80325506,
                    "district_id"=>25,
                    "status"=>1
                ],
                [
                    "name"=>"ईशनाथ गाउँपालिका",
                    "code"=>80220401,
                    "district_id"=>20,
                    "status"=>1
                ],
                [
                    "name"=>"परवानीपुर गाउँपालिका",
                    "code"=>80221504,
                    "district_id"=>21,
                    "status"=>1
                ],
                [
                    "name"=>"इच्छाकामना गाउँपालिका",
                    "code"=>80331501,
                    "district_id"=>31,
                    "status"=>1
                ],
                [
                    "name"=>"गोरखा नगरपालिका ",
                    "code"=>80436401,
                    "district_id"=>36,
                    "status"=>1
                ],
                [
                    "name"=>"पालुङटार नगरपालिका ",
                    "code"=>80436402,
                    "district_id"=>36,
                    "status"=>1
                ],
                [
                    "name"=>"अर्नमा गाउँपालिका",
                    "code"=>80216501,
                    "district_id"=>16,
                    "status"=>1
                ],
                [
                    "name"=>"नवराजपुर गाउँपालिका",
                    "code"=>80216504,
                    "district_id"=>16,
                    "status"=>1
                ],
                [
                    "name"=>"राजदेवी नगरपालिका",
                    "code"=>80220415,
                    "district_id"=>20,
                    "status"=>1
                ],
                [
                    "name"=>"यमुनामाई गाउँपालिका",
                    "code"=>80220502,
                    "district_id"=>20,
                    "status"=>1
                ],
                [
                    "name"=>"फुङलीङ नगरपालिका ",
                    "code"=>80101401,
                    "district_id"=>1,
                    "status"=>1
                ],
                [
                    "name"=>"आठराई त्रिवेणी गाउँपालिका",
                    "code"=>80101501,
                    "district_id"=>1,
                    "status"=>1
                ],
                [
                    "name"=>"केपिलासगढी गाउँपालिका",
                    "code"=>80108502,
                    "district_id"=>8,
                    "status"=>1
                ],
                [
                    "name"=>"फाकफोकथुम गाउँपालिका",
                    "code"=>80103502,
                    "district_id"=>3,
                    "status"=>1
                ],
                [
                    "name"=>"दिप्रुङ गाउँपालिका",
                    "code"=>80108505 ,
                    "district_id"=>8,
                    "status"=>1
                ],
                [
                    "name"=>"साकेला गाउँपालिका",
                    "code"=>80108508,
                    "district_id"=>8,
                    "status"=>1
                ],
                [
                    "name"=>"त्रियुगा नगरपालिका ",
                    "code"=>80111403,
                    "district_id"=>11,
                    "status"=>1
                ],
                [
                    "name"=>"भद्रपुर नगरपालिका ",
                    "code"=>80112405,
                    "district_id"=>12,
                    "status"=>1
                ],
                [
                    "name"=>"कमल गाउँपालिका",
                    "code"=>80112502,
                    "district_id"=>12,
                    "status"=>1
                ],
                [
                    "name"=>"राजविराज नगरपालिका ",
                    "code"=>80215405,
                    "district_id"=>15,
                    "status"=>1
                ],
                [
                    "name"=>"हल्दिबारी गाउँपालिका",
                    "code"=>80112507,
                    "district_id"=>12,
                    "status"=>1
                ],
                [
                    "name"=>"बेलवारी नगरपालिका ",
                    "code"=>80113403,
                    "district_id"=>13,
                    "status"=>1
                ],
                [
                    "name"=>"साम्सी गाउँपालिका",
                    "code"=>80218505,
                    "district_id"=>18,
                    "status"=>1
                ],
                [
                    "name"=>"छिन्नमस्ता गाउँपालिका",
                    "code"=>80215502,
                    "district_id"=>15,
                    "status"=>1
                ],
                [
                    "name"=>"मटिहानी गाउँपालिका",
                    "code"=>80218406,
                    "district_id"=>18,
                    "status"=>1
                ],
                [
                    "name"=>"सुन्दर हरैचा नगरपालिका ",
                    "code"=>80113407,
                    "district_id"=>13,
                    "status"=>1
                ],
                [
                    "name"=>"सिरहा नगरपालिका ",
                    "code"=>80216408,
                    "district_id"=>16,
                    "status"=>1
                ],
                [
                    "name"=>"गोलबजार नगरपालिका ",
                    "code"=>80216403,
                    "district_id"=>16,
                    "status"=>1
                ],
                [
                    "name"=>"जहदा गाउँपालिका",
                    "code"=>80113505,
                    "district_id"=>13,
                    "status"=>1
                ],
                [
                    "name"=>"लक्ष्मीपुर पतारी गाउँपालिका",
                    "code"=>80216405,
                    "district_id"=>16,
                    "status"=>1
                ],
                [
                    "name"=>"मिर्चैया नगरपालिका ",
                    "code"=>80216507,
                    "district_id"=>16,
                    "status"=>1
                ],
                [
                    "name"=>"ईश्वरपुर नगरपालिका ",
                    "code"=>80219401,
                    "district_id"=>19,
                    "status"=>1
                ],
                [
                    "name"=>"छथर गाउँपालिका",
                    "code"=>80105502,
                    "status"=>1,"
                    district_id"=>5,
                ],
                [
                    "name"=>"फेदाप गाउँपालिका",
                    "code"=>80105503,
                    "status"=>1,
                    "district_id"=>5,
                ],
                [
                    "name"=>"भोजपुर नगरपालिका ",
                    "code"=>80107401,
                    "status"=>1,
                    "district_id"=>7,
                ],
                [
                    "name"=>"हतुवागढी गाउँपालिका",
                    "code"=>80107507,
                    "status"=>1,
                    "district_id"=>7,
                ],
                [
                    "name"=>"रघुगंगा गाउँपालिका",
                    "code"=>80444505,
                    "status"=>1,
                    "district_id"=>44,
                ],
                [
                    "name"=>"NULL",
                    "code"=>80442505,
                    "status"=>1,
                    "district_id"=>42,
                ],
                [
                    "name"=>"महालक्ष्मी नगरपालिका ",
                    "code"=>80334402,
                    "status"=>1,
                    "district_id"=>34,
                ],
                [
                    "name"=>"बाग्लुङ नगरपालिका ",
                    "code"=>80445404,
                    "status"=>1,
                    "district_id"=>45,
                ],
                [
                    "name"=>"गल्कोट नगरपालिका ",
                    "code"=>80445401,
                    "status"=>1,
                    "district_id"=>45,
                ],
                [
                    "name"=>"कोन्ज्योसोम गाउँपालिका",
                    "code"=>80334501,
                    "status"=>1,
                    "district_id"=>34,
                ],
                [
                    "name"=>"विगु गाउँपालिका",
                    "code"=>80323506,
                    "status"=>1,
                    "district_id"=>23,
                ],
                [
                    "name"=>"वैतेश्वर गाउँपालिका",
                    "code"=>80323505,
                    "status"=>1,
                    "district_id"=>23,
                ],
                [
                    "name"=>"राईनास नगरपालिका ",
                    "code"=>80437403,
                    "status"=>1,
                    "district_id"=>37,
                ],
                [
                    "name"=>"सुन्दरबजार नगरपालिका ",
                    "code"=>80437404,
                    "status"=>1,
                    "district_id"=>37,
                ],
                [
                    "name"=>"क्व्होलासोथार गाउँपालिका",
                    "code"=>80437501,
                    "status"=>1,
                    "district_id"=>37,
                ],
                [
                    "name"=>"दूधपोखरी गाउँपालिका",
                    "code"=>80437502,
                    "status"=>1,
                    "district_id"=>37,
                ],
                [
                    "name"=>"हेलम्बु गाउँपालिका",
                    "code"=>80327509,
                    "status"=>1,
                    "district_id"=>27,
                ],
                [
                    "name"=>"पनौती नगरपालिका ",
                    "code"=>80326403,
                    "status"=>1,
                    "district_id"=>26,
                ],
                [
                    "name"=>"रोशी गाउँपालिका",
                    "code"=>80326507,
                    "status"=>1,
                    "district_id"=>26,
                ],
                [
                    "name"=>"मकवानपुरगढी गाउँपालिका",
                    "code"=>80332505,
                    "status"=>1,
                    "district_id"=>32,
                ],
                [
                    "name"=>"मनहरी गाउँपालिका",
                    "code"=>80332506,
                    "status"=>1,
                    "district_id"=>32,
                ],
                [
                    "name"=>"देवाही गोनाही गाँउपालिका",
                    "code"=>80220408,
                    "status"=>1,
                    "district_id"=>20,
                ],
                [
                    "name"=>"दुर्गा भगवती गाउँपालिका",
                    "code"=>80220501,
                    "status"=>1,
                    "district_id"=>20,
                ],
                [
                    "name"=>"गढीमाई गाउँपालिका",
                    "code"=>80220404,
                    "status"=>1,
                    "district_id"=>20,
                ],
                [
                    "name"=>"रेसुंगा नगरपालिका ",
                    "code"=>80552402,
                    "status"=>1,
                    "district_id"=>52,
                ],
                [
                    "name"=>"आदर्श कोतवाल गाउँपालिका",
                    "code"=>80221501,
                    "status"=>1,
                    "district_id"=>21,
                ],
                [
                    "name"=>"फेटा गाउँपालिका",
                    "code"=>80221506,
                    "status"=>1,
                    "district_id"=>21,
                ],
                [
                    "name"=>"धार्चे गाउँपालिका",
                    "code"=>80436505,
                    "status"=>1,
                    "district_id"=>36,
                ],
                [
                    "name"=>"खप्तडछान्ना गाँउपालिका ",
                    "code"=>80770503,
                    "status"=>1,
                    "district_id"=>70,
                ],
                [
                    "name"=>"सरुमारानी गाउँपालिका",
                    "code"=>80555507,
                    "status"=>1,
                    "district_id"=>55,
                ],
                [
                    "name"=>"शिवराज नगरपालिका ",
                    "code"=>80549406,
                    "status"=>1,
                    "district_id"=>49,
                ],
                [
                    "name"=>"महाराजगञ्ज नगरपालिका ",
                    "code"=>80549405,
                    "status"=>1,
                    "district_id"=>49,
                ],
                [
                    "name"=>"यसोधरा गाउँपालिका",
                    "code"=>80549502,
                    "status"=>1,
                    "district_id"=>49,
                ],
                [
                    "name"=>"शिखर नगरपालिका ",
                    "code"=>80771402,
                    "status"=>1,
                    "district_id"=>71,
                ],
                [
                    "name"=>"लेकबेसी नगरपालिका ",
                    "code"=>80668405,
                    "status"=>1,
                    "district_id"=>68,
                ],
                [
                    "name"=>"पूर्वीचौकी गाउँपालिका",
                    "code"=>80771504,
                    "status"=>1,
                    "district_id"=>71,
                ],
                [
                    "name"=>"जोरायल गाउँपालिका",
                    "code"=>80771503,
                    "status"=>1,
                    "district_id"=>71,
                ],
                [
                    "name"=>"झिमरुक गाउँपालिका",
                    "code"=>80555503,
                    "status"=>1,
                    "district_id"=>55,
                ],
                [
                    "name"=>"रोल्पा नगरपालिका ",
                    "code"=>80554401,
                    "status"=>1,
                    "district_id"=>54,
                ],
                [
                    "name"=>"मालारानी गाउँपालिका",
                    "code"=>80551503,
                    "status"=>1,
                    "district_id"=>51,
                ],
                [
                    "name"=>"भेरीगंगा नगरपालिका ",
                    "code"=>80668404,
                    "status"=>1,
                    "district_id"=>68,
                ],
                [
                    "name"=>"बराहताल गाउँपालिका",
                    "code"=>80668503,
                    "status"=>1,
                    "district_id"=>68,
                ],
                [
                    "name"=>"थवाङ गाउँपालिका",
                    "code"=>80554502,
                    "status"=>1,
                    "district_id"=>54,
                ],
                [
                    "name"=>"चिङ्गाड गाउँपालिका",
                    "code"=>80668501,
                    "status"=>1,
                    "district_id"=>68,
                ],
                [
                    "name"=>"धनगढी उपमहानगरपालिका",
                    "code"=>80777301,
                    "status"=>1,
                    "district_id"=>77,
                ],
                [
                    "name"=>"गुराँस गाउँपालिका",
                    "code"=>80667501,
                    "status"=>1,
                    "district_id"=>67,
                ],
                [
                    "name"=>"भैरवी गाउँपालिका",
                    "code"=>80667506,
                    "status"=>1,
                    "district_id"=>67,
                ],
                [
                    "name"=>"बनगाँड कुपिण्डे नगरपालिका ",
                    "code"=>80660401,
                    "status"=>1,
                    "district_id"=>60,
                ],
                [
                    "name"=>"कालिमाटी गाउँपालिका",
                    "code"=>80660503,
                    "status"=>1,
                    "district_id"=>60,
                ],
                [
                    "name"=>"कपुरकोट गाउँपालिका",
                    "code"=>80660501,
                    "status"=>1,
                    "district_id"=>60,
                ],
                [
                    "name"=>"त्रिपुरासुन्दरी नगरपालिका ",
                    "code"=>80661402,
                    "status"=>1,
                    "district_id"=>61,
                ],
                [
                    "name"=>"माहाकाली नगरपालिका ",
                    "code"=>80776406,
                    "status"=>1,
                    "district_id"=>76,
                ],
                [
                    "name"=>"तातोपानी गाउँपालिका",
                    "code"=>80662503,
                    "status"=>1,
                    "district_id"=>62,
                ],
                [
                    "name"=>"पातारासी गाउँपालिका",
                    "code"=>80662505,
                    "status"=>1,
                    "district_id"=>62,
                ],
                [
                    "name"=>"बबई गाउँपालिका",
                    "code"=>80556504,
                    "status"=>1,
                    "district_id"=>56,
                ],
                [
                    "name"=>"उत्तरगया गाउँपालिका",
                    "code"=>80328501,
                    "status"=>1,
                    "district_id"=>28,
                ],
                [
                    "name"=>"विर्तामोड नगरपालिका ",
                    "code"=>80112407,
                    "status"=>1,
                    "district_id"=>12,
                ],
                [
                    "name"=>"मिथिला बिहारी गाउँपालिका",
                    "code"=>80217506,
                    "status"=>1,
                    "district_id"=>17,
                ],
                [
                    "name"=>"भानु नगरपालिका ",
                    "code"=>80438401,
                    "status"=>1,
                    "district_id"=>38,
                ],
                [
                    "name"=>"सत्यवती गाउँपालिका",
                    "code"=>80552510,
                    "status"=>1,
                    "district_id"=>52,
                ],
                [
                    "name"=>"चन्द्रकोट गाउँपालिका",
                    "code"=>80552504,
                    "status"=>1,
                    "district_id"=>52,
                ],
                [
                    "name"=>"रुरु गाउँपालिका",
                    "code"=>80552509,
                    "status"=>1,
                    "district_id"=>52,
                ],
                [
                    "name"=>"ऋषिङ्ग गाउँपालिका",
                    "code"=>80438502,
                    "status"=>1,
                    "district_id"=>38,
                ],
                [
                    "name"=>"बगनासकाली गाउँपालिका",
                    "code"=>80550504,
                    "status"=>1,
                    "district_id"=>50,
                ],
                [
                    "name"=>"रिब्दीकोट गाउँपालिका",
                    "code"=>80550508,
                    "status"=>1,
                    "district_id"=>50,
                ],
                [
                    "name"=>"म्याग्दे गाउँपालिका",
                    "code"=>80438506,
                    "status"=>1,
                    "district_id"=>38,
                ],
                [
                    "name"=>"पोखरा लेखनाथ महानगरपालिका",
                    "code"=>80439201,
                    "status"=>1,
                    "district_id"=>39,
                ],
                [
                    "name"=>"लालीगुराँस नगरपालिका ",
                    "code"=>80105402,
                    "status"=>1,
                    "district_id"=>5,
                ],
                [
                    "name"=>"रामप्रसाद राई गाउँपालिका",
                    "code"=>80107505,
                    "status"=>1,
                    "district_id"=>7,
                ],
                [
                    "name"=>"आमचोक गाउँपालिका",
                    "code"=>80107502,
                    "status"=>1,
                    "district_id"=>7,
                ],
                [
                    "name"=>"ईन्द्रसरोवर गाउँपालिका",
                    "code"=>80332501,
                    "status"=>1,
                    "district_id"=>32,
                ],
                [
                    "name"=>"कैलाश गाउँपालिका",
                    "code"=>80332502,
                    "status"=>1,
                    "district_id"=>32,
                ],
                [
                    "name"=>"बकैया गाउँपालिका",
                    "code"=>80332503,
                    "status"=>1,
                    "district_id"=>32,
                ],
                [
                    "name"=>"बाग्मती गाउँपालिका",
                    "code"=>80332508,
                    "status"=>1,
                    "district_id"=>32,
                ],
                [
                    "name"=>"गुजरा गाउँपालिका",
                    "code"=>80220403,
                    "status"=>1,
                    "district_id"=>20,
                ],
                [
                    "name"=>"कोल्हवी नगरपालिका ",
                    "code"=>80221401,
                    "status"=>1,
                    "district_id"=>21,
                ],
                [
                    "name"=>"देवताल गाउँपालिका",
                    "code"=>80221503,
                    "status"=>1,
                    "district_id"=>21,
                ],
                [
                    "name"=>"काँडा गाउँपालिका ",
                    "code"=>80770502,
                    "status"=>1,
                    "district_id"=>70,
                ],
                [
                    "name"=>"मेल्लेख गाउँपालिका",
                    "code"=>80772505,
                    "status"=>1,
                    "district_id"=>72,
                ],
                [
                    "name"=>"बान्नीगढी जयगढ गाउँपालिका",
                    "code"=>80772504,
                    "status"=>1,
                    "district_id"=>72,
                ],
                [
                    "name"=>"बाणगंगा नगरपालिका ",
                    "code"=>80549404,
                    "status"=>1,
                    "district_id"=>49,
                ],
                [
                    "name"=>"बीरेन्द्रनगर नगरपालिका ",
                    "code"=>80668403,
                    "status"=>1,
                    "district_id"=>68,
                ],
                [
                    "name"=>"शितगंगा नगरपालिका ",
                    "code"=>80551402,
                    "status"=>1,
                    "district_id"=>51,
                ],
                [
                    "name"=>"सायल गाउँपालिका",
                    "code"=>80771507,
                    "status"=>1,
                    "district_id"=>71,
                ],
                [
                    "name"=>"पाणिनी गाउँपालिका",
                    "code"=>80551502,
                    "status"=>1,
                    "district_id"=>51,
                ],
                [
                    "name"=>"आदर्श गाउँपालिका",
                    "code"=>80771501,
                    "status"=>1,
                    "district_id"=>71,
                ],
                [
                    "name"=>"सुवर्णावती गाउँपालिका",
                    "code"=>80554509,
                    "status"=>1,
                    "district_id"=>54,
                ],
                [
                    "name"=>"लम्किचुहा नगरपालिका ",
                    "code"=>80777406,
                    "status"=>1,
                    "district_id"=>77,
                ],
                [
                    "name"=>"चौरजहारी नगरपालिका ",
                    "code"=>80659402,
                    "status"=>1,
                    "district_id"=>59,
                ],
                [
                    "name"=>"चामुण्डा बिन्द्रासैनी नगरपालिका ",
                    "code"=>80667402,
                    "status"=>1,
                    "district_id"=>67,
                ],
                [
                    "name"=>"आठबिसकोट नगरपालिका ",
                    "code"=>80659401,
                    "status"=>1,
                    "district_id"=>59,
                ],
                [
                    "name"=>"डुंगेश्वर गाउँपालिका",
                    "code"=>80667503,
                    "status"=>1,
                    "district_id"=>67,
                ],
                [
                    "name"=>"जोशीपुर गाउँपालिका",
                    "code"=>80777504,
                    "status"=>1,
                    "district_id"=>77,
                ],
                [
                    "name"=>"बागचौर नगरपालिका ",
                    "code"=>80660402,
                    "status"=>1,
                    "district_id"=>60,
                ],
                [
                    "name"=>"चुरे गाउँपालिका",
                    "code"=>80777502,
                    "status"=>1,
                    "district_id"=>77,
                ],
                [
                    "name"=>"बारेकोट गाउँपालिका",
                    "code"=>80666503,
                    "status"=>1,
                    "district_id"=>66,
                ],
                [
                    "name"=>"कुमाखमालिका गाउँपालिका",
                    "code"=>80660502,
                    "status"=>1,
                    "district_id"=>60,
                ],
                [
                    "name"=>"शे फोक्सुन्डो गाउँपालिका",
                    "code"=>80661506,
                    "status"=>1,
                    "district_id"=>61,
                ],
                [
                    "name"=>"शुक्लाफाँट नगरपालिका ",
                    "code"=>80776407,
                    "status"=>1,
                    "district_id"=>76,
                ],
                [
                    "name"=>"पंचेश्वर गाउँपालिका",
                    "code"=>80774503,
                    "status"=>1,
                    "district_id"=>74,
                ],
                [
                    "name"=>"नेपालगञ्ज उपमहानगरपालिका",
                    "code"=>80557301,
                    "status"=>1,
                    "district_id"=>57,
                ],
                [
                    "name"=>"कोहलपुर नगरपालिका ",
                    "code"=>80557401,
                    "status"=>1,
                    "district_id"=>57,
                ],
                [
                    "name"=>"नरैनापुर गाउँपालिका",
                    "code"=>80557504,
                    "status"=>1,
                    "district_id"=>57,
                ],
                [
                    "name"=>"मालिकार्जुन गाउँपालिका",
                    "code"=>80773401,
                    "status"=>1,
                    "district_id"=>73,
                ],
                [
                    "name"=>"शैल्यशिखर नगरपालिका ",
                    "code"=>80773402,
                    "status"=>1,
                    "district_id"=>73,
                ],
                [
                    "name"=>"डुडुवा गाउँपालिका",
                    "code"=>80557503,
                    "status"=>1,
                    "district_id"=>57,
                ],
                [
                    "name"=>"गेरुवा गाउँपालिका",
                    "code"=>80558501,
                    "status"=>1,
                    "district_id"=>58,
                ],
                [
                    "name"=>"सोनमा गाउँपालिका",
                    "code"=>80218506,
                    "status"=>1,
                    "district_id"=>18,
                ],
                [
                    "name"=>"कटहरी गाउँपालिका",
                    "code"=>80113501,
                    "status"=>1,
                    "district_id"=>13,
                ],
                [
                    "name"=>"सुखीपुर नगरपालिका",
                    "code"=>80216407,
                    "status"=>1,
                    "district_id"=>16,
                ],
                [
                    "name"=>"कर्जन्हा गाउँपालिका",
                    "code"=>80216401,
                    "status"=>1,
                    "district_id"=>16,
                ],
                [
                    "name"=>"इनरुवा नगरपालिका ",
                    "code"=>80114301,
                    "status"=>1,
                    "district_id"=>14,
                ],
                [
                    "name"=>"रामपुर नगरपालिका ",
                    "code"=>80550402,
                    "status"=>1,
                    "district_id"=>50,
                ],
                [
                    "name"=>"देवघाट गाउँपालिका",
                    "code"=>80438504,
                    "status"=>1,
                    "district_id"=>38,
                ],
                [
                    "name"=>"पुतलीबजार नगरपालिका ",
                    "code"=>80443403,
                    "status"=>1,
                    "district_id"=>43,
                ],
                [
                    "name"=>"सुनवल नगरपालिका ",
                    "code"=>80547403,
                    "status"=>1,
                    "district_id"=>47,
                ],
                [
                    "name"=>"विनयी गाउँपालिका",
                    "code"=>80547503,
                    "status"=>1,
                    "district_id"=>47,
                ],
                [
                    "name"=>"बेनी नगरपालिका ",
                    "code"=>80444401,
                    "status"=>1,
                    "district_id"=>44,
                ],
                [
                    "name"=>"हरिपुर नगरपालिका ",
                    "code"=>80219409,
                    "status"=>1,
                    "district_id"=>19,
                ],
                [
                    "name"=>"मध्यपुर थिमी नगरपालिका ",
                    "code"=>80333403,
                    "status"=>1,
                    "district_id"=>33,
                ],
                [
                    "name"=>"पचरौता गाउँपालिका",
                    "code"=>80221403,
                    "status"=>1,
                    "district_id"=>21,
                ],
                [
                    "name"=>"सन्धिखर्क नगरपालिका ",
                    "code"=>80551403,
                    "status"=>1,
                    "district_id"=>51,
                ],
                [
                    "name"=>"पंचदेवल विनायक नगरपालिका ",
                    "code"=>80772402,
                    "status"=>1,
                    "district_id"=>72,
                ],
                [
                    "name"=>"गुर्भाकोट नगरपालिका ",
                    "code"=>80668401,
                    "status"=>1,
                    "district_id"=>68,
                ],
                [
                    "name"=>"बोगटान गाउँपालिका",
                    "code"=>80771506,
                    "status"=>1,
                    "district_id"=>71,
                ],
                [
                    "name"=>"सुनछहरी गाउँपालिका",
                    "code"=>80554508,
                    "status"=>1,
                    "district_id"=>54,
                ],
                [
                    "name"=>"मुसिकोट नगरपालिका ",
                    "code"=>80659403,
                    "status"=>1,
                    "district_id"=>59,
                ],
                [
                    "name"=>"घोडाघोडी नगरपालिका ",
                    "code"=>80777403,
                    "status"=>1,
                    "district_id"=>77,
                ],
                [
                    "name"=>"भगवतीमाई गाउँपालिका",
                    "code"=>80667505,
                    "status"=>1,
                    "district_id"=>67,
                ],
                [
                    "name"=>"सिस्ने गाउँपालिका",
                    "code"=>80553503,
                    "status"=>1,
                    "district_id"=>53,
                ],
                [
                    "name"=>"जानकी गाउँपालिका",
                    "code"=>80777503,
                    "status"=>1,
                    "district_id"=>77,
                ],
                [
                    "name"=>"बाँफिकोट गाउँपालिका",
                    "code"=>80659502,
                    "status"=>1,
                    "district_id"=>59,
                ],
                [
                    "name"=>"छेडागाड नगरपालिका ",
                    "code"=>80666401,
                    "status"=>1,
                    "district_id"=>66,
                ],
                [
                    "name"=>"नवदुर्गा गाउँपालिका",
                    "code"=>80775504,
                    "status"=>1,
                    "district_id"=>75,
                ],
                [
                    "name"=>"रास्कोट नगरपालिका ",
                    "code"=>80665403,
                    "status"=>1,
                    "district_id"=>65,
                ],
                [
                    "name"=>"डीलासैनी गाउँपालिका",
                    "code"=>80774501,
                    "status"=>1,
                    "district_id"=>74,
                ],
                [
                    "name"=>"सान्नी त्रिवेणी गाउँपालिका",
                    "code"=>80665506,
                    "status"=>1,
                    "district_id"=>65,
                ],
                [
                    "name"=>"खजुरा गाउँपालिका",
                    "code"=>80557501,
                    "status"=>1,
                    "district_id"=>57,
                ],
                [
                    "name"=>"कालिका गाउँपालिका",
                    "code"=>80665501,
                    "status"=>1,
                    "district_id"=>65,
                ],
                [
                    "name"=>"सिमकोट गाउँपालिका",
                    "code"=>80664507,
                    "status"=>1,
                    "district_id"=>64,
                ],
                [
                    "name"=>"मिक्लाजुङ गाउँपालिका",
                    "code"=>80102505,
                    "status"=>1,
                    "district_id"=>2,
                ],
                [
                    "name"=>"मानेभञ्याङ गाउँपालिका",
                    "code"=>80110504,
                    "status"=>1,
                    "district_id"=>10,
                ],
                [
                    "name"=>"याङवरक गाउँपालिका",
                    "code"=>80102506,
                    "status"=>1,
                    "district_id"=>2,
                ],
                [
                    "name"=>"लिखु गाउँपालिका",
                    "code"=>80110506,
                    "status"=>1,
                    "district_id"=>10,
                ],
                [
                    "name"=>"गणेशमान–चारनाथ नगरपालिका ",
                    "code"=>80217403,
                    "status"=>1,
                    "district_id"=>17,
                ],
                [
                    "name"=>"नगराइन नगरपालिका ",
                    "code"=>80217405,
                    "status"=>1,
                    "district_id"=>17,
                ],
                [
                    "name"=>"तारकेश्वर गाउँपालिका",
                    "code"=>80329504,
                    "status"=>1,
                    "district_id"=>29,
                ],
                [
                    "name"=>"दमक नगरपालिका ",
                    "code"=>80112404,
                    "status"=>1,
                    "district_id"=>12,
                ],
                [
                    "name"=>"कन्काई  नगरपालिका ",
                    "code"=>80112402,
                    "status"=>1,
                    "district_id"=>12,
                ],
                [
                    "name"=>"गौरिगंज गाउँपालिका",
                    "code"=>80112503,
                    "status"=>1,
                    "district_id"=>12,
                ],
                [
                    "name"=>"ताप्ली गाउँपालिका",
                    "code"=>80111502,
                    "status"=>1,
                    "district_id"=>11,
                ],
                [
                    "name"=>"रौतामाई गाउँपालिका",
                    "code"=>80111503,
                    "status"=>1,
                    "district_id"=>11,
                ],
                [
                    "name"=>"भिमाद नगरपालिका ",
                    "code"=>80438402,
                    "status"=>1,
                    "district_id"=>38,
                ],
                [
                    "name"=>"पूर्वखोला गाउँपालिका",
                    "code"=>80550503,
                    "status"=>1,
                    "district_id"=>50,
                ],
                [
                    "name"=>"रम्भा गाउँपालिका",
                    "code"=>80550507,
                    "status"=>1,
                    "district_id"=>50,
                ],
                [
                    "name"=>"देवचुली नगरपालिका ",
                    "code"=>80446403,
                    "status"=>1,
                    "district_id"=>46,
                ],
                [
                    "name"=>"बर्दघाट नगरपालिका ",
                    "code"=>80547401,
                    "status"=>1,
                    "district_id"=>47,
                ],
                [
                    "name"=>"हरिनास गाउँपालिका ",
                    "code"=>80443506,
                    "status"=>1,
                    "district_id"=>43,
                ],
                [
                    "name"=>"महालक्ष्मी  नगरपालिका ",
                    "code"=>80106403,
                    "status"=>1,
                    "district_id"=>6,
                ],
                [
                    "name"=>"खाल्सा छिन्ताङ सहिद भूमि गाउँपालिका",
                    "code"=>80106501,
                    "status"=>1,
                    "district_id"=>6,
                ],
                [
                    "name"=>"छथर जोरपाटी गाउँपालिका",
                    "code"=>80106503,
                    "status"=>1,
                    "district_id"=>6,
                ],
                [
                    "name"=>"चौबिसे गाउँपालिका",
                    "code"=>80106502,
                    "status"=>1,
                    "district_id"=>6,
                ],
                [
                    "name"=>"म्याङलुङ नगरपालिका ",
                    "code"=>80105401,
                    "status"=>1,
                    "district_id"=>5,
                ],
                [
                    "name"=>"पाँचखपन नगरपालिका ",
                    "code"=>80104404,
                    "status"=>1,
                    "district_id"=>4,
                ],
                [
                    "name"=>"भोटखोला गाउँपालिका",
                    "code"=>80104502,
                    "status"=>1,
                    "district_id"=>4,
                ],
                [
                    "name"=>"चिचिला गाउँपालिका",
                    "code"=>80104501,
                    "status"=>1,
                    "district_id"=>4,
                ],
                [
                    "name"=>"सभापोखरी गाउँपालिका",
                    "code"=>80104504,
                    "status"=>1,
                    "district_id"=>4,
                ],
                [
                    "name"=>"सिलीचोङ गाउँपालिका",
                    "code"=>80104505,
                    "status"=>1,
                    "district_id"=>4,
                ],
                [
                    "name"=>"सखुवानान्कारकट्टी गाउँपालिका",
                    "code"=>80216509,
                    "status"=>1,
                    "district_id"=>16,
                ],
                [
                    "name"=>"लालबन्दी नगरपालिका ",
                    "code"=>80219408,
                    "status"=>1,
                    "district_id"=>19,
                ],
                [
                    "name"=>"NULL",
                    "code"=>80442504,
                    "status"=>1,
                    "district_id"=>42,
                ],
                [
                    "name"=>"कीर्तिपुर  नगरपालिका ",
                    "code"=>80335402,
                    "status"=>1,
                    "district_id"=>35,
                ],
                [
                    "name"=>"चन्द्रनगर गाउँपालिका",
                    "code"=>80219503,
                    "status"=>1,
                    "district_id"=>19,
                ],
                [
                    "name"=>"धनकौल गाउँपालिका",
                    "code"=>80219504,
                    "status"=>1,
                    "district_id"=>19,
                ],
                [
                    "name"=>"दक्षिणकाली नगरपालिका ",
                    "code"=>80335407,
                    "status"=>1,
                    "district_id"=>35,
                ],
                [
                    "name"=>"कमलामाई नगरपालिका ",
                    "code"=>80325401,
                    "status"=>1,
                    "district_id"=>25,
                ],
                [
                    "name"=>"दोरम्बा गाउँपालिका",
                    "code"=>80324504,
                    "status"=>1,
                    "district_id"=>24,
                ],
                [
                    "name"=>"लिखु गाउँपालिका",
                    "code"=>80324505,
                    "status"=>1,
                    "district_id"=>24,
                ],
                [
                    "name"=>"जिरी नगरपालिका ",
                    "code"=>80323401,
                    "status"=>1,
                    "district_id"=>23,
                ],
                [
                    "name"=>"भिमेश्वर  नगरपालिका ",
                    "code"=>80323402,
                    "status"=>1,
                    "district_id"=>23,
                ],
                [
                    "name"=>"गौरीशङ्कर गाउँपालिका",
                    "code"=>80323502,
                    "status"=>1,
                    "district_id"=>23,
                ],
                [
                    "name"=>"शैलुङ्ग गाउँपालिका",
                    "code"=>80323507,
                    "status"=>1,
                    "district_id"=>23,
                ],
                [
                    "name"=>"त्रिपुरासुन्दरी गाउँपालिका",
                    "code"=>80327503,
                    "status"=>1,
                    "district_id"=>27,
                ],
                [
                    "name"=>"मर्स्याङदी गाउँपालिका",
                    "code"=>80437504,
                    "status"=>1,
                    "district_id"=>37,
                ],
                [
                    "name"=>"बनेपा नगरपालिका ",
                    "code"=>80326405,
                    "status"=>1,
                    "district_id"=>26,
                ],
                [
                    "name"=>"घरपझोङ गाउँपालिका",
                    "code"=>80441501,
                    "status"=>1,
                    "district_id"=>41,
                ],
                [
                    "name"=>"तेमाल गाउँपालिका",
                    "code"=>80326503,
                    "status"=>1,
                    "district_id"=>26,
                ],
                [
                    "name"=>"भुम्लु गाउँपालिका",
                    "code"=>80326505,
                    "status"=>1,
                    "district_id"=>26,
                ],
                [
                    "name"=>"मण्डनदेउपुर नगरपालिका ",
                    "code"=>80326406,
                    "status"=>1,
                    "district_id"=>26,
                ],
                [
                    "name"=>"थाक्रे गाउँपालिका",
                    "code"=>80330507,
                    "status"=>1,
                    "district_id"=>30,
                ],
                [
                    "name"=>"महागढीमाई नगरपालिका ",
                    "code"=>80221404,
                    "status"=>1,
                    "district_id"=>21,
                ],
                [
                    "name"=>"मायादेवी गाउँपालिका",
                    "code"=>80549501,
                    "status"=>1,
                    "district_id"=>49,
                ],
                [
                    "name"=>"तुर्माखाँद गाउँपालिका",
                    "code">80772503,
                    "status"=>1,
                    "district_id"=>72,
                ],
                [
                    "name"=>"आठबीस नगरपालिका ",
                    "code">80667401,
                    "status"=>1,
                    "district_id"=>67,
                ],
                [
                    "name"=>"गन्यापधुरा गाउँपालिका",
                    "code"=>80775503,
                    "status"=>1,
                    "district_id"=>75,
                ],
                [
                    "name"=>"शिवनाथ गाउँपालिका",
                    "code"=>80774504,
                    "status"=>1,
                    "district_id"=>74,
                ],
                [
                    "name"=>"सोरु गाउँपालिका",
                    "code"=>80663503,
                    "status"=>1,
                    "district_id"=>63,
                ],
                [
                    "name"=>"ठाकुरबाबा नगरपालिका ",
                    "code"=>80558402,
                    "status"=>1,
                    "district_id"=>58,
                ],
                [
                    "name"=>"हंसपुर गाउँपालिका",
                    "code"=>80217410,
                    "status"=>1,
                    "district_id"=>17,
                ],
                [
                    "name"=>"कटारी नगरपालिका ",
                    "code"=>80111401,
                    "status"=>1,
                    "district_id"=>11,
                ],
                [
                    "name"=>"वेलका नगरपालिका ",
                    "code"=>80111404,
                    "status"=>1,
                    "district_id"=>11,
                ],
                [
                    "name"=>"शिवसताक्षी नगरपालिका ",
                    "code"=>80112408,
                    "status"=>1,
                    "district_id"=>12,
                ],
                [
                    "name"=>"इस्मा गाउँपालिका",
                    "code"=>80552501,
                    "status"=>1,
                    "district_id"=>52,
                ],
                [
                    "name"=>"आँबुखैरेनी गाउँपालिका",
                    "code"=>80438501,
                    "status"=>1,
                    "district_id"=>38,
                ],
                [
                    "name"=>"तानसेन नगरपालिका ",
                    "code"=>80550401,
                    "status"=>1,
                    "district_id"=>50,
                ],
                [
                    "name"=>"तिनाउ गाउँपालिका",
                    "code"=>80550501,
                    "status"=>1,
                    "district_id"=>50,
                ],
                [
                    "name"=>"कावासोती नगरपालिका ",
                    "code"=>80446401,
                    "status"=>1,
                    "district_id"=>46,
                ],
                [
                    "name"=>"गल्याङ नगरपालिका ",
                    "code"=>80443401,
                    "status"=>1,
                    "district_id"=>43,
                ],
                [
                    "name"=>"आँधिखोला गाउँपालिका ",
                    "code"=>80443502,
                    "status"=>1,
                    "district_id"=>43,
                ],
                [
                    "name"=>"बिरुवा गाउँपालिका ",
                    "code"=>80443505,
                    "status"=>1,
                    "district_id"=>43,
                ],
                [
                    "name"=>"बुङ्दीकाली गाउँपालिका",
                    "code"=>80446502,
                    "status"=>1,
                    "district_id"=>46,
                ],
                [
                    "name"=>"NULL",
                    "code"=>80442503,
                    "status"=>1,
                    "district_id"=>42,
                ],
                [
                    "name"=>"हरिपुर्वा नगरपालिका ",
                    "code"=>80219410,
                    "status"=>1,
                    "district_id"=>19,
                ],
                [
                    "name"=>"दुधौली नगरपालिका ",
                    "code"=>80325402,
                    "status"=>1,
                    "district_id"=>25,
                ],
                [
                    "name"=>"तीनपाटन गाउँपालिका",
                    "code"=>80325503,
                    "status"=>1,
                    "district_id"=>25,
                ],
                [
                    "name"=>"चौतारा सागाचोकगढी नगरपालिका ",
                    "code"=>80327401,
                    "status"=>1,
                    "district_id"=>27,
                ],
                [
                    "name"=>"वाह्रगाउँ मुक्तिक्षेत्र गाउँपालिका",
                    "code"=>80441504,
                    "status"=>1,
                    "district_id"=>41,
                ],
                [
                    "name"=>"गौमुखी गाउँपालिका",
                    "code"=>80555502,
                    "status"=>1,
                    "district_id"=>55,
                ],
                [
                    "name"=>"माण्डवी गाउँपालिका",
                    "code"=>80555506,
                    "status"=>1,
                    "district_id"=>55,
                ],
                [
                    "name"=>"दोगडाकेदार गाउँपालिका",
                    "code"=>80774502,
                    "status"=>1,
                    "district_id"=>74,
                ],
                [
                    "name"=>"राप्तीसोनारी गाउँपालिका",
                    "code"=>80557506,
                    "status"=>1,
                    "district_id"=>57,
                ],
                [
                    "name"=>"महावै गाउँपालिका",
                    "code"=>80665505,
                    "status"=>1,
                    "district_id"=>65,
                ],
                [
                    "name"=>"बुढीगंगा नगरपालिका ",
                    "code"=>80769403,
                    "status"=>1,
                    "district_id"=>69,
                ],
                [
                    "name"=>"मेचीनगर  नगरपालिका ",
                    "code"=>80112406,
                    "status"=>1,
                    "district_id"=>12,
                ],
                [
                    "name"=>"औरही गाउँपालिका",
                    "code"=>80217501,
                    "status"=>1,
                    "district_id"=>17,
                ],
                [
                    "name"=>"अर्जुनधारा  नगरपालिका ",
                    "code"=>80112401,
                    "status"=>1,
                    "district_id"=>12,
                ],
                [
                    "name"=>"लेटाङ नगरपालिका ",
                    "code"=>80113406,
                    "status"=>1,
                    "district_id"=>13,
                ],
                [
                    "name"=>"धनपालथान गाउँपालिका",
                    "code"=>80113506,
                    "status"=>1,
                    "district_id"=>13,
                ],
                [
                    "name"=>"सिद्धार्थनगर नगरपालिका ",
                    "code"=>80548405,
                    "status"=>1,
                    "district_id"=>48,
                ],
                [
                    "name"=>"तिलोत्तमा नगरपालिका ",
                    "code"=>80548401,
                    "status"=>1,
                    "district_id"=>48,
                ],
                [
                    "name"=>"धर्मदेवी नगरपालिका ",
                    "code"=>80104403,
                    "status"=>1,
                    "district_id"=>4,
                ],
                [
                    "name"=>"खाँदवारी नगरपालिका ",
                    "code"=>80104401,
                    "status"=>1,
                    "district_id"=>4,
                ],
                [
                    "name"=>"NULL",
                    "code"=>80442402,
                    "status"=>1,
                    "district_id"=>42,
                ],
                [
                    "name"=>"पार्वतीकुण्ड गाउँपालिका",
                    "code"=>80328505,
                    "status"=>1,
                    "district_id"=>28,
                ],
                [
                    "name"=>"फिक्कल गाउँपालिका",
                    "code"=>80325504,
                    "status"=>1,
                    "district_id"=>25,
                ],
                [
                    "name"=>"मार्मा गाउँपालिका",
                    "code"=>80773505,
                    "status"=>1,
                    "district_id"=>73,
                ],
                [
                    "name"=>"जानकी गाउँपालिका",
                    "code"=>80557502,
                    "status"=>1,
                    "district_id"=>57,
                ],
                [
                    "name"=>"पलाता गाउँपालिका",
                    "code"=>80665504,
                    "status"=>1,
                    "district_id"=>65,
                ],
                [
                    "name"=>"अपिहिमाल गाउँपालिका",
                    "code"=>80773501,
                    "status"=>1,
                    "district_id"=>73,
                ],
                [
                    "name"=>"बडिमालिका नगरपालिका ",
                    "code"=>80769402,
                    "status"=>1,
                    "district_id"=>69,
                ],
                [
                    "name"=>"कमला सिद्धीदात्री गाउँपालिका",
                    "code"=>80217401,
                    "status"=>1,
                    "district_id"=>17,
                ],
                [
                    "name"=>"जन्तेढुंगा गाउँपालिका",
                    "code"=>80108504,
                    "status"=>1,
                    "district_id"=>8,
                ],
                [
                    "name"=>"विदेह नगरपालिका ",
                    "code"=>80217407,
                    "status"=>1,
                    "district_id"=>17,
                ],
                [
                    "name"=>"खोटेहाङ गाउँपालिका",
                    "code"=>80108503,
                    "status"=>1,
                    "district_id"=>8,
                ],
                [
                    "name"=>"रोङ गाउँपालिका",
                    "code"=>80103505,
                    "status"=>1,
                    "district_id"=>3,
                ],
                [
                    "name"=>"धनौजी गाउँपालिका",
                    "code"=>80217503,
                    "status"=>1,
                    "district_id"=>17,
                ],
                [
                    "name"=>"महादेवा गाउँपालिका",
                    "code"=>80215508,
                    "status"=>1,
                    "district_id"=>15,
                ],
                [
                    "name"=>"महोत्तरी गाउँपालिका",
                    "code"=>80218504,
                    "status"=>1,
                    "district_id"=>18,
                ],
                [
                    "name"=>"तिलाठीकोईलाडी गाउंपालिका",
                    "code"=>80215504,
                    "status"=>1,
                    "district_id"=>15,
                ],
                [
                    "name"=>"उर्लाबारी नगरपालिका ",
                    "code"=>80113401,
                    "status"=>1,
                    "district_id"=>13,
                ],
                [
                    "name"=>"कानेपोखरी गाउँपालिका",
                    "code"=>80113503,
                    "status"=>1,
                    "district_id"=>13,
                ],
                [
                    "name"=>"कल्याणपुर नगरपालिका ",
                    "code"=>80216402,
                    "status"=>1,
                    "district_id"=>16,
                ],
                [
                    "name"=>"केराबारी गाउँपालिका",
                    "code"=>80113502,
                    "status"=>1,
                    "district_id"=>13,
                ],
                [
                    "name"=>"मिक्लाजुङ गाउँपालिका",
                    "code"=>80113508,
                    "status"=>1,
                    "district_id"=>13,
                ],
                [
                    "name"=>"बराह नगरपालिका ",
                    "code"=>80114403,
                    "status"=>1,
                    "district_id"=>14,
                ],
                [
                    "name"=>"शुक्लागण्डकी नगरपालिका ",
                    "code"=>80438404,
                    "status"=>1,
                    "district_id"=>38,
                ],
                [
                    "name"=>"घिरिङ गाउँपालिका",
                    "code"=>80438503,
                    "status"=>1,
                    "district_id"=>38,
                ],
                [
                    "name"=>"निस्दी गाउँपालिका",
                    "code"=>80550502,
                    "status"=>1,
                    "district_id"=>50,
                ],
                [
                    "name"=>"माथागढी गाउँपालिका",
                    "code"=>80550505,
                    "status"=>1,
                    "district_id"=>50,
                ],
                [
                    "name"=>"हरिनगरा गाउँपालिका",
                    "code"=>80114506,
                    "status"=>1,
                    "district_id"=>14,
                ],
                [
                    "name"=>"पौवादुङ्मा गाउँपालिका",
                    "code"=>80107504,
                    "status"=>1,
                    "district_id"=>7,
                ],
                [
                    "name"=>"मालिका गाउँपालिका",
                    "code"=>80444504,
                    "status"=>1,
                    "district_id"=>44,
                ],
                [
                    "name"=>"NULL",
                    "code"=>80442401,
                    "status"=>1,
                    "district_id"=>42,
                ],
                [
                    "name"=>"NULL",
                    "code"=>80442501,
                    "status"=>1,
                    "district_id"=>42,
                ],
                [
                    "name"=>"NULL",
                    "code"=>80442502,
                    "status"=>1,
                    "district_id"=>42,
                ],
                [
                    "name"=>"सूर्यविनायक नगरपालिका ",
                    "code"=>80333404,
                    "status"=>1,
                    "district_id"=>33,
                ],
                [
                    "name"=>"नागार्जुन नगरपालिका ",
                    "code"=>80335408,
                    "status"=>1,
                    "district_id"=>35,
                ],
                [
                    "name"=>"मेरिङदेन गाउँपालिका",
                    "code"=>80101503,
                    "status"=>1,
                    "district_id"=>1,
                ],
                [
                    "name"=>"माहाकुलुङ गाउँपालिका",
                    "code"=>80109505,
                    "status"=>1,
                    "district_id"=>9,
                ],
                [
                    "name"=>"सिरीजङ्घा गाउँपालिका",
                    "code"=>80101508,
                    "status"=>1,
                    "district_id"=>1,
                ],
                [
                    "name"=>"फालेलुङ गाउँपालिका",
                    "code"=>80102504,
                    "status"=>1,
                    "district_id"=>2,
                ],
                [
                    "name"=>"क्षिरेश्वरनाथ नगरपालिका ",
                    "code"=>80217402,
                    "status"=>1,
                    "district_id"=>17,
                ],
                [
                    "name"=>"मादी गाउँपालिका",
                    "code"=>80439503,
                    "status"=>1,
                    "district_id"=>39,
                ],
                [
                    "name"=>"चामे गाउँपालिका",
                    "code"=>80440501,
                    "status"=>1,
                    "district_id"=>40,
                ],
                [
                    "name"=>"गैडहवा गाउँपालिका",
                    "code"=>80548504,
                    "status"=>1,
                    "district_id"=>48,
                ],
                [
                    "name"=>"मर्चवारी गाउँपालिका",
                    "code"=>80548505,
                    "status"=>1,
                    "district_id"=>48,
                ],
                [
                    "name"=>"मायादेवी गाउँपालिका",
                    "code"=>80548506,
                    "status"=>1,
                    "district_id"=>48,
                ],
                [
                    "name"=>"सम्मरीमाई गाउँपालिका",
                    "code"=>80548509,
                    "status"=>1,
                    "district_id"=>48,
                ],
                [
                    "name"=>"साँगुरीगढी गाउँपालिका",
                    "code"=>80106504,
                    "status"=>1,
                    "district_id"=>6,
                ],
                [
                    "name"=>"धवलागिरी गाउँपालिका",
                    "code"=>80444502,
                    "status"=>1,
                    "district_id"=>44,
                ],
                [
                    "name"=>"रुवी भ्याली गाउँपालिका",
                    "code"=>80330510,
                    "status"=>1,
                    "district_id"=>30,
                ],
                [
                    "name"=>"सिद्धलेक गाउँपालिका",
                    "code"=>80330511,
                    "status"=>1,
                    "district_id"=>30,
                ],
                [
                    "name"=>"ताँजाकोट गाउँपालिका",
                    "code"=>80664504,
                    "status"=>1,
                    "district_id"=>64,
                ],
                [
                    "name"=>"त्रिवेणी नगरपालिका ",
                    "code"=>80769401,
                    "status"=>1,
                    "district_id"=>69,
                ],
                [
                    "name"=>"डाक्नेश्वरी नगरपालिका ",
                    "code"=>80215403,
                    "status"=>1,
                    "district_id"=>15,
                ],
                [
                    "name"=>"रामधुनी नगरपालिका ",
                    "code"=>80114404,
                    "status"=>1,
                    "district_id"=>14,
                ],
                [
                    "name"=>"बन्दिपुर गाउँपालिका",
                    "code"=>80438505,
                    "status"=>1,
                    "district_id"=>38,
                ],
                [
                    "name"=>"गोकर्णेश्वर नगरपालिका ",
                    "code"=>80335403,
                    "status"=>1,
                    "district_id"=>35,
                ],
                [
                    "name"=>"हेटौंडा उप-महानगरपालिका",
                    "code"=>80332301,
                    "status"=>1,
                    "district_id"=>32,
                ],
                [
                    "name"=>"अदानचुली गाउँपालिका",
                    "code"=>80664501,
                    "status"=>1,
                    "district_id"=>64,
                ],
                [
                    "name"=>"ऐसेलुखर्क गाउँपालिका",
                    "code"=>80108501,
                    "status"=>1,
                    "district_id"=>8,
                ],
                [
                    "name"=>"माई नगरपालिका ",
                    "code"=>80103403,
                    "status"=>1,
                    "district_id"=>3,
                ],
                [
                    "name"=>"सुनकोशी गाउँपालिका",
                    "code"=>80111504,
                    "status"=>1,
                    "district_id"=>11,
                ],
                [
                    "name"=>"कृष्णासवरन गाउँपालिका",
                    "code"=>80215401,
                    "status"=>1,
                    "district_id"=>15,
                ],
                [
                    "name"=>"झापा गाउँपालिका",
                    "code"=>80112504,
                    "status"=>1,
                    "district_id"=>12,
                ],
                [
                    "name"=>"शम्भुनाथ  नगरपालिका ",
                    "code"=>80215406,
                    "status"=>1,
                    "district_id"=>15,
                ],
                [
                    "name"=>"सुरुगां नगरपालिका ",
                    "code"=>80215408,
                    "status"=>1,
                    "district_id"=>15,
                ],
                [
                    "name"=>"मध्यविन्दु नगरपालिका ",
                    "code"=>80446404,
                    "status"=>1,
                    "district_id"=>46,
                ],
                [
                    "name"=>"भीरकोट नगरपालिका ",
                    "code"=>80443404,
                    "status"=>1,
                    "district_id"=>43,
                ],
                [
                    "name"=>"कालीगण्डकी गाउँपालिका ",
                    "code"=>80443503,
                    "status"=>1,
                    "district_id"=>43,
                ],
                [
                    "name"=>"सुस्ता गाउँपालिका",
                    "code"=>80446501,
                    "status"=>1,
                    "district_id"=>46,
                ],
                [
                    "name"=>"फेदीखोला गाउँपालिका ",
                    "code"=>80443504,
                    "status"=>1,
                    "district_id"=>43,
                ],
                [
                    "name"=>"पाल्हीनन्दन गाउँपालिका",
                    "code"=>80547502,
                    "status"=>1,
                    "district_id"=>47,
                ],
                [
                    "name"=>"प्रतापपुर गाउँपालिका",
                    "code"=>80547501,
                    "status"=>1,
                    "district_id"=>47,
                ],
                [
                    "name"=>"बुलिङटार गाउँपालिका",
                    "code"=>80446503,
                    "status"=>1,
                    "district_id"=>46,
                ],
                [
                    "name"=>"सरावल गाउँपालिका",
                    "code"=>80547504,
                    "status"=>1,
                    "district_id"=>47,
                ],
                [
                    "name"=>"हुप्सेकोट गाउँपालिका",
                    "code"=>80446504,
                    "status"=>1,
                    "district_id"=>46,
                ],
                [
                    "name"=>"बुटवल उपमहानगरपालिका",
                    "code"=>80548301,
                    "status"=>1,
                    "district_id"=>48,
                ],
                [
                    "name"=>"लुम्विनी सांस्कृतिक नगरपालिका ",
                    "code"=>80548403,
                    "status"=>1,
                    "district_id"=>48,
                ],
                [
                    "name"=>"रूपा गाउँपालिका",
                    "code"=>80439504,
                    "status"=>1,
                    "district_id"=>39,
                ],
                [
                    "name"=>"रोहिणी गाउँपालिका",
                    "code"=>80548507,
                    "status"=>1,
                    "district_id"=>48,
                ],
                [
                    "name"=>"सियारी गाउँपालिका",
                    "code"=>80548510,
                    "status"=>1,
                    "district_id"=>48,
                ],
                [
                    "name"=>"गल्छी गाउँपालिका",
                    "code"=>80330504,
                    "status"=>1,
                    "district_id"=>30,
                ],
                [
                    "name"=>"गङ्गाजमुना गाउँपालिका",
                    "code"=>80330502,
                    "status"=>1,
                    "district_id"=>30,
                ],
                [
                    "name"=>"ज्वालामूखी गाउँपालिका",
                    "code"=>80330505,
                    "status"=>1,
                    "district_id"=>30,
                ],
                [
                    "name"=>"हिमाली गाउँपालिका",
                    "code"=>80769505,
                    "status"=>1,
                    "district_id"=>69,
                ],
                [
                    "name"=>"फक्ताङलुङ गाउँपालिका",
                    "code"=>80101502,
                    "status"=>1,
                    "district_id"=>1,
                ],
                [
                    "name"=>"सिद्धिचरण नगरपालिका ",
                    "code"=>80110401,
                    "status"=>1,
                    "district_id"=>10,
                ],
                [
                    "name"=>"खिजीदेम्वा गाउँपालिका",
                    "code"=>80110501,
                    "status"=>1,
                    "district_id"=>10,
                ],
                [
                    "name"=>"चौदण्डीगढी नगरपालिका ",
                    "code"=>80111402,
                    "status"=>1,
                    "district_id"=>11,
                ],
                [
                    "name"=>"जलेश्वर नगरपालिका ",
                    "code"=>80218402,
                    "status"=>1,
                    "district_id"=>18,
                ],
                [
                    "name"=>"गौशाला नगरपालिका ",
                    "code"=>80218401,
                    "status"=>1,
                    "district_id"=>18,
                ],
                [
                    "name"=>"वालिङ नगरपालिका ",
                    "code"=>80443405,
                    "status"=>1,
                    "district_id"=>43,
                ],
                [
                    "name"=>"बेनीघाट रोराङ्ग गाउँपालिका",
                    "code"=>80330509,
                    "status"=>1,
                    "district_id"=>30,
                ],
                [
                    "name"=>"गौमुल गाउँपालिका",
                    "code"=>80769501,
                    "status"=>1,
                    "district_id"=>69,
                ],
                [
                    "name"=>"छेडेदह गाउँपालिका",
                    "code"=>80769502,
                    "status"=>1,
                    "district_id"=>69,
                ],
                [
                    "name"=>"दुधकोशी गाउँपालिका",
                    "code"=>80109502,
                    "status"=>1,
                    "district_id"=>9,
                ],
                [
                    "name"=>"फिदिम नगरपालिका ",
                    "code"=>80102401,
                    "status"=>1,
                    "district_id"=>2,
                ],
                [
                    "name"=>"लक्ष्मीनिया गाउँपालिका",
                    "code"=>80217507,
                    "status"=>1,
                    "district_id"=>17,
                ],
                [
                    "name"=>"एकडारा गाउँपालिका",
                    "code"=>80218501,
                    "status"=>1,
                    "district_id"=>18,
                ],
                [
                    "name"=>"बिष्णुपुर गाउँपालिका",
                    "code"=>80215507,
                    "status"=>1,
                    "district_id"=>15,
                ],
                [
                    "name"=>"धनगढीमाई नगरपालिका ",
                    "code"=>80216404,
                    "status"=>1,
                    "district_id"=>16,
                ],
                [
                    "name"=>"दुहवी नगरपालिका ",
                    "code"=>80114402,
                    "status"=>1,
                    "district_id"=>14,
                ],
                [
                    "name"=>"माछापुछ्रे गाउँपालिका",
                    "code"=>80439502,
                    "status"=>1,
                    "district_id"=>39,
                ],
                [
                    "name"=>"नार्फु गाउँपालिका",
                    "code"=>80440503,
                    "status"=>1,
                    "district_id"=>40,
                ],
                [
                    "name"=>"लहान नगरपालिका ",
                    "code"=>80216508,
                    "status"=>1,
                    "district_id"=>16,
                ],
                [
                    "name"=>"बुढीनन्दा नगरपालिका ",
                    "code"=>80769404,
                    "status"=>1,
                    "district_id"=>69,
                ],
                [
                    "name"=>"गौरादह नगरपालिका ",
                    "code"=>80112403,
                    "status"=>1,
                    "district_id"=>12,
                ],
                [
                    "name"=>"बोदेबरसाईन नगरपालिका ",
                    "code"=>80215404,
                    "status"=>1,
                    "district_id"=>15,
                ],
                [
                    "name"=>"बाह्रदशी गाउँपालिका",
                    "code"=>80112506,
                    "status"=>1,
                    "district_id"=>12,
                ],
                [
                    "name"=>"बुद्धशान्ति गाउँपालिका",
                    "code"=>80112505,
                    "status"=>1,
                    "district_id"=>12,
                ],
                [
                    "name"=>"अर्जुनचौपारी गाउँपालिका ",
                    "code"=>80443501,
                    "status"=>1,
                    "district_id"=>43,
                ],
                [
                    "name"=>"सैनामैना नगरपालिका ",
                    "code"=>80548404,
                    "status"=>1,
                    "district_id"=>48,
                ],
                [
                    "name"=>"ओमसतिया गाउँपालिका",
                    "code"=>80548501,
                    "status"=>1,
                    "district_id"=>48,
                ],
                [
                    "name"=>"शुद्धोधन गाउँपालिका",
                    "code"=>80548508,
                    "status"=>1,
                    "district_id"=>48,
                ],
                [
                    "name"=>"नाशोङ गाउँपालिका",
                    "code"=>80440504,
                    "status"=>1,
                    "district_id"=>40,
                ],
                [
                    "name"=>"नरहा गाउँपालिका",
                    "code"=>80216503,
                    "status"=>1,
                    "district_id"=>16,
                ],
                [
                    "name"=>"खडक नगरपालिका ",
                    "code"=>80215402,
                    "status"=>1,
                    "district_id"=>15,
                ],
                [
                    "name"=>"उदयपुरगढी गाउँपालिका",
                    "code"=>80111501,
                    "status"=>1,
                    "district_id"=>11,
                ],
                [
                    "name"=>"हनुमाननगर कंकालिनी नगरपालिका ",
                    "code"=>80215409,
                    "status"=>1,
                    "district_id"=>15,
                ],
                [
                    "name"=>"कञ्चनरुप  नगरपालिका ",
                    "code"=>80215501,
                    "status"=>1,
                    "district_id"=>15,
                ],
                [
                    "name"=>"सोलुदुधकुण्ड  नगरपालिका ",
                    "code"=>80109401,
                    "status"=>1,
                    "district_id"=>9,
                ],
                [
                    "name"=>"नेस्याङ गाउँपालिका",
                    "code"=>80440502,
                    "status"=>1,
                    "district_id"=>40,
                ],
                [
                    "name"=>"मैवाखोला गाउँपालिका",
                    "code"=>80101504,
                    "status"=>1,
                    "district_id"=>1,
                ],
                [
                    "name"=>"लिखु पिके गाउँपालिका",
                    "code"=>80109506,
                    "status"=>1,
                    "district_id"=>9,
                ],
                [
                    "name"=>"याङवरक गाउँपालिका",
                    "code"=>80101506,
                    "status"=>1,
                    "district_id"=>1,
                ],
                [
                    "name"=>"सोताङ गाउँपालिका",
                    "code"=>80109507,
                    "status"=>1,
                    "district_id"=>9,
                ],
                [
                    "name"=>"तुम्वेवा गाउँपालिका",
                    "code"=>80102502,
                    "status"=>1,
                    "district_id"=>2,
                ],
                [
                    "name"=>"मोलुङ गाउँपालिका",
                    "code"=>80110505,
                    "status"=>1,
                    "district_id"=>10,
                ],
                [
                    "name"=>"मिक्वाखोला गाउँपालिका",
                    "code"=>80101505,
                    "status"=>1,
                    "district_id"=>1,
                ],
                [
                    "name"=>"नेचासल्यान गाउँपालिका",
                    "code"=>80109504,
                    "status"=>1,
                    "district_id"=>9,
                ]
            ]
        );
    }
}


