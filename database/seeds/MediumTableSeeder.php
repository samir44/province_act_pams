<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MediumTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mediums')->insert([
            'user_id' => 1,
            'role_id' => 1,
        ]);
    }
}






