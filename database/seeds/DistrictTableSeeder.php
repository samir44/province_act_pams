<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DistrictTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('districts')->insert(
            [
                [
                    "code"=>1,
                    "name"=> "ताप्लेजुङ",
                    "province_id"=> 1,
                ],
                [
                    "code"=>2,
                    "name"=> "पाँचथर",
                    "province_id"=> 1,
                ],
                [
                    "code"=>3,
                    "name"=> "इलाम",
                    "province_id"=> 1,
                ],
                [
                    "code"=>4,
                    "name"=> "संखुवासभा",
                    "province_id"=> 1,
                ],
                [
                    "code"=>5,
                    "name"=> "तेह्रथुम",
                    "province_id"=> 1,
                ],
                [
                    "code"=>6,
                    "name"=> "धनकुटा",
                    "province_id"=> 1,
                ],
                [
                    "code"=>7,
                    "name"=> "भोजपुर",
                    "province_id"=> 1,
                ],
                [
                    "code"=>8,
                    "name"=> "खोटांग",
                    "province_id"=> 1,
                ],
                [
                    "code"=>9,
                    "name"=> "सोलुखुम्बु",
                    "province_id"=> 1,
                ],
                [
                    "code"=>10,
                    "name"=> "ओखलढुंगा",
                    "province_id"=> 1,
                ],
                [
                    "code"=>11,
                    "name"=> "उदयपुर",
                    "province_id"=> 1,
                ],
                [
                    "code"=>12,
                    "name"=> "झापा",
                    "province_id"=> 1,
                ],
                [
                    "code"=>13,
                    "name"=> "मोरङ",
                    "province_id"=> 1,
                ],
                [
                    "code"=>14,
                    "name"=> "सुनसरी",
                    "province_id"=> 1,
                ],
                [
                    "code"=>15,
                    "name"=> "सप्तरी",
                    "province_id"=> 2,
                ],
                [
                    "code"=>16,
                    "name"=> "सिराहा",
                    "province_id"=> 2,
                ],
                [
                    "code"=>17,
                    "name"=> "धनुषा",
                    "province_id"=> 2,
                ],
                [
                    "code"=>18,
                    "name"=> "महोत्तरी",
                    "province_id"=> 2,
                ],
                [
                    "code"=>19,
                    "name"=> "सर्लाही",
                    "province_id"=> 2,
                ],
                [
                    "code"=>20,
                    "name"=> "रौतहट",
                    "province_id"=> 2,
                ],
                [
                    "code"=>21,
                    "name"=> "बारा",
                    "province_id"=> 2,
                ],
                [
                    "code"=>22,
                    "name"=> "पर्सा",
                    "province_id"=> 2,
                ],
                [
                    "code"=>23,
                    "name"=> "दोलखा",
                    "province_id"=> 3,
                ],
                [
                    "code"=>24,
                    "name"=> "रामेछाप",
                    "province_id"=> 3,
                ],
                [
                    "code"=>25,
                    "name"=> "सिन्धुली",
                    "province_id"=> 3,
                ],
                [
                    "code"=>26,
                    "name"=> "काभ्रेपलाञ्चोक",
                    "province_id"=> 3,
                ],
                [
                    "code"=>27,
                    "name"=> "सिन्धुपाल्चोक",
                    "province_id"=> 3,
                ],
                [
                    "code"=>28,
                    "name"=> "रसुवा",
                    "province_id"=> 3,
                ],
                [
                    "code"=>29,
                    "name"=> "नुवाकोट",
                    "province_id"=> 3,
                ],
                [
                    "code"=>30,
                    "name"=> "धादिंग",
                    "province_id"=> 3,
                ],
                [
                    "code"=>31,
                    "name"=> "चितवन",
                    "province_id"=> 3,
                ],
                [
                    "code"=>32,
                    "name"=> "मकवानपुर",
                    "province_id"=> 3,
                ],
                [
                    "code"=>33,
                    "name"=> "भक्तपुर",
                    "province_id"=> 3,
                ],
                [
                    "code"=>34,
                    "name"=> "ललितपुर",
                    "province_id"=> 3,
                ],
                [
                    "code"=>35,
                    "name"=> "काठमाडौँ",
                    "province_id"=> 3,
                ],
                [
                    "code"=>36,
                    "name"=> "गोरखा",
                    "province_id"=> 4,
                ],
                [
                    "code"=>37,
                    "name"=> "लमजुङ",
                    "province_id"=> 4,
                ],
                [
                    "code"=>38,
                    "name"=> "तनहुँ",
                    "province_id"=> 4,
                ],
                [
                    "code"=>39,
                    "name"=> "कास्की",
                    "province_id"=> 4,
                ],
                [
                    "code"=>40,
                    "name"=> "मनाङ",
                    "province_id"=> 4,
                ],
                [
                    "code"=>41,
                    "name"=> "मुस्ताङ",
                    "province_id"=> 4,
                ],
                [
                    "code"=>42,
                    "name"=> "पर्बत",
                    "province_id"=> 4,
                ],
                [
                    "code"=>43,
                    "name"=> "स्यांजा",
                    "province_id"=> 4,
                ],
                [
                    "code"=>44,
                    "name"=> "म्याग्दी",
                    "province_id"=> 4,
                ],
                [
                    "code"=>45,
                    "name"=> "बागलुङ",
                    "province_id"=> 4,
                ],
                [
                    "code"=>46,
                    "name"=> "नवलपुर",
                    "province_id"=> 4,
                ],
                [
                    "code"=>47,
                    "name"=> "नवलपरासी",
                    "province_id"=> 5,
                ],
                [
                    "code"=>48,
                    "name"=> "रुपन्देही",
                    "province_id"=> 5,
                ],
                [
                    "code"=>49,
                    "name"=> "कपिलवस्तु",
                    "province_id"=> 5,
                ],
                [
                    "code"=>50,
                    "name"=> "पाल्पा",
                    "province_id"=> 5,
                ],
                [
                    "code"=>51,
                    "name"=> "अर्घाखाँची",
                    "province_id"=> 5,
                ],
                [
                    "code"=>52,
                    "name"=> "गुल्मी",
                    "province_id"=> 5,
                ],
                [
                    "code"=>53,
                    "name"=> "रुकुमकोट",
                    "province_id"=> 5,
                ],
                [
                    "code"=>54,
                    "name"=> "रोल्पा",
                    "province_id"=> 5,
                ],
                [
                    "code"=>55,
                    "name"=> "प्युठान",
                    "province_id"=> 5,
                ],
                [
                    "code"=>56,
                    "name"=> "दाङ",
                    "province_id"=> 5,
                ],
                [
                    "code"=>57,
                    "name"=> "बाँके",
                    "province_id"=> 5,
                ],
                [
                    "code"=>58,
                    "name"=> "बर्दिया",
                    "province_id"=> 5,
                ],
                [
                    "code"=>59,
                    "name"=> "रुकुम",
                    "province_id"=> 6,
                ],
                [
                    "code"=>60,
                    "name"=> "सल्यान",
                    "province_id"=> 6,
                ],
                [
                    "code"=>61,
                    "name"=> "डोल्पा",
                    "province_id"=> 6,
                ],
                [
                    "code"=>62,
                    "name"=> "जुम्ला",
                    "province_id"=> 6,
                ],
                [
                    "code"=>63,
                    "name"=> "मुगु",
                    "province_id"=> 6,
                ],
                [
                    "code"=>64,
                    "name"=> "हुम्ला",
                    "province_id"=> 6,
                ],
                [
                    "code"=>65,
                    "name"=> "कालिकोट",
                    "province_id"=> 6,
                ],
                [
                    "code"=>66,
                    "name"=> "जाजरकोट",
                    "province_id"=> 6,
                ],
                [
                    "code"=>67,
                    "name"=> "दैलेख",
                    "province_id"=> 6,
                ],
                [
                    "code"=>68,
                    "name"=> "सुर्खेत",
                    "province_id"=> 6,
                ],
                [
                    "code"=>69,
                    "name"=> "बाजुरा",
                    "province_id"=> 7,
                ],
                [
                    "code"=>70,
                    "name"=> "बझाङ",
                    "province_id"=> 7,
                ],
                [
                    "code"=>71,
                    "name"=> "डोटी",
                    "province_id"=> 7,
                ],
                [
                    "code"=>72,
                    "name"=> "अछाम",
                    "province_id"=> 7,
                ],
                [
                    "code"=>73,
                    "name"=> "दार्चुला",
                    "province_id"=> 7,
                ],
                [
                    "code"=>74,
                    "name"=> "बैतडी",
                    "province_id"=> 7,
                ],
                [
                    "code"=>75,
                    "name"=> "डडेलधुरा",
                    "province_id"=> 7,
                ],
                [
                    "code"=>76,
                    "name"=> "कंचनपुर",
                    "province_id"=> 7,
                ],
                [
                    "code"=>77,
                    "name"=> "कैलाली",
                    "province_id"=> 7,
                ]
            ]
        );
    }
}
