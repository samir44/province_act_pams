<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VatOfficeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vat_offices')->insert(
            [
                ['name'=> 'करदाता सेवा कार्यालय, फिदिम',      'address' => 'फिदिम',    'status' => 1],
                ['name'=> 'करदाता सेवा कार्यालय, इलाम',      'address' => 'इलाम',    'status' => 1],
                ['name'=> 'करदाता सेवा कार्यालय, खाँदवारी',     'address'=> 'खाँदवारी',    'status' => 1],
                ['name'=> 'करदाता सेवा कार्यालय, धनकुटा',     'address' => 'धनकुटा',    'status' => 1],
                ['name'=> 'करदाता सेवा कार्यालय, दिक्तेल',      'address' => 'दिक्तेल',        'status' => 1],
                ['name'=> 'करदाता सेवा कार्यालय, लुक्ला',      'address' => 'लुक्ला',         'status' => 1],
                ['name'=> 'करदाता सेवा कार्यालय, गाईघाट',    'address' => 'गाईघाट',       'status' => 1],
                ['name'=> 'आन्तरिक राजस्व कार्यालय, दमक',    'address' => 'दमक',     'status' => 1],
                ['name'=> 'आन्तरिक राजस्व कार्यालय, भद्रपुर',     'address' => 'भद्रपुर',     'status' => 1],
                ['name'=> 'आन्तरिक राजस्व कार्यालय, विराटनगर',    'address' => 'विराटनगर',   'status' => 1],
                ['name'=> 'करदाता सेवा कार्यालय, विराटचोक',       'address' => 'विराटचोक',     'status' => 1],
                ['name'=> 'आन्तरिक राजस्व कार्यालय, धरान',        'address' => 'धरान',      'status' => 1],
                ['name'=> 'करदाता सेवा कार्यालय, इटहरी',         'address' => 'इटहरी',        'status' => 1],
                ['name'=> 'करदाता सेवा कार्यालय, राजविराज',       'address' => 'राजविराज',     'status' => 1],
                ['name'=> 'आन्तरिक राजस्व कार्यालय, लाहान',       'address' => 'लाहान',       'status' => 1],
                ['name'=> 'आन्तरिक राजस्व कार्यालय, जनकपुरधाम',   'address' => 'जनकपुरधाम',     'status' => 1],
                ['name'=> 'करदाता सेवा कार्यालय, बर्दिवास',         'address' => 'बर्दिवास',           'status' => 1],
                ['name'=> 'करदाता सेवा कार्यालय, मलंगवा',        'address' => 'मलंगवा',         'status' => 1],
                ['name'=> 'करदाता सेवा कार्यालय, गौर',          'address' => 'गौर',            'status' => 1],
                ['name'=> 'आन्तरिक राजस्व कार्यालय, सिमरा',      'address' => 'सिमरा',     'status' => 1],
                ['name'=> 'आन्तरिक राजस्व कार्यालय, विरगंज',     'address' => 'विरगंज',     'status' => 1],
                ['name'=> 'आन्तरिक राजस्व कार्यालय, धुलिखेल',     'address' => 'धुलिखेल',     'status' => 1],
                ['name'=> 'करदाता सेवा कार्यालय, गल्छी',          'address' => 'गल्छी',     'status' => 1],
                ['name'=> 'आन्तरिक राजस्व कार्यालय, भरतपुर',     'address' => 'भरतपुर',     'status' => 1],
                ['name'=> 'आन्तरिक राजस्व कार्यालय, हेटौंडा',     'address' => 'हेटौंडा',     'status' => 1],
                ['name'=> 'आन्तरिक राजस्व कार्यालय, सुर्यविनायक',  'address' => 'सुर्यविनायक',     'status' => 1],
                ['name'=> 'ठूला करदाता कार्यालय, हरिहरभवन',     'address' => 'हरिहरभवन',     'status' => 1],
                ['name'=> 'आन्तरिक राजस्व कार्यालय, हरिहरभवन',  'address' => 'हरिहरभवन',     'status' => 1],
                ['name'=> 'आन्तरिक राजस्व कार्यालय, लगनखेल',  'address' => 'लगनखेल',     'status' => 1],
                ['name'=> 'आन्तरिक राजस्व विभाग, लाजिम्पाट',    'address' => 'लाजिम्पाट',     'status' => 1],
                ['name'=> 'मध्यमस्तरीय करदाता कार्यालय, बबरमहल',  'address' => 'बबरमहल',  'status' => 1],
                ['name'=> 'आन्तरिक राजस्व कार्यालय, चावहिल',     'address' => 'चावहिल',     'status' => 1],
                ['name'=> 'आन्तरिक राजस्व कार्यालय, कोटेश्वर',     'address' => 'कोटेश्वर',     'status' => 1],
                ['name'=> 'आन्तरिक राजस्व कार्यालय, बानेश्वर',     'address' => 'बानेश्वर',     'status' => 1],
                ['name'=> 'आन्तरिक राजस्व कार्यालय, पुतलीसडक',  'address' => 'पुतलीसडक',     'status' => 1],
                ['name'=> 'आन्तरिक राजस्व कार्यालय, बत्तिसपुतली',  'address' => 'बत्तिसपुतली',     'status' => 1],
                ['name'=> 'आन्तरिक राजस्व कार्यालय, कलङ्की',  'address' => 'कलङ्की',     'status' => 1],
                ['name'=> 'आन्तरिक राजस्व कार्यालय, टंगाल',  'address' => 'टंगाल',     'status' => 1],
                ['name'=> 'आन्तरिक राजस्व कार्यालय, ठमेल',  'address' => 'ठमेल',     'status' => 1],
                ['name'=> 'आन्तरिक राजस्व कार्यालय, बालाजु',  'address' => 'बालाजु',     'status' => 1],
                ['name'=> 'आन्तरिक राजस्व कार्यालय, महाराजगञ्ज',  'address' => 'महाराजगञ्ज',     'status' => 1],
                ['name'=> 'आन्तरिक राजस्व कार्यालय, नयाँसडक',  'address' => 'नयाँसडक',     'status' => 1],
                ['name'=> 'आन्तरिक राजस्व कार्यालय, कालिमाटी',  'address' => 'कालिमाटी',     'status' => 1],
                ['name'=> 'आन्तरिक राजस्व कार्यालय, त्रिपुरेश्वर',  'address' => 'त्रिपुरेश्वर',     'status' => 1],
                ['name'=> 'आन्तरिक राजस्व कार्यालय, दमौली',  'address' => 'दमौली',     'status' => 1],
                ['name'=> 'आन्तरिक राजस्व कार्यालय, पोखरा',  'address' => 'पोखरा',     'status' => 1],
                ['name'=> 'आन्तरिक राजस्व कार्यालय, बाग्लुङ',  'address' => 'बाग्लुङ',     'status' => 1],
                ['name'=> 'करदाता सेवा कार्यालय, कावासोती',  'address' => 'कावासोती',     'status' => 1],
                ['name'=> 'आन्तरिक राजस्व कार्यालय, भैरहवा',  'address' => 'भैरहवा',     'status' => 1],
                ['name'=> 'आन्तरिक राजस्व कार्यालय, बुटवल',  'address' => 'बुटवल',     'status' => 1],
                ['name'=> 'आन्तरिक राजस्व कार्यालय, कृष्णनगर',  'address' => 'कृष्णनगर',     'status' => 1],
                ['name'=> 'करदाता सेवा कार्यालय, पाल्पा',      'address' => 'पाल्पा',         'status' => 1],
                ['name'=> 'आन्तरिक राजस्व कार्यालय, तुलसीपुर',  'address' => 'तुलसीपुर',     'status' => 1],
                ['name'=> 'आन्तरिक राजस्व कार्यालय, नेपालगञ्ज',  'address' => 'नेपालगञ्ज',     'status' => 1],
                ['name'=> 'करदाता सेवा कार्यालय, जुम्ला',       'address' => 'जुम्ला',         'status' => 1],
                ['name'=> 'आन्तरिक राजस्व कार्यालय, विरेन्द्रनगर',  'address' => 'विरेन्द्रनगर',     'status' => 1],
                ['name'=> 'करदाता सेवा कार्यालय, डडेल्धुरा',       'address' => 'डडेल्धुरा',          'status' => 1],
                ['name'=> 'आन्तरिक राजस्व कार्यालय, महेन्द्रनगर',  'address' => 'महेन्द्रनगर',     'status' => 1],
                ['name'=> 'आन्तरिक राजस्व कार्यालय, धनगढी',    'address' => 'धनगढी',     'status' => 1],
                ['name'=> 'करदाता सेवा कार्यालय, लम्की',      'address' => 'लम्की',          'status' => 1],

            ]
        );
    }
}



