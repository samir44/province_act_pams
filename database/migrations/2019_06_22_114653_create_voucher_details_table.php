<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVoucherDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voucher_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('journel_id');
            $table->integer('office_id');
            $table->integer('budget_sub_head_id');
            $table->integer('main_activity_id')->nullable();
            $table->integer('dr_or_cr');
            $table->integer('ledger_type_id');
            $table->integer('expense_head_id');
            $table->string('bibaran');
            $table->integer('peski_paune_id')->nullable();
            $table->integer('party_type')->nullable();
            $table->float('dr_amount');
            $table->float('cr_amount');
            $table->integer('status')->nullable();
            $table->string('date_nepali');
            $table->dateTime('date_english');
            $table->integer('month');
            $table->string('fiscal_year');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voucher_details');

    }
}
