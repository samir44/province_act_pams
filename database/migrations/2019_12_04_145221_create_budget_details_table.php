<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBudgetDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('budget_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('office_id');
            $table->integer('budget_sub_head_id');
            $table->integer('budget_id');
            $table->string('sub_activity');
            $table->integer('expense_head_id');
            $table->integer('unit')->nullable();
            $table->integer('total_unit');
            $table->float('total_budget');
            $table->integer('first_quarter_unit')->nullable();
            $table->integer('second_quarter_unit')->nullable();
            $table->integer('third_quarter_unit')->nullable();
            $table->float('first_quarter_budget')->nullable();
            $table->float('second_quarter_budget')->nullable();
            $table->float('third_quarter_budget')->nullable();
            $table->float('first_chaimasik_bhar', 11, 2)->nullable();
            $table->float('second_chaimasik_bhar', 11, 2)->nullable();
            $table->float('third_chaimasik_bhar', 11, 2)->nullable();
            $table->integer('source_type');
            $table->integer('source_level');
            $table->integer('source');
            $table->integer('medium');
            $table->integer('user_id');
            $table->integer('fiscal_year');
            $table->dateTime('date');
            $table->integer('status');
            $table->integer('contenjency')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('budget_details');

    }
}
