<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRetentionPreBhuktaniTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('retention_pre_bhuktani', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('journel_id');
            $table->integer('office_id');
            $table->integer('party_type');
            $table->integer('party');
            $table->float('amount');
            $table->integer('bhuktani_id')->nullable();
            $table->integer('month');
            $table->string('date_nepali');
            $table->dateTime('date_english');
            $table->integer('status');
            $table->string('fiscal_year');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('retention_pre_bhuktani');
    }
}
