<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePreBhuktaniTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pre_bhuktani', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('journel_id');
            $table->integer('office_id');
            $table->integer('budget_sub_head_id');
            $table->integer('main_activity_id');
            $table->integer('bhuktani_paaune');
            $table->integer('party_type');
            $table->float('amount');
            $table->integer('bhuktani_id')->nullable();
            $table->integer('vat_bill_number')->nullable();
            $table->integer('pratibadhhata_number')->nullable();
            $table->integer('payee_code_number')->nullable();
            $table->float('advance_vat_deduction')->nullable();
            $table->float('vat_amount')->nullable();
            $table->integer('month');
            $table->string('date_nepali');
            $table->dateTime('date_english');
            $table->integer('status');
            $table->string('fiscal_year');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pre_bhuktani');

    }
}
