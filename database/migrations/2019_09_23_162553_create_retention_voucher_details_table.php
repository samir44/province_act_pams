<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRetentionVoucherDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('retention_vouchers_details', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('retention_voucher_id');
            $table->integer('office_id');
            $table->integer('dr_or_cr');
            $table->integer('byahora');
            $table->integer('hisab_number');
            $table->string('details');
            $table->integer('retention_record')->nullable();
            $table->integer('retention_type')->nullable();
            $table->integer('depositor_type')->nullable();
            $table->integer('depositor')->nullable();
            $table->integer('bill_number')->nullable();
            $table->string('amount');
            $table->string('date_nep');
            $table->datetime('date_eng');
            $table->integer('month');
            $table->string('fiscal_year');
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('retention_vouchers_details');
    }
}
