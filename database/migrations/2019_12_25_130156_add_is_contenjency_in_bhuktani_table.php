<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsContenjencyInBhuktaniTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bhuktani', function (Blueprint $table) {

            $table->integer('is_contenjency')->nullable();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bhuktani', function (Blueprint $table) {
            $table->dropColumn('is_contenjency');
        });
    }
}
