<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAkhtiyariTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('akhtiyari', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('budget_sub_head_id');
            $table->integer('office_id');
            $table->integer('akhtiyari_type');
            $table->string('amount');
            $table->integer('source_type');
            $table->integer('source_level');
            $table->integer('source');
            $table->integer('medium');
            $table->dateTime('date_nepali_roman');
            $table->dateTime('date_english');
            $table->string('fiscal_year');
            $table->string('detail');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('akhtiyari');

    }
}
