<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RetentionBankGuaranteeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('retention_bank_guarantee', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('office_id');
            $table->integer('fiscal_year_id');
            $table->integer('retention_record_purpose_id');
            $table->integer('retention_depositor_type_id');
            $table->integer('retention_depositor_id');
            $table->integer('bank_id');
            $table->integer('reference_number');
            $table->dateTime('date');
            $table->integer('retention_bank_guarantee_number');
            $table->float('amount');
            $table->dateTime('guarantee_start_date');
            $table->dateTime('guarantee_end_date');
            $table->integer('month');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('retention_bank_guarantee');
    }
}
