<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsInBudgetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('budget', function (Blueprint $table) {
            $table->string('project_code')->nullable();
            $table->string('sub_project_code')->nullable();
            $table->string('component_code')->nullable();
            $table->string('component_ndesc')->nullable();
            $table->string('activity_code')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('budget', function (Blueprint $table) {
            $table->dropColumn('project_code');
            $table->dropColumn('sub_project_code');
            $table->dropColumn('component_code');
            $table->dropColumn('activity_code');
        });
    }
}
