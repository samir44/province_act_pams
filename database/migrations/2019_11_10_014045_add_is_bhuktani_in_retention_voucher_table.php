<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsBhuktaniInRetentionVoucherTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('retention_vouchers', function (Blueprint $table) {
            $table->integer('is_bhuktani')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('retention_vouchers', function (Blueprint $table) {
            $table->dropColumn('is_bhuktani');
        });
    }
}
