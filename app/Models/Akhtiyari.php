<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Akhtiyari extends Model
{
    protected $table = 'akhtiyari';
    protected $guarded = [];
    public $timestamps = false;



    public function program(){

        return $this->belongsTo('App\Models\Programs','budget_sub_head_id','id');
    }

    public function source_type(){

        return $this->belongsTo('App\Models\SourceType','source_type','id');
    }

    public function source_level(){

        return $this->belongsTo('App\Models\BudgetSourceLevel','source_level','id');
    }

    public function source(){

        return $this->belongsTo('App\Models\Source','source','id');
    }

    public function medium(){

        return $this->belongsTo('App\Models\Medium','medium','id');
    }


    public function sub_areas(){
        return $this->hasMany('App\Models\Akhtiyari');
    }


    public function budget(){
        return $this->hasMany('App\Models\Budget','akhtiyari_type','id');
    }



}
