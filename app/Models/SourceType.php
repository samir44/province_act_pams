<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\This;

class SourceType extends Model
{
    protected $table = 'source_types';
    protected $guarded = [];

    public function sourceLevel()
    {
        return $this->hasMany('App\Models\BudgetSourceLevel');
    }

    public function source()
    {
        return $this->hasMany('App\Models\Source');
    }

    public function mofs()
    {
        return $this->hasMany('App\Models\Mof');
    }

    public function ministries()
    {
        return $this->hasMany('App\Models\Ministries');
    }

    public function departments()
    {
        return $this->hasMany('App\Models\Department');
    }

    public function offices()
    {
        return $this->hasMany('App\Models\Office');
    }

    public function users()
    {
        return $this->hasMany('App\Users');
    }

    public function budget()
    {

        return $this->hasMany('App\Models\Budget', 'source_type', 'id');
    }


    public function getExpenseHead($officeId, $budgetSubHeadId)
    {
        return $budgetByExpenseHead = Budget::where('office_id', $officeId)
            ->where('budget_sub_head', $budgetSubHeadId)
            ->groupBy('expense_head')
            ->orderBy('expense_head')
            ->get();
//        dd($budgetByExpenseHead);
    }

    public function getInitialBudget($officeId, $budgetSubHeadId, $expneseHeadId, $sourceTypeId)
    {

        return $initialBudget = Budget::
            join('akhtiyari', 'budget.akhtiyari_type', '=', 'akhtiyari.id')
            ->where('budget.office_id', $officeId)
            ->where('akhtiyari.akhtiyari_type', 1)
            ->where('budget.budget_sub_head', $budgetSubHeadId)
            ->where('budget.expense_head_id', $expneseHeadId)
            ->where('budget.source_type', $sourceTypeId)
            ->sum('budget.total_budget');
    }

    public function getAddBudget($officeId, $budgetSubHeadId, $expneseHeadId, $sourceTypeId)
    {
        return $initialBudget = Budget::join('akhtiyari', 'budget.akhtiyari_type', '=', 'akhtiyari.id')
            ->where('budget.office_id', $officeId)
            ->where('akhtiyari.akhtiyari_type', 2)
            ->where('budget.budget_sub_head', $budgetSubHeadId)
            ->where('budget.expense_head_id', $expneseHeadId)
            ->where('budget.source_type', $sourceTypeId)
            ->sum('budget.total_budget');
    }

    public function getReduceBudget($officeId, $budgetSubHeadId, $expneseHeadId, $sourceTypeId)
    {
        return $initialBudget = Budget::join('akhtiyari', 'budget.akhtiyari_type', '=', 'akhtiyari.id')
            ->where('budget.office_id', $officeId)
            ->where('akhtiyari.akhtiyari_type', 3)
            ->where('budget.budget_sub_head', $budgetSubHeadId)
            ->where('budget.expense_head_id', $expneseHeadId)
            ->where('budget.source_type', $sourceTypeId)
            ->sum('budget.total_budget');
    }

    public function getFinalBudget($officeId, $budgetSubHeadId, $expneseHeadId, $sourceTypeId)
    {
        return $initialBudget = Budget::

        where('office_id', $officeId)
            ->where('budget_sub_head', $budgetSubHeadId)
            ->where('expense_head_id', $expneseHeadId)
            ->where('source_type', $sourceTypeId)
            ->sum('total_budget');
    }

    public function getTotalExpense($officeId, $budgetSubHeadId, $expneseHeadId, $sourceTypeId)
    {
        return $initialBudget = VoucherDetail::join('budget_details', 'voucher_details.main_activity_id', '=', 'budget_details.id')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('voucher_details.office_id', $officeId)
            ->where('voucher_details.budget_sub_head_id', $budgetSubHeadId)
            ->where('vouchers.status', 1)
            ->where('voucher_details.expense_head_id', $expneseHeadId)
            ->where('budget_details.source_type', $sourceTypeId)
            ->where('dr_or_cr', 1)
            ->where('ledger_type_id', ['1', '4'])
            ->sum('dr_amount');
    }


    public function remainBudget($officeId, $budgetSubHeadId, $expneseHeadId, $sourceTypeId)
    {

        $totalFianlBudget = $this->getFinalBudget($officeId, $budgetSubHeadId, $expneseHeadId, $sourceTypeId);
        $totalExpense = $this->getTotalExpense($officeId, $budgetSubHeadId, $expneseHeadId, $sourceTypeId);
        return $remainBudget = (float)$totalFianlBudget - (float)$totalExpense;
    }

    public function getTotalInitialBudget($officeId, $budgetSubHeadId, $sourceTypeId)
    {

        return $totalInitialBudget = Budget::join('akhtiyari', 'budget.akhtiyari_type', '=', 'akhtiyari.id')
            ->where('budget.office_id', $officeId)
            ->where('akhtiyari.akhtiyari_type', 1)
            ->where('budget.budget_sub_head', $budgetSubHeadId)
            ->where('budget.source_type', $sourceTypeId)
            ->sum('budget.total_budget');
    }

    public function getTotalAddBudget($officeId, $budgetSubHeadId, $sourceTypeId)
    {

        return $totalInitialBudget = Budget::join('akhtiyari', 'budget.akhtiyari_type', '=', 'akhtiyari.id')
            ->where('budget.office_id', $officeId)
            ->where('akhtiyari.akhtiyari_type', 2)
            ->where('budget.budget_sub_head', $budgetSubHeadId)
            ->where('budget.source_type', $sourceTypeId)
            ->sum('budget.total_budget');
    }

    public function getTotalReduceBudget($officeId, $budgetSubHeadId, $sourceTypeId)
    {

        return $totalInitialBudget = Budget::join('akhtiyari', 'budget.akhtiyari_type', '=', 'akhtiyari.id')
            ->where('budget.office_id', $officeId)
            ->where('akhtiyari.akhtiyari_type', 3)
            ->where('budget.budget_sub_head', $budgetSubHeadId)
            ->where('budget.source_type', $sourceTypeId)
            ->sum('budget.total_budget');
    }

    public function getTotalFinalBudget($officeId, $budgetSubHeadId, $sourceTypeId)
    {

        return $totalInitialBudget = Budget::

        where('office_id', $officeId)
            ->where('budget_sub_head', $budgetSubHeadId)
            ->where('source_type', $sourceTypeId)
            ->sum('total_budget');
    }

    public function getGranTotalExpense($officeId, $budgetSubHeadId, $sourceTypeId)
    {
        return $initialBudget = VoucherDetail::join('budget_details', 'voucher_details.main_activity_id', '=', 'budget_details.id')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.office_id', $officeId)
            ->where('voucher_details.budget_sub_head_id', $budgetSubHeadId)
            ->where('budget_details.source_type', $sourceTypeId)
            ->where('dr_or_cr', 1)
            ->where('ledger_type_id', ['1', '4'])
            ->sum('dr_amount');
    }

    public function totalRemainBudget($officeId, $budgetSubHeadId, $sourceTypeId)
    {

        $granTotalBudget = $this->getTotalFinalBudget($officeId, $budgetSubHeadId, $sourceTypeId);
        $granTotalExp = $this->getGranTotalExpense($officeId, $budgetSubHeadId, $sourceTypeId);
        return $granTotalRemain = (float)$granTotalBudget - (float)$granTotalExp;
    }


}
