<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MainActivity extends Model
{
    protected $table = 'general_activities';
    protected $guarded = [];


    public function area(){
        return $this->belongsTo('App\Models\Area');
    }

    public function sub_area(){
        return $this->belongsTo('App\Models\SubArea');
    }

    public function main_program(){
        return $this->belongsTo('App\Models\MainProgram');
    }


}
