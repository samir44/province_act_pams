<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Budget extends Model
{
    protected $table = 'budget';
    protected $guarded = [];

    public function budgetArea()
    {

        return $this->belongsTo('App\Models\Area', 'area', 'id');
    }

    public function chaineDetail()
    {
        return $this->hasMany('App\Models\BudgetDetails', 'budget_id', 'id');
    }

    public function voucherDetails()
    {
        return $this->hasMany('App\Models\VoucherDetail');
    }

    public function getVoucherDetails()
    {

        return $this->hasMany('App\Models\VoucherDetail', 'main_activity_id', 'id');

    }

    public function expense_by_expense_head()
    {

        return $this->hasOne('App\Models\ExpenseHead', 'expense_head_code', 'expense_head');
    }


    public function expense_head_by_id()
    {

        return $this->hasOne('App\Models\ExpenseHead', 'id', 'expense_head_id');
    }

    public function get_source()
    {

        return $this->hasOne('App\Models\Source', 'id', 'source');

    }

    public function get_source_type()
    {

        return $this->hasOne('App\Models\SourceType', 'id', 'source_type');
    }

    public function get_budget_source_level()
    {

        return $this->hasOne('App\Models\BudgetSourceLevel', 'id', 'source_level');
    }

    public function get_medium()
    {

        return $this->hasOne('App\Models\Medium', 'id', 'medium');
    }


    public function getLastBudget($expenHead, $program_id)
    {
        return $last_budget = Budget::
            where('fiscal_year', $this->fiscal_year)
            ->where('budget_sub_head', $program_id)
            ->where('expense_head', $expenHead)
            ->sum('total_budget');

    }

    public function get_expense_this_month($budget_sub_head, $month_post)
    {

        return $expense = DB::table('voucher_details')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.fiscal_year', '=', $this->fiscal_year)
            ->where('voucher_details.office_id', '=', $this->office_id)
            ->where('voucher_details.budget_sub_head_id', '=', $budget_sub_head)
            ->where('voucher_details.month', '=', $month_post)
            ->where('voucher_details.expense_head_id', '=', $this->expense_head_id)
            ->where('voucher_details.dr_or_cr', '=', 1)
//            ->where('voucher_details.ledger_type_id','=',1)
            ->whereIn('voucher_details.ledger_type_id', [1, 4])
            ->sum('dr_amount');

    }

    public function get_expense_upto_prev_month($budget_sub_head, $month_post)
    {

        return $expense = DB::table('voucher_details')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.fiscal_year', '=', $this->fiscal_year)
            ->where('voucher_details.office_id', '=', $this->office_id)
            ->where('voucher_details.budget_sub_head_id', '=', $budget_sub_head)
            ->where('voucher_details.month', '<', $month_post)
            ->where('voucher_details.expense_head_id', '=', $this->expense_head_id)
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('dr_amount');

//         $expense = VoucherDetail::where('expense_head_id',$this->expense_head_id)
//            ->where('fiscal_year',$this->fiscal_year)
//            ->where('office_id',$this->office_id)
//            ->where('month','<',$month_post)
//
//            ->sum('dr_amount');
//        dd($expense);
    }

    public function get_expense_upto_this_month($budget_sub_head, $month_post)
    {

        return $expense = DB::table('voucher_details')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.fiscal_year', '=', $this->fiscal_year)
            ->where('voucher_details.office_id', '=', $this->office_id)
            ->where('voucher_details.budget_sub_head_id', '=', $budget_sub_head)
            ->where('voucher_details.month', '<=', $month_post)
            ->where('voucher_details.expense_head_id', '=', $this->expense_head_id)
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('dr_amount');

//        return $expense = VoucherDetail::where('expense_head_id',$this->expense_head_id)
//            ->where('fiscal_year',$this->fiscal_year)
//            ->where('office_id',$this->office_id)
//            ->where('month','<=',$month_post)
//            ->where('dr_or_cr','1')
//            ->where('ledger_type_id','1')
////            ->where('ledger_type_id','4')
//            ->sum('dr_amount');
//        dd($expense);
    }

    public function get_peski_upto_this_month($budget_sub_head, $month_post)
    {

//        dd($this);
        return $expense = DB::table('voucher_details')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.fiscal_year', '=', $this->fiscal_year)
            ->where('voucher_details.office_id', '=', $this->office_id)
            ->where('voucher_details.budget_sub_head_id', '=', $budget_sub_head)
            ->where('voucher_details.month', '<=', $month_post)
            ->where('voucher_details.expense_head_id', '=', $this->expense_head_id)
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->where('ledger_type_id', '4')
            ->sum('dr_amount');
//        dd($expense);
    }

    public function expense_without_advance($budget_sub_head, $month_post)
    {

        $get_total_expense = $this->get_expense_upto_this_month($budget_sub_head, $month_post);
        $total_advance = $this->get_peski_upto_this_month($budget_sub_head, $month_post);
        return $total_exp_with_out_advance = (float)$get_total_expense - (float)$total_advance;

    }

    public function remain_budget($total_budget, $month_post, $expenHead, $program_id, $budget_sub_head)
    {

//        dd($expenHead);
        $total_budget = $this->getLastBudget($expenHead, $program_id);
//        dd($total_budget);
        $get_total_expense = $this->get_expense_upto_this_month($budget_sub_head, $month_post);

        return $remain = (float)$total_budget - (float)$get_total_expense;


    }

    public function getExpenseHead($officeId, $budgetSubHeadId)
    {

        return $budgetByExpenseHead = Budget::where('office_id', $officeId)
            ->where('budget_sub_head', $budgetSubHeadId)
            ->groupBy('expense_head')
            ->orderBy('expense_head')
            ->get();
    }


//    Ministry Report

    public function getLastBudgetOfMinistry($ministryId, $expenseHead)
    {

        return $last_budget = Budget::join('offices', 'budget.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->where('ministries.id', $ministryId)
            ->where('budget.expense_head_id', $expenseHead)
            ->sum('budget.total_budget');
//        dd($last_budget);
    }


    public function getTotalExpenseOfMinistry($ministryId, $expenseHeadId)
    {

        $ldate = date('Y-m-d H:i:s');
        $ldate_y = date('d.m.Y', strtotime("-1 days"));

        return $expense = DB::table('voucher_details')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->join('offices', 'voucher_details.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->where('ministries.id', $ministryId)
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.expense_head_id', '=', $this->expense_head_id)
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('dr_amount');
    }

    public function getTotalExpenseUpToYesterdayOfMinistry($ministryId, $expenseHeadId)
    {

        $ldate = date('Y-m-d H:i:s');
        $yesterday = date('d.m.Y', strtotime("-1 days"));

        return $expense = DB::table('voucher_details')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->join('offices', 'voucher_details.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->where('ministries.id', $ministryId)
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.expense_head_id', '=', $expenseHeadId)
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->where('voucher_details.date_english', '<=', $yesterday)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('dr_amount');
    }

    public function getTotalExpenseTodayOfMinistry($ministryId, $expenseHeadId)
    {

        $today = date('Y-m-d H:i:s');
        $yesterday = date('d.m.Y', strtotime("-1 days"));

        return $expense = DB::table('voucher_details')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->join('offices', 'voucher_details.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->where('ministries.id', $ministryId)
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.expense_head_id', '=', $expenseHeadId)
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->where('voucher_details.date_english', '=', $today)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('dr_amount');
    }

    public function getTotalExpenseUpToThisDayOfMinistry($ministryId, $expenseHeadId)
    {

        $today = date('Y-m-d H:i:s');
        $yesterday = date('d.m.Y', strtotime("-1 days"));

        return $expense = DB::table('voucher_details')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->join('offices', 'voucher_details.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->where('ministries.id', $ministryId)
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.expense_head_id', '=', $expenseHeadId)
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->where('voucher_details.date_english', '<=', $today)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('dr_amount');
    }

    public function getTotalAdvanceOfMinistry($ministryId, $expenseHeadId)
    {

        $today = date('Y-m-d H:i:s');
        $yesterday = date('d.m.Y', strtotime("-1 days"));

        return $expense = DB::table('voucher_details')
            ->join('offices', 'voucher_details.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->where('ministries.id', $ministryId)
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.expense_head_id', '=', $expenseHeadId)
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->where('voucher_details.ledger_type_id', 4)
            ->sum('dr_amount');
    }

    public function expenseWithOutAdvance($ministryId, $expenseHeadId)
    {


        $totalExpense = $this->getTotalExpenseUpToThisDayOfMinistry($ministryId, $expenseHeadId);
        $totalAdvance = $this->getTotalAdvanceOfMinistry($ministryId, $expenseHeadId);
        return $totalExpenseWithOutAdvance = (float)$totalExpense - (float)$totalAdvance;
    }

    public function getRemainBudget($ministryId, $expenseHeadId)
    {

        $totalBudget = $this->getLastBudgetOfMinistry($ministryId, $expenseHeadId);
        $totalExpense = $this->getTotalExpenseUpToThisDayOfMinistry($ministryId, $expenseHeadId);
        return $remainBudget = (float)$totalBudget - (float)$totalExpense;
    }


//    Office and Budget Sub Head wise

    public function getLastBudgetOfOfficeByBudgetSubHead($office_id, $budget_sub_head_id, $expense_head_id)
    {

        return $last_budget = DB::table('budget')
            ->where('office_id', $office_id)
            ->where('budget_sub_head', $budget_sub_head_id)
            ->where('expense_head_id', $expense_head_id)
            ->sum('total_budget');


    }

    public function getTotalExpenseOfOfficeByBudgetSubHead($office_id, $budget_sub_head_id, $expense_head_id)
    {

        return $expense = DB::table('voucher_details')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.budget_sub_head_id', '=', $budget_sub_head_id)
            ->where('voucher_details.expense_head_id', '=', $expense_head_id)
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('dr_amount');
    }

    public function getTotalExpenseUpToYesterdayOfOfficeByBudgetSubHead($office_id, $budget_sub_head_id, $expense_head_id)
    {
        $ldate = date('Y-m-d H:i:s');
        $yesterday = date('d.m.Y', strtotime("-1 days"));

        return $expense = DB::table('voucher_details')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.office_id', '=', $office_id)
            ->where('voucher_details.budget_sub_head_id', '=', $budget_sub_head_id)
            ->where('voucher_details.expense_head_id', '=', $expense_head_id)
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->where('voucher_details.date_english', '<=', $yesterday)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('dr_amount');

    }

    public function getTotalExpenseTodayOfOfficeByBudgetSubHead($office_id, $budget_sub_head_id, $expense_head_id)
    {

        $today = date('Y-m-d H:i:s');
        $yesterday = date('d.m.Y', strtotime("-1 days"));

        return $expense = DB::table('voucher_details')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.office_id', '=', $office_id)
            ->where('voucher_details.budget_sub_head_id', '=', $budget_sub_head_id)
            ->where('voucher_details.expense_head_id', '=', $expense_head_id)
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->where('voucher_details.date_english', '=', $today)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('dr_amount');
    }

    public function getTotalExpenseUpToThisDayOfOfficeByBudgetSubHead($office_id, $budget_sub_head_id, $expense_head_id)
    {

        $today = date('Y-m-d H:i:s');
        $yesterday = date('d.m.Y', strtotime("-1 days"));

        return $expense = DB::table('voucher_details')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.office_id', '=', $office_id)
            ->where('voucher_details.budget_sub_head_id', '=', $budget_sub_head_id)
            ->where('voucher_details.expense_head_id', '=', $expense_head_id)
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->where('voucher_details.date_english', '<=', $today)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('dr_amount');
    }

    public function getTotalAdvanceOfOfficeByBudgetSubHead($office_id, $budget_sub_head_id, $expense_head_id)
    {

        $today = date('Y-m-d H:i:s');
        $yesterday = date('d.m.Y', strtotime("-1 days"));

        return $expense = DB::table('voucher_details')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.office_id', '=', $office_id)
            ->where('voucher_details.budget_sub_head_id', '=', $budget_sub_head_id)
            ->where('voucher_details.expense_head_id', '=', $expense_head_id)
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->where('voucher_details.ledger_type_id', 4)
            ->sum('dr_amount');
    }

    public function expenseWithOutAdvanceOfOfficeByBudgetSubHead($office_id, $budget_sub_head_id, $expense_head_id)
    {


        $totalExpense = $this->getTotalExpenseUpToThisDayOfOfficeByBudgetSubHead($office_id, $budget_sub_head_id, $expense_head_id);
        $totalAdvance = $this->getTotalAdvanceOfOfficeByBudgetSubHead($office_id, $budget_sub_head_id, $expense_head_id);
        return $totalExpenseWithOutAdvance = (float)$totalExpense - (float)$totalAdvance;
    }

    public function getRemainBudgetOfOfficeByBudgetSubHead($office_id, $budget_sub_head_id, $expense_head_id)
    {

        $totalBudget = $this->getLastBudgetOfOfficeByBudgetSubHead($office_id, $budget_sub_head_id, $expense_head_id);
        $totalExpense = $this->getTotalExpenseUpToThisDayOfOfficeByBudgetSubHead($office_id, $budget_sub_head_id, $expense_head_id);
        return $remainBudget = (float)$totalBudget - (float)$totalExpense;
    }


//    Activity Wise Budget and expense report By Ministry
    public function firstChaumasikExpense($office_id, $budgetSubHead, $activityId)
    {

//        dd($budgetSubHead);
        return $expenseOnActivity = VoucherDetail::
        join('budget_details', 'voucher_details.main_activity_id', '=', 'budget_details.id')
            ->join('budget', 'budget_details.budget_id', '=', 'budget.id')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
//            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.fiscal_year', '=', $this->fiscal_year)
            ->where('voucher_details.office_id', '=', $office_id)
            ->where('voucher_details.budget_sub_head_id', '=', $budgetSubHead)
            ->where('budget.id', '=', $activityId)
            ->whereBetween('voucher_details.month', array(1, 4))
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->whereIn('voucher_details.ledger_type_id', [1, 4])
            ->sum('voucher_details.dr_amount');
//         dd($expenseOnActivity);
    }

    public function secondChaumasikExpense($office_id, $budgetSubHead, $activityId)
    {

        return $expenseOnActivity = VoucherDetail::
        join('budget_details', 'voucher_details.main_activity_id', '=', 'budget_details.id')
            ->join('budget', 'budget_details.budget_id', '=', 'budget.id')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
//            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
//            ->where('voucher_details.fiscal_year', '=', $this->fiscal_year)
            ->where('voucher_details.office_id', '=', $office_id)
            ->where('voucher_details.budget_sub_head_id', '=', $budgetSubHead)
            ->where('budget.id', '=', $activityId)
            ->whereBetween('voucher_details.month', array(5, 8))
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->whereIn('voucher_details.ledger_type_id', [1, 4])
            ->sum('voucher_details.dr_amount');
    }

    public function thirdChaumasikExpense($office_id, $budgetSubHead, $activityId)
    {

        return $expenseOnActivity = VoucherDetail::
        join('budget_details', 'voucher_details.main_activity_id', '=', 'budget_details.id')
            ->join('budget', 'budget_details.budget_id', '=', 'budget.id')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
//            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
//            ->where('voucher_details.fiscal_year', '=', $this->fiscal_year)
            ->where('voucher_details.office_id', '=', $office_id)
            ->where('voucher_details.budget_sub_head_id', '=', $budgetSubHead)
            ->where('budget.id', '=', $activityId)
            ->whereBetween('voucher_details.month', array(9, 12))
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->whereIn('voucher_details.ledger_type_id', [1, 4])
            ->sum('voucher_details.dr_amount');
    }


    public function totalExpenseByActivity($office_id, $budgetSubHead, $activityId)
    {

        $firstChaumasikExpense = $this->firstChaumasikExpense($office_id, $budgetSubHead, $activityId);
        $secondChaumasikExpense = $this->secondChaumasikExpense($office_id, $budgetSubHead, $activityId);
        $thirdChaumasikExpense = $this->thirdChaumasikExpense($office_id, $budgetSubHead, $activityId);
        return $totalExpnes = (float)$firstChaumasikExpense + (float)$secondChaumasikExpense + (float)$thirdChaumasikExpense;
    }


//-------------------------------------------

    public function getInitialBudget($officeId, $budgetSubHeadId, $expneseHeadId)
    {
        return $initialBudget = Budget::

        where('office_id', $officeId)
            ->where('akhtiyari_type', 1)
            ->where('budget_sub_head', $budgetSubHeadId)
            ->where('expense_head_id', $expneseHeadId)
            ->sum('total_budget');
    }

    public function getAddBudget($officeId, $budgetSubHeadId, $expneseHeadId)
    {
        return $initialBudget = Budget::
        where('office_id', $officeId)
            ->where('akhtiyari_type', 2)
            ->where('budget_sub_head', $budgetSubHeadId)
            ->where('expense_head_id', $expneseHeadId)
            ->sum('total_budget');
    }

    public function getReduceBudget($officeId, $budgetSubHeadId, $expneseHeadId)
    {
//        dd($expneseHeadId);
        return $initialBudget = Budget::
                where('office_id', $officeId)
            ->where('akhtiyari_type', 3)
            ->where('budget_sub_head', $budgetSubHeadId)
            ->where('expense_head_id', $expneseHeadId)
            ->sum('total_budget');
    }

    public function getFinalBudget($officeId, $budgetSubHeadId, $expneseHeadId)
    {
        return $initialBudget = Budget::

        where('office_id', $officeId)
            ->where('budget_sub_head', $budgetSubHeadId)
            ->where('expense_head_id', $expneseHeadId)
            ->sum('total_budget');
    }

    public function getTotalExpense($officeId, $budgetSubHeadId, $expneseHeadId)
    {
//        dd($officeId);
        return $initialBudget = VoucherDetail::join('budget', 'voucher_details.main_activity_id', '=', 'budget.id')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('voucher_details.office_id', $officeId)
            ->where('voucher_details.budget_sub_head_id', $budgetSubHeadId)
            ->where('vouchers.status', 1)
            ->where('voucher_details.expense_head_id', $expneseHeadId)
            ->where('dr_or_cr', 1)
            ->where('ledger_type_id', ['1', '4'])
            ->sum('dr_amount');
    }

    public function thisMonthTotalExpense($officeId, $budgetSubHeadId, $expneseHeadId,$month)
    {
//        dd($officeId);
        return $initialBudget = VoucherDetail::
            join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('voucher_details.office_id', $officeId)
            ->where('voucher_details.budget_sub_head_id', $budgetSubHeadId)
            ->where('vouchers.status', 1)
            ->where('voucher_details.expense_head_id', $expneseHeadId)
            ->where('voucher_details.month', $month)
            ->where('dr_or_cr', 1)
            ->where('ledger_type_id', ['1', '4'])
            ->sum('dr_amount');
    }

    public function preMonthTotalExpense($officeId, $budgetSubHeadId, $expneseHeadId,$month)
    {
        return $initialBudget = VoucherDetail::
            join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('voucher_details.office_id', $officeId)
            ->where('voucher_details.budget_sub_head_id', $budgetSubHeadId)
            ->where('vouchers.status', 1)
            ->where('voucher_details.expense_head_id', $expneseHeadId)
            ->where('voucher_details.month', '<=',$month)
            ->where('dr_or_cr', 1)
            ->where('ledger_type_id', ['1', '4'])
            ->sum('dr_amount');
    }

    public function totalExpense($officeId, $budgetSubHeadId, $expneseHeadId,$month)
    {
//        dd($expneseHeadId);
        return $initialBudget = VoucherDetail::
            join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('voucher_details.office_id', $officeId)
            ->where('voucher_details.budget_sub_head_id', $budgetSubHeadId)
            ->where('vouchers.status', 1)
            ->where('voucher_details.expense_head_id', $expneseHeadId)
            ->where('voucher_details.month', '<=',$month)
            ->where('dr_or_cr', 1)
            ->where('ledger_type_id', ['1', '4'])
            ->sum('dr_amount');
    }

    public function totalRemainBudget($officeId, $budgetSubHeadId, $expneseHeadId,$month)
    {
        $totalBudget = $this->getFinalBudget($officeId,$budgetSubHeadId,$expneseHeadId);
        $totalExpense = $this->totalExpense($officeId,$budgetSubHeadId,$expneseHeadId,$month);
        return $remainBudget = (float)$totalBudget - (float)$totalExpense;
    }

    public function thisMonthNikasa($officeId,$budgetSubHeadId,$expneseHeadId,$month){

        return $initialBudget = VoucherDetail::
        join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('voucher_details.office_id', $officeId)
            ->where('voucher_details.budget_sub_head_id', $budgetSubHeadId)
            ->where('vouchers.status', 1)
            ->where('voucher_details.expense_head_id', $expneseHeadId)
            ->where('voucher_details.month', $month)
            ->where('dr_or_cr', 2)
            ->where('ledger_type_id', '8')
            ->sum('cr_amount');
    }

    public function upToPreMonthNikasa($officeId,$budgetSubHeadId,$expneseHeadId,$month){

        return $initialBudget = VoucherDetail::
        join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('voucher_details.office_id', $officeId)
            ->where('voucher_details.budget_sub_head_id', $budgetSubHeadId)
            ->where('vouchers.status', 1)
            ->where('voucher_details.expense_head_id', $expneseHeadId)
            ->where('voucher_details.month','<=', $month)
            ->where('dr_or_cr', 2)
            ->where('ledger_type_id', '8')
            ->sum('cr_amount');
    }

    public function totalNikasa($officeId,$budgetSubHeadId,$expneseHeadId,$month){

        return $initialBudget = VoucherDetail::
        join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('voucher_details.office_id', $officeId)
            ->where('voucher_details.budget_sub_head_id', $budgetSubHeadId)
            ->where('vouchers.status', 1)
            ->where('voucher_details.expense_head_id', $expneseHeadId)
            ->where('voucher_details.month','<=', $month)
            ->where('dr_or_cr', 2)
            ->where('ledger_type_id', '8')
            ->sum('cr_amount');
    }

}
