<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExpenseHead extends Model
{
    protected $table = 'main_expense_head';
    protected $guarded = [];

//    public function sub_areas(){
//        return $this->hasMany('App\Models\SubArea');
//    }
    public function voucher_detail(){
        return $this->belongsTo('App\Models\VoucherDetail');
    }

    public function budget(){
        return $this->belongsTo('App\Models\Budget');
    }



}
