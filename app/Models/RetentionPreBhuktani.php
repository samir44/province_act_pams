<?php

namespace App\Models;

use App\helpers\Money_words;
use Illuminate\Database\Eloquent\Model;

class RetentionPreBhuktani extends Model
{
    protected $table = 'retention_pre_bhuktani';
    protected $guarded = [];

    public function retentionBhuktani(){

        return $this->belongsTo('App\Models\RetentionBhuktani');
    }

    public function voucher(){
        return $this->belongsTo('App\Models\RetentionVoucher', 'journel_id','id');
    }

    public function getParty(){

        return $this->belongsTo('App\Models\AdvanceAndPayment','party','id');
    }

    public function expense_head(){

        return $this->belongsTo('App\Models\ExpenseHead','hisab_number','id');
    }

    public function get_amount_in_word(){

        $amount = $this->amount;
        $obj = new Money_words();
        return $words = $obj->convert_number($amount);

    }
}
