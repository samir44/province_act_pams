<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdvanceAndPayment extends Model
{
    protected $table = 'advance_payment';
    protected $guarded = [];


    public function get_party_type(){

        return $this->belongsTo('App\Models\PartyTypes','party_type','id');
    }


    public function preBhktani(){

        return $this->hasMany('App\Models\PreBhuktani','bhuktani_paaune','id');
    }

    public function getKarmachariInVoucherDetails(){

        return $this->hasMany('App\Models\VoucherDetail','peski_paune_id','id');
    }

    public function vatOffice(){

        return $this->belongsTo('App\Models\VatOffice','vat_office','id');

    }

    public function retentionVoucherDetails(){

        return $this->hasMany('App\Models\RetentionVoucherDetails','depositor','id');

    }
}
