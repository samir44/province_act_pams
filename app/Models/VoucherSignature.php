<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VoucherSignature extends Model
{
    protected $table = 'voucher_signature';
    protected $guarded = [];



    public function karmachari_prepare_by(){

        return $this->belongsTo('App\Models\Karmachari','prepare_by','id');
    }

    public function karmachari_submit_by(){

        return $this->belongsTo('App\Models\Karmachari','submit_by','id');
    }

    public function karmachari_approved_by(){

        return $this->belongsTo('App\Models\Karmachari','verify_by','id');
    }
}
