<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MainProgram extends Model
{
    protected $table = 'main_programs';
    protected $guarded = [];


    public function area(){
        return $this->belongsTo('App\Models\Area');
    }

    public function sub_area(){
        return $this->belongsTo('App\Models\SubArea');
    }
}
