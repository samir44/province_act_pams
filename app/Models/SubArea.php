<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubArea extends Model
{
    protected $table = 'sub_areas';
    protected $guarded = [];


    public function area(){
        return $this->belongsTo('App\Models\Area');
    }

    public function main_program(){
        return $this->hasMany('App\Models\MainProgram');
    }
}
