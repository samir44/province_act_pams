<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LedgerType extends Model
{
    protected $table = 'ledger_types';
    protected $guarded = [];

    public function voucherDetails(){

        return $this->belongsTo('App\Models\VoucherDetails');
    }
}
