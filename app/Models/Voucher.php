<?php

namespace App\Models;

use App\helpers\Money_words;
use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{
    public $IS_LAST = false;

    protected $table = 'vouchers';
    protected $guarded = [];

    protected $appends = array('IS_LAST');

    public function getISLASTAttribute()
    {
        return $this->IS_LAST;
    }

    public function preBhuktani(){

        return $this->hasMany('App\Models\PreBhuktani','journel_id','id');
    }

    public function budget_sub_head(){
        return $this->belongsTo('App\Models\Programs');
    }

    public function details(){

        return $this->hasMany('App\Models\VoucherDetail', 'journel_id', 'id');
    }

    public function program(){

        return $this->belongsTo('App\Models\Programs','budget_sub_head_id','id');
    }

    public function karmachari_prepare_by(){

        return $this->belongsTo('App\Models\Karmachari','prepared_by','id');
    }

    public function karmachari_submit_by(){

        return $this->belongsTo('App\Models\Karmachari','submitted_by','id');
    }

    public function karmachari_approved_by(){

        return $this->belongsTo('App\Models\Karmachari','approved_by','id');
    }

    public function office(){

        return $this->belongsTo('App\Models\Office','office_id','id');
    }



//    functions
    public function calculate_total_expense(){

        $details = $this->details;
//        dd($details);
        return $total_expense = $details->where('dr_or_cr',1)
                        ->whereIn('ledger_type_id',['1','4'])
                        ->sum('dr_amount');
//        $total_expense = 0;
//        foreach ($details as $detail){
//            if($detail->dr_or_cr == 1 && $detail->ledger_type_id == 1) {
//
//                $total_expense += $detail->dr_amount;
//
//            }
//        }
//        return $total_expense;

    }

    public function nikasa_check(){
//        dd($this);
        $details = $this->details;
        foreach ($details as $detail){
            if($detail->dr_or_cr == 1 && $detail->ledger_type_id == 3) {
                return $this->payement_amount;

            }
        }
    }
    public function check_payment(){
//        dd($this);
        $details = $this->details;
        foreach ($details as $detail){
            if($detail->dr_or_cr == 2 && $detail->ledger_type_id == 3) {
                return $this->payement_amount;

            }
        }
    }

    public  function peski(){

        $details = $this->details;
        $peski = 0;
        foreach ($details as $detail){
            if($detail->dr_or_cr == 1 && $detail->ledger_type_id == 4) {

                $peski += $detail->dr_amount;

            }
        }
        return $peski;

    }

    public function calculate_total_peski_given(){
        $details = $this->details;
        $peskiGiven = 0;
        foreach ($details as $detail){
            if($detail->dr_or_cr == 1 && $detail->ledger_type_id == 4) {

                $peskiGiven += $detail->dr_amount;

            }
        }
        return $peskiGiven;
    }
    public function calculate_total_cr_liabilities(){
        $details = $this->details;
        $CrLiabilities = 0;
        foreach ($details as $detail){
            if($detail->dr_or_cr == 2 && $detail->ledger_type_id == 2) {

                $CrLiabilities += $detail->cr_amount;

                }
        }
        return $CrLiabilities;
    }

    public function calculate_total_dr_liabilities(){
        $details = $this->details;
        $drLiabilities = 0;
        foreach ($details as $detail){
            if($detail->dr_or_cr == 1 && $detail->ledger_type_id == 2) {

                $drLiabilities += $detail->dr_amount;

            }
        }
        return $drLiabilities;

    }


    public function get_expense_heads(){

        $details = $this->details;
        $expense_heads = '';
        foreach ($details as $key=>$detail){
            if($detail->dr_or_cr == 1 && ($detail->ledger_type_id == 1 || $detail->ledger_type_id == 4)){
//                dd(strpos($expense_heads,(string)($detail->expense_head->expense_head_code)));
                if(!strpos($expense_heads,(string)($detail->expense_head->expense_head_code))){
                    if(strlen($expense_heads) > 1){
                        $expense_heads .= ',';
                    }
                    $expense_heads .= ' '.(string)($detail->expense_head->expense_head_code);
                }
            }
        }
        return $expense_heads;
    }

    public function isNikasa(){

       $nikasa =  $this->details()->where('dr_or_cr',2)->where('ledger_type_id',8);
       if($nikasa->count()> 0){
           return true;
       }
       return false;
    }

    public function isExpense(){

        $expense = $this->details()->where('dr_or_cr',1)->whereIn('ledger_type_id',['1','4']);
        if ($expense->count() > 0){

            return true;
        }
        return false;
    }

    public function thisTotalExp(){

       return $thisExp = $this->details()->where('dr_or_cr',1)->whereIn('ledger_type_id',['1','4'])->sum('dr_amount');
    }


    public function set_voucher_number(){

        $voucher_numbers ='';
        $voucher_numbers .= ''.(string)($this->jv_number);
        return $voucher_numbers;
    }
    public function get_unique(){


//        if(strpos(jv_number))
    }

//    VOucher Report

    public function get_total_dr(){

        $details = $this->details;
        $dr_amount = 0;
        return $dr_amount = $details->sum('dr_amount');

    }

    public function get_total_cr(){

        $details = $this->details;
        $cr_amount = 0;
        return $cr_amount = $details->sum('cr_amount');

    }

    public function get_amount_in_word(){

        $amount = $this->get_total_dr();
        $obj = new Money_words();
        return $words = $obj->convert_number($amount);

    }

    public function get_nikasa_by_budget_expense_head_id($expense_head_id){

        $details = $this->details;
        $cr_amount = $details->where('expense_head_id', $expense_head_id)->where('ledger_type_id', 8)->sum('cr_amount');
        return $cr_amount;
    }

    public function get_kharcha_by_budget_expense_head_id($expense_head_id){
        $details = $this->details;
        $dr_amount = $details->where('expense_head_id', $expense_head_id)->whereIn('ledger_type_id',['1','4'])->sum('dr_amount');
        return $dr_amount;
    }

    public function getPeskiClearance(){

        $details = $this->details;
        $peskiClearance = 0;
        foreach ($details as $detail){
            if($detail->dr_or_cr == 2 and $detail->ledger_type_id == 9){
                $peskiClearance += $detail->cr_amount;
            }
        }

        return $peskiClearance;
    }

}
