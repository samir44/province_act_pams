<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Karmachari extends Model
{
    protected $table = 'karmacharis';
    protected $guarded = [];


    public function get_pad(){

        return $this->belongsTo('App\Models\Designation','pad_id','id');
    }

    public function advancePayment()
    {
        return $this->belongsTo('App\Models\AdvanceAndPayment','id','karmachari_id');

    }
}
