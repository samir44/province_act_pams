<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    protected $table = 'area';
    protected $guarded = [];

    public function sub_areas(){
        return $this->hasMany('App\Models\SubArea');
    }


    public function budget(){
        return $this->belongsTo('App\Models\Budget');
    }

}
