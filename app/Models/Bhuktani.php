<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bhuktani extends Model
{
    protected $table = 'bhuktani';
    protected $guarded = [];


    public function preBhuktani(){

        return $this->hasMany('App\Models\PreBhuktani', 'bhuktani_id', 'id');
    }

    public function budget_sub_head(){

        return $this->belongsTo('App\Models\Programs');
    }
}
