<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RetentionBankGuarantee extends Model
{
    protected $table = 'retention_bank_guarantee';
    protected $guarded = [];

}
