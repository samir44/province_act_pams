<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Programs extends Model
{
    protected $table = 'programs';
    protected $guarded = [];

    public function voucher()
    {

        return $this->belongsTo('App\Models\Voucher');
    }

    public function bhuktani()
    {

        return $this->belongsTo('App\Models\Voucher');
    }

    public function office()
    {

        return $this->belongsTo('App\Models\Office');
    }


    public function akhtiyari()
    {

        return $this->belongsTo('App\Models\Akhtiyari', 'id', 'budget_sub_head_id');
    }

    public function getSourceType($office_id, $budgetSubHead)
    {


        return $resources = SourceType::join('budget', 'source_types.id', '=', 'budget.source_type')
            ->select(['source_types.id', 'source_types.name'])
            ->where('budget.office_id', $office_id)
            ->where('budget.budget_sub_head', $budgetSubHead)
            ->groupBy('budget.source_type')
            ->get();
//        dd($resources);
    }

    public function getExpenseHead($office_id, $budgetSubHead)
    {
       return $budgetByExpenseHead = Budget::
            where('office_id', $office_id)
                ->where('budget_sub_head', $budgetSubHead)
                ->groupBy('expense_head')
                ->orderBy('expense_head')
                ->get();
//        dd($budgetByExpenseHead->count());
    }


    public function getLastBudgetOfOfficeByBudgetSubHead($office_id, $budget_sub_head, $expense_head)
    {

        return $last_budget = DB::table('budget')
            ->where('office_id', $office_id)
            ->where('budget_sub_head', $budget_sub_head)
            ->where('expense_head_id', $expense_head)
            ->sum('total_budget');

    }

//    Office and Budget Sub Head

    public function getTotalFinalBudgetByOfficeAndBudgetSubHead($office_id, $budget_sub_head)
    {

        return $last_budget = DB::table('budget')
            ->where('office_id', $office_id)
            ->where('budget_sub_head', $budget_sub_head)
            ->sum('total_budget');

    }

    public function getTotalExpenseByOfficeAndBudgetSubHead($office_id, $budget_sub_head)
    {

        return $expense = DB::table('voucher_details')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.office_id', '=', $office_id)
            ->where('voucher_details.budget_sub_head_id', '=', $budget_sub_head)
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('dr_amount');
    }

    public function getTotalExpenseUpTpYesterdayByOfficeAndBudgetSubHead($office_id, $budget_sub_head)
    {

        $ldate = date('Y-m-d H:i:s');
        $yesterday = date('d.m.Y', strtotime("-1 days"));

        return $expense = DB::table('voucher_details')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.office_id', '=', $office_id)
            ->where('voucher_details.budget_sub_head_id', '=', $budget_sub_head)
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->where('voucher_details.date_english', '<=', $yesterday)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('dr_amount');
    }

    public function getTotalExpenseTodayByOfficeAndBudgetSubHead($office_id, $budget_sub_head)
    {

        $today = date('Y-m-d H:i:s');
        $yesterday = date('d.m.Y', strtotime("-1 days"));
        return $expense = DB::table('voucher_details')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.office_id', '=', $office_id)
            ->where('voucher_details.budget_sub_head_id', '=', $budget_sub_head)
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->where('voucher_details.date_english', '=', $today)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('dr_amount');
    }

    public function getTotalExpenseUpToTodayByOfficeAndBudgetSubHead($office_id, $budget_sub_head)
    {

        $today = date('Y-m-d H:i:s');
        $yesterday = date('d.m.Y', strtotime("-1 days"));
        return $expense = DB::table('voucher_details')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.office_id', '=', $office_id)
            ->where('voucher_details.budget_sub_head_id', '=', $budget_sub_head)
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->where('voucher_details.date_english', '<=', $today)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('dr_amount');
    }

    public function getTotalAdvanceByOfficeAndBudgetSubHead($office_id, $budget_sub_head)
    {

        $today = date('Y-m-d H:i:s');
        $yesterday = date('d.m.Y', strtotime("-1 days"));
        return $expense = DB::table('voucher_details')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.office_id', '=', $office_id)
            ->where('voucher_details.budget_sub_head_id', '=', $budget_sub_head)
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->where('voucher_details.ledger_type_id', 4)
            ->sum('dr_amount');
    }

    public function getTotalExpenseWithOutAdvanceByOfficeAndBudgetSubHead($office_id, $budget_sub_head)
    {

        $totalExpense = $this->getTotalExpenseUpToTodayByOfficeAndBudgetSubHead($office_id, $budget_sub_head);
        $totalAdvance = $this->getTotalAdvanceByOfficeAndBudgetSubHead($office_id, $budget_sub_head);
        return $totalExpenseWithOutAdvance = (float)$totalExpense - (float)$totalAdvance;
    }

    public function getRemainBudgetByOfficeAndBudgetSubHead($office_id, $budget_sub_head)
    {
        $totalBudget = $this->getTotalFinalBudgetByOfficeAndBudgetSubHead($office_id, $budget_sub_head);
        $totalExpense = $this->getTotalExpenseUpToTodayByOfficeAndBudgetSubHead($office_id, $budget_sub_head);
        return $remainBudget = (float)$totalBudget - (float)$totalExpense;
    }


//    Activities Report
    public function getActivities($budget_sub_head_id){

        return $activities = BudgetDetails::where('budget_sub_head_id',$budget_sub_head_id)->orderBy('expense_head_id')->get();
    }


    public function getTotalBudgetByBudgetSubHead($budget_Sub_head_id){


       return $budget = BudgetDetails::where('budget_sub_head_id',$budget_Sub_head_id)->sum('total_budget');
    }

    //    Fatbari All office report

    public function getExpenseHeadByOffice($ofice_id,$budget_sub_head_id){

        return $budgetByExpenseHead = Budget::where('office_id',$ofice_id)
            ->where('budget_sub_head',$budget_sub_head_id)
            ->groupBy('expense_head')
            ->orderBy('expense_head')
            ->get();
    }

    public function get_expense_this_month($budget_sub_head, $month_post)
    {
        return $expense = VoucherDetail::
            join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.fiscal_year', '=', $this->fiscal_year)
            ->where('voucher_details.office_id', '=', $this->office_id)
            ->where('voucher_details.budget_sub_head_id', '=', $budget_sub_head)
            ->where('voucher_details.month', '=', $month_post)
            ->where('voucher_details.expense_head_id', '=', $this->expense_head_id)
            ->where('voucher_details.dr_or_cr', '=', 1)
//            ->where('voucher_details.ledger_type_id','=',1)
            ->whereIn('voucher_details.ledger_type_id', [1, 4])
            ->sum('dr_amount');
    }



}
