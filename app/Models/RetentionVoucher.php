<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RetentionVoucher extends Model
{
    protected $table = 'retention_vouchers';
    protected $guarded = [];


    public function retention_voucher_details(){

        return $this->hasMany('App\Models\RetentionVoucherDetails','retention_voucher_id','id');
    }

    public function fiscalYear(){

        return $this->belongsTo('App\Models\FiscalYear','fiscal_year','id');
    }

}
