<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Source extends Model
{
    protected $table = 'sources';
    protected $guarded = [];

    public function sourceType(){
        return $this->belongsTo('App\Models\SourceType');
    }
}
