<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RetentionBhuktani extends Model
{
    protected $table = 'retention_bhuktani';
    protected $guarded = [];

    public function retentionPreBhuktani(){

        return $this->hasMany('App\Models\RetentionPreBhuktani','bhuktani_id','id');
    }

}
