<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Models\SourceType;
use App\Repositories\SourceTypeRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SourceTypeController extends Controller
{

    private $sourcetypeRepo;

    public function __construct(SourceTypeRepository $sourcetypeRepo)
    {
        $this->middleware('auth');
        $this->sourcetypeRepo = $sourcetypeRepo;
    }

    public function index()
    {
        $sourcetypes= $this->sourcetypeRepo->get_all_sourcetypes();
        return view('backend.settings.sourcetypes.index', compact('sourcetypes'));
    }

    public function create(){


        return view('backend.settings.sourcetypes.create');
    }
    public function store(Request $request){

        $sourcetype = $this->sourcetypeRepo->create($request->all());
        return redirect(route('admin.sourcetypes'));
    }

}
