<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Models\Ministry;
use App\Models\Mof;
use App\Models\Province;
use Illuminate\Http\Request;
use App\Repositories\HomeRepository;
use Illuminate\Support\Facades\Auth;

class MinistryController extends Controller
{

    private $homeRepo;

    public function __construct(HomeRepository $homeRepo)
    {

        $this->middleware('auth');
        $this->homeRepo = $homeRepo;
    }

    public function index()
    {
        $ministries = Ministry::all();
        return view('backend.settings.ministry.index', compact('ministries'));
    }

    public function create(){
        $provinces = Province::all();
        $mofs = Mof::all();
        return view('backend.settings.ministry.create', compact('provinces', 'mofs'));
    }
    public function store(Request $request){

        $ministry = new Ministry();
        $ministry->name = $request->name;
        $ministry->province_id = $request->province_id;
        $ministry->mof_id = $request->mof_id;
        $ministry->status = 1;
        $ministry->save();
        return redirect(route('admin.ministry'));
    }


}
