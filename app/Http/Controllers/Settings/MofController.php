<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Models\Mof;
use App\Models\Province;
use Illuminate\Http\Request;
use App\Repositories\HomeRepository;
use Illuminate\Support\Facades\Auth;

class MofController extends Controller
{

    private $homeRepo;

    public function __construct(HomeRepository $homeRepo)
    {

        $this->middleware('auth');
        $this->homeRepo = $homeRepo;
    }

    public function index()
    {
        $mofs = Mof::all();
        return view('backend.settings.mof.index', compact('mofs'));
    }

    public function create(){
        $provinces = Province::all();
        return view('backend.settings.mof.create', compact('provinces'));
    }
    public function store(Request $request){

        $mof = new Mof();
        $mof->name = $request->name;
        $mof->province_id = $request->province_id;
        $mof->status = 1;
        $mof->save();
        return redirect(route('admin.mof'));
    }

}
