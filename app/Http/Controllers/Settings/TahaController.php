<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Models\Sewa;
use App\Models\Taha;
use App\Models\Samuha;
use App\Repositories\SamuhaRepositoryEloquent;
use App\Repositories\TahaRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TahaController extends Controller
{

    private $tahaRepo;
    private $samuha;

    public function __construct(TahaRepository $tahaRepo, SamuhaRepositoryEloquent $samuha)
    {
        $this->middleware('auth');
        $this->tahaRepo = $tahaRepo;
        $this->samuha = $samuha;
    }

    public function index()
    {
        $tahas= $this->tahaRepo->get_all_tahas();

        return view('backend.settings.tahas.index', compact('tahas'));
    }

    public function create(){

        $sewas = Sewa::all();

        return view('backend.settings.tahas.create',compact('sewas'));
    }
    public function store(Request $request){

        $taha = $this->tahaRepo->create($request->all());
        return redirect(route('admin.tahas'));
    }

    public function get_taha_by_sewa($sewa_id){

        $tahas = $this->tahaRepo->get_by_sewa_id($sewa_id);

        return json_encode($tahas);
    }

}
