<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Models\Area;
use App\Repositories\AreaRepository;
use Illuminate\Http\Request;
use App\Repositories\HomeRepository;
use Illuminate\Support\Facades\Auth;

class AreaController extends Controller
{

    private $areaRepo;

    public function __construct(AreaRepository $areaRepo)
    {
        $this->middleware('auth');
        $this->areaRepo = $areaRepo;
    }

    public function index()
    {
        $areas= Area::all();
        return view('backend.settings.area.index', compact('areas'));
    }

    public function create(){

        return view('backend.settings.area.create');
    }
    public function store(Request $request){

        $program = $this->areaRepo->create($request->all());
        return redirect(route('admin.area'));
    }

}
