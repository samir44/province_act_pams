<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Models\Source;
use App\Models\SourceType;
use App\Repositories\SourceRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SourceController extends Controller
{

    private $sourceRepo;

    public function __construct(SourceRepository $sourceRepo)
    {
        $this->middleware('auth');
        $this->sourceRepo = $sourceRepo;
    }

    public function index()
    {
        $sources= Source::all();
        return view('backend.settings.source.index', compact('sources'));
    }

    public function create(){

        $source_prakars = SourceType::all();
        return view('backend.settings.source.create',compact('source_prakars'));
    }
    public function store(Request $request){

        $source = $this->sourceRepo->create($request->all());
        return redirect(route('admin.source'));
    }

    public function get_source_by_source_type(Request $request, $source_type_id){

        $sources = $this->sourceRepo->get_by_source_type($source_type_id);
        return json_encode($sources);

    }

}
