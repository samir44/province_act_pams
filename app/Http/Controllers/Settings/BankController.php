<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Models\Bank;
use App\Repositories\BankRepository;
use Illuminate\Http\Request;
use App\Repositories\HomeRepository;
use Illuminate\Support\Facades\Auth;

class BankController extends Controller
{

    private $bankRepo;

    public function __construct(BankRepository $bankRepo)
    {

        $this->middleware('auth');
        $this->bankRepo = $bankRepo;
    }

    public function index()
    {
        $banks= Bank::all();
        return view('frontend.bank.index', compact('banks'));
    }

    public function create(){

        return view('backend.settings.bank.create');
    }
    public function store(Request $request){

        $bank = $this->bankRepo->create($request->all());
        return redirect(route('admin.bank'));
    }




}
