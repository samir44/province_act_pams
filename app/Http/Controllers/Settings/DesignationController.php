<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Models\Designation;
use App\Models\Samuha;
use App\Models\Taha;
use App\Repositories\DesignationRepository;
use App\Repositories\TahaRepositoryEloquent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DesignationController extends Controller
{

    private $designationRepo;
    private $tahaRepo;

    public function __construct(DesignationRepository $designationRepo, TahaRepositoryEloquent $tahaRepo)
    {
        $this->middleware('auth');
        $this->designationRepo = $designationRepo;
        $this->tahaRepo = $tahaRepo;
    }

    public function index()
    {
        $designations= $this->designationRepo->get_all_designations();
        return view('backend.settings.designations.index', compact('designations'));
    }

    public function create(){
        $samuhas = Samuha::all();
        $tahas = Taha::all();
        return view('backend.settings.designations.create',compact('tahas'));
    }
    public function store(Request $request){

        $designation = $this->designationRepo->create($request->all());
        return redirect(route('admin.designations'));
    }


//    यो Function मा धेरै लोचा छ। आएको value sreni/taha id हो but table मा समुह id छ। Just for move on. पछि हेर्ने गरि
    public function get_pad_by_sreni($samuha_id){

        $sreni = Designation::where('samuha_id',$samuha_id)->pluck("name","id");
        return json_encode($sreni);
    }

}
