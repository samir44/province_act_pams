<?php

namespace App\Http\Controllers\Frontend;
use App\helpers\FiscalYearHelper;
use App\Http\Controllers\Controller;
use App\Models\Akhtiyari;
use App\Models\Area;
use App\Models\Department;
use App\Models\Budget;
use App\Models\ExpenseHead;
use App\Models\MainProgram;
use App\Models\Medium;
use App\Models\Ministry;
use App\Models\Mof;
use App\Models\Office;
use App\Models\Province;
use App\Models\Programs;
use App\Models\SourceType;
use App\Models\SubArea;
use App\Models\Unit;
use App\Repositories\AreaRepository;
use App\Repositories\ExpenseHeadRepository;
use App\Repositories\MainProgramRepository;
use App\Repositories\ProgramRepository;
use App\Repositories\SourceTypeRepository;
use App\Repositories\SubAreaRepository;
use Illuminate\Http\Request;
use App\Repositories\BudgetRepository;
use Illuminate\Support\Facades\Auth;

class BudgetController extends Controller
{

    private $programRepo;
    private $budgetRepo;
    private $areaRepo;
    private $subAreaRepo;
    private $expenseHeadRepo;
    private $mainProgramRepo;
    private $sourceTypeRepo;
    private $fiscalYearHelper;

    public function __construct(

        ProgramRepository $programRepo,
        BudgetRepository $budgetRepo,
        AreaRepository $areaRepo,
        SubAreaRepository $subAreaRepo,
        MainProgramRepository $mainProgramRepo,
        SourceTypeRepository $sourceTypeRepo,
        ExpenseHeadRepository $expenseHeadRepo
    )
    {

        $this->middleware('auth');
        $this->budgetRepo = $budgetRepo;
        $this->programRepo = $programRepo;
        $this->areaRepo = $areaRepo;
        $this->subAreaRepo = $subAreaRepo;
        $this->mainProgramRepo= $mainProgramRepo;
//        $this->$sourceTypeRepo = $sourceTypeRepo;
        $this->mainProgramRepo= $mainProgramRepo;
        $this->expenseHeadRepo = $expenseHeadRepo;
        $this->fiscalYearHelper = new FiscalYearHelper();

    }

    public function create(Request $request)
    {

        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $sources = SourceType::all();
        $programs = $this->programRepo->get_programs_for_budget();

        $areas = $this->areaRepo->getAllArea();
        $expenseHeads = $this->expenseHeadRepo->get_expense_head_by_ledger_type();
        $mediums = Medium::all();
        $request->session()->put('budget_sub_head', '');
//        $request->session()->put('success', 'Created Successfully');

        return view('frontend.budget.create', compact('programs','areas','expenseHeads','sources','mediums', 'fiscalYear'));
    }

    public function store(Request $request)
    {
        $datas= $request->all();
        $expense_head = $this->expenseHeadRepo->get_by_id($datas['expense_head']);
        $datas['expense_head_code'] = $expense_head['expense_head_code'];
        $request->session()->put('budget_sub_head', $datas['budget_sub_head']);
        $this->budgetRepo->create($datas);
        return redirect(route('budget.create.continue'));
    }


    public function edit(){

        session()->forget('budget_sub_head');
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $sources = SourceType::all();
        $programs = $this->programRepo->get_by_office_id();
        $areas = $this->areaRepo->getAllArea();
        $expenseHeads = $this->expenseHeadRepo->get_expense_head_by_ledger_type();
        $mediums = Medium::all();
        $units = Unit::all();


//        $request->session()->put('budget_sub_head', '');
//        $request->session()->put('success', 'Created Successfully');

        return view('frontend.budget.edit', compact('programs','areas','expenseHeads','sources','mediums', 'fiscalYear','units'));
    }

    public function create_continue(Request $request)
    {
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $sources = SourceType::all();
        $programs = $this->programRepo->get_by_office_id();
        $areas = $this->areaRepo->getAllArea();
        $expenseHeads = $this->expenseHeadRepo->get_expense_head_by_ledger_type();
        $mediums = Medium::all();
        $request->session()->remove('success');

        return view('frontend.budget.create', compact('programs','areas','expenseHeads','sources','mediums', 'fiscalYear'));
    }



    public function get_main_activity_by_budget_sub_head(Request $request,$budget_sub_head){
        $office_id = Auth::user()->office->id;
        $budgets = $this->budgetRepo->get_by_budget_sub_head_and_office_id($office_id, $budget_sub_head);
//        dd($budgets);
        return json_encode($budgets);
    }

    public function get_expense_head_by_activity_id(Request $request, $activity_id){

        $budget = $this->budgetRepo->get_by_id($activity_id);
//       $data =  $expenseHeads = $this->expenseHeadRepo->get_by_expense_head_code($budget->expense_head);

        return json_encode($budget);
    }


//    budget create greda
    public function get_main_activity_by_akhtiyari(Request $request, $akhtiyari_id){

         $main_activities = $this->budgetRepo->get_by_akhtiyari_id($akhtiyari_id);
//         dd($main_activities);
         return json_encode($main_activities);

    }


    public function get_total_akhtiyari_by_budget_sub_head($budget_sub_head){

        $totalAkhtiyari = $this->budgetRepo->get_total_akhtiyari_by_budget_sub_head($budget_sub_head);
        return json_encode($totalAkhtiyari);

    }

    public function get_total_budget_by_akhtiyari(Request $request, $akhtiyari_id){

        $totalBudget = $this->budgetRepo->get_total_budget_by_akhtiyari($akhtiyari_id);
        return json_encode($totalBudget);
    }

    public function akhtiyariIndex(){

        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $sources = SourceType::all();
        $programs = $this->programRepo->get_by_office_id();
        $expenseHeads = $this->expenseHeadRepo->get_expense_head_by_ledger_type();
        $mediums = Medium::all();

        $office_id = Auth::user()->office_id;
        $budget_sub_heads = $this->programRepo->get_by_office_id($office_id);
        return view('frontend.budget.akhtiyari',compact('fiscalYear','programs','expenseHeads','mediums','sources'));
    }

    public function akhtiyariStore(Request $request){

//        dd($request->all());
        $akhtiYari = $request->all();
        $akhtiyar = $this->budgetRepo->createAkhtiyari($akhtiYari);
        return redirect(route('akhtiyari.param'));
    }


    public function akhtiyariAddParam($id){


        $budget_sub_head_id = $id;
        $bdget_sub_heads = $this->programRepo->get_by_program_id($budget_sub_head_id);
        return view('frontend.budget.akhtiyariAddParam',compact('bdget_sub_heads'));
    }

    public function getAkhtiyariByBudgetSubHeadId(Request $request,$budget_sub_head_id){

        $akhtiyari = $this->budgetRepo->getAkhtiyariByBudgetSubHeadId($budget_sub_head_id);
//        dd($akhtiyari[0]->budget->count());
        return json_encode($akhtiyari);
    }

    public function editAkhtiyari($akhtiyari_id){

        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $sources = SourceType::all();
        $programs = $this->programRepo->get_by_office_id();
        $expenseHeads = $this->expenseHeadRepo->get_expense_head_by_ledger_type();
        $mediums = Medium::all();

        $office_id = Auth::user()->office_id;
        $budget_sub_heads = $this->programRepo->get_by_office_id($office_id);

        $akhtiyary = $this->budgetRepo->get_akhtiyari_by_akhtiyari_id($akhtiyari_id);
       $totalBudgetOnAkhtiyar =  ($akhtiyary->budget->sum('total_budget'));
//        foreach ($akhtiyary->budget as $)
        return view('frontend.budget.akhtiyari_edit',compact('fiscalYear','programs','expenseHeads','mediums','sources','akhtiyary','totalBudgetOnAkhtiyar'));

    }

    public function get_budget_details_by_id($activity_id){

        $budgetDetail = $this->budgetRepo->get_budget_details_by_id($activity_id);
        return json_encode($budgetDetail);



//        $budgetDetailArray = [];
//        $budgetDetailArray['details'] = $this->budgetRepo->get_budget_details_by_id($activity_id);
////        dd($budgetDetailArray['details']->getVoucherDetails[0]->dr_or_cr);
//        $budgetDetailArray['expense'] = 0;
//        foreach ($budgetDetailArray['details']->getVoucherDetails as $voucherDetails){
//
//            if($voucherDetails->dr_or_cr == 1 and $voucherDetails->ledger_type_id){
//
//                $budgetDetailArray['expense'] = $budgetDetailArray['expense'] + $voucherDetails->dr_amount;
//            }
//        }
//        dd($budgetDetailArray['expense']);
    }

    public function update(Request $request, $id){

//        dd($request->all());
        $datas = $request->all();
        $expense_head = $this->expenseHeadRepo->get_by_id($datas['expense_head']);
        $datas['expense_head_code'] = $expense_head['expense_head_code'];
        $budget_create = $this->budgetRepo->update($datas, $id);
        return redirect(route('budget.edit'));
    }


    public function akhtiyariUpdate(Request $request, $id){

//        dd($request->all());
        $updateAKhtiyari = $this->budgetRepo->updateAkhtiyari($request->all(), $id);
        return redirect(route('akhtiyari.param'));

    }

    public function akhtiyariDelete($activity_id){

        $deleteAkhtiyari = $this->budgetRepo->deleteAkhtiyariById($activity_id);
        return json_encode($deleteAkhtiyari);


    }

    public function delete_activity($id){

//        dd("test here");
        $delete_activity = $this->budgetRepo->delete_activity_by_id($id);
        return json_encode($delete_activity);
    }
}
