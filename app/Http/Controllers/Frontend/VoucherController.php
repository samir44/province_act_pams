<?php

namespace App\Http\Controllers\Frontend;

use App\helpers\FiscalYearHelper;
use App\Http\Controllers\Controller;
use App\Models\AllBank;
use App\Models\Area;
use App\Models\Bank;
use App\Models\DarbandiSrot;
use App\Models\DarbandiType;
use App\Models\Department;
use App\Models\Budget;
use App\Models\Designation;
use App\Models\ExpenseHead;
use App\Models\MainProgram;
use App\Models\Medium;
use App\Models\Ministry;
use App\Models\Mof;
use App\Models\Month;
use App\Models\Office;
use App\Models\PartyTypes;
use App\Models\PreBhuktani;
use App\Models\Province;
use App\Models\Programs;
use App\Models\Samuha;
use App\Models\Sewa;
use App\Models\Source;
use App\Models\SourceType;
use App\Models\SubArea;
use App\Models\Taha;
use App\Models\VatOffice;
use App\Models\Voucher;
use App\Models\LedgerType;
use App\Models\VoucherDetail;
use App\Repositories\AreaRepository;
use App\Repositories\MainProgramRepository;
use App\Repositories\PreBhuktaniRepository;
use App\Repositories\BhuktaniRepository;
use App\Repositories\ProgramRepository;
use App\Repositories\SourceTypeRepository;
use App\Repositories\SubAreaRepository;
use App\Repositories\VoucherDetailsRepository;
use App\Repositories\KarmachariRepository;
use App\Repositories\VoucherRepository;
use Illuminate\Http\Request;
use App\Repositories\BudgetRepository;
use App\Repositories\VoucherSignatureRepository;
use Illuminate\Support\Facades\Auth;
use App\Repositories\ExpenseHeadRepository;


class VoucherController extends Controller
{

    private $programRepo;
    private $budgetRepo;
    private $areaRepo;
    private $subAreaRepo;
    private $mainProgramRepo;
    private $sourceTypeRepo;
    private $voucherSignatureRepo;
    private $bhuktaniRepo;
    private $expenseHeadRepo;


    public function __construct(

        ProgramRepository $programRepo,
        BudgetRepository $budgetRepo,
        AreaRepository $areaRepo,
        SubAreaRepository $subAreaRepo,
        MainProgramRepository $mainProgramRepo,
        SourceTypeRepository $sourceTypeRepo,
        VoucherRepository $voucherRepo,
        VoucherDetailsRepository $voucherDetailsRepo,
        PreBhuktaniRepository $preBhuktaniRepo,
        KarmachariRepository $karmachariRepo,
        VoucherSignatureRepository $voucherSignatureRepo,
        BhuktaniRepository $bhuktaniRepo,
        ExpenseHeadRepository $expenseHeadRepo

    )
    {

        $this->middleware('auth');

        $this->voucherRepo = $voucherRepo;
        $this->voucherDetailsRepo = $voucherDetailsRepo;
        $this->preBhuktaniRepo = $preBhuktaniRepo;

        $this->budgetRepo = $budgetRepo;
        $this->programRepo = $programRepo;
        $this->areaRepo = $areaRepo;
        $this->subAreaRepo = $subAreaRepo;
        $this->mainProgramRepo = $mainProgramRepo;
        $this->karmachariRepo = $karmachariRepo;
//        $this->$sourceTypeRepo = $sourceTypeRepo;
        $this->mainProgramRepo = $mainProgramRepo;
        $this->voucherSignatureRepo = $voucherSignatureRepo;
        $this->bhuktaniRepo = $bhuktaniRepo;
        $this->fiscalYearHelper = new FiscalYearHelper();


    }

    public function create()
    {

        $provinces = Province::all();
        $offices = Office::all();
        $darbandisrots = DarbandiSrot::all();
        $darbanditypes = DarbandiType::all();
        $sewas = Sewa::all();
        $samuhas = Samuha::all();
        $pads = Designation::all();
        $tahas = Taha::all();

        $sources = SourceType::all();
        $ledgerTypes = LedgerType::where('status',1)->get();
        $programs = $this->programRepo->get_program_for_voucher();
        $expenseHeads = ExpenseHead::all();
        $mediums = Medium::all();

//        for advance payment modal
        $office_id = Auth::user()->office_id;
//        $banks = Bank::where('office_id', $office_id)->get();
        $party_types = PartyTypes::all();
        $all_banks = AllBank::all();
        $vat_offices = VatOffice::all();

        return view('frontend.voucher.create', compact('programs', 'expenseHeads', 'sources', 'mediums', 'ledgerTypes', 'all_banks', 'office_id', 'party_types', 'vat_offices', 'provinces', 'offices', 'darbandisrots', 'darbanditypes', 'sewas', 'samuhas', 'pads', 'tahas'));
    }

    public function store(Request $request)
    {
        $datas = $request->all();
//        dd($datas);
//      Voucher Save
        $office_id = Auth::user()->office_id;
        $voucher_signature = $this->voucherSignatureRepo->get_by_office_id($office_id);
        $id = $this->voucherRepo->create($request['voucher'], $voucher_signature);

//        VOucher Details Save
        foreach ($datas['voucherDetails'] as $voucherDetail) {

            $voucherDetail['voucher_id'] = $id;
            $this->voucherDetailsRepo->create($voucherDetail);

        }
//        Pre Bhuktani save
        if (array_key_exists('preBhuktani', $datas) and sizeof($datas['preBhuktani']) > 0) {

            foreach ($datas['preBhuktani'] as $preBhuktani) {

                $preBhuktani['voucher_id'] = $id;
                $budget = $this->budgetRepo->get_by_id_for_pre_bhuktani($preBhuktani['main_activity_id']);
                $preBhuktani['expense_head_id']= $budget['expense_head_id'];
                $this->preBhuktaniRepo->create($preBhuktani);
            }

        }
        $request->session()->put('success', 'Created Successfully');
        return response()->json(['success' => 'Successfully Created']);
    }

    public function get_voucher_by_budget_sub_head_and_office(Request $request, $budget_sub_head_id)
    {


        $office_id = Auth::user()->office->id;
        $voucherNumber = $this->voucherRepo->get_vouhcer_number_by_budget_sub_head_and_office_id($office_id, $budget_sub_head_id);
        return json_encode($voucherNumber);
    }

    public function voucher_accept_create()
    {

        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $office_id = Auth::user()->office->id;
        $programs = $this->programRepo->get_program_for_report();  // office repo मै तानेको  छ।

        return view('frontend.voucher.getList', compact('programs',
            'fiscalYear'));

    }

    public function get_voucher_list_by_status(Request $request, $budget_sub_head)
    {
        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $year = ($fiscalYear['year']);
        $office_id = Auth::user()->office->id;

        $voucherLists = $this->voucherRepo->getVoucherByStatusAndBudgetSubHead($budget_sub_head, $office_id, $year);
//        dd($voucherLists);
        return json_encode($voucherLists);
    }

    public function set_approved(Request $request, $voucher_id)
    {

        $voucher_unapproved = $this->voucherRepo->set_voucher_approved($voucher_id);
//       dd($voucher_unapproved);
        $pre_bhuktanies = $this->preBhuktaniRepo->get_preBhuktani_by_voucher_id($voucher_id);
        if($pre_bhuktanies->count() > 0){

            if ($pre_bhuktanies[0]->bhuktani != null) {
                if ($pre_bhuktanies->count() > 0) {
                    foreach ($pre_bhuktanies as $pre_bhuktani) {

                        $update_pre_bhuktani = $this->preBhuktaniRepo->update_status_one_by_id($pre_bhuktani['id']);
                    }

                    $pre_bhuktani_amount = $pre_bhuktanies->sum('amount');
                    $bhuktani_id = $pre_bhuktanies[0]->bhuktani_id;
                    $update_hunktani = $this->bhuktaniRepo->update_by_bhuktani_add_amount_and_status($bhuktani_id, $pre_bhuktani_amount);

                }
            }
        }

        return json_encode($voucher_unapproved);
    }

    public function voucher_edit_parameter()
    {

        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $office_id = Auth::user()->office->id;
        $programs = $this->programRepo->get_program_for_voucher();  // office, repo मै तानेको  छ।
//        dd($programs);
        $all_banks = AllBank::all();
        return view('frontend.voucher.editList', compact('programs', 'fiscalYear', 'all_banks'));

    }

    public function update(Request $request)
    {

        $dataList = $request->all();
//        dd($dataList);
        $voucher_id = $dataList['voucher_id'];
        $voucher = $dataList['voucher'];
        $voucherDetails = $dataList['voucherDetails'];
        $office_id = Auth::user()->office_id;
        $voucher_signature = $this->voucherSignatureRepo->get_by_office_id($office_id);
        if (array_key_exists('voucher', $dataList)) {

            $voucher_save = $this->voucherRepo->updateVoucher($voucher, $voucher_signature);
        }

        if (array_key_exists('voucherDetails', $dataList)) {

            $new_voucher_details = $this->voucherDetailsRepo->updateVoucherDetails($voucher_id, $voucherDetails);
        }

        if (array_key_exists('preBhuktani', $dataList)) {

            $preBhuktani = $dataList['preBhuktani'];
            $voucher = Voucher::findorfail($voucher_id);
            $voucher->preBhuktani()->delete();
            foreach ($preBhuktani as $pre){

                $budget = $this->budgetRepo->get_by_id_for_pre_bhuktani($pre['main_activity_id']);
                $pre['expense_head_id']= $budget['expense_head_id'];
                $new_pre_bhuktani = $this->preBhuktaniRepo->updatePreBhuktani($voucher_id, $pre);
            }



        } else {

            $delete_pre_bhuktani = $this->preBhuktaniRepo->delete_by_vouhcer_id($voucher_id);
        }
        return json_encode("success");

    }

    public function get_budget_and_expe_by_activity($activity_id)
    {

        $data = [];
        $data['budget'] = $this->budgetRepo->get_total_budget_by_Activity_id($activity_id);
        $data['expense'] = $this->voucherDetailsRepo->get_expense_by_activity_id($activity_id);
        $data['advance'] = $this->voucherDetailsRepo->get_advance_by_activity_id($activity_id);
        $data['remain'] = $data['budget'] - $data['advance'] - $data['expense'];
        return json_encode($data);
    }

    public function delete($id)
    {

        $voucher_delete = $this->voucherRepo->delete($id);
        return json_encode($voucher_delete);
    }

    public function getVoucherSignature()
    {

        $karmacharies = $this->karmachariRepo->getAllKarmachariByApproveStatus();
        $office_id = Auth::user()->office_id;
        return view('frontend.voucher.voucher_signature', compact('karmacharies', 'office_id'));

    }

    public function voucherSignatureCreate(Request $request)
    {


        $voucher_signature = $this->voucherSignatureRepo->create($request->all());
        return redirect()->back();
    }

    public function voucherSignatureList(Request $request, $office_id)
    {

        $voucher_signature = $this->voucherSignatureRepo->get_by_office_id($office_id);
//dd($voucher_signature);
        return json_encode($voucher_signature);
    }

    public function voucherUnApprovedParam()
    {

        $office_id = Auth::user()->office_id;
        $programs = $this->programRepo->get_program_for_voucher();
        return view('frontend.voucher.voucher_unapprove', compact('programs', 'office_id'));

    }

    public function voucherUnApprove(Request $request)
    {

        $office_id = Auth::user()->office_id;
        $programs = $this->programRepo->get_program_for_voucher();

        $data = $request->all();
        $voucher_id = $this->voucherRepo->change_status_by_budget_sub_head_and_office_id($data);
//        dd($voucher_id);
        if ($voucher_id) {
            $pre_bhuktanies = $this->preBhuktaniRepo->get_preBhuktani_by_voucher_id($voucher_id);

            if ($pre_bhuktanies->count() > 0) {
//                dd("test");
                if ($pre_bhuktanies[0]->bhuktani != null) {

                    foreach ($pre_bhuktanies as $pre_bhuktani) {
                        $update_pre_bhuktani = $this->preBhuktaniRepo->update_status_by_id($pre_bhuktani['id']);
                    }

                    $pre_bhuktani_amount = $pre_bhuktanies->sum('amount');
                    $bhuktani_id = $pre_bhuktanies[0]->bhuktani_id;
                    $update_hunktani = $this->bhuktaniRepo->update_by_bhuktani_amount_and_status($bhuktani_id, $pre_bhuktani_amount);

                }
            }
            return redirect()->back()->with('success', 'Voucher Approved successfully');
        }


        return redirect()->back()->with('error', 'canot find voucher');

    }

    public function grantVoucherParam()
    {

        $office_id = Auth::user()->office_id;
        $programs = $this->programRepo->get_program_for_voucher();
        return view('frontend.voucher.grant_voucher_param', compact('programs', 'office_id'));

    }

    public function grantVoucher($budget_sub_head_id)
    {

        $office_id = Auth::user()->office_id;
        $programs = $this->programRepo->getById($budget_sub_head_id);
        $bhuktanies = $this->bhuktaniRepo->get_by_budget_sub_head_id($budget_sub_head_id);
        return view('frontend.voucher.grant_voucher_create', compact('programs', 'office_id', 'bhuktanies'));

    }

    public function grantVoucherStore(Request $request)
    {
        $data = $request->all();
        $office_id = Auth::user()->office_id;
        $data['date_english'] = $data['bs-roman'];
//        $data['amount'] = $data['totalAmount'] = $this->bhuktaniRepo->get_amount_by_multiple_id($request['bhuktani']);
        $myPrebhuktanis = PreBhuktani::whereIn('bhuktani_id', $data['bhuktani'])
                                        ->where('office_id', $office_id)
                                        ->groupBy('main_activity_id')
                                        ->selectRaw('*,sum(amount) as sum')
                                        ->pluck('sum', 'main_activity_id');
        $encodedBhuktaniId = json_encode($data['bhuktani']);
        $data['amount'] =$data['totalAmount'] =array_sum($myPrebhuktanis->toArray());
        $data['voucher_number'] = $this->voucherRepo->get_vouhcer_number_by_budget_sub_head_and_office_id($office_id, $data['budget_sub_head']);
        $voucher_signature = $this->voucherSignatureRepo->get_by_office_id($office_id);
        $voucher_id = $this->voucherRepo->createNikasaVoucher($data, $voucher_signature);

        $data['drOrCr'] = 1;
        $data['ledger_type_id'] = 3;
        $this->voucherDetailsRepo->createNikasaVoucherDetails($data, $voucher_id);  // Debit wala
        $this->voucherDetailsRepo->createBhuktaniVoucher($data, $voucher_id,$encodedBhuktaniId);  // Debit wala
        foreach ($myPrebhuktanis as $key => $value) {

            $data['activity_id'] = $key;
            $data['amount'] = $value;
            $data['drOrCr'] = 2;
            $data['ledger_type_id'] = 8;
            $budget = $this->budgetRepo->get_by_id($key);
            $this->voucherDetailsRepo->createNikasaVoucherDetails($data, $voucher_id, $budget);  // Credit
        }

        foreach ($data['bhuktani'] as $key => $value){

            $updateBhuktani = $this->bhuktaniRepo->updateIsContenjencyById($value);
        }

        return redirect()->back();
    }


}
