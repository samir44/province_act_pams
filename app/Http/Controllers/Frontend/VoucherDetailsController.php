<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Controller;
use App\Models\AllBank;
use App\Models\Bank;
use App\Models\DarbandiSrot;
use App\Models\DarbandiType;
use App\Models\Designation;
use App\Models\ExpenseHead;
use App\Models\LedgerType;
use App\Models\Medium;
use App\Models\Office;
use App\Models\PartyTypes;
use App\Models\Province;
use App\Models\Samuha;
use App\Models\Sewa;
use App\Models\SourceType;
use App\Models\Taha;
use App\Models\VatOffice;
use App\Models\Voucher;
use App\Models\VoucherDetail;
use App\Repositories\PreBhuktaniRepository;
use App\Repositories\ProgramRepository;
use App\Repositories\VoucherDetailsRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VoucherDetailsController extends Controller
{


    private $voucherDetailsRepo;
    private $programRepo;
    private $preBuktaniRrepo;
    public function __construct(VoucherDetailsRepository $voucherDetailsRepo, ProgramRepository $programRepo, PreBhuktaniRepository $preBuktaniRrepo)
    {
        $this->middleware('auth');

        $this->voucherDetailsRepo = $voucherDetailsRepo;
        $this->programRepo = $programRepo;
        $this->preBuktaniRrepo= $preBuktaniRrepo;

    }


    public function get_expense_head_in_voucher_details_by_budget_sub_head_id(Request $request){

        $budget_sub_head_id = $request['budget_sub_head_id'];
        $ledger_type_id = $request['ledger_type_id'];
        $expenseHeadList = $this->voucherDetailsRepo->get_expense_head_by_budget_sub_head_and_office_id($budget_sub_head_id, $ledger_type_id);
        return json_encode($expenseHeadList);
    }


    //voucher
    public function get_voucher_details_by_voucher_id(Request $request, $voucher_id){


        $provinces = Province::all();
        $offices = Office::all();
        $darbandisrots = DarbandiSrot::all();
        $darbanditypes = DarbandiType::all();
        $sewas = Sewa::all();
        $samuhas = Samuha::all();
        $pads = Designation::all();
        $tahas = Taha::all();

        $sources = SourceType::all();
        $ledgerTypes = LedgerType::all();
        $programs = $this->programRepo->get_program_for_voucher();
        $expenseHeads = ExpenseHead::all();
        $mediums = Medium::all();

//        for advance payment modal
        $office_id = Auth::user()->office_id;
//        $banks = Bank::where('office_id', $office_id)->get();
        $party_types = PartyTypes::all();
        $all_banks = AllBank::all();
        $vat_offices = VatOffice::all();




        $sources = SourceType::all();
        $ledgerTypes = LedgerType::all();
        $programs = $this->programRepo->get_by_office_id();
        $expenseHeads = ExpenseHead::all();
        $mediums = Medium::all();
        $preBhuktanies = $this->preBuktaniRrepo->get_preBhuktani_by_voucher_id($voucher_id);
//        dd($preBhuktanies[0]->all_bank);
        $voucherDetailsLIst = $this->voucherDetailsRepo->get_details_by_voucher_id($voucher_id);
//        dd($voucherDetailsLIst);
        $voucher = Voucher::findorfail($voucher_id);
//        dd($voucher);
        $all_banks = AllBank::all();


        return view('frontend.voucher.edit', compact('programs','expenseHeads','sources','mediums','ledgerTypes','voucherDetailsLIst','voucher','preBhuktanies','all_banks','all_banks', 'office_id', 'party_types', 'vat_offices', 'provinces', 'offices', 'darbandisrots', 'darbanditypes', 'sewas', 'samuhas', 'pads', 'tahas'));


    }

    public function get_expense_by_activity($activity_id){

        $total_expense = $this->voucherDetailsRepo->get_expense_by_activity_id($activity_id);
        return json_encode($total_expense);
    }

    public function getLiabilityByLiabilityHead(Request $request,$budgetSubHead,$expenseHeadId){


        $CuttingLiability = $this->voucherDetailsRepo->getCuttingLiability($budgetSubHead,$expenseHeadId);
        $depositLiability = $this->voucherDetailsRepo->getdepositLiability($budgetSubHead,$expenseHeadId);
        $remainLiability = floatval($CuttingLiability) - floatval($depositLiability);
//        dd($CuttingLiability);
        return json_encode($remainLiability);
    }


    public function voucherDetailsDeleteById($voucher_detail_id){
//        dd($voucher_detail_id);
        $voucher_detail = $this->voucherDetailsRepo->find($voucher_detail_id);
//        dd($voucher_detail);
        if($voucher_detail){

            $delete_voucher_details = $this->voucherDetailsRepo->delete_by_id($voucher_detail_id);

        }
//        if($voucher_detail){
////                dd("test one");
////        }else{
////                dd("test");
////            $voucher =  $voucher_detail ? $voucher_detail->voucher : null;
////            $pre_bhuktanies = $voucher ? $voucher->preBhuktani : null;
//
////            dd($voucher_detail->voucher->preBhuktani);
//            $delete_voucher_details = $this->voucherDetailsRepo->delete_by_id($voucher_detail_id);
//        }

        return json_encode($delete_voucher_details);
    }

    public function getRemainPeski($activity_id,$party_id,$budget_sub_head_id){

        $voucherDetails = $this->voucherDetailsRepo->getVouchersByBudgetSubHeadAndParty($budget_sub_head_id,$activity_id,$party_id);
//        dd($voucherDetails);
//        $advance = $this->voucherDetailsRepo->getTotalAdvanceByActivity($activity_id,$party_id,$budget_sub_head_id);
//        $advance_clear = $this->voucherDetailsRepo->getTotalClearedAdvanceByActivity($activity_id,$party_id,$budget_sub_head_id);
//        $remainAdvance = floatval($advance) - floatval($advance_clear);
        return json_encode($voucherDetails);
    }






}
