<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Bhuktani;
use App\Models\PreBhuktani;
use App\Models\Programs;
use App\Repositories\BhuktaniRepository;
use App\Repositories\PreBhuktaniRepository;
use App\Repositories\ProgramRepository;
use Illuminate\Http\Request;
use App\Repositories\HomeRepository;
use Illuminate\Support\Facades\Auth;

class BhuktaniController extends Controller
{
    private $bankRepo;
    private $programRepo;
    private $preBhuktaniRepo;
    private $bhuktaniRepo;


    public function __construct(
        BhuktaniRepository $bankRepo,
        ProgramRepository $programRepo,
        PreBhuktaniRepository $preBhuktaniRepo,
        BhuktaniRepository $bhuktaniRepo
    )
    {

        $this->middleware('auth');
        $this->bankRepo = $bankRepo;
        $this->programRepo = $programRepo;
        $this->preBhuktaniRepo = $preBhuktaniRepo;
        $this->bhuktaniRepo = $bhuktaniRepo;
    }

//    public function bhuktaniParam

    public function preIndex()
    {
        $programs = $this->programRepo->get_program_for_voucher();
        return view('frontend.bhuktani.pre_index', compact('programs'));
    }

    public function index($program_id)
    {
//        dd($program_id);
        $office_id = Auth::user()->office_id;
        $programs = $this->programRepo->get_by_program_id($program_id);
//        dd($programs);
//        $program = $programs[0];
        $bhuktanies = Bhuktani::where('office_id',$office_id)->where('budget_sub_head',$program_id)->get();
//        dd(($program));
        return view('frontend.bhuktani.index', compact('bhuktanies','program_id','programs'));
    }

    public function create(Request $request, $program_id){

        try{

            $program = $this->programRepo->get_by_program_id($program_id);

            $bhuktani_adesh_number = $this->bhuktaniRepo->get_bhuktani_adesh_number_by_program($program_id);

            $preBhuktanies = $this->preBhuktaniRepo->get_by_program($program_id);
            return view('frontend.bhuktani.create',compact('program','preBhuktanies','bhuktani_adesh_number'));

        } catch (\Exception $e){
            $message = "डाटा प्रविष्टि भएको छैन!";
            return redirect()->back()->with('error', $message);
        }

    }

    public function store(Request $request){

//        dd($request);
       $request['totalAmount'] = $this->preBhuktaniRepo->get_amount_by_multiple_id($request['bhuktani']);
       $bhuktani_id = $this->bhuktaniRepo->create($request->all());
       foreach ($request['bhuktani'] as $pre_bhuktani){

           $this->preBhuktaniRepo->update_status_and_set_bhuktani_id($bhuktani_id,$pre_bhuktani);
       }
        return redirect()->back();
    }

    public function get_bhuktani(Request $request){

        $parameter = $request->all();
        $bhuktani = $this->bhuktaniRepo->get_by_budget_sub_head_and_other($parameter);
        return json_encode($bhuktani);


    }

    public function bhuktani_delete($bhuktani_id,$program_id){

        $this->bhuktaniRepo->delete($bhuktani_id);

        return redirect(route('bhuktani.index',$program_id));


    }

}
