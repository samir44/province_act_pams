<?php

namespace App\Http\Controllers\Frontend\Report;

use App\helpers\FiscalYearHelper;
use App\helpers\Money_words;
use App\Http\Controllers\Controller;
use App\Imports\BudgetImport;
use App\Models\Month;
use App\Models\Office;
use App\Models\Programs;
use App\Models\Report;
use App\Models\Voucher;
use App\Repositories\BhuktaniRepositoryEloquent;
use App\Repositories\ExpenseHeadRepository;
use App\Repositories\PreBhuktaniRepository;
use App\Repositories\ProgramRepository;
use App\Repositories\ProgramRepositoryEloquent;
use App\Repositories\ReportRepository;
use App\Repositories\SourceRepository;
use App\Repositories\SourceTypeRepository;
use App\Repositories\VoucherDetailsRepository;
use App\Repositories\VoucherRepository;
use App\Repositories\RetentionBhuktaniRepository;
use App\Repositories\BudgetRepository;
use App\Repositories\BudgetRepositoryEloquent;
use function foo\func;
use Illuminate\Http\Request;
use App\Repositories\HomeRepository;
use Illuminate\Support\Facades\Auth;
use phpDocumentor\Reflection\Types\Float_;
use phpDocumentor\Reflection\Types\Null_;
use App\Repositories\VoucherSignatureRepository;
use App\Repositories\RetentionVoucherRepository;


class ReportController extends Controller
{

    private $programRepo;
    private $voucherrepo;
    private $budgetRepo;
    private $programRrepo;
    private $voucheDetailsRrepo;
    private $preBuktaniRrepo;
    private $bhuktaniRrepo;
    private $expenseHeadRrepo;
    private $fiscalYearHelper;
    private $budgetRrepo;
    private $voucherSignatureRepo;
    private $retentionVoucherDetails;
    private $retentionBhuktaniRepo;
    private $sourceRepo;
    private $sourcetypeRepo;


    public function __construct(

        ProgramRepository $programRepo,
        BudgetRepository $budgetRepo,
        VoucherRepository $voucherrepo,
        VoucherDetailsRepository $voucheDetailsRrepo,
        PreBhuktaniRepository $preBhuktaniRepository,
        ExpenseHeadRepository $expenseHeadRrepo,
        ProgramRepositoryEloquent $programRrepo,
        BhuktaniRepositoryEloquent $bhuktaniRrepo,
        BudgetRepositoryEloquent $budgetRrepo,
        VoucherSignatureRepository $voucherSignatureRepo,
        RetentionVoucherRepository $retentionVoucherRepo,
        RetentionBhuktaniRepository $retentionBhuktaniRepo,
        SourceRepository $sourceRepo,
        SourceTypeRepository $sourcetypeRepo


    )
    {

        $this->middleware('auth');
        $this->programRepo = $programRepo;
        $this->voucherrepo = $voucherrepo;
        $this->voucheDetailsRrepo = $voucheDetailsRrepo;
        $this->preBuktaniRrepo = $preBhuktaniRepository;
        $this->expenseHeadRrepo = $expenseHeadRrepo;
        $this->fiscalYearHelper = new FiscalYearHelper();
        $this->expenseHeadRrepo = $expenseHeadRrepo;
        $this->programRrepo = $programRrepo;
        $this->bhuktaniRrepo = $bhuktaniRrepo;
        $this->budgetRrepo = $budgetRrepo;
        $this->voucherSignatureRepo = $voucherSignatureRepo;
        $this->retentionVoucherRepo = $retentionVoucherRepo;
        $this->retentionBhuktaniRepo = $retentionBhuktaniRepo;
        $this->budgetRepo = $budgetRepo;
        $this->sourceRepo = $sourceRepo;
        $this->sourcetypeRepo = $sourcetypeRepo;



    }

    public function voucher(Request $request)
    {

        $budgetSubHeads = $this->programRepo->get_program_for_report();
        $vouchersList = null;
        $budget_sub_head = $request['budget_sub_head'];
        if ($request->has('budget_sub_head')) {

            $vouchersList = $this->voucherrepo->get_all_voucher_budget_sub_head_id($budget_sub_head);
//            dd($vouchersList);
        }

        return view('frontend.report.voucher', compact('budgetSubHeads', 'vouchersList'));
    }

    public function voucher_view(Request $request, $id)
    {

        $preBhuktanies = $this->preBuktaniRrepo->get_preBhuktani_by_voucher_id($id);
        $total_bhuktani = $preBhuktanies->sum('amount');
        $total_bhuktani_word = $this->bhuktaniRrepo->get_amount_in_word($total_bhuktani);
        $voucher = $this->voucherrepo->find($id);
        $voucherDetails = $this->voucheDetailsRrepo->get_details_by_voucher_id_with_activity($id);

        return view('frontend.report.voucher_view', compact('voucher', 'preBhuktanies', 'voucherDetails', 'district_name', 'total_bhuktani', 'total_bhuktani_word'));
    }

    public function bhuktani_adesh_param()
    {

        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $programs = $this->programRepo->get_program_for_report();
        return view('frontend.report.bhuktaniParam', compact('programs', 'fiscalYear'));

    }


    public function bhuktani_adesh_report(Request $request, $id)
    {

        $bhuktani = $this->bhuktaniRrepo->get_bhuktani_by_bhuktani_id($id);
        $pre_bhuktanies = $this->preBuktaniRrepo->get_pre_bhuktani_by_bhuktani_id($id);
        $pre_bhuktani = $bhuktani->prebhuktani->groupBy('bhuktani_paaune','expense_head_id');
//        dd($pre_bhuktani);
        $pre_bhuktani_array= $pre_bhuktani;
        $bhuktani_amount_word = $this->bhuktaniRrepo->get_amount_in_word($bhuktani['amount']);
        $voucher = ($bhuktani->preBhuktani[0]->voucher);
        $program = $this->programRepo->get_by_program_id($bhuktani['budget_sub_head']);

        return view('frontend.report.bhuktaniReport', compact('bhuktani', 'program', 'voucher', 'bhuktani_amount_word','pre_bhuktani','pre_bhuktani_array'));

    }

    public function malepa_five_param()
    {

        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $programs = $this->programRepo->get_program_for_report();
        return view('frontend.report.malepaFiveParam', compact('programs', 'fiscalYear'));
    }


    public function malepa_five_report(Request $request)
    {
        $bank_cr_total = 0;
        $liability_cr_total = 0;
        $liability_dr_total = 0;

        $datas = $request->all();
        $budget_sub_head = $datas['budget_sub_head'];
        $program = $this->programRepo->get_by_program_id($datas['budget_sub_head']);
        $month_post = $datas['month'];
        $month = Month::findorfail($month_post);
        if ($month) {

            $month_name = ($month->name);
        }
        $fiscalYear = $datas['fiscal_year'];
        if ($month_post <= 9) {

            $year = substr($fiscalYear, 0, 4);
        } else {

            $year = substr($fiscalYear, 0, 4) + 1;

        }

        $voucherList = $this->voucherrepo->get_all_voucher_by_budget_sub_head_id($budget_sub_head, $fiscalYear, $month_post);
//        dd($voucherList);
        if ($voucherList->count() > 0) {
            $budget_sub_head_name = $voucherList[0]->budget_sub_head->name;
        }

        $preVoucherDetailList = $this->voucheDetailsRrepo->search(['budget_sub_head' => $budget_sub_head, 'fiscalYear' => $fiscalYear, 'less_than_this_month' => $month_post, 'status' => 1, 'office_id' => Auth::user()->office_id]);
        $thisMonthVoucherDetailList = $this->voucheDetailsRrepo->search(['budget_sub_head' => $budget_sub_head, 'fiscalYear' => $fiscalYear, 'month' => $month_post, 'status' => 1, 'office_id' => Auth::user()->office_id]);
        $UpToThisVoucherDetailList = $this->voucheDetailsRrepo->search(['budget_sub_head' => $budget_sub_head, 'fiscalYear' => $fiscalYear, 'up_to_this_month' => $month_post, 'status' => 1, 'office_id' => Auth::user()->office_id]);

        if ($preVoucherDetailList) {

            $preMonthTotal = $this->voucheDetailsRrepo->get_all_field_total($preVoucherDetailList);
            $thisMonthTotal = $this->voucheDetailsRrepo->get_all_field_total($thisMonthVoucherDetailList);
//           dd($thisMonthTotal);
            $upTothisMonthTotal = $this->voucheDetailsRrepo->get_all_field_total($UpToThisVoucherDetailList);

        }
        $office_id = Auth::user()->office_id;
        $voucher_signature = $this->voucherSignatureRepo->get_by_office_id($office_id);
        return view('frontend.report.malepaFive', compact('voucherList', 'thisMonthTotal', 'program', 'preMonthTotal', 'upTothisMonthTotal', 'budget_sub_head', 'year', 'month_name', 'voucher_signature'));

    }


    public function malepa_thirteen_param()
    {

        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $programs = $this->programRepo->get_program_for_report();
        return view('frontend.report.malepaThirteenParam', compact('programs', 'fiscalYear'));
    }


//   Phatbari
    public function malepa_thirteen_report(Request $request)
    {
        $datas = $request->all();
        $budget_sub_head_id = $datas['budget_sub_head'];
        $program = $this->programRepo->get_by_program_id($datas['budget_sub_head']);
        $month_post = $datas['month'];
        $month = Month::findorfail($month_post);
        if ($month) {

            $month_name = ($month->name);
            $month_id = $month->id;
        }
        $fiscalYear = $datas['fiscal_year'];
        if ($month_post <= 9) {

            $year = substr($fiscalYear, 0, 4);
        } else {

            $year = substr($fiscalYear, 0, 4) + 1;

        }
        $budgetByExpenseHeads = $this->voucheDetailsRrepo->get_all_voucher_by_group_by_expense_head($budget_sub_head_id, $fiscalYear);
//        dd($budgetByExpenseHeads);
        $upToLastMonthVoucherDetailList = $this->voucheDetailsRrepo->searchByGroupBy(['budget_sub_head' => $budget_sub_head_id, 'fiscalYear' => $fiscalYear, 'less_than_this_month' => $month_post, 'status' => 1, 'office_id' => Auth::user()->office_id]);
        $upToThisMonthVoucherDetailList = $this->voucheDetailsRrepo->searchByGroupBy(['budget_sub_head' => $budget_sub_head_id, 'fiscalYear' => $fiscalYear, 'up_to_this_month' => $month_post, 'status' => 1, 'office_id' => Auth::user()->office_id]);
        $thisMonthVoucherDetailList = $this->voucheDetailsRrepo->searchByGroupBy(['budget_sub_head' => $budget_sub_head_id, 'fiscalYear' => $fiscalYear, 'month' => $month_post, 'status' => 1, 'office_id' => Auth::user()->office_id]);
        if ($thisMonthVoucherDetailList) {

            $upToLastMonthExp = $this->voucheDetailsRrepo->get_all_field_total($upToLastMonthVoucherDetailList);
            $upToThisMonthExp = $this->voucheDetailsRrepo->get_all_field_total($upToThisMonthVoucherDetailList);
            $thisMonthExp = $this->voucheDetailsRrepo->get_all_field_total($thisMonthVoucherDetailList);
            $upToThisMonthpeski = $this->voucheDetailsRrepo->get_all_field_total($upToThisMonthVoucherDetailList);
            $expense_with_out_peski = ($upToThisMonthExp['expense'] - $upToThisMonthExp['peski']);

        }
        $voucher_signature = $this->voucherSignatureRepo->get_by_office_id(Auth::user()->office_id);
        return view('frontend.report.malepaThirteen', compact('year', 'fiscalYear', 'datas','month_name', 'month_post', 'program', 'voucherDetails', 'month_id', 'upToLastMonthExp', 'upToThisMonthExp', 'thisMonthExp', 'expense_with_out_peski', 'budgetByExpenseHeads','voucher_signature'));
    }


    public function malepa_fourteen_param()
    {

        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $programs = $this->programRepo->get_program_for_report();
        return view('frontend.report.malepaFourteenParam', compact('programs', 'fiscalYear'));
    }

    public function malepa_fourteen_report(Request $request)
    {

        $datas = $request->all();
        $office_id = Auth::user()->office_id;
        $budget_sub_head = $datas['budget_sub_head'];
        $program = $this->programRepo->get_by_program_id($datas['budget_sub_head']);
        $month_post = $datas['month'];
        $month = Month::findorfail($month_post);
        if ($month) {

            $month_name = ($month->name);
        }
        $fiscalYear = $datas['fiscal_year'];
        if ($month_post <= 9) {

            $year = substr($fiscalYear, 0, 4);

        } else {

            $year = substr($fiscalYear, 0, 4) + 1;

        }


        $UpToThisVoucherDetailList = $this->voucheDetailsRrepo->search(['budget_sub_head' => $budget_sub_head, 'fiscalYear' => $fiscalYear, 'up_to_this_month' => $month_post, 'office_id' => Auth::user()->office_id]);
        $karmachariAdvance = $this->voucheDetailsRrepo->get_advance($UpToThisVoucherDetailList);
        $total_peski = [];
//        $total_peski['karmachari_total'] = ->sum('dr_amount');
//        $total_peski['upabhokta_total'] = $karmachariAdvance['upaBhokta']->sum('dr_amount');
//        $total_peski['thekedar_total'] = $karmachariAdvance['thekedar']->sum('dr_amount');
//        $total_peski['byaktigat_total'] = $karmachariAdvance['byaktigat']->sum('dr_amount');
//        $total_peski['sasthagat_total'] = $karmachariAdvance['sasthagat']->sum('dr_amount');
//        $total_peski['upabhokta_total'] = ($UpToThisVoucherDetailList->where('dr_or_cr', 1)->where('ledger_type_id', 4)->where('party_type',1)->sum('dr_amount'));
        $total_current_peski = ($UpToThisVoucherDetailList->where('dr_or_cr', 1)->where('ledger_type_id', 4)->sum('dr_amount'));
        $voucher_signature = $this->voucherSignatureRepo->get_by_office_id($office_id);
        return view('frontend.report.malepaFourteen', compact('program', 'year', 'month_name', 'karmachariAdvance', 'total_peski', 'total_current_peski', 'voucher_signature'));

    }

    public function malepa_seventeen_param()
    {

        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $programs = $this->programRepo->get_program_for_report();
        return view('frontend.report.malepaSeventeenParam', compact('programs', 'fiscalYear'));
    }

    public function malepa_seventeen_report(Request $request)
    {
        $datas = $request->all();
//        dd($datas);

        if ($datas['program'] == 1234 and $datas['source_type'] == 123) {

            $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
            $office = Office::where('id', $datas['office_id'])->get();
            $budgetSubHeadLists = $this->programRepo->get_by_office_id_for_ministry($datas['office_id']);
            $granTotalInitialBudget = $this->budgetRepo->getTotalInitialBudgetByOffice($datas['office_id']);
            $granTotalAddBudget = $this->budgetRepo->getTotalAddBudgetByOffice($datas['office_id']);
            $granReduceAddBudget = $this->budgetRepo->getTotalReduceBudgetByOffice($datas['office_id']);
            $finalBudget = $this->budgetRepo->getFinalBudget($datas['office_id']);
            $totalExpense = $this->voucheDetailsRrepo->getTotalExpenseByOffice($datas['office_id']);
            $totalRemainBudget = (float)$finalBudget - (float)$totalExpense;
            return view('frontend.report.malepaSeventeenAllBudgetSubAndAllSources', compact('fiscalYear', 'office', 'budgetSubHeadLists', 'datas', 'granTotalInitialBudget', 'granTotalAddBudget', 'granReduceAddBudget', 'finalBudget', 'totalExpense', 'totalRemainBudget'));

        }
        if ($datas['program'] != 1234 and $datas['source_type'] != 123) {

            $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
            $program = $this->programRepo->get_by_program_id($datas['program']);
            $office = Office::where('id', $datas['office_id'])->get();
            $sourceTypes = $this->sourcetypeRepo->getSourceByOfficeAndBudgetSubHeadAndSourceType($datas['office_id'], $datas['program'], $datas['source_type']);
            return view('frontend.report.malepaSeventeenNotAllBudgetSubNotAllSources', compact('fiscalYear', 'program', 'office', 'sourceTypes', 'datas'));
        }


        if ($datas['program'] != 1234 and $datas['source_type'] == 123) {

            $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
            $program = $this->programRepo->get_by_program_id($datas['program']);
            $office = Office::where('id', $datas['office_id'])->get();
            $sourceTypes = $this->sourcetypeRepo->getSourceByOfficeAndBudgetSubHead($datas['office_id'], $datas['program']);
            return view('frontend.report.malepaSeventeenNotAllBudgetSubHeadButAllSource', compact('fiscalYear', 'program', 'office', 'sourceTypes', 'datas'));

        }


    }

    public function budgetSheetParam(){


        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $programs = $this->programRepo->get_program_for_report();
        return view('frontend.report.budgetKhataParam', compact('programs', 'fiscalYear'));
    }

    public function budgetSheet(Request $request){

        $datas = $request->all();
        $budget_sub_head_id = $datas['budget_sub_head'];
        $program = $this->programRepo->get_by_program_id($datas['budget_sub_head']);
        $vouchers = Voucher::where('office_id',$datas['office_id'])->where('budget_sub_head_id',$datas['budget_sub_head'])->get();
        $thisMonthVouchers = $vouchers->where('month',$datas['month']);
//        dd($vouchers->where('month',$datas['month']));
        $thisMonthTotalNikasa = 0;
        $upToPreMonthTotalNikasa = 0;
        $thisMonthTotalExpense = 0;
        $totalNikasa = 0;
        $upToPreMonthTotalExpense = 0;
        $totalExpense = 0;
        foreach ($vouchers as $voucher){

            $thisMonthTotalNikasa += $voucher->details->where('dr_or_cr',2)->where('ledger_type_id',8)->where('month',$datas['month'])->sum('cr_amount');
            $upToPreMonthTotalNikasa += $voucher->details->where('dr_or_cr',2)->where('ledger_type_id',8)->where('month','<=',$datas['month']-1)->sum('cr_amount');
            $totalNikasa += $voucher->details->where('dr_or_cr',2)->where('ledger_type_id',8)->where('month','<=',$datas['month'])->sum('cr_amount');

            $thisMonthTotalExpense += $voucher->details->where('dr_or_cr',1)->whereIn('ledger_type_id',['1','4'])->where('month',$datas['month'])->sum('dr_amount');
            $upToPreMonthTotalExpense += $voucher->details->where('dr_or_cr',1)->whereIn('ledger_type_id',['1','4'])->where('month','<=',$datas['month']-1)->sum('dr_amount');
            $totalExpense += $voucher->details->where('dr_or_cr',1)->whereIn('ledger_type_id',['1','4'])->where('month','<=',$datas['month'])->sum('dr_amount');
        }
        $totalBudget = $this->budgetRepo->getTotalBudgetByBudgetSubHead($datas['office_id'],$datas['budget_sub_head']);
        $remainBudget = (float)$totalBudget - (float)$totalExpense;
        $month_post = $datas['month'];
        $month = Month::findorfail($month_post);
        if ($month) {

            $month_name = ($month->name);
            $month_id = $month->id;
        }
        $fiscalYear = $datas['fiscal_year'];
        if ($month_post <= 9) {

            $year = substr($fiscalYear, 0, 4);
        } else {

            $year = substr($fiscalYear, 0, 4) + 1;

        }
//        voucherDetails repo भए पनि data Budget बाटै तानेको छ। पछि मिलाउने गरी
        $budgetByExpenseHeads = $this->voucheDetailsRrepo->get_all_voucher_by_group_by_expense_head($budget_sub_head_id, $fiscalYear);
//        dd($budgetByExpenseHeads);
        return view('frontend.report.budgetKhata', compact('program', 'datas','month_name','budgetByExpenseHeads','thisMonthVouchers','vouchers','thisMonthTotalNikasa','upToPreMonthTotalNikasa','totalNikasa','thisMonthTotalExpense','upToPreMonthTotalExpense','totalExpense','totalBudget','remainBudget'));
    }

    public function getKhataByKhataPrakar($budget_sub_head_id, $ledger_type_id)
    {

        if ($ledger_type_id == 1) {

            $expenseHeadList = $this->voucheDetailsRrepo->get_expense_head_by_budget_sub_head_and_office_id($budget_sub_head_id, $ledger_type_id);
            return json_encode($expenseHeadList);
        } else if ($ledger_type_id == 2) {

            $expenseHeadList = $this->voucheDetailsRrepo->get_liability_by_budget_sub_head_and_office_id($budget_sub_head_id, $ledger_type_id);
//           dd($expenseHeadList);

            return json_encode($expenseHeadList);

        } else if ($ledger_type_id == 3) {

            $bhuktani_list = $this->preBuktaniRrepo->get_parties_by_budget_head_and_office($budget_sub_head_id);
            return json_encode($bhuktani_list);

        } else if ($ledger_type_id == 4) {

            $peski_party_list = $this->voucheDetailsRrepo->get_peski_party_by_budget_head_and_office($budget_sub_head_id, $ledger_type_id);
            return json_encode($peski_party_list);

        } else if ($ledger_type_id == 5) {

            $activitiesList = $this->voucheDetailsRrepo->get_activities_by_budget_head_and_office($budget_sub_head_id, $ledger_type_id);


        } else if ($ledger_type_id == 6) {

            $bhuktani_list = $this->preBuktaniRrepo->get_parties_by_budget_head_and_office($budget_sub_head_id);
//           dd($bhuktani_list);
            return json_encode($bhuktani_list);
        }

    }

    public function malepa_twentyTwo_param()
    {

        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $programs = $this->programRepo->get_program_for_report();
        return view('frontend.report.malepaTwentyTwoParam', compact('programs', 'fiscalYear'));
    }


    public function malepa_tweentyTwo_report(Request $request)
    {

        $sn = 1;
        $data = $request->all();
//        dd($data);
        $budget_sub_id = $request['program'];
        if ($request['khata-prakar'] == 1) {
            if ($request['khata'] == 1234) {
//                खाता All
                return view('frontend.report.malepa22.malepa22_karcha_all');
            } else {

                $program = $this->programRepo->get_by_program_id($request['program']);
                $dr_amount = 0;
                $cr_amount = 0;
                $ledger_type_id = $request['khata'];
                $vouchersLists = $this->voucheDetailsRrepo->get_all_voucher_by_budget_sub_head_and_ledger_type_id($budget_sub_id, $ledger_type_id);
                foreach ($vouchersLists as $voucher) {

                    $dr_amount = ($voucher->dr_amount);
                    $cr_amount = ($voucher->cr_amount);
                    $remain = (float)$dr_amount - (float)$cr_amount;
                }
                $dr_toptal = $vouchersLists->sum('dr_amount');
                $cr_toptal = $vouchersLists->sum('cr_amount');
                $total_remain = (float)$dr_toptal - (float)$cr_toptal;
                $expense_head_sirsak = ($vouchersLists[0]->expense_head->expense_head_sirsak);
                $expense_head_code = ($vouchersLists[0]->expense_head->expense_head_code);
                return view('frontend.report.malepa22.malepa22_karcha', compact('sn', 'vouchersLists', 'program', 'expense_head_sirsak', 'expense_head_code', 'dr_toptal', 'cr_toptal', 'total_remain', 'remain'));

            }

        }

        if ($request['khata-prakar'] == 2) {

            if ($request['khata'] == 1234) {
//                खाता All
                return view('frontend.report.malepa22.payment_receiver_all', compact('datas'));

            } else {
//                dd($data);
                $liabilityLists = $this->voucheDetailsRrepo->get_liability_expenses_by_liability_id($data);
//                dd($liabilityLists);
                $dr_amount_total = $liabilityLists->sum('dr_amount');
                $cr_amount_total = $liabilityLists->sum('cr_amount');
                $remain_total_temp = (float)$dr_amount_total - (float)$cr_amount_total;
                if ($remain_total_temp < 0) {

                    $remain_total = '(' . (string)((-1) * (float)$remain_total_temp) . ')';
                } else {

                    $remain_total = $remain_total_temp;
                }
                $expense_head_code = (float)($liabilityLists[0]->expense_head->expense_head_code);
                $expense_head_sirsak = $liabilityLists[0]->expense_head->expense_head_sirsak;
                return view('frontend.report.malepa22.liability_expense', compact('liabilityLists', 'data', 'expense_head_code', 'expense_head_sirsak', 'dr_amount_total', 'cr_amount_total', 'remain_total'));

            }

        }
        if ($request['khata-prakar'] == 3) {

            if ($request['khata'] == 1234) {

                return view('frontend.report.malepa22.advance_receiver_all');

            } else {
//                    dd($data);
                $preBhuktaniList = $this->preBuktaniRrepo->get_party_by_party_id($data);

                $total_bhuktani = $preBhuktaniList->sum('amount');
                $total_advance_vat_deduction = $preBhuktaniList->sum('advance_vat_deduction');
                $total_vat_amount = $preBhuktaniList->sum('vat_amount');
//                    dd($preBhuktaniList);
                $party_name = $preBhuktaniList[0]->party->name_nep;
                return view('frontend.report.malepa22.payment_receiver', compact('preBhuktaniList', 'data', 'party_name', 'total_bhuktani', 'total_advance_vat_deduction', 'total_vat_amount'));

            }

        }
        if ($request['khata-prakar'] == 4) {
            $remain = 0;
            if ($request['khata'] == 1234) {

            } else {

                $peskiDetails = $this->voucheDetailsRrepo->getPeskiDetailsByParty($data);
                $party_name = ($peskiDetails[0]->advancePayment->name_nep);
                $dr_amount_total = $peskiDetails->sum('dr_amount');
                $cr_amount_total = $peskiDetails->sum('cr_amount');
                $total_remain = (float)$dr_amount_total - (float)$cr_amount_total;
                return view('frontend.report.malepa22.peski', compact('peskiDetails', 'data', 'dr_amount_total', 'cr_amount_total', 'party_name', 'total_remain'));

            }

        }
        if ($request['khata-prakar'] == 6) {

            if ($request['khata'] == 1234) {

            } else {

                $preBhuktaniList = $this->preBuktaniRrepo->get_party_by_party_id($data);
//                dd($preBhuktaniList[0]->voucher);
                $total_bhuktani_amount = $preBhuktaniList->sum('amount');
                $total_advance_vat_deduction = $preBhuktaniList->sum('advance_vat_deduction');
                $total_vat_amount = $preBhuktaniList->sum('vat_amount');
//                dd($total_bhuktani_amount);
                $party_name = $preBhuktaniList[0]->party->name_nep;

                return view('frontend.report.malepa22.vat', compact('data', 'party_name', 'preBhuktaniList', 'total_bhuktani_amount', 'total_advance_vat_deduction', 'total_vat_amount'));

            }

        }


    }


    public function getActivityParam()
    {


        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $programs = $this->programRepo->get_programs_for_budget();
        return view('frontend.report.activityParam', compact('fiscalYear', 'programs'));

    }

    public function getActivityByBudgetSubHead(Request $request)
    {

        $datas = $request->all();
        $office_id = Auth::user()->office_id;
        $budget_sub_head_id = $datas['budget_sub_head'];
        $program = $this->programRepo->get_by_program_id($datas['budget_sub_head']);
        $fiscalYear = $datas['fiscal_year'];


        $activities = $this->budgetRrepo->get_activities_by_budget_sub_head_and_budget_type($budget_sub_head_id, $office_id);
        $budget_sum = $activities->sum('total_budget');
        $first_quarter_budget_total = $activities->sum('first_quarter_budget');
        $second_quarter_budget_total = $activities->sum('second_quarter_budget');
        $third_quarter_budget_total = $activities->sum('third_quarter_budget');

        $total_first_quarter_expense = $this->voucheDetailsRrepo->getFirstChaumasikTotalExpense($datas['office'], $datas['budget_sub_head'], $datas['fiscal_year']);
        $total_second_quarter_expense = $this->voucheDetailsRrepo->getSecondChaumasikTotalExpense($datas['office'], $datas['budget_sub_head'], $datas['fiscal_year']);
        $total_third_quarter_expense = $this->voucheDetailsRrepo->getThirdChaumasikTotalExpense($datas['office'], $datas['budget_sub_head'], $datas['fiscal_year']);
        $totalExpenseByBudgetSubHead = (float)$total_first_quarter_expense + (float)$total_second_quarter_expense + (float)$total_third_quarter_expense;
        return view('frontend.report.activityList', compact('program', 'fiscalYear','datas', 'activities', 'budget_sum', 'first_quarter_budget_total', 'second_quarter_budget_total', 'third_quarter_budget_total','total_first_quarter_expense','total_second_quarter_expense','total_third_quarter_expense','totalExpenseByBudgetSubHead'));

    }

    public function goswaraDharautiKhataParameter()
    {

        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        return view('frontend.report.goswara_dharauti_khata_param', compact('fiscalYear'));
    }

    public function goswaraDharautiKhata()
    {

        $office_id = Auth::user()->office_id;
        $retentionVoucher = $this->retentionVoucherRepo->get_by_office_id($office_id);
//        dd($retentionVoucher);

        if ($retentionVoucher) {
            foreach ($retentionVoucher as $details) {
                $totalRetentionIncome = $retentionVoucher
                    ->where('dr_or_cr', 2)
                    ->where('byahora', 9)
                    ->where('hisab_number', 395)
                    ->sum('amount');


                $totalRetentionReturn = $retentionVoucher
                    ->where('dr_or_cr', 1)
                    ->where('byahora', 9)
                    ->where('hisab_number', 397)
                    ->sum('amount');

                $totalRetentionCollaps = $retentionVoucher
                    ->where('dr_or_cr', 1)
                    ->where('byahora', 9)
                    ->where('hisab_number', 398)
                    ->sum('amount');
            }

        }

        return view('frontend.report.goswara_dharauti_khata', compact('retentionVoucher', 'totalRetentionIncome', 'totalRetentionReturn', 'totalRetentionCollaps'));
    }

    public function byaktigatDharautiKhataParam()
    {

        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        $office_id = Auth::user()->office_id;
        $depositorTypes = $this->retentionVoucherRepo->get_party_by_office_id($office_id);
//        dd($depositorTypes);
        return view('frontend.report.byaktigat_dharauti_khata_param', compact('fiscalYear', 'depositorTypes'));
    }

    public function byaktigatDharautiKhata(Request $request)
    {

        $postData = $request->all();
        $retentionDatas = $this->retentionVoucherRepo->getDetailsByDepositor($postData['depositor'], $postData['office']);
//        dd($retentionDatas[0]->advancePayment->name_nep);

        foreach ($retentionDatas as $details) {

            $totalRetentionIncome = $retentionDatas
                ->where('dr_or_cr', 2)
                ->where('byahora', 9)
                ->where('hisab_number', 395)
                ->sum('amount');


            $totalRetentionReturn = $retentionDatas
                ->where('dr_or_cr', 1)
                ->where('byahora', 9)
                ->where('hisab_number', 397)
                ->sum('amount');

            $totalRetentionCollaps = $retentionDatas
                ->where('dr_or_cr', 1)
                ->where('byahora', 9)
                ->where('hisab_number', 398)
                ->sum('amount');
        }

        return view('frontend.report.byaktigat_dharauti_khata', compact('retentionDatas', 'totalRetentionIncome', 'totalRetentionReturn', 'totalRetentionCollaps', 'postData'));

    }

    public function dharautiCashBookParam()
    {

        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        return view('frontend.report.dharauti_cash_book_param', compact('fiscalYear'));

    }

    public function dharautiCashBook(Request $request)
    {

        $datas = $request->all();
        $month_post = $datas['month'];
        $month = Month::findorfail($month_post);
        if ($month) {

            $month_name = ($month->name);
        }
        $fiscalYear = $datas['fiscal_year'];
        if ($month_post <= 9) {

            $year = substr($fiscalYear, 0, 4);
        } else {

            $year = substr($fiscalYear, 0, 4) + 1;

        }
//        $retentionVoucherDetails =
        return view('frontend.report.dharauti_cash_book', compact('year', 'month_name'));

    }

    public function retentionDharautiParam()
    {

        $fiscalYear = $this->fiscalYearHelper->get_active_fiscal_year();
        return view('frontend.report.retentionBhuktaniParam', compact('fiscalYear'));
    }

    public function getRetentionBhuktani(Request $request)
    {
//        dd("test");
        $parameter = $request->all();
        $bhuktani = $this->retentionBhuktaniRepo->get_by_office($parameter);
        return json_encode($bhuktani);
    }

    public function retentionBhuktaniAdeshReport($id)
    {


        $bhuktani = $this->retentionBhuktaniRepo->getById($id);
        $bhuktani_amount_word = $this->retentionBhuktaniRepo->get_amount_in_word($bhuktani['amount']);
//        dd($bhuktani->retentionPreBhuktani[0]->voucher->retention_voucher_details[1]->byahora);
        return view('frontend.report.retention_bhuktaniReport', compact('bhuktani', 'bhuktani_amount_word'));

    }

    public function importExcel(Request $request){
        return view('frontend.report.import_form');
    }

    public function storeExcel(Request $request){
        Excel::import(new BudgetImport(), request()->file('excel'));
        dd($request->all());
    }
}
