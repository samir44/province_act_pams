<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Akhtiyari;
use App\Models\BudgetDetails;
use App\Models\Office;
use App\Models\Role;
use App\Models\VoucherDetail;
use App\Repositories\BudgetRepository;
use App\Repositories\ProgramRepository;
use App\User;
use Illuminate\Http\Request;
use Input;
use App\Post;
use DB;
use Session;
use Excel;
use App\Models\Programs;
use App\Models\Budget;
use App\helpers\BsHelper;
use App\Repositories\VoucherDetailsRepository;


class MaatwebsiteController extends Controller
{
    private $programRepo;
    private $budgetRepo;
    private $voucherDetailsRepo;


    public function __construct(
        ProgramRepository $programRepo,
        BudgetRepository $budgetRepo,
        VoucherDetailsRepository $voucherDetailsRepo


    )
    {

        $this->programRepo = $programRepo;
        $this->budgetRepo = $budgetRepo;
        $this->voucherDetailsRepo = $voucherDetailsRepo;


    }

    public function importExport()
    {
        return view('importExport');
    }

    public function downloadExcel($type)
    {
        $data = Post::get()->toArray();
        return Excel::create('laravelcode', function ($excel) use ($data) {
            $excel->sheet('mySheet', function ($sheet) use ($data) {
                $sheet->fromArray($data);
            });
        })->download($type);
    }

    public function importExcel(Request $request)
    {
        if ($request->hasFile('import_file')) {
            Excel::load($request->file('import_file')->getRealPath(), function ($reader) {
                $programs_array = $reader->toArray();
                $program_collections = collect($programs_array);

                foreach ($program_collections as $row) {

                    $data = [];
                    $data['project_code'] = (string)$row['project_code'];
                    $data['name'] = $row['project_ndesc'];
                    $data['sub_project_code'] = (string)$row['sub_project_code'];
                    $data['sub_project_ndesc_office'] = $row['sub_project_ndesc_office'];
                    $officeId = $row['office_id'];
                    $data['office_id'] = $officeId;
                    $data['program_code'] = $row['program_code'];
                    $lastProgramNumber = (substr($data['program_code'], 8));
                    if ($lastProgramNumber == 3) {

                        $data['expense_type'] = 1;
                    } else {

                        $data['expense_type'] = 2;
                    }
                    $data['component_code'] = (int)$row['component_code'];
                    $data['component_ndesc'] = $row['component_ndesc'];
                    $data['district_code'] = (int)$row['district_code'];
                    $data['economic_code5'] = (string)$row['economic_code5'];
                    $data['expense_head_id'] = (int)$row['expense_head_id'];
                    $data['activity_code'] = $row['activity_code'];
                    $data['main_activity_ndesc4'] = $row['main_activity_ndesc4'];
                    $data['amount'] = $row['amount'];
                    $data['source_type'] = (int)$row['source_type'];
                    $data['source_level'] = (int)$row['source_level'];
                    $data['source'] = (int)$row['source'];
                    $data['medium'] = (int)$row['medium'];
                    if (!empty($data)) {

//                        Program check and insert
                        $programs = Programs::where('program_code', $data['program_code'])->where('office_id', $data['office_id'])->first();
                        if (!$programs) {

                            $data['budget_sub_head_id'] = $this->programRepo->create($data, $lastProgramNumber);

                        } else {
                            $data['budget_sub_head_id'] = $programs->id;
                        }

                        $akhtiyari = Akhtiyari::where('budget_sub_head_id', $data['budget_sub_head_id'])->where('office_id', $data['office_id'])->first();
                        if (!$akhtiyari) {

                            $data['akhtiyari_amount'] = $program_collections->where('office_id', $data['office_id'])->where('program_code', $data['program_code'])->sum('amount');
                            $data['akhtiyar_id'] = $this->budgetRepo->createAkhtiyariTemp($data);

                        } else {
                            $data['akhtiyar_id'] = $akhtiyari->id;
                        }

                        $this->budgetRepo->createTemp($data);
//                        dd('stop');

                    }
                }
            });
        }
//        Session::put('success', 'Youe file successfully import in database!!!');
        return back();
    }


    public function importUser()
    {

        return view('importUser');
    }

    public function storeUser(Request $request)
    {

        if ($request->hasFile('import_file')) {

            Excel::load($request->file('import_file')->getRealPath(), function ($reader) {
                $users_array = $reader->toArray();
                $users_collection = collect($users_array);
                foreach ($users_collection as $row) {

//                    dd($row);
                    $user = new User();

                    $user->name = $row['office_name'];
                    $user->username = $row['user_name'];
                    $user->email = $row['user_name'];
                    $user->password = bcrypt($row['password']);
                    $user->province_id = (int)$row['province_id'];
                    $user->mof_id = (int)$row['mof_id'];
                    $user->ministry_id = (int)$row['ministry_id'];
                    $user->department_id = NULL;
                    $user->office_id = (int)$row['office_id'];
                    $user->status = 1;
                    $user->save();
                    $role = Role::find(2);  // admin
                    $user->roles()->attach($role);


                }
            });
        }
//        Session::put('success', 'Youe file successfully import in database!!!');
        return back();
    }

    public function importBudgetDetails_temp()
    {

        $offices = ['1', '4', '43'];
        foreach ($offices as $office) {
            $budgetSubHeads = Programs::where('office_id', $office)->get();

            foreach ($budgetSubHeads as $budgetSubHead) {
                $budgets = Budget::where('budget_sub_head', $budgetSubHead->id)->get();
                foreach ($budgets as $budget) {
                    $budgetDetals = new BudgetDetails();
                    $budgetDetals->budget_sub_head_id = $budgetSubHead->id;
                    $budgetDetals->budget_id = $budget['id'];
                    $budgetDetals->sub_activity = $budget['activity'];
                    $budgetDetals->expense_head_id = $budget['expense_head_id'];
                    $budgetDetals->unit = 1;
                    $budgetDetals->total_budget = $budget['total_budget'];
                    $budgetDetals->first_quarter_unit = NULL;
                    $budgetDetals->second_quarter_unit = NULL;
                    $budgetDetals->third_quarter_unit = 1;
                    $budgetDetals->first_quarter_budget = NULL;
                    $budgetDetals->first_chaimasik_bhar = NULL;
                    $budgetDetals->second_quarter_budget = NULL;
                    $budgetDetals->second_chaimasik_bhar = NULL;
                    $budgetDetals->third_quarter_budget = $budget['total_budget'];
                    $budgetDetals->third_chaimasik_bhar = NULL;
                    $budgetDetals->source_type = $budget['source_type'];
                    $budgetDetals->source_level = $budget['source_level'];
                    $budgetDetals->source = $budget['source'];
                    $budgetDetals->medium = $budget['medium'];
                    $budgetDetals->office_id = $budget['office_id'];
                    $budgetDetals->user_id = 1;
                    $budgetDetals->status = 1;
                    $budgetDetals->fiscal_year = "2076/77";
                    $budgetDetals->date = date('Y-m-d H:i:s');
                    $budgetDetals->save();

                }
            }
        }
        return back();
    }


    public function importBudgetDetails()
    {

//        $offices = ['1','4','43'];
//        foreach ($offices as $office) {
//        $budgetSubHeads = Programs::where('office_id', 12)->get();
//        $i = 0;
//        foreach ($budgetSubHeads as $budgetSubHead) {
//            $budgets = Budget::where('budget_sub_head', $budgetSubHead->id)->get();
//            foreach ($budgets as $budget) {
//                if ($budget['expense_head'] == '31156') {
//                    echo "Pujigat", "<br>";
//                    for ($x = 1; $x <= 2; $x++) {
//                        $amount = $budget['total_budget'] * 0.04;
//                        $sub_activity = $budget['activity'] . " - " . "कन्टेन्जेन्सी";
//                        if ($x == 1) {
//                            $amount = $budget['total_budget'] * 0.96;
//                            $sub_activity = $budget['activity'];
//                        }
//                        $budgetDetals = new BudgetDetails();
//                        $budgetDetals->office_id = $budget['office_id'];
//                        $budgetDetals->budget_sub_head_id = $budgetSubHead->id;
//                        $budgetDetals->budget_id = $budget['id'];
//                        $budgetDetals->sub_activity = $sub_activity;
//                        $budgetDetals->expense_head_id = $budget['expense_head_id'];
//                        $budgetDetals->unit = 1;
////                        $program_amount = $amount;
//                        $budgetDetals->total_budget = (float)$amount;
//                        $budgetDetals->first_quarter_unit = NULL;
//                        $budgetDetals->second_quarter_unit = NULL;
//                        $budgetDetals->third_quarter_unit = 1;
//                        $budgetDetals->first_quarter_budget = NULL;
//                        $budgetDetals->first_chaimasik_bhar = NULL;
//                        $budgetDetals->second_quarter_budget = NULL;
//                        $budgetDetals->second_chaimasik_bhar = NULL;
//                        $budgetDetals->third_quarter_budget = (float)$amount;
//                        $budgetDetals->third_chaimasik_bhar = NULL;
//                        $budgetDetals->source_type = $budget['source_type'];
//                        $budgetDetals->source_level = $budget['source_level'];
//                        $budgetDetals->source = $budget['source'];
//                        $budgetDetals->medium = $budget['medium'];
//                        $budgetDetals->user_id = 1;
//                        $budgetDetals->status = 1;
//                        $budgetDetals->fiscal_year = "2076/77";
//                        $budgetDetals->date = date('Y-m-d H:i:s');
//                        $budgetDetals->save();
//
//                    }
//                } else {
//
//                    $budgetDetals = new BudgetDetails();
//                    $budgetDetals->budget_sub_head_id = $budgetSubHead->id;
//                    $budgetDetals->budget_id = $budget['id'];
//                    $budgetDetals->sub_activity = $budget['activity'];
//                    $budgetDetals->expense_head_id = $budget['expense_head_id'];
//                    $budgetDetals->unit = 1;
//                    $budgetDetals->total_budget = $budget['total_budget'];
//                    $budgetDetals->first_quarter_unit = NULL;
//                    $budgetDetals->second_quarter_unit = NULL;
//                    $budgetDetals->third_quarter_unit = 1;
//                    $budgetDetals->first_quarter_budget = NULL;
//                    $budgetDetals->first_chaimasik_bhar = NULL;
//                    $budgetDetals->second_quarter_budget = NULL;
//                    $budgetDetals->second_chaimasik_bhar = NULL;
//                    $budgetDetals->third_quarter_budget = $budget['total_budget'];
//                    $budgetDetals->third_chaimasik_bhar = NULL;
//                    $budgetDetals->source_type = $budget['source_type'];
//                    $budgetDetals->source_level = $budget['source_level'];
//                    $budgetDetals->source = $budget['source'];
//                    $budgetDetals->medium = $budget['medium'];
//                    $budgetDetals->office_id = $budget['office_id'];
//                    $budgetDetals->user_id = 1;
//                    $budgetDetals->status = 1;
//                    $budgetDetals->fiscal_year = "2076/77";
//                    $budgetDetals->date = date('Y-m-d H:i:s');
//                    $budgetDetals->save();
//                    echo "Chalu", "<br>";
//                }
//            }
//        }

//            }

        $voucher_details = VoucherDetail::whereIn('office_id', ['1', '4', '12'])->get();
        foreach ($voucher_details as $index => $voucher_detail) {

            $budget = $voucher_detail->budget;
            if ($budget) {
                $budget_detail = $this->get_detail_by_budget_id($budget->id);
                if ($budget_detail) {
                    $voucher_detail->main_activity_id = $budget_detail->id;
                    $voucher_detail->save();
                    echo "V_D " . $voucher_detail->id . "from b _ id " . $budget->id . 'detai_id' . $budget_detail->id, "<br>";
                } else {
                    echo "NOOOO Detail for budget =>", $budget->id, "<br>";
                }


            } else {
                echo "No Budget in vouchee detail =>", $voucher_detail->id, "<br>";
            }
        }

        dd("Success");
        return back();
    }

    function get_detail_by_budget_id($budget_id)
    {
        $budget_detail = BudgetDetails::where('budget_id', $budget_id)->where('sub_activity', 'not like', '%कन्टेन्जेन्सी%')->first();
        return $budget_detail;
    }

    function setOperationalActivity()
    {
//        $offices = Office::where('ministry_id', 4)
//            ->where('province_id', 4)
//            ->whereNotIn('id', [9, 12])->get();
//        foreach ($offices as $office) {

            $budgetSubHeads = Programs::where('office_id', 9)->where('program_code', 'like', '%' . 3)->first();
            if ($budgetSubHeads) {
                $activities = Budget::where('budget_sub_head', $budgetSubHeads->id)->where('expense_head', 22212)->get();//several activities
                if ($activities->count() > 0) {
                    $activity = $activities[0];
                    $total_budget = $activities->sum('total_budget');
                    $budget = $this->budgetRepo->createBudgetForOperationalActivity($activity, $total_budget);
                    $budgetDetailsSave = $this->budgetRepo->createBudgetDetailsForOperationalActivity($activity, $total_budget, $budget);

                    foreach ($activities as $activity) {
                        if ($activity->chaineDetail()->count() > 0) {
                            $activity->chaineDetail()->delete();
                        }
                        $activity->delete();
                    }
                }

            }
//        }
        return back();
    }

}