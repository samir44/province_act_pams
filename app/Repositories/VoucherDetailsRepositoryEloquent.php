<?php

namespace App\Repositories;

use App\helpers\BsHelper;
use App\Models\BhuktaniVoucher;
use App\Models\Budget;
use App\Models\Voucher;
use App\Models\VoucherDetail;
use Illuminate\Support\Facades\Auth;
use DB;
use phpDocumentor\Reflection\Types\Array_;


class VoucherDetailsRepositoryEloquent implements VoucherDetailsRepository
{


    public function find($id)
    {

        return VoucherDetail::findorfail($id);
    }

    public function create($attributes)
    {
//        dd($attributes);
        $voucherDetails = new VoucherDetail();
        $voucherDetails->office_id = Auth::user()->office->id;
        $voucherDetails->journel_id = $attributes['voucher_id'];
        $voucherDetails->budget_sub_head_id = $attributes['budget_sub_head'];
        $voucherDetails->main_activity_id = $attributes['activity_id'];
        $voucherDetails->dr_or_cr = $attributes['drOrCr'];
        $voucherDetails->ledger_type_id = $attributes['ledger_type_id'];
        $voucherDetails->expense_head_id = $attributes['hisab_number'];
        $voucherDetails->bibaran = $attributes['bibran'];
        $voucherDetails->party_type = $attributes['party_type'];
//        $voucherDetails->peski_type = $attributes['peski_type'];
        $voucherDetails->voucher_details_id = $attributes['voucher_details_id'];
        $party_id = $attributes['party_id'];

        if (!$party_id) {
            $voucherDetails->peski_paune_id = 0;
        } else {

            $voucherDetails->peski_paune_id = $attributes['party_id'];

        }
        if ($attributes['drOrCr'] == 1) {
            $voucherDetails->dr_amount = $attributes['amount'];
            $voucherDetails->cr_amount = 0;


        }
        if ($attributes['drOrCr'] == 2) {

            $voucherDetails->dr_amount = 0;
            $voucherDetails->cr_amount = $attributes['amount'];
        }
        $voucherDetails->status = 1;
        $voucherDetails->date_nepali = $attributes['date_nepali'];
        $voucherDetails->date_english = $attributes['date_english'];

        $date_array = explode('-', $attributes['date_english']);
        $bsObj = new BsHelper();
        $data_ad_array = $bsObj->nep_to_eng($date_array[0], $date_array[1], $date_array[2]);
        $voucherDetails->date_english = $data_ad_array['year'] . '-' . $data_ad_array['month'] . '-' . $data_ad_array['date'];

        $date_eng = $attributes['date_english'];
        $myyearfirst = (substr($date_eng, 0, 2));
        $myyearlast = (substr($date_eng, 2, 2));
        $mymonth = (substr($date_eng, 5, 1));
        $month = intval($mymonth);

        if ($month == 1) {
            $finalmonth = 10;
        }
        if ($month == 2) {
            $finalmonth = 11;
        }
        if ($month == 3) {
            $finalmonth = 12;
        }
        if ($month == 4) {
            $finalmonth = 1;
        }
        if ($month == 5) {
            $finalmonth = 2;
        }
        if ($month == 6) {
            $finalmonth = 3;
        }
        if ($month == 7) {
            $finalmonth = 4;
        }
        if ($month == 8) {
            $finalmonth = 5;
        }
        if ($month == 9) {
            $finalmonth = 6;
        }
        if ($month == 10) {
            $finalmonth = 7;
        }
        if ($month == 11) {
            $finalmonth = 8;
        }
        if ($month == 12) {
            $finalmonth = 9;
        }
        if (intval($mymonth) >= 4) {
            $voucherDetails->fiscal_year = $myyearfirst . $myyearlast . "/" . (intval($myyearlast) + 1);
        } else {
            $voucherDetails->fiscal_year = $myyearfirst . (intval($myyearlast) - 1) . "/" . $myyearlast;
        }
        $voucherDetails->month = $finalmonth;
        $voucherDetails->save();
        return $voucherDetails;
    }


    public function createNikasaVoucherDetails($attributes, $voucher_id, $budget = null)
    {

        $voucherDetails = new VoucherDetail();
        $voucherDetails->office_id = Auth::user()->office->id;
        $voucherDetails->journel_id = $voucher_id;
        $voucherDetails->budget_sub_head_id = $attributes['budget_sub_head'];

        if (array_key_exists('activity_id', $attributes)) {
            $voucherDetails->main_activity_id = $attributes['activity_id'];
        }
        $voucherDetails->dr_or_cr = $attributes['drOrCr'];

        $voucherDetails->ledger_type_id = $attributes['ledger_type_id'];
        if ($budget) {
            $voucherDetails->expense_head_id = $budget[0]->expense_head_by_id->id;
            $voucherDetails->bibaran = $budget[0]->expense_head_by_id->expense_head_sirsak;
        }
        $voucherDetails->party_type = NULL;
//        $voucherDetails->peski_type = $attributes['peski_type'];
        $party_id = NULL;

        if ($attributes['drOrCr'] == 1) {
            $voucherDetails->dr_amount = $attributes['amount'];
            $voucherDetails->cr_amount = 0;


        }
        if ($attributes['drOrCr'] == 2) {

            $voucherDetails->dr_amount = 0;
            $voucherDetails->cr_amount = $attributes['amount'];
        }
        $voucherDetails->status = 1;
        $voucherDetails->date_nepali = $attributes['date'];
        $date_array = explode('-', $attributes['bs-roman']);
        $bsObj = new BsHelper();
        $data_ad_array = $bsObj->nep_to_eng($date_array[0], $date_array[1], $date_array[2]);
        $voucherDetails->date_english = $data_ad_array['year'] . '-' . $data_ad_array['month'] . '-' . $data_ad_array['date'];

        $date_eng = $attributes['bs-roman'];
        $myyearfirst = (substr($date_eng, 0, 2));
        $myyearlast = (substr($date_eng, 2, 2));
        $mymonth = (substr($date_eng, 5, 1));
        $month = intval($mymonth);
        if ($month == 1) {
            $finalmonth = 10;
        }
        if ($month == 2) {
            $finalmonth = 11;
        }
        if ($month == 3) {
            $finalmonth = 12;
        }
        if ($month == 4) {
            $finalmonth = 1;
        }
        if ($month == 5) {
            $finalmonth = 2;
        }
        if ($month == 6) {
            $finalmonth = 3;
        }
        if ($month == 7) {
            $finalmonth = 4;
        }
        if ($month == 8) {
            $finalmonth = 5;
        }
        if ($month == 9) {
            $finalmonth = 6;
        }
        if ($month == 10) {
            $finalmonth = 7;
        }
        if ($month == 11) {
            $finalmonth = 8;
        }
        if ($month == 12) {
            $finalmonth = 9;
        }
        if (intval($mymonth) >= 4) {
            $voucherDetails->fiscal_year = $myyearfirst . $myyearlast . "/" . (intval($myyearlast) + 1);
        } else {
            $voucherDetails->fiscal_year = $myyearfirst . (intval($myyearlast) - 1) . "/" . $myyearlast;
        }
        $voucherDetails->month = $finalmonth;
        $voucherDetails->save();
        return $voucherDetails;
    }

    public function get_all_pre_month_voucher_by_budget_sub_head_id($budget_sub_head_id, $fiscalYear, $month)
    {

        $office_id = Auth::user()->office->id;
        return $voucherList = VoucherDetail::where('office_id', $office_id)
            ->where('fiscal_year', $fiscalYear)
            ->where('month', $month)
            ->where('budget_sub_head_id', $budget_sub_head_id)->get();
    }

    public function get_expense_head_by_budget_sub_head_and_office_id($budget_sub_head_id, $ledger_type_id)
    {

//         $vouchers = VoucherDetail::where('budget_sub_head_id',$budget_sub_head_id)
//             ->where('ledger_type_id',$ledger_type_id)
//            ->with('expense_head')->get()->toArray();

        $vouchers = DB::table('voucher_details')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->join('main_expense_head', 'voucher_details.expense_head_id', '=', 'main_expense_head.id')
            ->select('main_expense_head.id', 'main_expense_head.expense_head_sirsak', 'main_expense_head.expense_head_code')
            ->select('main_expense_head.*')
            ->where('voucher_details.budget_sub_head_id', $budget_sub_head_id)
            ->where('vouchers.status', 1)
            ->where('voucher_details.ledger_type_id', $ledger_type_id)->get()->toArray();
        $all_expense_heads = [];
//         dd($vouchers);
        foreach ($vouchers as $voucher) {
//             $expense_heads = $voucher->expense_head;

            $temp = [];
            $temp["id"] = $voucher->id;
            $temp["sirsak"] = $voucher->expense_head_sirsak;
            $temp["code"] = $voucher->expense_head_code;
            array_push($all_expense_heads, $temp);

        }
        $unique_heads = $this->multi_unique($all_expense_heads);
        return $unique_heads;


    }

    public function get_liability_by_budget_sub_head_and_office_id($budget_sub_head_id, $ledger_type_id)
    {

        $vouchers = VoucherDetail::where('budget_sub_head_id', $budget_sub_head_id)
            ->where('ledger_type_id', $ledger_type_id)
            ->with('expense_head')->get()->toArray();
        $all_expense_heads = [];
//         dd($vouchers);
        foreach ($vouchers as $voucher) {
            $expense_heads = $voucher['expense_head'];
//             dd($expense_heads);

            $temp = [];
            $temp["id"] = $expense_heads['id'];
            $temp["sirsak"] = $expense_heads["expense_head_sirsak"];
            $temp["code"] = $expense_heads["expense_head_code"];
//            if(in_array($expense_heads['id'], array_column($all_expense_heads, 'id'))) { // search value in the array
//                array_push($all_expense_heads, $temp);
//            }
            if (!$this->check_unique($all_expense_heads, 'id', $expense_heads['id']))
                array_push($all_expense_heads, $temp);
        }
//        $unique_heads = $this->multi_unique($all_expense_heads);
//        dd($all_expense_heads);
        return $all_expense_heads;
    }

    function check_unique($array, $key, $val)
    {
        foreach ($array as $item)
            if (isset($item[$key]) && $item[$key] == $val)
                return true;
        return false;
    }

    public function get_peski_party_by_budget_head_and_office($budget_sub_head_id, $ledger_type_id)
    {

        $vouchers = VoucherDetail::where('budget_sub_head_id', $budget_sub_head_id)
            ->where('ledger_type_id', $ledger_type_id)
            ->with('advancePayment')->get()->toArray();
        $all_peski_party = [];
//         dd($vouchers);
        foreach ($vouchers as $voucher) {

            $peski_party = $voucher['advance_payment'];
//             dd($peski_party);

            $temp = [];
            $temp["id"] = $peski_party['id'];
            $temp["sirsak"] = $peski_party["name_nep"];
//            $temp["code"] = $peski_party["expense_head_code"];
            array_push($all_peski_party, $temp);

        }
        $unique_heads = $this->multi_unique($all_peski_party);
        return $unique_heads;
    }


    public function get_activities_by_budget_head_and_office($budget_sub_head_id, $ledger_type_id)
    {

        $activities = VoucherDetail::where('budget_sub_head_id', $budget_sub_head_id)
            ->where('dr_or_cr', 1)
            ->where('ledger_type_id', 1)
            ->with('advancePayment')->get()->toArray();
        $all_activities = [];
//         dd($activities);
        foreach ($activities as $activity) {

            $activity = $activity['advance_payment'];
//             dd($activity);

            $temp = [];
            $temp["id"] = $activity['id'];
            $temp["sirsak"] = $activity["name_nep"];
//            $temp["code"] = $peski_party["expense_head_code"];
            array_push($all_activities, $temp);

        }
        $unique_heads = $this->multi_unique($all_activities);
        return $unique_heads;
    }

    function multi_unique($src)
    {

        $output = array_map("unserialize",
            array_unique(array_map("serialize", $src)));
        return $output;
    }

    public function get_all_voucher_by_budget_sub_head_and_ledger_type_id($budget_sub_head_id, $ledger_type_id)
    {

        return $voucherList = VoucherDetail::where('budget_sub_head_id', $budget_sub_head_id)
            ->where('expense_head_id', $ledger_type_id)->get();

//        dd($voucherList);
    }

    public function get_details_by_voucher_id($voucher_id)
    {

        return $voucherDetailsList = VoucherDetail::where('journel_id', $voucher_id)->with('voucher')->get();
    }

    public function get_details_by_voucher_id_with_activity($id)
    {

        return $voucherDetailsList = VoucherDetail::where('journel_id', $id)
            ->with(['mainActivity'])
            ->orderBy('dr_or_cr', 'asc')
            ->get();

    }


//    VOucher Update
    public function updateVoucherDetails($voucher_id, $voucher_detail_data)
    {

        $voucher = Voucher::findorfail($voucher_id);
        $voucher->details()->delete();

        foreach ($voucher_detail_data as $detail) {

            $voucherDetails = new VoucherDetail();
            $voucherDetails->office_id = Auth::user()->office->id;
            $voucherDetails->journel_id = $voucher->id;
            $voucherDetails->budget_sub_head_id = $detail['budget_sub_head'];
            $voucherDetails->main_activity_id = $detail['activity_id'];
            $voucherDetails->dr_or_cr = $detail['drOrCr'];
            $voucherDetails->ledger_type_id = $detail['ledger_type_id'];
            $voucherDetails->expense_head_id = $detail['hisab_number'];
            $voucherDetails->bibaran = $detail['bibran'];
            $voucherDetails->party_type = array_key_exists('party_type', $detail) ? $detail['party_type'] : null;
            $voucherDetails->peski_paune_id = array_key_exists('party_id', $detail) ? $detail['party_id'] : null;
            $voucherDetails->voucher_details_id = array_key_exists('voucher_details_id', $detail) ? $detail['voucher_details_id'] : null;

            if ($detail['drOrCr'] == 1) {
                $voucherDetails->dr_amount = $detail['amount'];
                $voucherDetails->cr_amount = 0;
            }
            if ($detail['drOrCr'] == 2) {
                $voucherDetails->dr_amount = 0;
                $voucherDetails->cr_amount = $detail['amount'];
            }
            $voucherDetails->status = 0;
            $voucherDetails->date_nepali = $detail['date_nepali'];
            $date_array = explode('-', $detail['date_english']);
            $bsObj = new BsHelper();
            $data_ad_array = $bsObj->nep_to_eng($date_array[0], $date_array[1], $date_array[2]);
            $voucherDetails->date_english = $data_ad_array['year'] . '-' . $data_ad_array['month'] . '-' . $data_ad_array['date'];

            $date_eng = $detail['date_english'];
            $myyearfirst = (substr($date_eng, 0, 2));
            $myyearlast = (substr($date_eng, 2, 2));
            $mymonth = (substr($date_eng, 5, 1));
            $month = intval($mymonth);

            if ($month == 1) {
                $finalmonth = 10;
            }
            if ($month == 2) {
                $finalmonth = 11;
            }
            if ($month == 3) {
                $finalmonth = 12;
            }
            if ($month == 4) {
                $finalmonth = 1;
            }
            if ($month == 5) {
                $finalmonth = 2;
            }
            if ($month == 6) {
                $finalmonth = 3;
            }
            if ($month == 7) {
                $finalmonth = 4;
            }
            if ($month == 8) {
                $finalmonth = 5;
            }
            if ($month == 9) {
                $finalmonth = 6;
            }
            if ($month == 10) {
                $finalmonth = 7;
            }
            if ($month == 11) {
                $finalmonth = 8;
            }
            if ($month == 12) {
                $finalmonth = 9;
            }
            if (intval($mymonth) >= 4) {
                $voucherDetails->fiscal_year = $myyearfirst . $myyearlast . "/" . (intval($myyearlast) + 1);
            } else {
                $voucherDetails->fiscal_year = $myyearfirst . (intval($myyearlast) - 1) . "/" . $myyearlast;
            }

            $voucherDetails->month = $finalmonth;
            $voucherDetails->save();
        }
        return $voucher;
    }

    public function get_all_field_total($VoucherList)
    {
//        dd($VoucherList);
        $data = [];
        $data['cr_tsa'] = $VoucherList->where('dr_or_cr', 2)->where('ledger_type_id', 3)->sum('cr_amount');
        $data['nikasa'] = $VoucherList->where('dr_or_cr', 1)->where('ledger_type_id', 3)->sum('dr_amount');
        $data['expense'] = $VoucherList->where('dr_or_cr', 1)->where('ledger_type_id', 1)->sum('dr_amount');
        $data['dr_liability'] = $VoucherList->where('dr_or_cr', 1)->where('ledger_type_id', 2)->sum('dr_amount');
        $data['cr_liability'] = $VoucherList->where('dr_or_cr', 2)->whereIn('ledger_type_id', ['2', '8'])->sum('cr_amount');


        $data['expense'] = $VoucherList->where('dr_or_cr', 1)->whereIn('ledger_type_id', [1, 4])->sum('dr_amount');
        $data['peski'] = $VoucherList->where('dr_or_cr', 1)->whereIn('ledger_type_id', [4])->sum('dr_amount');

        $data['advance_given'] = $VoucherList->where('dr_or_cr', 1)->whereIn('ledger_type_id', [4])->sum('dr_amount');

        return $data;
    }

    public function get_advance($VoucherList)
    {
        $data = [];
//        upa bhokta
        $data['upaBhokta'] = $VoucherList->where('dr_or_cr', 1)->where('ledger_type_id', ['4', '9'])->where('party_type', 1);
        $peskiVoucherDetails = $data['upaBhokta']->where('dr_or_cr', 1)->where('ledger_type_id', '4');
        $clearanceVoucherDetails = $data['upaBhokta']->where('dr_or_cr', 2)->where('ledger_type_id', '9');
        $remain_peski['upaBhokta'] = [];
        foreach ($peskiVoucherDetails as $peski) {

            $temp = [];
            $temp['id'] = $peski->id;
            $temp['date'] = $peski->voucher->data_nepali;
            $temp['voucher_no'] = $peski->voucher->jv_number;
            $temp['narration'] = $peski->voucher->short_narration;
            $temp['party_name'] = $peski->advancePayment->name_nep;
            $temp['expense_head_title'] = $peski->expense_head->expense_head_code;
            $temp['peski'] = $peski->dr_amount;
            $clearance_amount = $clearanceVoucherDetails->where('voucher_details_id', $peski->id)->sum('cr_amount');
            $remaining = $peski->dr_amount;
            if ($clearance_amount) {
                $remaining = $remaining - $clearance_amount;
            }
            $temp['baki'] = $remaining;
            if($temp['baki'] > 0){

                array_push($remain_peski['upaBhokta'], $temp);
            }
        }


//        Thekedar
        $data['thekedar'] = $VoucherList->where('dr_or_cr', 1)->where('ledger_type_id', ['4', '9'])->where('party_type', 2);
        $peskiVoucherDetails = $data['thekedar']->where('dr_or_cr', 1)->where('ledger_type_id', '4');
        $clearanceVoucherDetails = $data['thekedar']->where('dr_or_cr', 2)->where('ledger_type_id', '9');
        $remain_peski['thekedar'] = [];
        foreach ($peskiVoucherDetails as $peski) {

            $temp = [];
            $temp['id'] = $peski->id;
            $temp['date'] = $peski->voucher->data_nepali;
            $temp['voucher_no'] = $peski->voucher->jv_number;
            $temp['narration'] = $peski->voucher->short_narration;
            $temp['party_name'] = $peski->advancePayment->name_nep;
            $temp['expense_head_title'] = $peski->expense_head->expense_head_code;
            $temp['peski'] = $peski->dr_amount;
            $clearance_amount = $clearanceVoucherDetails->where('voucher_details_id', $peski->id)->sum('cr_amount');
            $remaining = $peski->dr_amount;
            if ($clearance_amount) {
                $remaining = $remaining - $clearance_amount;
            }
            $temp['baki'] = $remaining;
            if($temp['baki'] > 0){

                array_push($remain_peski['thekedar'], $temp);
            }
        }

//        sasthagat
        $data['sasthagat'] = $VoucherList->where('dr_or_cr', 1)->whereIn('ledger_type_id',['4','9'])->where('party_type', 4);
        $peskiVoucherDetails = $data['sasthagat']->where('dr_or_cr', 1)->where('ledger_type_id', '4');
        $clearanceVoucherDetails = $data['sasthagat']->where('dr_or_cr', 2)->where('ledger_type_id', '9');
        $remain_peski['sasthagat'] = [];
        foreach ($peskiVoucherDetails as $peski) {

            $temp = [];
            $temp['id'] = $peski->id;
            $temp['date'] = $peski->voucher->data_nepali;
            $temp['voucher_no'] = $peski->voucher->jv_number;
            $temp['narration'] = $peski->voucher->short_narration;
            $temp['party_name'] = $peski->advancePayment->name_nep;
            $temp['expense_head_title'] = $peski->expense_head->expense_head_code;
            $temp['peski'] = $peski->dr_amount;
            $clearance_amount = $clearanceVoucherDetails->where('voucher_details_id', $peski->id)->sum('cr_amount');
            $remaining = $peski->dr_amount;
            if ($clearance_amount) {
                $remaining = $remaining - $clearance_amount;
            }
            $temp['baki'] = $remaining;
            if($temp['baki'] > 0){

                array_push($remain_peski['sasthagat'], $temp);
            }
        }

//        karmachari
        $data['karmachari'] = $VoucherList->whereIn('dr_or_cr', ['1', '2'])->whereIn('ledger_type_id', ['4', '9'])->where('party_type', 5);
        $peskiVoucherDetails = $data['karmachari']->where('dr_or_cr', 1)->where('ledger_type_id', '4');
        $clearanceVoucherDetails = $data['karmachari']->where('dr_or_cr', 2)->where('ledger_type_id', '9');
        $remain_peski['karmachari'] = [];
        foreach ($peskiVoucherDetails as $peski) {

            $temp = [];
            $temp['id'] = $peski->id;
            $temp['date'] = $peski->voucher->data_nepali;
            $temp['voucher_no'] = $peski->voucher->jv_number;
            $temp['narration'] = $peski->voucher->short_narration;
            $temp['party_name'] = $peski->advancePayment->name_nep;
            $temp['expense_head_title'] = $peski->expense_head->expense_head_code;
            $temp['peski'] = $peski->dr_amount;
            $clearance_amount = $clearanceVoucherDetails->where('voucher_details_id', $peski->id)->sum('cr_amount');
            $remaining = $peski->dr_amount;
            if ($clearance_amount) {
                $remaining = $remaining - $clearance_amount;
            }
            $temp['baki'] = $remaining;
            if($temp['baki'] > 0){

                array_push($remain_peski['karmachari'], $temp);
            }
        }

//        Personal
        $data['byaktigat'] = $VoucherList->whereIn('dr_or_cr', ['1', '2'])->whereIn('ledger_type_id', ['4', '9'])->where('party_type', 3);
        $peskiVoucherDetails = $data['byaktigat']->where('dr_or_cr', 1)->where('ledger_type_id', '4');
        $clearanceVoucherDetails = $data['byaktigat']->where('dr_or_cr', 2)->where('ledger_type_id', '9');
        $remain_peski['byaktigat'] = [];
        foreach ($peskiVoucherDetails as $peski) {

            $temp = [];
            $temp['id'] = $peski->id;
            $temp['date'] = $peski->voucher->data_nepali;
            $temp['voucher_no'] = $peski->voucher->jv_number;
            $temp['narration'] = $peski->voucher->short_narration;
            $temp['party_name'] = $peski->advancePayment->name_nep;
            $temp['expense_head_title'] = $peski->expense_head->expense_head_code;
            $temp['peski'] = $peski->dr_amount;
            $clearance_amount = $clearanceVoucherDetails->where('voucher_details_id', $peski->id)->sum('cr_amount');
            $remaining = $peski->dr_amount;
            if ($clearance_amount) {
                $remaining = $remaining - $clearance_amount;
            }
            $temp['baki'] = $remaining;
            if($temp['baki'] > 0){

                array_push($remain_peski['byaktigat'], $temp);
            }
        }
//        dd($remain_peski);
        return $remain_peski;

    }


    public function report_by_month($data)
    {

        $voucherDetail = VoucherDetail::query();
        if (array_key_exists('month', $data)) {
            $voucherDetail->where('month', $data['month']);
        }
        if (array_key_exists('less_than_this_month', $data)) {
            $voucherDetail->where('month', '<', $data['less_than_this_month']);
        }
        if (array_key_exists('up_to_this_month', $data)) {
            $voucherDetail->where('month', '<=', $data['up_to_this_month']);
        }
        if (array_key_exists('year', $data)) {
            $voucherDetail->where('year', $data['year']);
        }
        if (array_key_exists('fiscal_year', $data)) {
            $voucherDetail->where('fiscal_year', $data['fiscal_year']);
        }
        if (array_key_exists('budget_sub_head_id', $data)) {
            $voucherDetail->where('budget_sub_head_id', $data['budget_sub_head_id']);
        }


        $voucherDetails = $voucherDetail->groupBy('expense_head_id')->selectRaw('*, sum(cr_amount) as total_cr')->get();
//        dd($voucherDetails);
        return $voucherDetails;
    }

    public function search($data)
    {

//        dd($data);
        $voucherDetail = VoucherDetail::query();
//        dd($voucherDetail);

//        if(array_key_exists('status', $data)){
//            $voucherDetail->where('status','1')->with('voucher');
//        }
        if (array_key_exists('less_than_this_month', $data)) {
            $voucherDetail->where('month', '<', $data['less_than_this_month']);
        }

        if (array_key_exists('month', $data)) {
            $voucherDetail->where('month', '=', $data['month']);
        }

        if (array_key_exists('up_to_this_month', $data)) {
            $voucherDetail->where('month', '<=', $data['up_to_this_month']);
        }

        if (array_key_exists('year', $data)) {
            $voucherDetail->where('year', $data['year']);
        }
        if (array_key_exists('fiscal_year', $data)) {
            $voucherDetail->where('fiscal_year', $data['fiscal_year']);
        }
        if (array_key_exists('budget_sub_head', $data)) {
            $voucherDetail->where('budget_sub_head_id', $data['budget_sub_head']);
        }

        return $voucherDetail->get();
//        dd($voucherDetail->vouucher);
    }

    public function searchByGroupBy($data)
    {
//        dd($data);
//        use query if this does not done

        $voucherDetail = DB::table('voucher_details')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->where('vouchers.status', 1);

//        dd($voucherDetail);

        if (array_key_exists('month', $data)) {
            $voucherDetail->where('voucher_details.month', $data['month']);
        }

//        if (array_key_exists('status', $data)) {
//            $voucherDetail->where('status', $data['status']);
//        }

        if (array_key_exists('less_than_this_month', $data)) {
            $voucherDetail->where('voucher_details.month', '<', $data['less_than_this_month']);
        }
        if (array_key_exists('up_to_this_month', $data)) {
            $voucherDetail->where('voucher_details.month', '<=', $data['up_to_this_month']);
        }
//
//        if (array_key_exists('year', $data)) {
//            $voucherDetail->where('year', $data['year']);
//        }
        if (array_key_exists('fiscalYear', $data)) {
            $voucherDetail->where('voucher_details.fiscal_year', $data['fiscalYear']);
        }
        if (array_key_exists('budget_sub_head', $data)) {
            $voucherDetail->where('voucher_details.budget_sub_head_id', $data['budget_sub_head']);
        }


        return $voucherDetail->get();
    }

    public function get_expense_by_activity_id($activity_id)
    {

        return $total_expense_of_activity = VoucherDetail::where('main_activity_id', $activity_id)->where('dr_or_cr', 1)->where('ledger_type_id', 1)->sum('dr_amount');
    }

    public function get_advance_by_activity_id($activity_id)
    {

        return $total_expense_of_activity = VoucherDetail::where('main_activity_id', $activity_id)->where('dr_or_cr', 1)->where('ledger_type_id', 4)->sum('dr_amount');
    }

    public function getCuttingLiability($budgetSubHead, $expenseHeadId)
    {

//        dd($budgetSubHead);
        return $cuttingLiability = VoucherDetail::where('budget_sub_head_id', $budgetSubHead)
//
            ->where('expense_head_id', $expenseHeadId)
            ->where('dr_or_cr', 2)
            ->where('ledger_type_id', 2)->sum('cr_amount');
//        dd($cuttingLiability);
    }

    public function getdepositLiability($budgetSubHead, $expenseHeadId)
    {

        return $cuttingLiability = VoucherDetail::where('budget_sub_head_id', $budgetSubHead)
//                ->where('main_activity_id',$activityId)
            ->where('expense_head_id', $expenseHeadId)
            ->where('dr_or_cr', 1)
            ->where('ledger_type_id', 2)->sum('dr_amount');
//        dd($cuttingLiability);
    }

    public function get_all_voucher_by_group_by_expense_head($budget_sub_head_id, $fiscalYear)
    {

        $office_id = Auth::user()->office_id;
        return $budgetByExpenseHead = Budget::where('office_id', $office_id)
            ->where('budget_sub_head', $budget_sub_head_id)
            ->groupBy('expense_head')
            ->orderBy('expense_head')
            ->get();

//        $expenseHead = DB::table('budget')
//            ->select('expense_head')
//            ->join('main_expense_head', 'main_expense_head.id', '=', 'budget.expense_head_id')
//            ->where('budget.office_id', $office_id)
//            ->where('budget.budget_sub_head', $budget_sub_head_id)
//            ->get();
//        return $expenseHead;


//       return $voucher_details = VoucherDetail::with('expense_head','budgetExpenseHead')
//            ->where('office_id',$office_id)
//            ->where('fiscal_year',$fiscalYear)
//            ->where('budget_sub_head_id',$budget_sub_head_id)
//            ->where('dr_or_cr',1)
//            ->where('ledger_type_id',1)
//
//           ->groupBy('expense_head_id')
//            ->get();
////        dd($voucher_details);
    }

    public function get_expense_in_budget_for_ministry($budget_sub_head_id, $fiscalYear, $office_id)
    {

        return $budgetByExpenseHead = Budget::where('office_id', $office_id)
            ->where('budget_sub_head', $budget_sub_head_id)
            ->groupBy('expense_head')
            ->orderBy('expense_head')
            ->get();
    }

    public function get_expense_in_budget_by_ministry($ministry_id)
    {

        return $budgetByExpenseHead = Budget::join('offices', 'budget.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->where('ministries.id', $ministry_id)
            ->groupBy('expense_head')
            ->orderBy('expense_head')
            ->get();
    }

    public function delete_by_id($voucher_details_id)
    {

        return $delete_voucher_details = VoucherDetail::where('id', $voucher_details_id)->delete();
    }

    public function get_liability_expenses_by_liability_id($data)
    {

//        $office_id = Auth::user()->office_id;
//       return $liabilityExpenseList = VoucherDetail::where('office_id',$office_id)
//            ->where('budget_sub_head_id',$data['program'])
//            ->where('expense_head_id',$data['khata'])
//            ->get();
//        dd($data['program']);
        $office_id = Auth::user()->office_id;
        return $liabilityExpenseList = VoucherDetail::
        join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.fiscal_year', '=', $data['fiscal_year'])
            ->where('voucher_details.office_id', '=', $office_id)
            ->where('voucher_details.budget_sub_head_id', $data['program'])
            ->where('voucher_details.expense_head_id', $data['khata'])
            ->get();
//            dd($liabilityExpenseList);
    }

    public function getPeskiDetailsByParty($data)
    {

//        dd($data);
        $office_id = Auth::user()->office_id;
        return $peskiDetails = VoucherDetail::where('office_id', $office_id)
            ->where('budget_sub_head_id', $data['program'])
            ->where('peski_paune_id', $data['khata'])
            ->get();
//        dd($peskiDetails);


    }

    public function gettotalProvinceChaluExpense($ministry_id)
    {

        return $expense = DB::table('voucher_details')
            ->join('budget', 'voucher_details.main_activity_id', '=', 'budget.id')
            ->join('offices', 'budget.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->where('vouchers.status', 1)
            ->where('ministries.id', $ministry_id)
            ->select('voucher_details.*')
            ->where('budget.source_type', 2)
            ->whereBetween('budget.expense_head', ['21000', '29000'])
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('dr_amount');
    }

    public function gettotalProvincePujiExpense($ministry_id)
    {

        return $expense = DB::table('voucher_details')
            ->join('budget', 'voucher_details.main_activity_id', '=', 'budget.id')
            ->join('offices', 'budget.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->where('vouchers.status', 1)
            ->where('ministries.id', $ministry_id)
            ->select('voucher_details.*')
            ->where('budget.source_type', 2)
            ->whereBetween('budget.expense_head', ['30000', '31511'])
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('voucher_details.dr_amount');
    }

    public function gettotalProvinceExpense($ministry_id)
    {

        return $expense = DB::table('voucher_details')
            ->join('budget', 'voucher_details.main_activity_id', '=', 'budget.id')
            ->join('offices', 'budget.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->where('vouchers.status', 1)
            ->where('ministries.id', $ministry_id)
            ->select('voucher_details.*')
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('voucher_details.dr_amount');
    }

    public function getTotalFederalChaluExpense($ministry_id)
    {

        return $expense = DB::table('voucher_details')
            ->join('budget', 'voucher_details.main_activity_id', '=', 'budget.id')
            ->join('offices', 'budget.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->where('vouchers.status', 1)
            ->where('ministries.id', $ministry_id)
            ->select('voucher_details.*')
            ->where('budget.source_type', 1)
            ->whereBetween('budget.expense_head', ['21000', '29000'])
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('voucher_details.dr_amount');
    }

    public function getTotalFederalPujiExpense($ministry_id)
    {

        return $expense = DB::table('voucher_details')
            ->join('budget', 'voucher_details.main_activity_id', '=', 'budget.id')
            ->join('offices', 'budget.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->where('vouchers.status', 1)
            ->where('ministries.id', $ministry_id)
            ->select('voucher_details.*')
            ->where('budget.source_type', 1)
            ->whereBetween('budget.expense_head', ['30000', '31511'])
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('voucher_details.dr_amount');
    }

    public function getTotalFederalExpense($ministry_id)
    {

        return $expense = DB::table('voucher_details')
            ->join('budget', 'voucher_details.main_activity_id', '=', 'budget.id')
            ->join('offices', 'budget.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->where('vouchers.status', 1)
            ->where('ministries.id', $ministry_id)
            ->select('voucher_details.*')
            ->where('budget.source_type', 1)
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('voucher_details.dr_amount');
    }

    public function getTotalExpense($ministry_id)
    {

        return $expense = DB::table('voucher_details')
            ->join('budget', 'voucher_details.main_activity_id', '=', 'budget.id')
            ->join('offices', 'budget.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->where('vouchers.status', 1)
            ->where('ministries.id', $ministry_id)
            ->select('voucher_details.*')
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('voucher_details.dr_amount');
    }


    public function totalProvinceChaluExpenseByOffice($office_id)
    {

        return $expense = DB::table('voucher_details')
            ->join('budget', 'voucher_details.main_activity_id', '=', 'budget.id')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->where('vouchers.status', 1)
            ->select('voucher_details.*')
            ->where('budget.source_type', 2)
            ->where('voucher_details.office_id', $office_id)
            ->whereBetween('budget.expense_head', ['21000', '29000'])
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('voucher_details.dr_amount');
    }

    public function totalFederalChaluExpenseByOffice($office_id)
    {

        return $expense = DB::table('voucher_details')
            ->join('budget', 'voucher_details.main_activity_id', '=', 'budget.id')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->where('vouchers.status', 1)
            ->select('voucher_details.*')
            ->where('budget.source_type', 1)
            ->where('voucher_details.office_id', $office_id)
            ->whereBetween('budget.expense_head', ['21000', '29000'])
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('voucher_details.dr_amount');
    }

    public function totalProvincePujiExpenseByOffice($office_id)
    {

        return $expense = DB::table('voucher_details')
            ->join('budget', 'voucher_details.main_activity_id', '=', 'budget.id')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->where('vouchers.status', 1)
            ->select('voucher_details.*')
            ->where('voucher_details.office_id', $office_id)
            ->where('budget.source_type', 2)
            ->whereBetween('budget.expense_head', ['30000', '31511'])
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('voucher_details.dr_amount');
    }

    public function totalFederalPujiExpenseByOffice($office_id)
    {

        return $expense = DB::table('voucher_details')
            ->join('budget', 'voucher_details.main_activity_id', '=', 'budget.id')
            ->select('voucher_details.*')
            ->where('voucher_details.office_id', $office_id)
            ->where('budget.source_type', 1)
            ->whereBetween('budget.expense_head', ['30000', '31511'])
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('dr_amount');
    }


//    Annual Report Source Wise

    public function getTotalExpenseByOffice($office_id)
    {
        return $totalExpense = VoucherDetail::

        join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->where('vouchers.status', 1)
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.office_id', $office_id)
            ->where('voucher_details.dr_or_cr', 1)
            ->where('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('voucher_details.dr_amount');
    }

    public function getTotalExpenseByMinistry($ministry_id)
    {

        return $totalExpense = VoucherDetail::

        join('offices', 'voucher_details.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->where('vouchers.status', 1)
            ->where('ministries.id', $ministry_id)
            ->where('dr_or_cr', 1)
            ->whereIn('ledger_type_id', ['1', '4'])
            ->sum('dr_amount');
    }


    public function getTotalExpenseUpToYesterday($ministry_id)
    {
        $today = date('Y-m-d H:i:s');
        $yesterday = date('d.m.Y', strtotime("-1 days"));
        return $totalExpense = VoucherDetail::
        join('offices', 'voucher_details.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->where('vouchers.status', 1)
            ->where('ministries.id', $ministry_id)
            ->where('voucher_details.date_english', '<=', $yesterday)
            ->where('voucher_details.dr_or_cr', 1)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('voucher_details.dr_amount');
    }

    public function gettotalTodayExpense($ministry_id)
    {
        $today = date('Y-m-d H:i:s');
        $yesterday = date('d.m.Y', strtotime("-1 days"));
        return $totalExpense = VoucherDetail::
        join('offices', 'voucher_details.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->where('vouchers.status', 1)
            ->where('ministries.id', $ministry_id)
            ->where('voucher_details.date_english', '=', $today)
            ->where('voucher_details.dr_or_cr', 1)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('voucher_details.dr_amount');
    }

    public function getTotalExpenseUpToToday($ministry_id)
    {
        $today = date('Y-m-d H:i:s');
        $yesterday = date('d.m.Y', strtotime("-1 days"));
        return $totalExpense = VoucherDetail::
        join('offices', 'voucher_details.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->where('vouchers.status', 1)
            ->where('ministries.id', $ministry_id)
            ->where('voucher_details.date_english', '<=', $today)
            ->where('voucher_details.dr_or_cr', 1)
            ->where('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('voucher_details.dr_amount');
    }

    public function getTotalAdvance($ministry_id)
    {
        return $expense = DB::table('voucher_details')
            ->join('offices', 'voucher_details.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->where('ministries.id', $ministry_id)
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->where('voucher_details.ledger_type_id', 4)
            ->sum('dr_amount');
    }


//    Office wise expense
    public function getTotalExpenseUpToYesterdayByOffice($office_id)
    {

        $today = date('Y-m-d H:i:s');
        $yesterday = date('d.m.Y', strtotime("-1 days"));
        return $totalExpense = VoucherDetail::

        join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.date_english', '<=', $yesterday)
            ->where('voucher_details.office_id', '=', $office_id)
            ->where('voucher_details.dr_or_cr', 1)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('voucher_details.dr_amount');
    }

    public function getTotalExpenseTodayByOffice($office_id)
    {
        $today = date('Y-m-d H:i:s');
        $yesterday = date('d.m.Y', strtotime("-1 days"));
        return $totalExpense = VoucherDetail::

        join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.date_english', '=', $today)
            ->where('voucher_details.office_id', '=', $office_id)
            ->where('voucher_details.dr_or_cr', 1)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('voucher_details.dr_amount');
    }

    public function getTotalExpenseUpToTodayByOffice($office_id)
    {

        $today = date('Y-m-d H:i:s');
        $yesterday = date('d.m.Y', strtotime("-1 days"));
        return $totalExpense = VoucherDetail::

        join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.date_english', '<=', $today)
            ->where('voucher_details.office_id', '=', $office_id)
            ->where('voucher_details.dr_or_cr', 1)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('voucher_details.dr_amount');
    }

    public function getTotalAdvanceByOffice($office_id)
    {
        $today = date('Y-m-d H:i:s');
        $yesterday = date('d.m.Y', strtotime("-1 days"));
        return $totalExpense = VoucherDetail::

        join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.office_id', '=', $office_id)
            ->where('voucher_details.dr_or_cr', 1)
            ->where('voucher_details.ledger_type_id', 4)
            ->sum('voucher_details.dr_amount');
    }


//    Budget Sub Head Wise

    public function getTotalExpenseByBudgetSubHead($office_id, $budget_sub_head_id)
    {

        return $totalExpense = VoucherDetail::

        join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.office_id', $office_id)
            ->where('voucher_details.budget_sub_head_id', $budget_sub_head_id)
            ->where('voucher_details.dr_or_cr', 1)
            ->where('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('voucher_details.dr_amount');
    }


    public function getTotalExpenseUpToYesterdayByBudgetSubHead($office_id, $budget_sub_head_id)
    {

        $today = date('Y-m-d H:i:s');
        $yesterday = date('d.m.Y', strtotime("-1 days"));
        return $totalExpense = VoucherDetail::

        join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.date_english', '<=', $yesterday)
            ->where('voucher_details.office_id', '=', $office_id)
            ->where('voucher_details.budget_sub_head_id', '=', $budget_sub_head_id)
            ->where('voucher_details.dr_or_cr', 1)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('voucher_details.dr_amount');
    }

    public function getTotalExpenseTodayByBudgetSubHead($office_id, $budget_sub_head_id)
    {

        $today = date('Y-m-d H:i:s');
        $yesterday = date('d.m.Y', strtotime("-1 days"));
        return $totalExpense = VoucherDetail::

        join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.date_english', '=', $today)
            ->where('voucher_details.office_id', '=', $office_id)
            ->where('voucher_details.budget_sub_head_id', '=', $budget_sub_head_id)
            ->where('voucher_details.dr_or_cr', 1)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('voucher_details.dr_amount');
    }

    public function getTotalExpenseUpToTodayByBudgetSubHead($office_id, $budget_sub_head_id)
    {

        $today = date('Y-m-d H:i:s');
        $yesterday = date('d.m.Y', strtotime("-1 days"));
        return $totalExpense = VoucherDetail::

        join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.date_english', '<=', $today)
            ->where('voucher_details.office_id', '=', $office_id)
            ->where('voucher_details.budget_sub_head_id', '=', $budget_sub_head_id)
            ->where('voucher_details.dr_or_cr', 1)
            ->whereIn('voucher_details.ledger_type_id', ['1', '4'])
            ->sum('voucher_details.dr_amount');
    }

    public function getTotalAdvanceByByBudgetSubHead($office_id, $budget_sub_head_id)
    {

        return $totalExpense = VoucherDetail::

        join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.office_id', $office_id)
            ->where('voucher_details.budget_sub_head_id', $budget_sub_head_id)
            ->where('voucher_details.dr_or_cr', 1)
            ->where('voucher_details.ledger_type_id', 4)
            ->sum('voucher_details.dr_amount');
    }

    public function getFirstChaumasikTotalExpense($office_id, $budgetSubHead, $fiscal_year)
    {

        return $expenseOnBudgetSubHead = DB::table('voucher_details')
            ->join('budget', 'voucher_details.main_Activity_id', '=', 'budget.id')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.fiscal_year', '=', $fiscal_year)
            ->where('voucher_details.office_id', '=', $office_id)
            ->where('voucher_details.budget_sub_head_id', '=', $budgetSubHead)
            ->whereBetween('voucher_details.month', array(1, 4))
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->whereIn('voucher_details.ledger_type_id', [1, 4])
            ->sum('voucher_details.dr_amount');
    }

    public function getSecondChaumasikTotalExpense($office_id, $budgetSubHead, $fiscal_year)
    {

        return $expenseOnBudgetSubHead = DB::table('voucher_details')
            ->join('budget', 'voucher_details.main_Activity_id', '=', 'budget.id')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.fiscal_year', '=', $fiscal_year)
            ->where('voucher_details.office_id', '=', $office_id)
            ->where('voucher_details.budget_sub_head_id', '=', $budgetSubHead)
            ->whereBetween('voucher_details.month', array(5, 8))
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->whereIn('voucher_details.ledger_type_id', [1, 4])
            ->sum('voucher_details.dr_amount');
    }

    public function getThirdChaumasikTotalExpense($office_id, $budgetSubHead, $fiscal_year)
    {

        return $expenseOnBudgetSubHead = DB::table('voucher_details')
            ->join('budget', 'voucher_details.main_Activity_id', '=', 'budget.id')
            ->join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->select('voucher_details.*')
            ->where('vouchers.status', 1)
            ->where('voucher_details.fiscal_year', '=', $fiscal_year)
            ->where('voucher_details.office_id', '=', $office_id)
            ->where('voucher_details.budget_sub_head_id', '=', $budgetSubHead)
            ->whereBetween('voucher_details.month', array(9, 12))
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->whereIn('voucher_details.ledger_type_id', [1, 4])
            ->sum('voucher_details.dr_amount');
    }

    public function getTotalAdvanceByActivity($activity_id, $party_id, $budget_sub_head_id)
    {
        $office_id = Auth::user()->office_id;
        return $expenseOnBudgetSubHead = VoucherDetail::
        join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->where('vouchers.status', 1)
            ->where('voucher_details.office_id', '=', $office_id)
            ->where('voucher_details.budget_sub_head_id', '=', $budget_sub_head_id)
            //            ->where('voucher_details.main_activity_id', '=', $activity_id)
            ->where('voucher_details.peski_paune_id', '=', $party_id)
            ->where('voucher_details.dr_or_cr', '=', 1)
            ->where('voucher_details.ledger_type_id', 4)
            ->sum('voucher_details.dr_amount');
    }


    public function getTotalClearedAdvanceByActivity($budget_sub_head_id, $activity_id, $party_id)
    {

        $office_id = Auth::user()->office_id;
        return $expenseOnBudgetSubHead = VoucherDetail::
        join('vouchers', 'voucher_details.journel_id', '=', 'vouchers.id')
            ->where('vouchers.status', 1)
            ->where('voucher_details.office_id', '=', $office_id)
            ->where('voucher_details.budget_sub_head_id', '=', $budget_sub_head_id)
            ->where('voucher_details.main_activity_id', '=', $activity_id)
            ->where('voucher_details.peski_paune_id', '=', $party_id)
            ->where('voucher_details.dr_or_cr', '=', 2)
            ->where('voucher_details.ledger_type_id', 9)
            ->sum('voucher_details.dr_amount');
    }

    public function getVouchersByBudgetSubHeadAndParty($budget_sub_head_id, $activity_id, $party_id)
    {


        $voucherDetails = VoucherDetail::where('budget_sub_head_id', $budget_sub_head_id)
            ->where('main_activity_id', $activity_id)
            ->where('peski_paune_id', $party_id)
            ->whereIn('dr_or_cr', ['1', '2'])
            ->whereIn('ledger_type_id', ['4', '9'])
            ->get();
        $peskiVoucherDetails = $voucherDetails->where('dr_or_cr', 1)->where('ledger_type_id', '4');
        $clearanceVoucherDetails = $voucherDetails->where('dr_or_cr', 2)->where('ledger_type_id', '9');
        $voucher_details = [];
        foreach ($peskiVoucherDetails as $peski) {

            $temp = [];
            $temp['id'] = $peski->id;
            $temp['date'] = $peski->voucher->data_nepali;
            $temp['voucher_no'] = $peski->voucher->jv_number;
            $temp['peski'] = $peski->dr_amount;
            $clearance_amount = $clearanceVoucherDetails->where('voucher_details_id', $peski->id)->sum('cr_amount');
            $remaining = $peski->dr_amount;
            if ($clearance_amount) {
                $remaining = $remaining - $clearance_amount;
            }
            $temp['baki'] = $remaining;
            array_push($voucher_details, $temp);
        }
        return $voucher_details;

    }

    public function createBhuktaniVoucher($data, $voucher_id, $encodedBhuktaniId)
    {

        $voucherBhuktani = New BhuktaniVoucher();
        $voucherBhuktani->office_id = Auth::user()->office_id;
        $voucherBhuktani->budget_sub_head_id = $data['budget_sub_head'];
        $voucherBhuktani->voucher_id = $voucher_id;
        $voucherBhuktani->bhuktani_id = $encodedBhuktaniId;
        $voucherBhuktani->date_english = date('Y-m-d H:i:s');
        return $voucherBhuktani->save();


    }


//    Update budget_id by budget_details_id

    public function UpdateMainActivityId($activity_id, $budget_detail_id)
    {

//        dd($activity_id);
//        $voucher_details = new VoucherDetail();
        $voucher_details = VoucherDetail::where('main_activity_id', $activity_id)->get();
        foreach ($voucher_details as $voucher_detail) {
            $voucher_detail->main_activity_id = $budget_detail_id;
            $voucher_detail->save();
            echo "Succefully Updated";
            echo "Voucher Detail = " . $voucher_detail->id . ' from budget ' . $activity_id . ' to budget_detail_id = ' . $budget_detail_id;
            echo "===============================================================\r\n";

        }
        return true;


    }


}