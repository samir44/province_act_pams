<?php

namespace App\Repositories;

use App\Models\Budget;
use App\Models\Akhtiyari;
use App\Models\BudgetDetails;
use App\Models\VoucherDetail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BudgetRepositoryEloquent implements BudgetRepository
{
    public function create($attributes)
    {


        $budget = new Budget();
        $budget->expense_center = $attributes['expense_center'];
        $budget->budget_sub_head = $attributes['budget_sub_head'];
        $budget->akhtiyari_type = $attributes['akhtiyari'];
//        $budget->area = $attributes['area'];
//        $budget->sub_area = $attributes['sub_area'];
//        $budget->main_program = $attributes['main_program'];
//        $budget->general_activity = $attributes['general_activity'];
        $budget->activity = $attributes['main_activity'];
        $budget->expense_head_id = $attributes['expense_head'];
        $budget->expense_head = $attributes['expense_head_code'];
//        $budget->target_group = $attributes['target_group'];
        $budget->unit = $attributes['unit'];
//        $budget->per_unit_cost = $attributes['per_unit_cost'];
        $budget->total_unit = $attributes['total_unit'];
        $budget->total_budget = (int)$attributes['total_budget'];
        $budget->first_quarter_unit = $attributes['first_quarter'];
        $budget->second_quarter_unit = $attributes['second_quarter'];
        $budget->third_quarter_unit = $attributes['third_quarter'];
        $budget->first_quarter_budget = $attributes['first_total_budget'];
        $budget->first_chaimasik_bhar = $attributes['firstChaumasikBharHidden'];
        $budget->second_quarter_budget = $attributes['second_total_budget'];
        $budget->second_chaimasik_bhar = $attributes['secondChaumasikBharHidden'];
        $budget->third_quarter_budget = $attributes['third_total_budget'];
        $budget->third_chaimasik_bhar = $attributes['thirdChaumasikBharHidden'];
        $budget->source_type = $attributes['source_type'];
        $budget->source_level = $attributes['source_level'];
        $budget->source = $attributes['source'];
        $budget->medium = $attributes['medium'];
        $budget->office_id = Auth::user()->office->id;
        $budget->user_id = Auth::user()->id;
        $budget->status = 1;
        $budget->fiscal_year = $attributes['fiscal_year'];
        if (array_key_exists('contenjency_check_box', $attributes)) {
            $budget->is_contenjency = 1;
            $budget->contenjency = $attributes['hidden_contenjency_percent'];
        }
        $budget->date = date('Y-m-d H:i:s');
        $budget->save();
        $attributes['budget_id'] = $budget->id;

        if (array_key_exists('contenjency_check_box', $attributes)) {

            for ($x = 1; $x <= 2; $x++) {
                $amount = $attributes['total_budget'] * ($attributes['hidden_contenjency_percent'] * 0.01);
                $sub_activity = $attributes['main_activity'] . " - " . "कन्टेन्जेन्सी";
                if ($x == 1) {

                    $remain = 100 - $attributes['hidden_contenjency_percent'];
                    $amount = $attributes['total_budget'] * ($remain * 0.01);
                    $sub_activity = $attributes['main_activity'];
                }
                $budgetDetals = new BudgetDetails();
                $budgetDetals->office_id = Auth::user()->office->id;
                $budgetDetals->budget_sub_head_id = $attributes['budget_sub_head'];
                $budgetDetals->budget_id = $attributes['budget_id'];
                $budgetDetals->sub_activity = $sub_activity;
                $budgetDetals->expense_head_id = $attributes['expense_head'];
                $budgetDetals->unit = $attributes['unit'];
                $budgetDetals->total_budget = (float)$amount;
                $budgetDetals->first_quarter_unit = $attributes['first_quarter'];
                $budgetDetals->second_quarter_unit = $attributes['second_quarter'];
                $budgetDetals->third_quarter_unit = $attributes['third_quarter'];
                $budgetDetals->first_quarter_budget = $attributes['first_total_budget'];
                $budgetDetals->first_chaimasik_bhar = $attributes['firstChaumasikBharHidden'];
                $budgetDetals->second_quarter_budget = $attributes['second_total_budget'];
                $budgetDetals->second_chaimasik_bhar = $attributes['secondChaumasikBharHidden'];
                $budgetDetals->third_quarter_budget = $attributes['third_total_budget'];
                $budgetDetals->third_chaimasik_bhar = $attributes['thirdChaumasikBharHidden'];
                $budgetDetals->source_type = $attributes['source_type'];
                $budgetDetals->source_level = $attributes['source_level'];
                $budgetDetals->source = $attributes['source'];
                $budgetDetals->medium = $attributes['medium'];
                $budgetDetals->user_id = Auth::user()->id;
                $budgetDetals->status = 1;
                $budgetDetals->fiscal_year = $attributes['fiscal_year'];
                $budgetDetals->contenjency = $attributes['hidden_contenjency_percent'];
                $budgetDetals->date = date('Y-m-d H:i:s');
                $budgetDetals->save();


            }
        } else {

            $budgetDetals = new BudgetDetails();

            $budgetDetals->office_id = Auth::user()->office->id;
            $budgetDetals->budget_sub_head_id = $attributes['budget_sub_head'];
            $budgetDetals->budget_id = $attributes['budget_id'];
            $budgetDetals->sub_activity = $attributes['main_activity'];
            $budgetDetals->expense_head_id = $attributes['expense_head'];
            $budgetDetals->unit = $attributes['unit'];
            $budgetDetals->total_budget = $attributes['total_budget'];
            $budgetDetals->first_quarter_unit = $attributes['first_quarter'];
            $budgetDetals->second_quarter_unit = $attributes['second_quarter'];
            $budgetDetals->third_quarter_unit = $attributes['third_quarter'];
            $budgetDetals->first_quarter_budget = $attributes['first_total_budget'];
            $budgetDetals->first_chaimasik_bhar = $attributes['firstChaumasikBharHidden'];
            $budgetDetals->second_quarter_budget = $attributes['second_total_budget'];
            $budgetDetals->second_chaimasik_bhar = $attributes['secondChaumasikBharHidden'];
            $budgetDetals->third_quarter_budget = $attributes['third_total_budget'];
            $budgetDetals->third_chaimasik_bhar = $attributes['thirdChaumasikBharHidden'];
            $budgetDetals->source_type = $attributes['source_type'];
            $budgetDetals->source_level = $attributes['source_level'];
            $budgetDetals->source = $attributes['source'];
            $budgetDetals->medium = $attributes['medium'];
            $budgetDetals->user_id = Auth::user()->id;
            $budgetDetals->status = 1;
            $budgetDetals->fiscal_year = $attributes['fiscal_year'];
            $budgetDetals->date = date('Y-m-d H:i:s');
            $budgetDetals->save();
        }
        return $budgetDetals;
    }


    public function createTemp($attributes)
    {

        $budget = new Budget();
        $budget->expense_center = $attributes['office_id'];
        $budget->budget_sub_head = $attributes['budget_sub_head_id'];
        $budget->akhtiyari_type = $attributes['akhtiyar_id'];
//        $budget->area = $attributes['area'];
//        $budget->sub_area = $attributes['sub_area'];
//        $budget->main_program = $attributes['main_program'];
//        $budget->general_activity = $attributes['general_activity'];
        $budget->activity = $attributes['main_activity_ndesc4'];
        $budget->expense_head_id = $attributes['expense_head_id'];
        $budget->expense_head = $attributes['economic_code5'];
//        $budget->target_group = $attributes['target_group'];
        $budget->unit = 1;
//        $budget->per_unit_cost = $attributes['per_unit_cost'];
        $budget->total_unit = 1;
        $budget->total_budget = (float)$attributes['amount'] * 1000;
        $budget->first_quarter_unit = NULL;
        $budget->second_quarter_unit = NULL;
        $budget->third_quarter_unit = 1;
        $budget->first_quarter_budget = NULL;
        $budget->first_chaimasik_bhar = NULL;
        $budget->second_quarter_budget = NULL;
        $budget->second_chaimasik_bhar = NULL;
        $budget->third_quarter_budget = (float)$attributes['amount'] * 1000;
        $budget->third_chaimasik_bhar = NULL;
        $budget->source_type = $attributes['source_type'];
        $budget->source_level = $attributes['source_level'];
        $budget->source = $attributes['source'];
        $budget->medium = $attributes['medium'];
        $budget->office_id = $attributes['office_id'];
        $budget->user_id = 1;
        $budget->status = 1;
        $budget->fiscal_year = "2076/77";
        $budget->date = date('Y-m-d H:i:s');
        $budget->project_code = $attributes['project_code'];
        $budget->sub_project_code = $attributes['sub_project_code'];
        $budget->component_code = $attributes['component_code'];
        $budget->component_ndesc = $attributes['component_ndesc'];
        $budget->activity_code = $attributes['activity_code'];
        $budget->save();
        $attributes['budget_id'] = $budget->id;
        $expense_type = (substr($attributes['economic_code5'], 0, 1));

        if ($attributes['economic_code5'] == '31112' or $attributes['economic_code5'] == '31113' or $attributes['economic_code5'] == '31151' or $attributes['economic_code5'] == '31153' or $attributes['economic_code5'] == '31154' or $attributes['economic_code5'] == '31155' or $attributes['economic_code5'] == '31156' or $attributes['economic_code5'] == '31159') {
            for ($x = 1; $x <= 2; $x++) {
                $amount = $attributes['amount'] * 40;
                $sub_activity = $attributes['main_activity_ndesc4'] . " - " . "कन्टेन्जेन्सी";
                if ($x == 1) {
                    $amount = $attributes['amount'] * 960;
                    $sub_activity = $attributes['main_activity_ndesc4'];
                }
                $budgetDetals = new BudgetDetails();
                $budgetDetals->office_id = $attributes['office_id'];
                $budgetDetals->budget_sub_head_id = $attributes['budget_sub_head_id'];
                $budgetDetals->budget_id = $attributes['budget_id'];
                $budgetDetals->sub_activity = $sub_activity;
                $budgetDetals->expense_head_id = $attributes['expense_head_id'];
                $budgetDetals->unit = 1;
                $program_amount = $amount;
                $budgetDetals->total_budget = (float)$program_amount;
                $budgetDetals->first_quarter_unit = NULL;
                $budgetDetals->second_quarter_unit = NULL;
                $budgetDetals->third_quarter_unit = 1;
                $budgetDetals->first_quarter_budget = NULL;
                $budgetDetals->first_chaimasik_bhar = NULL;
                $budgetDetals->second_quarter_budget = NULL;
                $budgetDetals->second_chaimasik_bhar = NULL;
                $budgetDetals->third_quarter_budget = (float)$program_amount;
                $budgetDetals->third_chaimasik_bhar = NULL;
                $budgetDetals->source_type = $attributes['source_type'];
                $budgetDetals->source_level = $attributes['source_level'];
                $budgetDetals->source = $attributes['source'];
                $budgetDetals->medium = $attributes['medium'];
                $budgetDetals->office_id = $attributes['office_id'];
                $budgetDetals->user_id = 1;
                $budgetDetals->status = 1;
                $budgetDetals->fiscal_year = "2076/77";
                $budgetDetals->date = date('Y-m-d H:i:s');
                $budgetDetals->save();


            }

        } else {

            $budgetDetals = new BudgetDetails();
            $budgetDetals->office_id = $attributes['office_id'];
            $budgetDetals->budget_sub_head_id = $attributes['budget_sub_head_id'];
            $budgetDetals->budget_id = $attributes['budget_id'];
            $budgetDetals->sub_activity = $attributes['main_activity_ndesc4'];
            $budgetDetals->expense_head_id = $attributes['expense_head_id'];
            $budgetDetals->unit = 1;
            $budgetDetals->total_budget = (float)$attributes['amount'] * 1000;
            $budgetDetals->first_quarter_unit = NULL;
            $budgetDetals->second_quarter_unit = NULL;
            $budgetDetals->third_quarter_unit = 1;
            $budgetDetals->first_quarter_budget = NULL;
            $budgetDetals->first_chaimasik_bhar = NULL;
            $budgetDetals->second_quarter_budget = NULL;
            $budgetDetals->second_chaimasik_bhar = NULL;
            $budgetDetals->third_quarter_budget = (float)$attributes['amount'] * 1000;
            $budgetDetals->third_chaimasik_bhar = NULL;
            $budgetDetals->source_type = $attributes['source_type'];
            $budgetDetals->source_level = $attributes['source_level'];
            $budgetDetals->source = $attributes['source'];
            $budgetDetals->medium = $attributes['medium'];
            $budgetDetals->office_id = $attributes['office_id'];
            $budgetDetals->user_id = 1;
            $budgetDetals->status = 1;
            $budgetDetals->fiscal_year = "2076/77";
            $budgetDetals->date = date('Y-m-d H:i:s');
            $budgetDetals->save();
        }
        return $budgetDetals;
    }


    public function createBudgetForOperationalActivity($attributes, $total_budget)
    {

        $budget = new Budget();
        $budget->expense_center = $attributes['expense_center'];
        $budget->budget_sub_head = $attributes['budget_sub_head'];
        $budget->akhtiyari_type = $attributes['akhtiyari_type'];
        $budget->activity = $attributes['activity'];
        $budget->expense_head_id = $attributes['expense_head_id'];
        $budget->expense_head = $attributes['expense_head'];
        $budget->unit = $attributes['unit'];
        $budget->activity = "ईन्धन कार्यालय प्रायोजन";
        $budget->total_unit = $attributes['total_unit'];
        $budget->total_budget = (float)$total_budget;
        $budget->first_quarter_unit = NULL;
        $budget->second_quarter_unit = NULL;
        $budget->third_quarter_unit = 1;
        $budget->first_quarter_budget = NULL;
        $budget->first_chaimasik_bhar = NULL;
        $budget->second_quarter_budget = NULL;
        $budget->second_chaimasik_bhar = NULL;
        $budget->third_quarter_budget = (float)$total_budget;
        $budget->third_chaimasik_bhar = NULL;
        $budget->source_type = $attributes['source_type'];
        $budget->source_level = $attributes['source_level'];
        $budget->source = $attributes['source'];
        $budget->medium = $attributes['medium'];
        $budget->office_id = $attributes['office_id'];
        $budget->user_id = Auth::user()->id;
        $budget->status = 1;
        $budget->project_code = $attributes['project_code'];
        $budget->sub_project_code = $attributes['sub_project_code'];
        $budget->component_code = $attributes['component_code'];
        $budget->component_ndesc = $attributes['component_ndesc'];
        $budget->activity_code = "1.1.1";   //for 21111
        $budget->fiscal_year = $attributes['fiscal_year'];
        $budget->is_contenjency = null;
        $budget->contenjency = null;
        $budget->date = $attributes['date'];
        $budget->save();
        return $budget->id;

    }

//    changes to budgetDetails
    public function get_by_budget_sub_head_and_office_id($office_id, $budget_sub_head_id)
    {

        return $main_actvities = BudgetDetails::where('office_id', $office_id)
            ->where('budget_sub_head_id', $budget_sub_head_id)
            ->with(['budgetArea', 'expense_head_by_id'])
            ->orderBy('expense_head_id')
            ->get();
    }

//    changed to budgetDetails
    public function get_by_id($id)
    {
        return BudgetDetails::where('id', $id)->with('expense_head_by_id')->get();
    }

//    माथिको र यो function लाई पछि मिलाउने गरि
    public function get_by_id_for_pre_bhuktani($id)
    {
        return BudgetDetails::findorfail($id);
    }

    public function get_total_budget_by_akhtiyari($akhtiyari_id)
    {

        return $totalBudget = Budget::where('akhtiyari_type', $akhtiyari_id)->sum('total_budget');
    }

    public function get_total_akhtiyari_by_budget_sub_head($budget_sub_head)
    {

        return $totalAkhtiyari = Akhtiyari::where('budget_sub_head_id', $budget_sub_head)->sum('amount');
    }

//    changed to budgetDetails
    public function get_total_budget_by_Activity_id($id)
    {

        return $total_budget_by = BudgetDetails::where('id', $id)->sum('total_budget');
    }

    public function get_budget_details_by_id($activity_id)
    {

        return $budget_details = Budget::findorfail($activity_id);
    }

    public function update($attribute, $id)
    {

//        dd($attribute);
        $budget = Budget::findorfail($id);
        if (array_key_exists('area', $attribute)) {
            $budget->area = $attribute['area'];
        }
        if (array_key_exists('sub_area', $attribute)) {
            $budget->sub_area = $attribute['sub_area'];
        }
        if (array_key_exists('main_program', $attribute)) {
            $budget->main_program = $attribute['main_program'];
        }
        if (array_key_exists('general_activity', $attribute)) {
            $budget->general_activity = $attribute['general_activity'];
        }
        if (array_key_exists('main_activity', $attribute)) {
            $budget->activity = $attribute['main_activity'];
        }
        if (array_key_exists('expense_head_code', $attribute)) {
            $budget->expense_head = $attribute['expense_head_code'];
        }

        if (array_key_exists('expense_head', $attribute)) {
            $budget->expense_head_id = $attribute['expense_head'];
        }

        if (array_key_exists('target_group', $attribute)) {
            $budget->target_group = $attribute['target_group'];
        }
        if (array_key_exists('unit', $attribute)) {
            $budget->unit = $attribute['unit'];
        }
        if (array_key_exists('per_unit_cost', $attribute)) {
            $budget->per_unit_cost = $attribute['per_unit_cost'];
        }
        if (array_key_exists('total_unit', $attribute)) {
            $budget->total_unit = $attribute['total_unit'];
        }
        if (array_key_exists('total_budget', $attribute)) {
            $budget->total_budget = $attribute['total_budget'];
        }
        if (array_key_exists('first_quarter_unit', $attribute)) {
            $budget->first_quarter_unit = $attribute['first_quarter_unit'];
        }
        if (array_key_exists('second_quarter_unit', $attribute)) {
            $budget->second_quarter_unit = $attribute['second_quarter_unit'];
        }
        if (array_key_exists('third_quarter_unit', $attribute)) {
            $budget->third_quarter_unit = $attribute['third_quarter_unit'];
        }

        if (array_key_exists('first_total_budget', $attribute)) {
            $budget->first_quarter_budget = $attribute['first_total_budget'];
        }
        if (array_key_exists('second_total_budget', $attribute)) {
            $budget->second_quarter_budget = $attribute['second_total_budget'];
        }
        if (array_key_exists('third_total_budget', $attribute)) {
            $budget->third_quarter_budget = $attribute['third_total_budget'];
        }
        if (array_key_exists('source_type', $attribute)) {
            $budget->source_type = $attribute['source_type'];
        }
        if (array_key_exists('source_level', $attribute)) {
            $budget->source_level = $attribute['source_level'];
        }
        if (array_key_exists('source', $attribute)) {
            $budget->source = $attribute['source'];
        }
        if (array_key_exists('medium', $attribute)) {
            $budget->medium = $attribute['medium'];
        }

        if (array_key_exists('akhtiyari_type', $attribute)) {
            $budget->akhtiyari_type = $attribute['akhtiyari_type'];
        }
        if (array_key_exists('firstChaumasikBharHidden', $attribute)) {
            $budget->first_chaimasik_bhar = $attribute['firstChaumasikBharHidden'];
        }
        if (array_key_exists('secondChaumasikBharHidden', $attribute)) {
            $budget->second_chaimasik_bhar = $attribute['secondChaumasikBharHidden'];
        }
        if (array_key_exists('thirdChaumasikBharHidden', $attribute)) {
            $budget->third_chaimasik_bhar = $attribute['thirdChaumasikBharHidden'];
        }
        if (array_key_exists('thirdChaumasikBharHidden', $attribute)) {
            $budget->third_chaimasik_bhar = $attribute['thirdChaumasikBharHidden'];
        }
        if (array_key_exists('contenjency_check_box', $attribute)) {
            if (array_key_exists('thirdChaumasikBharHidden', $attribute)) {
                $budget->is_contenjency = 1;
                $budget->contenjency = $attribute['hidden_contenjency_percent'];
            }
        }

        $budget->save();

        if (array_key_exists('contenjency_check_box', $attribute)) {


            $budgetDetals = BudgetDetails::where('budget_id', $id)->get();
//            dd($attribute);
            foreach ($budgetDetals as $index => $budgetDetal) {

                $amount = $attribute['total_budget'] * ($attribute['hidden_contenjency_percent'] * 0.01);
                $sub_activity = $attribute['main_activity'] . " - " . "कन्टेन्जेन्सी";

                if (++$index == 1) {
                    $remain = 100 - $attribute['hidden_contenjency_percent'];
                    $amount = $attribute['total_budget'] * ($remain * 0.01);
                    $sub_activity = $attribute['main_activity'];
                }
                if (array_key_exists('main_activity', $attribute)) {
                    $budgetDetal->sub_activity = $sub_activity;
                }

                if (array_key_exists('expense_head', $attribute)) {
                    $budgetDetal->expense_head_id = $attribute['expense_head'];
                }

                if (array_key_exists('unit', $attribute)) {
                    $budgetDetal->unit = $attribute['unit'];
                }

                if (array_key_exists('total_unit', $attribute)) {
                    $budgetDetal->total_unit = $attribute['total_unit'];
                }
                if (array_key_exists('total_budget', $attribute)) {
                    $budgetDetal->total_budget = (float)$amount;
                }
                if (array_key_exists('first_quarter_unit', $attribute)) {
                    $budgetDetal->first_quarter_unit = $attribute['first_quarter_unit'];
                }
                if (array_key_exists('second_quarter_unit', $attribute)) {
                    $budgetDetal->second_quarter_unit = $attribute['second_quarter_unit'];
                }
                if (array_key_exists('third_quarter_unit', $attribute)) {
                    $budgetDetal->third_quarter_unit = $attribute['third_quarter_unit'];
                }

                if (array_key_exists('first_total_budget', $attribute)) {
                    $budgetDetal->first_quarter_budget = $attribute['first_total_budget'];
                }
                if (array_key_exists('second_total_budget', $attribute)) {
                    $budgetDetal->second_quarter_budget = $attribute['second_total_budget'];
                }
                if (array_key_exists('third_total_budget', $attribute)) {
                    $budgetDetal->third_quarter_budget = $attribute['third_total_budget'];
                }
                if (array_key_exists('source_type', $attribute)) {
                    $budgetDetal->source_type = $attribute['source_type'];
                }
                if (array_key_exists('source_level', $attribute)) {
                    $budgetDetal->source_level = $attribute['source_level'];
                }
                if (array_key_exists('source', $attribute)) {
                    $budgetDetal->source = $attribute['source'];
                }
                if (array_key_exists('medium', $attribute)) {
                    $budgetDetal->medium = $attribute['medium'];
                }

                if (array_key_exists('akhtiyari_type', $attribute)) {
                    $budgetDetal->akhtiyari_type = $attribute['akhtiyari_type'];
                }
                if (array_key_exists('firstChaumasikBharHidden', $attribute)) {
                    $budgetDetal->first_chaimasik_bhar = $attribute['firstChaumasikBharHidden'];
                }
                if (array_key_exists('secondChaumasikBharHidden', $attribute)) {
                    $budgetDetal->second_chaimasik_bhar = $attribute['secondChaumasikBharHidden'];
                }
                if (array_key_exists('thirdChaumasikBharHidden', $attribute)) {
                    $budgetDetal->third_chaimasik_bhar = $attribute['thirdChaumasikBharHidden'];
                }

                $budgetDetal->save();
            }

        } else {

            $budgetDetals = BudgetDetails::where('budget_id', $id)->first();

            if (array_key_exists('main_activity', $attribute)) {
                $budgetDetals->sub_activity = $attribute['main_activity'];
            }

            if (array_key_exists('expense_head', $attribute)) {
                $budgetDetals->expense_head_id = $attribute['expense_head'];
            }

            if (array_key_exists('unit', $attribute)) {
                $budgetDetals->unit = $attribute['unit'];
            }

            if (array_key_exists('total_unit', $attribute)) {
                $budgetDetals->total_unit = $attribute['total_unit'];
            }
            if (array_key_exists('total_budget', $attribute)) {
                $budgetDetals->total_budget = $attribute['total_budget'];
            }
            if (array_key_exists('first_quarter_unit', $attribute)) {
                $budgetDetals->first_quarter_unit = $attribute['first_quarter_unit'];
            }
            if (array_key_exists('second_quarter_unit', $attribute)) {
                $budgetDetals->second_quarter_unit = $attribute['second_quarter_unit'];
            }
            if (array_key_exists('third_quarter_unit', $attribute)) {
                $budgetDetals->third_quarter_unit = $attribute['third_quarter_unit'];
            }

            if (array_key_exists('first_total_budget', $attribute)) {
                $budgetDetals->first_quarter_budget = $attribute['first_total_budget'];
            }
            if (array_key_exists('second_total_budget', $attribute)) {
                $budgetDetals->second_quarter_budget = $attribute['second_total_budget'];
            }
            if (array_key_exists('third_total_budget', $attribute)) {
                $budgetDetals->third_quarter_budget = $attribute['third_total_budget'];
            }
            if (array_key_exists('source_type', $attribute)) {
                $budgetDetals->source_type = $attribute['source_type'];
            }
            if (array_key_exists('source_level', $attribute)) {
                $budgetDetals->source_level = $attribute['source_level'];
            }
            if (array_key_exists('source', $attribute)) {
                $budgetDetals->source = $attribute['source'];
            }
            if (array_key_exists('medium', $attribute)) {
                $budgetDetals->medium = $attribute['medium'];
            }

            if (array_key_exists('akhtiyari_type', $attribute)) {
                $budgetDetals->akhtiyari_type = $attribute['akhtiyari_type'];
            }
            if (array_key_exists('firstChaumasikBharHidden', $attribute)) {
                $budgetDetals->first_chaimasik_bhar = $attribute['firstChaumasikBharHidden'];
            }
            if (array_key_exists('secondChaumasikBharHidden', $attribute)) {
                $budgetDetals->second_chaimasik_bhar = $attribute['secondChaumasikBharHidden'];
            }
            if (array_key_exists('thirdChaumasikBharHidden', $attribute)) {
                $budgetDetals->third_chaimasik_bhar = $attribute['thirdChaumasikBharHidden'];
            }

            $budgetDetals->save();
        }
        return $budgetDetals;
    }

    public function createAkhtiyari($attributes)
    {

//        dd($attributes);
        $office_id = Auth::user()->office_id;
        $akhtiyari = new Akhtiyari();
        $akhtiyari->budget_sub_head_id = $attributes['budget_sub_head'];
        $akhtiyari->office_id = $office_id;
        $akhtiyari->akhtiyari_type = $attributes['akhtiyari_type'];
        $akhtiyari->amount = $attributes['amount'];
        $akhtiyari->source_type = $attributes['source_type'];
        $akhtiyari->source_level = $attributes['source_level'];
        $akhtiyari->source = $attributes['source'];
        $akhtiyari->medium = $attributes['medium'];
        $akhtiyari->date_nepali_roman = $attributes['date_in_roman'];
        $akhtiyari->date_english = date('Y-m-d H:i:s');
        $akhtiyari->fiscal_year = $attributes['fiscal_year'];
        $akhtiyari->detail = $attributes['detail'];
        return $akhtiyari->save();


    }

    public function createAkhtiyariTemp($attributes)
    {

        $akhtiyari = new Akhtiyari();
        $akhtiyari->office_id = $attributes['office_id'];
        $akhtiyari->budget_sub_head_id = $attributes['budget_sub_head_id'];
        $akhtiyari->akhtiyari_type = 1;
        $akhtiyari->amount = $attributes['akhtiyari_amount'] * 1000;
        $akhtiyari->source_type = $attributes['source_type'];
        $akhtiyari->source_level = $attributes['source_level'];
        $akhtiyari->source = $attributes['source'];
        $akhtiyari->medium = $attributes['medium'];
        $akhtiyari->date_nepali_roman = "2076-8-25";
        $akhtiyari->date_english = date('Y-m-d H:i:s');
        $akhtiyari->fiscal_year = "2076/77";
        $akhtiyari->detail = "सुरु अख्तियारी";
        $akhtiyari->save();
        return $akhtiyari->id;

    }

    public function getAkhtiyariByBudgetSubHeadId($budget_sub_head)
    {

        $office_id = Auth::user()->office_id;
        return $akhtiyrai = Akhtiyari::where('office_id', $office_id)
            ->where('budget_sub_head_id', $budget_sub_head)->with(['program', 'source_type', 'source_level', 'source', 'medium', 'budget'])->get();

    }

    public function get_by_akhtiyari_id($akhtiyari_id)
    {

        return $activities = Budget::where('akhtiyari_type', $akhtiyari_id)->with(['budgetArea', 'expense_head_by_id', 'getVoucherDetails'])->get();
    }

    public function updateAkhtiyari($attribute, $id)
    {

        $akhtiyari = Akhtiyari::findorfail($id);
//        dd($attribute);

        if (array_key_exists('budget_sub_head', $attribute)) {
            $akhtiyari->budget_sub_head_id = $attribute['budget_sub_head'];
        }

        if (array_key_exists('akhtiyar_type', $attribute)) {
            $akhtiyari->akhtiyari_type = $attribute['akhtiyar_type'];
        }

        if (array_key_exists('amount', $attribute)) {
            $akhtiyari->amount = $attribute['amount'];
        }

        if (array_key_exists('source_type', $attribute)) {
            $akhtiyari->source_type = $attribute['source_type'];
        }

        if (array_key_exists('source_level', $attribute)) {
            $akhtiyari->source_level = $attribute['source_level'];
        }

        if (array_key_exists('source', $attribute)) {
            $akhtiyari->source = $attribute['source'];
        }

        if (array_key_exists('medium', $attribute)) {
            $akhtiyari->medium = $attribute['medium'];
        }

        if (array_key_exists('date_in_roman', $attribute)) {
            $akhtiyari->date_nepali_roman = $attribute['date_in_roman'];
        }

        if (array_key_exists('detail', $attribute)) {
            $akhtiyari->detail = $attribute['detail'];
        }

        $akhtiyari->date_english = date('Y-m-d H:i:s');

        return $akhtiyari->save();
    }

    public function deleteAkhtiyariById($activity_id)
    {

        return $deleteAkhtiyari = Akhtiyari::where('id', $activity_id)->delete();
    }

    public function delete_activity_by_id($id)
    {

        $check_voucher_exists = VoucherDetail::where('main_activity_id', $id)->get();
        return $delete_Activity = Budget::where('id', $id)->delete();

    }

    public function get_akhtiyari_by_akhtiyari_id($akhtiyari)
    {

        return $akhtiyari = Akhtiyari::findorfail($akhtiyari);
    }

    public function get_activities_by_budget_sub_head_and_budget_type($budget_sub_head_id, $office_id)
    {

        return $activities = Budget::where('budget_sub_head', $budget_sub_head_id)
            ->where('office_id', $office_id)->orderBy('expense_head')->get();
    }

    public function getTotalProvinceChaluBudget($ministry_id)
    {

        return $totalProviceChaluBudget = DB::table('budget')
            ->join('offices', 'budget.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->where('ministries.id', $ministry_id)
            ->whereBetween('budget.expense_head', ['21000', '29000'])
            ->where('budget.source_type', 2)
            ->sum('total_budget');
    }

    public function getTotalProvincePujiBudget($ministry_id)
    {

        return $totalProviceChaluBudget = DB::table('budget')
            ->join('offices', 'budget.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->where('ministries.id', $ministry_id)
            ->whereBetween('budget.expense_head', ['30000', '31511'])
            ->where('budget.source_type', 2)
            ->sum('total_budget');
    }

    public function getTotalProvinceBudget($ministry_id)
    {

        return $totalProviceChaluBudget = DB::table('budget')
            ->join('offices', 'budget.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->where('ministries.id', $ministry_id)
            ->where('budget.source_type', 2)
            ->sum('total_budget');
    }

    public function getTotalFederalChaluBudget($ministry_id)
    {

        return $totalProviceChaluBudget = DB::table('budget')
            ->join('offices', 'budget.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->where('ministries.id', $ministry_id)
            ->whereBetween('budget.expense_head', ['21000', '29000'])
            ->where('budget.source_type', 1)
            ->sum('total_budget');
    }

    public function getTotalFederalPujiBudget($ministry_id)
    {

        return $totalProviceChaluBudget = DB::table('budget')
            ->join('offices', 'budget.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->where('ministries.id', $ministry_id)
            ->whereBetween('budget.expense_head', ['30000', '31511'])
            ->where('budget.source_type', 1)
            ->sum('total_budget');
    }

    public function getTotalFederalBudget($ministry_id)
    {

        return $totalProviceChaluBudget = DB::table('budget')
            ->join('offices', 'budget.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->where('ministries.id', $ministry_id)
            ->where('budget.source_type', 1)
            ->sum('total_budget');
    }

    public function getTotalbudgetOfMinistry($ministry_id)
    {
        return $totalProviceChaluBudget = Budget::
                                join('offices', 'budget.office_id', '=', 'offices.id')
                            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
                            ->where('ministries.id', $ministry_id)
                            ->sum('total_budget');
    }

    public function totalProvinceChaluBudgetByOfficeId($office_id)
    {

        return $provinceBudgetChalu = Budget::where('office_id', $office_id)
            ->where('source_type', 2)
            ->whereBetween('expense_head', ['21000', '29000'])->sum('total_budget');
    }

    public function totalProvincePujiBudgetByOfficeId($office_id)
    {

        return $provinceBudgetPuji = Budget::where('office_id', $office_id)
            ->where('source_type', 2)
            ->whereBetween('expense_head', ['30000', '31511'])->sum('total_budget');
    }

    public function totalFederalChaluBudgetByOfficeId($office_id)
    {

        return $provinceBudgetChalu = Budget::where('office_id', $office_id)
            ->where('source_type', 1)
            ->whereBetween('expense_head', ['21000', '29000'])->sum('total_budget');
    }

    public function totalFederalPujiBudgetByOfficeId($office_id)
    {

        return $provinceBudgetPuji = Budget::where('office_id', $office_id)
            ->where('source_type', 1)
            ->whereBetween('expense_head', ['30000', '31511'])->sum('total_budget');
    }

    public function getTotalInitialBudgetByOffice($office_id)
    {

        return $initialBudget = Budget::join('akhtiyari', 'budget.akhtiyari_type', '=', 'akhtiyari.id')
            ->where('budget.office_id', $office_id)
            ->where('akhtiyari.akhtiyari_type', 1)
            ->sum('budget.total_budget');

    }

    public function getTotalAddBudgetByOffice($office_id)
    {

        return $initialBudget = Budget::join('akhtiyari', 'budget.akhtiyari_type', '=', 'akhtiyari.id')
            ->where('budget.office_id', $office_id)
            ->where('akhtiyari.akhtiyari_type', 2)
            ->sum('budget.total_budget');

    }

    public function getTotalReduceBudgetByOffice($office_id)
    {

        return $initialBudget = Budget::join('akhtiyari', 'budget.akhtiyari_type', '=', 'akhtiyari.id')
            ->where('budget.office_id', $office_id)
            ->where('akhtiyari.akhtiyari_type', 3)
            ->sum('budget.total_budget');

    }

    public function getFinalBudget($office_id)
    {
        return $initialBudget = Budget::where('budget.office_id', $office_id)
            ->sum('budget.total_budget');

    }

    public function getTotalInitialBudgetByMinistry($ministry_id)
    {

        return $initialBudget = Budget::join('akhtiyari', 'budget.akhtiyari_type', '=', 'akhtiyari.id')
            ->join('offices', 'budget.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->where('ministries.id', $ministry_id)
            ->where('akhtiyari.akhtiyari_type', 1)
            ->sum('budget.total_budget');

    }

    public function getTotalAddBudgetByMinistry($ministry_id)
    {

        return $initialBudget = Budget::join('akhtiyari', 'budget.akhtiyari_type', '=', 'akhtiyari.id')
            ->join('offices', 'budget.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->where('ministries.id', $ministry_id)
            ->where('akhtiyari.akhtiyari_type', 2)
            ->sum('budget.total_budget');

    }

    public function getTotalReduceBudgetByMinistry($ministry_id)
    {

        return $initialBudget = Budget::join('akhtiyari', 'budget.akhtiyari_type', '=', 'akhtiyari.id')
            ->join('offices', 'budget.office_id', '=', 'offices.id')
            ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
            ->where('ministries.id', $ministry_id)
            ->where('akhtiyari.akhtiyari_type', 3)
            ->sum('budget.total_budget');

    }

    public function getFinalBudgetByMinistry($ministry_id)
    {

        return $initialBudget = Budget::
                                join('offices', 'budget.office_id', '=', 'offices.id')
                                    ->join('ministries', 'offices.ministry_id', '=', 'ministries.id')
                                    ->where('ministries.id', 4)
                                    ->sum('budget.total_budget');

    }


    public function deleteById($activity_id)
    {

        $activity = Budget::findorfail($activity_id);
        return $activity->delete();
    }

//    By Office, BudgetSub HEad
    public function getFinalBudgetByBudgetSubHead($office_id, $budget_sub_head_id)
    {

        return $finalBudget = Budget::where('budget.office_id', $office_id)
            ->where('budget.budget_sub_head', $budget_sub_head_id)
            ->sum('budget.total_budget');

    }

    public function createBudgetDetailsForOperationalActivity($activity, $total_budget, $budget_id)
    {


        $budgetDetals = new BudgetDetails();
        $budgetDetals->office_id = $activity['expense_center'];
        $budgetDetals->budget_sub_head_id = $activity['budget_sub_head'];
        $budgetDetals->budget_id = $budget_id;
        $budgetDetals->sub_activity = "ईन्धन कार्यालय प्रायोजन";
        $budgetDetals->expense_head_id = $activity['expense_head_id'];
        $budgetDetals->unit = 1;
        $budgetDetals->total_budget = (float)$total_budget;
        $budgetDetals->first_quarter_unit = NULL;
        $budgetDetals->second_quarter_unit = NULL;
        $budgetDetals->third_quarter_unit = 1;
        $budgetDetals->first_quarter_budget = NULL;
        $budgetDetals->first_chaimasik_bhar = NULL;
        $budgetDetals->second_quarter_budget = NULL;
        $budgetDetals->second_chaimasik_bhar = NULL;
        $budgetDetals->third_quarter_budget = (float)$total_budget;
        $budgetDetals->third_chaimasik_bhar = NULL;
        $budgetDetals->source_type = $activity['source_type'];
        $budgetDetals->source_level = $activity['source_level'];
        $budgetDetals->source = $activity['source'];
        $budgetDetals->medium = $activity['medium'];
        $budgetDetals->office_id = $activity['office_id'];
        $budgetDetals->user_id = 1;
        $budgetDetals->status = 1;
        $budgetDetals->fiscal_year = "2076/77";
        $budgetDetals->date = date('Y-m-d H:i:s');
        return $budgetDetals->save();
    }

    public function deleteBudgetDetailsByActivityId($budget_id)
    {

        $budgetDetails = BudgetDetails::where('budget_id', $budget_id)->get();
        foreach ($budgetDetails as $budgetDetail) {

            $budgetDetail->delete();
        }
        return $budgetDetail;
    }

    public function getTotalBudgetByBudgetSubHead($office_id,$budgetSubHeadId){

        return $totalBudget = Budget::where('office_id',$office_id)->where('budget_sub_head',$budgetSubHeadId)->sum('total_budget');
    }


}