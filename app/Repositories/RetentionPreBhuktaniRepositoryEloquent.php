<?php
namespace App\Repositories;

use App\helpers\BsHelper;
use App\Models\PreBhuktani;
use App\Models\RetentionPreBhuktani;
use App\Models\Voucher;
use App\Models\VoucherDetail;
use Illuminate\Support\Facades\Auth;
use DB;

class RetentionPreBhuktaniRepositoryEloquent implements RetentionPreBhuktaniRepository
{


    public function store($attribute){

//        dd($attribute);

        $preBhuktani = new RetentionPreBhuktani();
        $preBhuktani->journel_id = $attribute['retention_voucher_id'];
        $preBhuktani->office_id = Auth::user()->office_id;
        $preBhuktani->party_type = $attribute['party_type'];
        $preBhuktani->party = $attribute['party'];
        $preBhuktani->amount = $attribute['amount'];
        $preBhuktani->hisab_number = $attribute['hisab_number'];

        $preBhuktani->date_nepali = $attribute['roman_date'];

        $date_array = explode('-', $attribute['roman_date']);
        $bsObj = new BsHelper();
        $data_ad_array = $bsObj->nep_to_eng($date_array[0],$date_array[1],$date_array[2]);
        $preBhuktani->date_english = $data_ad_array['year'] .'-'. $data_ad_array['month'] .'-'.$data_ad_array['date'];
        $preBhuktani->status = 0;

        $date_eng = $attribute['roman_date'];

        $myyearfirst=(substr($date_eng, 0,2));
        $myyearlast=(substr($date_eng, 2,2));
        $mymonth=(substr($date_eng, 5,1));
        $month=intval($mymonth);

        if($month==1){$finalmonth=10;}
        if($month==2){$finalmonth=11;}
        if($month==3){$finalmonth=12;}
        if($month==4){$finalmonth=1;}
        if($month==5){$finalmonth=2;}
        if($month==6){$finalmonth=3;}
        if($month==7){$finalmonth=4;}
        if($month==8){$finalmonth=5;}
        if($month==9){$finalmonth=6;}
        if($month==10){$finalmonth=7;}
        if($month==11){$finalmonth=8;}
        if($month==12){$finalmonth=9;}
        if(intval($mymonth)>=4)
        {
            $preBhuktani->fiscal_year=$myyearfirst.$myyearlast."/".(intval($myyearlast)+1);
        }
        else
        {
            $preBhuktani->fiscal_year=$myyearfirst.(intval($myyearlast)-1)."/".$myyearlast;
        }
        $preBhuktani->month = $finalmonth;
        $preBhuktani->save();
        return $preBhuktani;

    }


    public function get_amount_by_multiple_id($ids){

        return $totalAmount = RetentionPreBhuktani::wherein('journel_id',$ids)->sum('amount');
    }

    public function update_status_and_set_bhuktani_id($bhuktani_id,$journel_id){
//        dd($bhuktani_id);
       return $preBhuktani = RetentionPreBhuktani::where('journel_id',$journel_id)
            ->update(
                array(
                    "bhuktani_id" => $bhuktani_id,
                    "status" => 1,

                )
            );


    }
}

