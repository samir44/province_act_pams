<?php
namespace App\Repositories;

use App\Models\SourceType;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SourceTypeRepositoryEloquent implements SourceTypeRepository
{
    public function create($attributes) {
        $sourcetype = new SourceType();
        $sourcetype->name = $attributes['name'];
        $sourcetype->status = 1;
        $sourcetype->save();
        return $sourcetype;
    }

    public function get_all_sourcetypes(){

        return $source_types = SourceType::all();
    }

    public function getSourceByOfficeAndBudgetSubHead($office_id,$budgetSubHead){

       return $resources = SourceType::join('budget', 'source_types.id', '=', 'budget.source_type')
            ->select(['source_types.id','source_types.name'])
            ->where('budget.office_id',$office_id)
            ->where('budget.budget_sub_head',$budgetSubHead)
            ->groupBy('budget.source_type')
            ->get();
//       dd($resources);
    }

    public function getSourceByOfficeAndBudgetSubHeadAndSourceType($office_id,$budgetSubHead,$sourceType){

       return $resources = SourceType::join('budget', 'source_types.id', '=', 'budget.source_type')
            ->select(['source_types.id','source_types.name'])
            ->where('budget.office_id',$office_id)
            ->where('budget.budget_sub_head',$budgetSubHead)
            ->where('budget.source_type',$sourceType)
            ->groupBy('budget.source_type')
            ->get();
//       dd($resources);
    }


}