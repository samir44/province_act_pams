<?php
namespace App\Repositories;

use App\Models\BudgetSourceLevel;
use Illuminate\Support\Facades\Auth;

class BudgetSourceLevelRepositoryEloquent implements BudgetSourceLevelRepository
{

    public function getAllBudgetSourceLevel(){

       return $areas = BudgetSourceLevel::all();

    }
    public function create($attributes) {
        $budgetSourceLevel = new BudgetSourceLevel();
        $budgetSourceLevel->name = $attributes['name'];
        $budgetSourceLevel->source_type_id = $attributes['sourceType'];
        $budgetSourceLevel->status = 1;
        $budgetSourceLevel->save();
        return $budgetSourceLevel;
    }

    public function get_all_by_source_type($source_type_id) {

      return  $source_levels =   BudgetSourceLevel::where('source_type_id',$source_type_id)->get();

    }
}