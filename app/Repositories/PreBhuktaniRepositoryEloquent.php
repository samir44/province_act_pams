<?php
namespace App\Repositories;

use App\helpers\BsHelper;
use App\Models\PreBhuktani;
use App\Models\Voucher;
use App\Models\VoucherDetail;
use Illuminate\Support\Facades\Auth;
use DB;

class PreBhuktaniRepositoryEloquent implements PreBhuktaniRepository
{

    public function getAllPreBhuktani(){

       return $areas = PreBhuktani::all();

    }

    public function getByBhuktaniId($bhuktani_id){

        return $pre_bhuktani = PreBhuktani::where('bhuktani_id',$bhuktani_id)->groupBy('main_activity_id')->selectRaw('*, sum(amount) as sum')->get();
    }

    public function create($attributes) {

//        dd($attributes);
        $preBhuktani = new PreBhuktani();
        $preBhuktani->office_id =Auth::user()->office->id;
        $preBhuktani->journel_id = $attributes['voucher_id'];
        $preBhuktani->budget_sub_head_id = $attributes['budget_sub_head_id'];
        $preBhuktani->main_activity_id = $attributes['main_activity_id'];
        $preBhuktani->bhuktani_paaune = $attributes['bhuktani_party_id'];
        $preBhuktani->expense_head_id = $attributes['expense_head_id'];
        $preBhuktani->bhuktani_id = 0;
        $preBhuktani->amount = $attributes['bhuktani_amount'];
        $preBhuktani->date_nepali = $attributes['date_nepali'];

        $date_eng = $attributes['date_english'];
        $myyearfirst=(substr($date_eng, 0,2));
        $myyearlast=(substr($date_eng, 2,2));
        $mymonth=(substr($date_eng, 5,1));
        $month=intval($mymonth);

        if($month==1){$finalmonth=10;}
        if($month==2){$finalmonth=11;}
        if($month==3){$finalmonth=12;}
        if($month==4){$finalmonth=1;}
        if($month==5){$finalmonth=2;}
        if($month==6){$finalmonth=3;}
        if($month==7){$finalmonth=4;}
        if($month==8){$finalmonth=5;}
        if($month==9){$finalmonth=6;}
        if($month==10){$finalmonth=7;}
        if($month==11){$finalmonth=8;}
        if($month==12){$finalmonth=9;}
        if(intval($mymonth)>=4)
        {
            $preBhuktani->fiscal_year=$myyearfirst.$myyearlast."/".(intval($myyearlast)+1);
        }
        else
        {
            $preBhuktani->fiscal_year=$myyearfirst.(intval($myyearlast)-1)."/".$myyearlast;
        }

        $date_array = explode('-', $attributes['date_english']);
        $bsObj = new BsHelper();
        $data_ad_array = $bsObj->nep_to_eng($date_array[0],$date_array[1],$date_array[2]);
        $preBhuktani->date_english = $data_ad_array['year'] .'-'. $data_ad_array['month'] .'-'.$data_ad_array['date'];
        $preBhuktani->month = $finalmonth;
        $preBhuktani->status = 0; // status 1 indicate -> भुक्तानी आदेश बन्न बाकी
        $preBhuktani->party_type = $attributes['bhuktani_party_type'];
        $preBhuktani->advance_vat_deduction = $attributes['advance_tax_deduction'];
        $preBhuktani->vat_amount = $attributes['vat_amount'];
        $preBhuktani->vat_bill_number = $attributes['vat_bill_number'];
        $preBhuktani->pratibadhhata_number = $attributes['pratibadhata_number'];
        $preBhuktani->cheque_type = $attributes['chequeTypeId'];
        $preBhuktani->save();
        return $preBhuktani;
    }

    public function get_by_program($program_id){

//        return $preBhuktanies = PreBhuktani::where('budget_sub_head_id',$program_id)
//            ->where('status',1)
//            ->get();
        $office_id = Auth::user()->office_id;
        return  $preBhuktanies =  PreBhuktani::select('pre_bhuktani.*')
            ->join('vouchers','vouchers.id','=','pre_bhuktani.journel_id')
            ->where(['vouchers.status' => '1', 'pre_bhuktani.status' => '0'])
            ->where(['pre_bhuktani.Office_id' => $office_id])
            ->where(['pre_bhuktani.budget_sub_head_id' => $program_id])
            ->get();
    }

    public function get_amount_by_id($id){

         $amount = PreBhuktani::select('amount')->where('id',$id)->first();
         return $amount->amount;
    }

    public function delete_by_vouhcer_id($voucher_id){

        $voucher = Voucher::findorfail($voucher_id);
//        dd($voucher);
        return $voucher->preBhuktani()->delete();

    }

    public function get_amount_by_multiple_id($ids){

        return $totalAmount = PreBhuktani::wherein('id',$ids)->sum('amount');
    }

    public function update_status_and_set_bhuktani_id($bhuktani_id,$pre_bhuktani){
//        dd($bhuktani_id);
        return  PreBhuktani::find($pre_bhuktani)
             ->update(['status'=>1,'bhuktani_id'=>$bhuktani_id]);
    }

    public function get_preBhuktani_by_voucher_id($id){

       return $data =  $preBhuktaniList = PreBhuktani::where('journel_id',$id)->with('bhuktani')->get();

    }

    public function updatePreBhuktani($voucher_id,$bhuktani){

//        dd($preBhuktani);
        $voucher = Voucher::findorfail($voucher_id);
//        dd($bhuktani);
//        $voucher->preBhuktani()->delete();

            $preBhuktani = new PreBhuktani();
            $preBhuktani->office_id =Auth::user()->office->id;
            $preBhuktani->journel_id = $voucher->id;
            $preBhuktani->budget_sub_head_id = $bhuktani['budget_sub_head_id'];
            $preBhuktani->main_activity_id = $bhuktani['main_activity_id'];
            $preBhuktani->expense_head_id = $bhuktani['expense_head_id'];
            $preBhuktani->party_type = array_key_exists('bhuktani_party_type',$bhuktani)? $bhuktani['bhuktani_party_type']  : 0;
            $preBhuktani->bhuktani_paaune = $bhuktani['bhuktani_party_id'];
            $preBhuktani->bhuktani_id = 0;
            $preBhuktani->amount = $bhuktani['bhuktani_amount'];
            $preBhuktani->advance_vat_deduction= array_key_exists('advance_tax_deduction',$bhuktani)? $bhuktani['advance_tax_deduction']  : 0;
            $preBhuktani->vat_amount= array_key_exists('vat_amount',$bhuktani)? $bhuktani['vat_amount']  : 0;
            $preBhuktani->vat_bill_number= array_key_exists('vat_bill_number',$bhuktani)? $bhuktani['vat_bill_number']  : 0;
            $preBhuktani->pratibadhhata_number= array_key_exists('pratibadhata_number',$bhuktani)? $bhuktani['pratibadhata_number']  : NULL;
            $preBhuktani->cheque_type= array_key_exists('cheque_type',$bhuktani)? $bhuktani['cheque_type']  : 0;

            $preBhuktani->date_nepali = $bhuktani['date_nepali'];

            $date_eng = $bhuktani['date_english'];
            $myyearfirst=(substr($date_eng, 0,2));
            $myyearlast=(substr($date_eng, 2,2));
            $mymonth=(substr($date_eng, 5,1));
            $month=intval($mymonth);

            if($month==1){$finalmonth=10;}
            if($month==2){$finalmonth=11;}
            if($month==3){$finalmonth=12;}
            if($month==4){$finalmonth=1;}
            if($month==5){$finalmonth=2;}
            if($month==6){$finalmonth=3;}
            if($month==7){$finalmonth=4;}
            if($month==8){$finalmonth=5;}
            if($month==9){$finalmonth=6;}
            if($month==10){$finalmonth=7;}
            if($month==11){$finalmonth=8;}
            if($month==12){$finalmonth=9;}
            if(intval($mymonth)>=4)
            {
                $preBhuktani->fiscal_year=$myyearfirst.$myyearlast."/".(intval($myyearlast)+1);
            }
        else
            {
                $preBhuktani->fiscal_year=$myyearfirst.(intval($myyearlast)-1)."/".$myyearlast;
            }

            $date_array = explode('-', $bhuktani['date_english']);
            $bsObj = new BsHelper();
            $data_ad_array = $bsObj->nep_to_eng($date_array[0],$date_array[1],$date_array[2]);
            $preBhuktani->date_english = $data_ad_array['year'] .'-'. $data_ad_array['month'] .'-'.$data_ad_array['date'];
            $preBhuktani->month = $finalmonth;
            $preBhuktani->status = 0; // status 0 indicate -> भुक्तानी आदेश बन्न बाकी
            $preBhuktani->save();

        return $preBhuktani;
    }

    public function update_status_by_id($id){

        return $pre_bhuktani = PreBhuktani::findorfail($id)
            ->update(['status'=>0]);

    }

    public function update_status_one_by_id($id){

        return $pre_bhuktani = PreBhuktani::findorfail($id)
            ->update(['status'=>1]);

    }

    public function get_parties_by_budget_head_and_office($budget_sub_head_id){

        $office_id = Auth::user()->office_id;
        $parties = PreBhuktani::where('budget_sub_head_id',$budget_sub_head_id)
            ->where('office_id',$office_id)
            ->with('party_code')->get()->toArray();
        $all_parties = [];
        foreach ($parties as $party){

            $party = $party['party_code'];
            $temp = [];
            $temp["id"] = $party['id'];
            $temp["sirsak"] = $party["name_nep"];

            if(!$this->check_unique($all_parties,'id',$party['id']))
                array_push($all_parties, $temp);
        }

        return $all_parties;
    }

    function check_unique($array, $key, $val) {
        foreach ($array as $item)
            if (isset($item[$key]) && $item[$key] == $val)
                return true;
        return false;
    }

    function multi_unique($src){
        $output = array_map("unserialize",
            array_unique(array_map("serialize", $src)));
        return $output;
    }

    public function get_party_by_party_id($data){

        $office_id = Auth::user()->office_id;
        return $party = PreBhuktani::where('office_id',$office_id)
            ->where('budget_sub_head_id',$data['program'])
            ->where('bhuktani_paaune',$data['khata'])
             ->with('voucher')
            ->get();
//         dd($party);
    }

    public function get_pre_bhuktani_by_bhuktani_id($bhuktani_id){

        return $pre_bhuktanies = PreBhuktani::where('bhuktani_id',$bhuktani_id)->groupBy('bhuktani_paaune','main_activity_id')->with('main_activity')->get();
    }
}

