<?php
namespace App\Repositories;

use App\Models\Voucher;
use App\Models\VoucherDetail;
use Illuminate\Support\Facades\Auth;
use App\helpers\BsHelper;
use Illuminate\Support\Facades\DB;
use mysql_xdevapi\Exception;

class VoucherRepositoryEloquent implements VoucherRepository
{

    public function getAllVoucher(){

       return $areas = Voucher::all();

    }

    public function find($id){
        return Voucher::findorfail($id);
    }

    public  function create($attributes,$voucher_signature) {

        $voucher = new Voucher();
        $voucher->jv_number = $attributes['voucher_number'];
        $voucher->office_id =Auth::user()->office->id;
        $voucher->budget_sub_head_id = $attributes['budget_sub_head_id'];
        $voucher->payement_amount =$attributes['paymentAmount'];
        $voucher->short_narration = $attributes['narration_short'];
        $voucher->long_narration = $attributes['narration_long'];
        $voucher->data_nepali = $attributes['date_nepali'];
        $voucher->user_id =Auth::user()->id;
        $voucher->status = 0;
        $date_eng = $attributes['date_english'];
        $myyearfirst=(substr($date_eng, 0,2));
        $myyearlast=(substr($date_eng, 2,2));
        $mymonth=(substr($date_eng, 5,1));
        $month=intval($mymonth);

        if($month==1){$finalmonth=10;}
        if($month==2){$finalmonth=11;}
        if($month==3){$finalmonth=12;}
        if($month==4){$finalmonth=1;}
        if($month==5){$finalmonth=2;}
        if($month==6){$finalmonth=3;}
        if($month==7){$finalmonth=4;}
        if($month==8){$finalmonth=5;}
        if($month==9){$finalmonth=6;}
        if($month==10){$finalmonth=7;}
        if($month==11){$finalmonth=8;}
        if($month==12){$finalmonth=9;}
        if(intval($mymonth)>=4)
        {
            $voucher->fiscal_year=$myyearfirst.$myyearlast."/".(intval($myyearlast)+1);
        }
        else
        {
            $voucher->fiscal_year=$myyearfirst.(intval($myyearlast)-1)."/".$myyearlast;
        }

        $date_array = explode('-', $attributes['date_english']);
        $bsObj = new BsHelper();
        $data_ad_array = $bsObj->nep_to_eng($date_array[0],$date_array[1],$date_array[2]);
        $voucher->date_english = $data_ad_array['year'] .'-'. $data_ad_array['month'] .'-'.$data_ad_array['date'];
        $voucher->month = $finalmonth;

        if($voucher_signature and $voucher_signature->karmachari_prepare_by){

            $voucher->prepared_by = $voucher_signature->karmachari_prepare_by->id;
        }
        if($voucher_signature and $voucher_signature->karmachari_submit_by){

            $voucher->submitted_by = $voucher_signature->karmachari_submit_by->id;
        }
        if($voucher_signature and $voucher_signature->karmachari_approved_by){

            $voucher->approved_by = $voucher_signature->karmachari_approved_by->id;
        }
        $voucher->save();
        return $voucher->id;
    }

    public function createNikasaVoucher($attributes,$voucher_signature){

        $voucher = new Voucher();
        $voucher->jv_number = $attributes['voucher_number'];
        $voucher->office_id =Auth::user()->office->id;
        $voucher->budget_sub_head_id = $attributes['budget_sub_head'];
        $voucher->payement_amount =$attributes['totalAmount'];
        $voucher->short_narration = "निकासा आम्दानी बाधिंयो।";
        $voucher->long_narration = "निकासा आम्दानी बाधिंयो।";
        $voucher->data_nepali = $attributes['date'];
        $voucher->user_id =Auth::user()->id;
        $voucher->status = 0;
        $date_eng = $attributes['date_english'];
        $myyearfirst=(substr($date_eng, 0,2));
        $myyearlast=(substr($date_eng, 2,2));
        $mymonth=(substr($date_eng, 5,1));
        $month=intval($mymonth);

        if($month==1){$finalmonth=10;}
        if($month==2){$finalmonth=11;}
        if($month==3){$finalmonth=12;}
        if($month==4){$finalmonth=1;}
        if($month==5){$finalmonth=2;}
        if($month==6){$finalmonth=3;}
        if($month==7){$finalmonth=4;}
        if($month==8){$finalmonth=5;}
        if($month==9){$finalmonth=6;}
        if($month==10){$finalmonth=7;}
        if($month==11){$finalmonth=8;}
        if($month==12){$finalmonth=9;}
        if(intval($mymonth)>=4)
        {
            $voucher->fiscal_year=$myyearfirst.$myyearlast."/".(intval($myyearlast)+1);
        }
        else
        {
            $voucher->fiscal_year=$myyearfirst.(intval($myyearlast)-1)."/".$myyearlast;
        }

        $date_array = explode('-', $attributes['date_english']);
        $bsObj = new BsHelper();
        $data_ad_array = $bsObj->nep_to_eng($date_array[0],$date_array[1],$date_array[2]);
        $voucher->date_english = $data_ad_array['year'] .'-'. $data_ad_array['month'] .'-'.$data_ad_array['date'];
        $voucher->month = $finalmonth;

        if($voucher_signature and $voucher_signature->karmachari_prepare_by){

            $voucher->prepared_by = $voucher_signature->karmachari_prepare_by->id;
        }
        if($voucher_signature and $voucher_signature->karmachari_submit_by){

            $voucher->submitted_by = $voucher_signature->karmachari_submit_by->id;
        }
        if($voucher_signature and $voucher_signature->karmachari_approved_by){

            $voucher->approved_by = $voucher_signature->karmachari_approved_by->id;
        }
        $voucher->save();
        return $voucher->id;
    }


    public function get_vouhcer_number_by_budget_sub_head_and_office_id($office_id, $budget_sub_head_id){


        $voucher  = Voucher::where('office_id',$office_id)
            ->where('budget_sub_head_id',$budget_sub_head_id)
            ->orderBy('jv_number', 'desc')->first();
//            ->latest()
//////            ->first();
        if($voucher){
            $jvNumber = $voucher->jv_number;
        }
        else {
            $jvNumber = 0;
        }
       return $jvNumber + 1;
    }

    public function get_all_voucher_budget_sub_head_id($budget_sub_head){

        $office_id = Auth::user()->office->id;
        return $voucherList = Voucher::where('office_id',$office_id)
            ->where('budget_sub_head_id',$budget_sub_head)
            ->where('status',1)
            ->get();
//         dd($budget_sub_head_id);
    }

    public function getVoucherByStatusAndBudgetSubHead($budget_sub_head_id,$office_id,$year){

       $voucherLists = Voucher::where('office_id',$office_id)
            ->where('budget_sub_head_id',$budget_sub_head_id)
            ->where('fiscal_year',$year)
            ->where('status',0)->get();

       $lastVoucher = Voucher::where('office_id',$office_id)
           ->where('budget_sub_head_id',$budget_sub_head_id)
           ->orderBy('jv_number', 'desc')->first();

        $voucherLists->map(function($item) use ($lastVoucher){

           if($item->id == $lastVoucher->id){
               $item->IS_LAST = true;
           }
           return $item;
        });
//       dd($year);
       return $voucherLists;
    }

    public function set_voucher_approved($id){

        $voucher = Voucher::findorfail($id);
        $voucher->status = 1;
       return $voucher->save();
    }

    public function updateVoucher($voucher_data,$voucher_signature){


        $id = $voucher_data['id'];
        $voucher = Voucher::where('id',$id)->first();

        if(array_key_exists('date', $voucher_data)){
            $voucher->data_nepali = $voucher_data['date_nepali'];
        }
        if(array_key_exists('payment_amount',$voucher_data)){
            $voucher->payement_amount = $voucher_data['payment_amount'];

        }
        if(array_key_exists('narration_short',$voucher_data)){
            $voucher->short_narration = $voucher_data['narration_short'];

        }
        if(array_key_exists('narration_long',$voucher_data)){
            $voucher->long_narration = $voucher_data['narration_long'];

        }
        if(array_key_exists('date_nepali',$voucher_data)){
            $voucher->data_nepali = $voucher_data['date_nepali'];

        }
        if(array_key_exists('date_nepali',$voucher_data)){
            $voucher->data_nepali = $voucher_data['date_nepali'];

        }

        $date_eng = $voucher_data['date_english'];
        $myyearfirst=(substr($date_eng, 0,2));
        $myyearlast=(substr($date_eng, 2,2));
        $mymonth=(substr($date_eng, 5,1));
        $month=intval($mymonth);

        if($month==1){$finalmonth=10;}
        if($month==2){$finalmonth=11;}
        if($month==3){$finalmonth=12;}
        if($month==4){$finalmonth=1;}
        if($month==5){$finalmonth=2;}
        if($month==6){$finalmonth=3;}
        if($month==7){$finalmonth=4;}
        if($month==8){$finalmonth=5;}
        if($month==9){$finalmonth=6;}
        if($month==10){$finalmonth=7;}
        if($month==11){$finalmonth=8;}
        if($month==12){$finalmonth=9;}
        if(intval($mymonth)>=4)
        {
            $voucher->fiscal_year=$myyearfirst.$myyearlast."/".(intval($myyearlast)+1);
        }
        else
        {
            $voucher->fiscal_year=$myyearfirst.(intval($myyearlast)-1)."/".$myyearlast;
        }

        $date_array = explode('-', $voucher_data['date_english']);
        $bsObj = new BsHelper();
        $data_ad_array = $bsObj->nep_to_eng($date_array[0],$date_array[1],$date_array[2]);
        $voucher->date_english = $data_ad_array['year'] .'-'. $data_ad_array['month'] .'-'.$data_ad_array['date'];
        $voucher->month = $finalmonth;

        if($voucher_signature and $voucher_signature->karmachari_prepare_by){

            $voucher->prepared_by = $voucher_signature->karmachari_prepare_by->id;
        }
        if($voucher_signature and $voucher_signature->karmachari_submit_by){

            $voucher->submitted_by = $voucher_signature->karmachari_submit_by->id;
        }
        if($voucher_signature and $voucher_signature->karmachari_approved_by){

            $voucher->approved_by = $voucher_signature->karmachari_approved_by->id;
        }

       return $voucher->save();
    }

    public function get_all_pre_month_voucher_by_budget_sub_head_id($budget_sub_head_id,$fiscalYear,$month){

        $office_id = Auth::user()->office->id;
        return $voucherList = Voucher::where('office_id',$office_id)
            ->where('fiscal_year',$fiscalYear)
            ->where('month',$month)
            ->where('budget_sub_head_id',$budget_sub_head_id)->get();
    }


    public function get_all_voucher_by_budget_sub_head_id($budget_sub_head,$fiscalYear,$month){

        $office_id = Auth::user()->office->id;
        return $voucherList = Voucher::where('office_id',$office_id)
            ->where('budget_sub_head_id',$budget_sub_head)
            ->where('fiscal_year',$fiscalYear)
            ->where('month',$month)
            ->where('status',1)->get();

    }
//यो माथीको optional code ho
//$office_id = Auth::user()->office->id;
//return $voucherList = Voucher::
//join('voucher_details', 'vouchers.id', '=', 'voucher_details.journel_id')
//->where('vouchers.office_id', $office_id)
//->where('vouchers.budget_sub_head_id', $budget_sub_head)
//->where('vouchers.fiscal_year', $fiscalYear)
//->where('vouchers.month', $month)
//->groupBy('voucher_details.expense_head_id')
//->where('vouchers.status', 1)->get();

    public function get_all_voucher_by_group_by_expense_head($budget_sub_head_id,$fiscalYear,$month_post){

//        $office_id = Auth::user()->office_id;
//        $voucherList = DB::table('Voucher')
//            ->select('')
//
//
//
//            $user_info = DB::table('usermetas')
//                ->select('browser', DB::raw('count(*) as total'))
//                ->groupBy('browser')
//                ->get();
    }

    public function delete($voucher_id){

        $voucher = $this->find($voucher_id);

        $voucher->details()->delete();
        $voucher->preBhuktani()->delete();
        try{

            return $voucher->delete();
        } catch (\Exception $e){

            return false;
        }
    }

    //    public function get_all_field_total($preVoucherList){
////        dd($preVoucherList);
//        $cr_tsa_total = $preVoucherList->whereHas('details', function ($q){
//            $q->where('dr_or_cr', 2)->where('ledger_type_id',3);
//        })->get();
//        User::whereHas('posts', function($q){
//            $q->where('created_at', '>=', '2015-01-01 00:00:00');
//        })->get();
//    }

    public function change_status_by_budget_sub_head_and_office_id($data){


            $vouhcer = Voucher::where('office_id',$data['office_id'])
                ->where('budget_sub_head_id',$data['budget_sub_head'])
                ->where('jv_number',$data['voucher_number'])
                ->first();

                if($vouhcer){
                    $vouhcer->status=0;
                    $vouhcer->save();
//                    dd($vouhcer);
                    return $vouhcer->id;
                }else {

                    return false;
                }
    }
}