<?php
namespace App\Repositories;

use App\Models\Medium;
use Illuminate\Support\Facades\Auth;

class MediumRepositoryEloquent implements MediumRepository
{
    public function create($attributes) {
        $medium = new Medium();
        $medium->name = $attributes['name'];
        $medium->status = 1;
        $medium->save();
        return $medium;
    }

    public function get_all_mediums()
    {
        return Medium::all();
    }


}