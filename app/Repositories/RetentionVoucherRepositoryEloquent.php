<?php

namespace App\Repositories;

use App\helpers\Money_words;
use App\Models\Area;
use App\Models\RetentionVoucher;
use App\Models\RetentionVoucherDetails;
use Illuminate\Support\Facades\Auth;
use App\helpers;

class RetentionVoucherRepositoryEloquent implements RetentionVoucherRepository
{

    public function get_last_voucher_number()
    {

        $office_id = Auth::user()->office_id;
        $voucher = RetentionVoucher::where('office_id', $office_id)
            ->latest()
            ->first();

        if ($voucher) {

            $voucherNumber = $voucher->voucher_number;
        } else {
            $voucherNumber = 0;
        }

        return $voucherNumber + 1;
    }

    public function store($attribute)
    {

        $retentionObj = new RetentionVoucher();
        $retentionObj->voucher_number = $attribute['voucher_number'];

        $dateHelperObj = new helpers\BsHelper();
        $date_roman = $dateHelperObj->convertNepaliToEnglish($attribute['date']);
        $date_array = explode('-', $date_roman);
        $date_eng = $dateHelperObj->nep_to_eng($date_array[0], $date_array[1], $date_array[2]);
        $retentionObj->date_eng = $date_eng['year'] . '-' . $date_eng['month'] . '-' . $date_eng['date'];
        $retentionObj->date_nep = $date_roman;

        $mymonth = (substr($date_roman, 5, 1));
        $month = intval($mymonth);
        if ($month == 1) {
            $finalmonth = 10;
        }
        if ($month == 2) {
            $finalmonth = 11;
        }
        if ($month == 3) {
            $finalmonth = 12;
        }
        if ($month == 4) {
            $finalmonth = 1;
        }
        if ($month == 5) {
            $finalmonth = 2;
        }
        if ($month == 6) {
            $finalmonth = 3;
        }
        if ($month == 7) {
            $finalmonth = 4;
        }
        if ($month == 8) {
            $finalmonth = 5;
        }
        if ($month == 9) {
            $finalmonth = 6;
        }
        if ($month == 10) {
            $finalmonth = 7;
        }
        if ($month == 11) {
            $finalmonth = 8;
        }
        if ($month == 12) {
            $finalmonth = 9;
        }

        $retentionObj->month = $finalmonth;
        $retentionObj->fiscal_year = $attribute['fiscal_year'];
        $retentionObj->office_id = $attribute['office'];
        $retentionObj->status = 0;
        $retentionObj->save();


    }

//Details
    public function storeDetails($attribute)
    {

        $retentionVoucherDetail = new RetentionVoucherDetails();
        $retentionVoucherDetail->retention_voucher_id = $attribute['retention_voucher_id'];
        $retentionVoucherDetail->office_id = Auth::user()->office_id;
        $retentionVoucherDetail->dr_or_cr = $attribute['drOrCr'];
        $retentionVoucherDetail->byahora = $attribute['byahora'];
        $retentionVoucherDetail->hisab_number = $attribute['hisab_number'];
        $retentionVoucherDetail->details = $attribute['details'];
        $retentionVoucherDetail->retention_record = $attribute['retention_record'];
        $retentionVoucherDetail->retention_type = $attribute['retention_type'];
        $retentionVoucherDetail->depositor_type = $attribute['party_type'];
        $retentionVoucherDetail->depositor = $attribute['party'];
        $retentionVoucherDetail->bill_number = $attribute['bill_number'];
        $retentionVoucherDetail->amount = $attribute['amount'];

        $dateHelperObj = new helpers\BsHelper();
        $date_roman = $dateHelperObj->convertNepaliToEnglish($attribute['date']);
        $date_array = explode('-', $date_roman);
        $date_eng = $dateHelperObj->nep_to_eng($date_array[0], $date_array[1], $date_array[2]);
        $retentionVoucherDetail->date_eng = $date_eng['year'] . '-' . $date_eng['month'] . '-' . $date_eng['date'];
        $retentionVoucherDetail->date_nep = $date_roman;


        $mymonth = (substr($date_roman, 5, 1));
        $myyearfirst = (substr($date_roman, 0, 2));
        $myyearlast = (substr($date_roman, 2, 2));
        $mymonth = (substr($date_roman, 5, 1));
        $month = intval($mymonth);
        if ($month == 1) {
            $finalmonth = 10;
        }
        if ($month == 2) {
            $finalmonth = 11;
        }
        if ($month == 3) {
            $finalmonth = 12;
        }
        if ($month == 4) {
            $finalmonth = 1;
        }
        if ($month == 5) {
            $finalmonth = 2;
        }
        if ($month == 6) {
            $finalmonth = 3;
        }
        if ($month == 7) {
            $finalmonth = 4;
        }
        if ($month == 8) {
            $finalmonth = 5;
        }
        if ($month == 9) {
            $finalmonth = 6;
        }
        if ($month == 10) {
            $finalmonth = 7;
        }
        if ($month == 11) {
            $finalmonth = 8;
        }
        if ($month == 12) {
            $finalmonth = 9;
        }

        $retentionVoucherDetail->month = $finalmonth;

        if (intval($mymonth) >= 4) {
            $retentionVoucherDetail->fiscal_year = $myyearfirst . $myyearlast . "/" . (intval($myyearlast) + 1);
        } else {
            $retentionVoucherDetail->fiscal_year = $myyearfirst . (intval($myyearlast) - 1) . "/" . $myyearlast;
        }
        $retentionVoucherDetail->status = 1;
        $retentionVoucherDetail->save();
    }


    public function getRetentionDetailsByRetentionVoucherId($voucher_id)
    {

        $office_id = Auth::user()->office_id;
        return $voucherDetails = RetentionVoucherDetails::where('office_id', $office_id)
            ->where('retention_voucher_id', $voucher_id)->get();
    }

    public function getVoucherByOfficeId()
    {

        $office_id = Auth::user()->office_id;
        return $voucherList = RetentionVoucher::where('office_id', $office_id)->get();
    }

    public function get_by_office_and_status($office_id){

        return $voucherList = RetentionVoucher::where('office_id', $office_id)->where('status',1)->where('is_bhuktani',0)->get();

    }

    public function update($attrubute, $voucherId)
    {

        $voucher = RetentionVoucher::findorfail($voucherId);

        $voucher->amount = $attrubute['tsa_amount'];
        $voucher->short_narration = $attrubute['shortInfo'];
        $voucher->long_narration = $attrubute['detailsInfo'];
       return $voucher->save();
    }

    public function getById($retentionVoucherId){

        return $voucher = RetentionVoucher::findorfail($retentionVoucherId);
    }

    public function getRetentionDetailsByDetailsId($retention_details_id){

       return $retentionDetails = RetentionVoucherDetails::findorfail($retention_details_id);
    }

    public function updateRetentionVoucherDetails($attrbutes,$voucherDetailsId){

        $voucherDetails = RetentionVoucherDetails::findorfail($voucherDetailsId);

        if(array_key_exists('drOrCr', $attrbutes)){
            $voucherDetails->dr_or_cr = $attrbutes['drOrCr'];
        }
        if(array_key_exists('byahora', $attrbutes)){
            $voucherDetails->byahora = $attrbutes['byahora'];
        }
        if(array_key_exists('hisab_number', $attrbutes)){
            $voucherDetails->hisab_number = $attrbutes['hisab_number'];
        }

        if(array_key_exists('details', $attrbutes)){
            $voucherDetails->details = $attrbutes['details'];
        }

        if(array_key_exists('retention_record', $attrbutes)){
            $voucherDetails->retention_record = $attrbutes['retention_record'];
        }
        if(array_key_exists('retention_type', $attrbutes)){
            $voucherDetails->retention_type = $attrbutes['retention_type'];
        }

        if(array_key_exists('party_type', $attrbutes)){
            $voucherDetails->depositor_type = $attrbutes['party_type'];
        }

        if(array_key_exists('party', $attrbutes)){
            $voucherDetails->depositor = $attrbutes['party'];
        }

        if(array_key_exists('bill_number', $attrbutes)){
            $voucherDetails->bill_number = $attrbutes['bill_number'];
        }
        if(array_key_exists('amount', $attrbutes)){
            $voucherDetails->amount = $attrbutes['amount'];
        }
      return $voucherDetails->save();
    }

    public function setStatusByVoucherId($voucherId){

        $voucher = RetentionVoucher::findorfail($voucherId);
        $voucher->status = 1;
        return $voucher->save();
    }

    public function getVoucherById($voucherId){

       return $voucher = RetentionVoucher::where('id',$voucherId)->first();
    }

    public function get_amount_in_word($amount){

        $obj = new Money_words();
        return $words = $obj->convert_number($amount);

    }

    //details
    public function get_by_office_id($office_id){

        return $retentionVOucher = RetentionVoucherDetails::where('office_id',$office_id)->get();
    }

    public function get_party_by_office_id($office_id){

        return $parties = RetentionVoucherDetails::whereNotNull('depositor_type')->where('office_id',$office_id)->groupBy('depositor_type')->with('advancePayment')->get();
    }

    public function getDetailsByDepositor($depositor,$office){


       return $depositor = RetentionVoucherDetails::where('office_id',$office)->where('depositor',$depositor)->get();

    }

    public function getUnapprovedVoucher(){

        $office_id = Auth::user()->office_id;

        return $unApproveVoucher = RetentionVoucher::where('office_id',$office_id)->where('status',0)->get();

    }
}