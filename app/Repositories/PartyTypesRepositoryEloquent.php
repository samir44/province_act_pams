<?php
namespace App\Repositories;

use App\Models\PartyTypes;
use Illuminate\Support\Facades\Auth;

class PartyTypesRepositoryEloquent implements PartyTypesRepository
{
    public function create($attributes) {
        $partytype = new PartyTypes();
        $partytype->name = $attributes['name'];
        $partytype->save();
        return $partytype;
    }

    public function get_by_id($id){
        return PartyTypes::findorfail($id);
    }

    public function update($attributes, $id){
        $partytype = $this->get_by_id($id);
        $partytype->name = $attributes['name'];
        $partytype->save();
        return $partytype;
    }
}