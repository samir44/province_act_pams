<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Budget_Sub_Head extends Model
{
    protected $table = "programs";

    protected $fillable = [
        'name','program_code', 'office_id','status'
    ];

    protected $guarded = [];

}
