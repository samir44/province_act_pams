@extends('frontend.layouts.app')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>

                <small>म.ले.प.फा.नं. ६०१::व्यक्तिगत धरौटी खाता</small>
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <form action="{{route('report.byaktigat.dharauti.khata')}}" method="post" target="_blank">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="name">कार्यालय : </label>
                            <select class="form-control" name="office" id="office_id">
                                <option value="{{Auth::user()->office->id}}">{{Auth::user()->office->name}}</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="name">आर्थिक वर्ष:</label>
                            <select class="form-control" name="fiscal_year">
                                <option value="{{$fiscalYear->year}}">{{$fiscalYear->year}}</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="name">धरौटी जम्मा गर्नेको प्रकार:</label>
                            <select class="form-control" name="depositor_type" id="depositor_type">
                                <option value="">...............</option>
                                @foreach($depositorTypes as $depositorType)
                                    @if($depositorType->party_type_name)
                                    <option value="{{$depositorType->party_type_name->id}}">{{$depositorType->party_type_name->name}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>धरौटी जम्मा गर्ने</label>
                            <select class="form-control" name="depositor" id="depositor">
                                <option value="">........</option>
                            </select>

                        </div>
                        <button type="submit" class="btn btn-default">Submit</button>
                    </form>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>

@endsection

@section('scripts')

    {{--on change party_type for Vocuher --}}
    <script>
        $(document).ready(function () {
            $('#depositor_type').change(function () {

                let party_type_id = $('#depositor_type :selected').val();
                getPartyByPartyType(party_type_id);

            })
        });
    </script>

    {{--get  party name for voucher--}}
    <script>
        let getPartyByPartyType = function (party_type_id, party_name = '') {
            let url = '{{route('get_party_name_by_party_type_and_office', 123)}}';
            url = url.replace(123, party_type_id);
            $.ajax({
                url: url,
                method: 'get',
                success: function (res) {
                    // console.log($.parseJSON(res));
                    let options = '<option selected>-----------</option>';
                    $.each($.parseJSON(res), function () {
                        // console.log(party_name,this.name_nep);
                        if (party_name == this.name_nep) {
                            options += '<option value="' + this.id + '" selected>' + this.name_nep + '</option>'

                        } else {

                            options += '<option value="' + this.id + '">' + this.name_nep + '</option>'
                        }
                    })

                    $('#depositor').html(options);
                }
            })
        }
    </script>
@endsection
