<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<link rel="stylesheet" href="{{asset('css/styles.css')}}">
<style>
    table {
        border-collapse: collapse;

    }

    .accept-table {

    }

    /*th{*/
    /*  font-weight: 200;*/
    /*}*/
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <table width="99%" style="font-size:13px" class="">
            <tr>
                <img src="{{ asset('img/nepal-govt-logo.png')}}" style="position: absolute;left: 20px;height: 80px;width:100px;
" alt="">
{{--                <td colspan="6" style="text-align: center"><b>प्रदेश सरकार</b></td>--}}
            </tr>
            <tr>
                <td colspan="6" style="text-align: center">{{Auth::user()->office->province->name}}</td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center">{{Auth::user()->office->ministry->name}}</td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center">@If(Auth::user()->office->department){{Auth::user()->office->department->name}}@endif</td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center"><b>{{Auth::user()->office->name}}</b></td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center"><b>{{Auth::user()->office->district->name}}</b></td>
            </tr>
            <tr>
                <td colspan="6">
                    <div style="width: 100%; text-align: center">
                        <b>गोश्वारा भौचर(धरौटी)</b>
                    </div>
                    <div style="float: right; margin-top: -20px">
                        म.ले.प.फा.नं. २०३
                    </div>
                </td>
            </tr>

            <tr>
                <td colspan="4"></td>
                <td class="kalimati" style="text-align: right; ">गोश्वारा भौचर :{{$voucher->voucher_number}}</td>

            </tr>
            <tr>
                <td class="kalimati" colspan="4" style="padding-left: 2px">आर्थिक वर्ष :{{$voucher->fiscalYear->year}}</td>
                <td class="kalimati" style="text-align: right;">मिति :{{$voucher->date_nep}}</td>
            </tr>
        </table>

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="panel panel-primary">
            <div class="panel-body">
                <table width="100%" class="table" id="voucher_table" border="1"
                       style="background-color:#dbdbdb; font-size: 12px">
                    <thead>
                    <tr>
                        <th style="text-align: center">क्र.सं.</th>
                        <th width="100px" style="width: 55px" rowspan="2">संकेत/उप-शीर्षक नंम्बर</th>
                        <th rowspan="2" style="width:88px">क्रियाकलाप/ कार्यक्रम संकेत नं:</th>
                        <th rowspan="2" style="width:555px !important">कारोवारको ब्यहोरा</th>
                        <th rowspan="2" style="width:50px !important">खा.पा.नं.</th>
                        <th rowspan="2" style="width:99px !important">डेबिट</th>
                        <th rowspan="2" style="width:99px !important">क्रेडिट</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($voucher->retention_voucher_details as $index=>$Voucherdetails)
                        <tr style="background-color: white">
                            <td class="kalimati" style="width: 10px;text-align: center">{{++$index}}</td>
                            <td></td>
                            <td>@if($Voucherdetails->retention_record_name){{$Voucherdetails->retention_record_name->purpose}}@endif</td>
                            <td>
                                @if($Voucherdetails->dr_or_cr ==1)
                                    डे. {{$Voucherdetails->hisab_number_name->expense_head_sirsak}} @if($Voucherdetails->advancePayment){{$Voucherdetails->advancePayment->name_nep}}@endif

                                @else
                                    क्रे. {{$Voucherdetails->hisab_number_name->expense_head_sirsak}}

                                @endif
                            </td>
                            <td></td>
                            <td class="kalimati dr-amount" style="text-align: center">
                                @if($Voucherdetails->dr_or_cr ==1)
                                    {{$Voucherdetails->amount}}
                                @endif
                            </td>
                            <td class="kalimati cr-amount" style="text-align: center">
                                @if($Voucherdetails->dr_or_cr ==2)
                                    {{$Voucherdetails->amount}}
                                @endif</td>
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="5" style="text-align: right">जम्मा</td>
                        <td class="kalimati" style="text-align: right"><span id="drTotal"></span></td>
                        <td class="kalimati" style="text-align: right"><span id="crTotal"></span></td>
                    </tr>
                    <tr>
                        <td colspan="11" style="background-color: white;">जम्मा रकम अक्षरमा :रुपैयां {{$amountInWod}} मात्र</td>

                    </tr>
                    <tr>
                        <td colspan="11" style="background-color: white;">कारोबारको
                            व्यहोरा: {{$voucher->long_narration}}</td>
                    </tr>
                    </tbody>
                </table>
                <p> यसमा तल उल्लेखित निम्न बमोजिमको रकम बुझिलियें/बुझिलियों |</p>
                <table class="table" border="1" width="100%" style="background-color:#dbdbdb; font-size: 12px">
                    <tr>
                        <thead>
                        <th>क्र.सं.</th>
                        <th>भुक्तानी पाउनेको नाम</th>
                        {{--                        <th>पान नं</th>--}}
                        <th>भुक्तानी रकम</th>
                        <th>अक्षरेपी रु</th>
                        <th>दस्तखत</th>
                        <th>संलग्न कागजात संख्या</th>
                        <th>कैफियत</th>
                        </thead>
                    </tr>
                    <tbody>
                    @foreach($depositors as $index=>$depositor)
                        <tr style="background-color: white">
                            <td class="kalimati">{{++$index}}</td>
                            <td>{{$depositor->advancePayment->name_nep}}</td>
                            <td class="kalimati" style="text-align: center">{{$depositor->amount}}</td>
                            <td>रुपैयां {{$depositor->get_amount_in_word()}} मात्र</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tr>
                        <td colspan="2" style="text-align: right">जम्मा</td>
                        <td class="kalimati" style="text-align: right">{{$totalDepositorAmount}}</td>
                        <td style="text-align: left">रुपैयां {{$totalDepositorAmountInWord}} मात्र</td>
                        <td></td>
                        <td></td>
                        <td></td>

                    </tr>

                </table>

                <table width="99%" style="font-size: 13px">
                    <tr>
                        <br>
                        <br>
                        <br>
                        <br>
                    </tr>
                    <tr>
                        <td style="padding-left: 52px;">तयार गर्ने :
                            {{--                            @if($voucher->karmachari_prepare_by and $voucher->karmachari_prepare_by->name_nepali)--}}
                            {{--                                {{$voucher->karmachari_prepare_by->name_nepali}}--}}
                            {{--                             @endif--}}
                        </td>
                        <td>पेश गर्ने :
                            {{--                            @if($voucher->karmachari_submit_by and $voucher->karmachari_submit_by->name_nepali)--}}
                            {{--                                {{$voucher->karmachari_submit_by->name_nepali}}--}}
                            {{--                             @endif--}}
                        </td>
                        <td>सदर गर्ने :
                        {{--                            @if($voucher->karmachari_approved_by and $voucher->karmachari_approved_by->name_nepali)--}}
                        {{--                            {{$voucher->karmachari_approved_by->name_nepali}}</td>--}}
                        {{--                            @endif--}}
                    </tr>
                    <tr>
                        <td style="padding-left: 52px;">पद :
                            {{--                            @if($voucher->karmachari_prepare_by and $voucher->karmachari_prepare_by->get_pad)--}}
                            {{--                                {{$voucher->karmachari_prepare_by->get_pad->name}}--}}
                            {{--                            @endif--}}
                        </td>
                        <td>पद :
                            {{--                            @if($voucher->karmachari_submit_by and $voucher->karmachari_submit_by->get_pad)--}}
                            {{--                                {{$voucher->karmachari_submit_by->get_pad->name}}--}}
                            {{--                            @endif--}}
                        </td>
                        <td>पद :
                            {{--                            @if($voucher->karmachari_approved_by and $voucher->karmachari_approved_by->get_pad)--}}
                            {{--                                {{$voucher->karmachari_approved_by->get_pad->name}}--}}
                            {{--                            @endif--}}
                        </td>
                    </tr>
                    <tr>
                        <td class="kalimati" style="padding-left: 52px;">मिति : {{$voucher->date_nep}}</td>
                        <td class="kalimati">मिति : {{$voucher->date_nep}}</td>
                        <td class="kalimati">मिति : {{$voucher->date_nep}}</td>
                    </tr>

                </table>
            </div>

        </div>

    </section>

    <!-- /.content -->
</div>

<script>
    $(document).ready(function () {
        let trs = $('#voucher_table tbody').find('tr');
        let crAmount = 0;
        let drAmount = 0;
        $.each(trs, function (key, tr) {

            let drAmountTemp = $(this).find('td.dr-amount').text();
            drAmountTemp = $.trim(drAmountTemp);
            if (drAmountTemp) {

                drAmount += parseFloat(drAmountTemp);

            }
            let crAmountTemp = $(this).find('td.dr-amount').text();
            crAmountTemp = $.trim(crAmountTemp);
            if (crAmountTemp) {

                crAmount += parseFloat(crAmountTemp);

            }


        });
        $('#drTotal').text(drAmount);
        $('#crTotal').text(crAmount);
    })
</script>

<script>
    let changeToNepali = function (text) {
        let numbers = $.trim(text).split('');
        let nepaliNo = '';
        $.each(numbers, function (key, value) {
            if (value) {
                if (!isNaN(value) && value != ' ') {
                    if (value == 1)
                        nepaliNo += "१";

                    else if (value == 2)
                        nepaliNo += "२";

                    else if (value == 3)
                        nepaliNo += "३";

                    else if (value == 4)
                        nepaliNo += "४";

                    else if (value == 5)
                        nepaliNo += "५";

                    else if (value == 6)
                        nepaliNo += "६";

                    else if (value == 7)
                        nepaliNo += "७";

                    else if (value == 8)
                        nepaliNo += "८";

                    else if (value == 9)
                        nepaliNo += "९";

                    else if (value == 0)
                        nepaliNo += "०";
                } else {
                    nepaliNo += value
                }
            }
        });
        // console.log(nepaliNo);
        return nepaliNo;
    };


    $('.e-n-t-n-n').each(function () {
        let nepaliNo = changeToNepali($(this).text());
        let nepaliVal = changeToNepali($(this).val());

        $(this).text(nepaliNo);
        $(this).val(nepaliVal);
    });

</script>


