@extends('frontend.layouts.app')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>

                <small>म.ले.प.फा.नं. २०८ :: बजेट खाता</small>
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <form action="{{route('report.budget.khata')}}" method="post" target="_blank">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="name">कार्यालय : </label>
                            <select name="office_id" class="form-control">
                                <option value="{{Auth::user()->office_id}}">{{Auth::user()->office->name}}</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="name">आर्थिक वर्ष:</label>
                            <select name="fiscal_year" class="form-control">
                                <option value="{{$fiscalYear->year}}">{{$fiscalYear->year}}</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>बजेट उप शिर्षक</label>
                            <select name="budget_sub_head" class="form-control">
                                @foreach($programs as $program)
                                    <option value="{{$program->id}}">{{$program->name}}
                                        | {{$program->program_code}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group center-block">
                            <label>महिना</label>
                            <select name="month" id="month" class="form-control">
                                <option value="10">बैशाख</option>
                                <option value="11">जेष्ठ</option>
                                <option value="12">असार</option>
                                <option value="1">साउन</option>
                                <option value="2">भदौ</option>
                                <option value="3">असोज</option>
                                <option value="4">कार्तिक</option>
                                <option value="5">मंसिर</option>
                                <option value="6">पुस</option>
                                <option value="7">माघ</option>
                                <option value="8">फगुन</option>
                                <option value="9">चैत्र</option>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-default">Submit</button>
                    </form>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>

@endsection