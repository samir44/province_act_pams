<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
{{--<link href="{{ asset('css/nepali-datepicker.css') }}" rel="stylesheet">--}}
<link rel="stylesheet" href="{{asset('css/styles.css')}}">

<style>
    table {
        border-collapse: collapse;
    }

    body {
        overflow-x: scroll !important;
    }

    .table {
        width: 100%;
        overflow-x: scroll;
    }

    td {
        min-height: 10px;
    }

    body {

        overflow-x: hidden;
    }

    .floatLeft {
        width: 50%;
        float: left;
    }

    .floatRight {
        width: 50%;
        float: right;
    }

    .container {
        overflow: hidden;
    }

    @media print {
        #downloadMe {
            display: none;
        }
    }

    /*th{*/
    /*  font-weight: 200;*/
    /*}*/
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <input type="button" style="position: absolute; top:20px;right: 20px" value="Export to Excel" id="downloadMe">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div id="tableHeadingWrapper">

            <table width="99%" style="border-collapse: collapse; font-size:13px">
                <tr>
                    <img src="{{ asset('img/nepal-govt-logo.png')}}" style="position: absolute;left: 20px;height: 80px;width:100px;
" alt="">
                    {{--                <td colspan="6" style="text-align: center"><b>प्रदेश सरकार</b></td>--}}
                </tr>
                <tr>
                    <td colspan="6" style="text-align: center">{{Auth::user()->office->province->name}}</td>
                </tr>
                <tr>
                    <td colspan="6" style="text-align: center">{{Auth::user()->office->ministry->name}}</td>
                </tr>
                <tr>
                    <td colspan="6" style="text-align: center">
                        @if(Auth::user()->office->department)
                            {{Auth::user()->office->department->name}}</td>
                    @endif
                </tr>
                <tr>
                    <td colspan="6" style="text-align: center"><b>{{Auth::user()->office->name}}</b></td>
                </tr>
                <tr>
                    <td colspan="6" style="text-align: center"><b>{{Auth::user()->office->district->name}}</b></td>
                </tr>
                <tr>
                    <td class="kalimati" colspan="6" style="text-align: center"><b>कार्यालय कोड
                            नं. {{Auth::user()->office->office_code}}</b></td>
                </tr>
                <tr>
                    <td colspan="6">
                        <div style="width: 100%; text-align: center">
                            <b>बजेट खाता</b>
                        </div>
                        <div style="float: right; margin-top: -20px">
                            म.ले.प.फा.नं. २०८
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br>
                        <br>

                    </td>
                </tr>
                <tr>
                    <td class="kalimati" style="text-align: right">आर्थिक वर्ष :{{$datas['fiscal_year']}} <span
                                class="e-n-t-n-n"></span><br>
                        {{--                    मिति :--}}
                    </td>

                </tr>
                <tr>
                    <td class="kalimati">बजेट उप शीर्षक : {{$program->name}}<b>
                        </b><br>

                    </td>

                </tr>

            </table>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <?php $b_count = $budgetByExpenseHeads->count() ?>
        <?php $v_count = $thisMonthVouchers->count() + 2 ?>
        <div style="position: relative" id="tableWrapper">

            <table class="table" border="1">
                <tr style="height: 23px">
                    <td style="height: 23px;border-bottom: 1px solid #fff;"></td>
                    <td style="height: 23px;"></td>
                    <td colspan="{{$b_count}}" style="height: 23px"></td>
                    <td style="height: 23px;"></td>
                </tr>
                <tr>
                    <td rowspan="{{$v_count+1}}">
                        <p style="width: 40px">प्रथम खण्ड</p>
                    </td>
                    <td><p style="width: 45px">बजेट</p></td>
                    <td>
                        <table style="width: 500px!important; font-size: 14px!important;">
                            <tr>
                                <td colspan="5" style="text-align:left;height: 23px">खर्च उपशिर्षकको नाम</td>
                            </tr>
                            <tr>
                                <td colspan="5" style="text-align:left;height: 100px"></td>
                            </tr>
                            <tr>
                                <td colspan="5" style="text-align:left;height: 23px">स्विकृत आर्थिक वर्षको बजेट बिनियोजन
                                </td>
                            </tr>
                            <tr>
                                <td colspan="5" style="text-align:left;height: 23px">स्विकृत आर्थिक वर्षको थप बजेट
                                    बिनियोजन
                                </td>
                            </tr>
                            <tr>
                                <td colspan="5" style="text-align:left;height: 23px">रकमान्तरबाट थप/घट रकम</td>
                            </tr>
                            <tr>
                                <td colspan="5" style="text-align:left;height: 23px">श्रोतान्तरबाट थप/घट</td>

                            </tr>
                            <tr>
                                <td colspan="5" style="text-align:left;height: 23px;border-top: 1px solid #afafaf;">
                                    <b>जमा कायम भएको बजेट</b></td>
                                <td class="kalimati">{{$totalBudget}}</td>
                            </tr>

                        </table>
                    </td>
                    @foreach($budgetByExpenseHeads as $budgetExpenseHead)
                        <td style="vertical-align: top; padding: 0!important; margin: 0; width: 150px">
                            <table class="table" style="width: 150px!important;">
                                <tr>
                                    <td class="kalimati"
                                        style="vertical-align: middle;text-align:center; border-bottom: 1px solid #000; height: 23px">
                                        <p style="width: 150px"></p>{{$budgetExpenseHead->expense_head}}</td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: middle;text-align:center; border-bottom: 1px solid #000;  height: 100px">
                                        <p style="width: 150px">{{$budgetExpenseHead->expense_by_expense_head->expense_head_sirsak}}</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="kalimati"
                                        style="vertical-align: middle;text-align:right; border-bottom: 1px solid #000;  height: 23px">
                                        <p style="width: 150px">{{$budgetExpenseHead->getInitialBudget($datas['office_id'],$datas['budget_sub_head'],$budgetExpenseHead->expense_head_id)}}</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="kalimati"
                                        style="vertical-align: middle;text-align:right; border-bottom: 1px solid #000;  height: 23px">
                                        <p style="width: 150px">{{$budgetExpenseHead->getAddBudget($datas['office_id'],$datas['budget_sub_head'],$budgetExpenseHead->expense_head_id)}}</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="kalimati"
                                        style="vertical-align: middle;text-align:right; border-bottom: 1px solid #000;  height: 23px">
                                        <p style="width: 150px"> {{$budgetExpenseHead->getReduceBudget($datas['office_id'],$datas['budget_sub_head'],$budgetExpenseHead->expense_head_id)}}</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="kalimati"
                                        style="vertical-align: middle;text-align:right; border-bottom: 1px solid #000;  height: 23px">
                                        <p style="width: 150px">{{$budgetExpenseHead->getReduceBudget($datas['office_id'],$datas['budget_sub_head'],$budgetExpenseHead->expense_head_id)}}</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="kalimati" style="text-align: right">{{$budgetExpenseHead->getFinalBudget($datas['office_id'],$datas['budget_sub_head'],$budgetExpenseHead->expense_head_id)}}</td>
                                </tr>
                            </table>
                        </td>
                    @endforeach
                </tr>

            </table>

            <table class="table" border="1">
                <tr>
                    <td rowspan="{{$v_count}}">
                        <p style="width: 40px">द्वितिय खण्ड</p>
                    </td>
                    <td rowspan="{{$v_count}}"><p style="width: 45px">निकासा</p></td>
                    <td><p style="width: 90px">मिति</p></td>
                    <td><p style="width: 35px">भौ न</p></td>
                    {{--                    <td><p style="width: 60px">संकेत न.</p></td>--}}
                    <td><p style="width: 237px">विवरण</p></td>
                    <td><p style="width: 85px">जम्मा</p></td>
                    @for($i=0; $i<$b_count; $i++)
                        <td style="vertical-align: top; padding: 0!important; margin: 0"><p
                                    style="width: 150px"></p>
                        </td>
                    @endfor
                </tr>
                @foreach ($thisMonthVouchers as $voucher)
                    @if($voucher->isNikasa())
                        <tr>
                            <td style="text-align: center"><p style="width: 85px">{{$voucher->data_nepali}}</p></td>
                            <td class="kalimati" style="text-align: center"><p
                                        style="width: 35px">{{$voucher->jv_number}}</td>
                            {{--                                    <td>2</td>--}}
                            <td><p style="width: 237px">{{$voucher->short_narration}}</p></td>
                            <td class="kalimati" style="text-align: center"><p
                                        style="width: 85px">{{$voucher->payement_amount}}</p></td>
                            @foreach($budgetByExpenseHeads as $budgetExpenseHead)
                                <td class="kalimati"
                                    style="vertical-align: top; padding: 0!important; margin: 0; text-align: center">
                                    <p style="width: 150px">
                                        {{$voucher->get_nikasa_by_budget_expense_head_id($datas['office_id'],$datas['budget_sub_head'],$budgetExpenseHead->expense_head_id,$datas['month'])}}
                                    </p>
                                </td>
                            @endforeach


                        </tr>
                    @endif

                @endforeach
                <tr>
                    <td colspan="3" style="text-align: right"><b>यो महिनाको निकासा</b></td>
                    <td class="kalimati" style="text-align: right"><p style="width: 85px">{{$thisMonthTotalNikasa}}</p>
                    </td>
                    @foreach($budgetByExpenseHeads as $budgetExpenseHead)
                        <td class="kalimati"
                            style="vertical-align: top; padding: 0!important; margin: 0; text-align: center">
                            <p style="width: 150px">
                                {{$budgetExpenseHead->thisMonthNikasa($datas['office_id'],$datas['budget_sub_head'],$budgetExpenseHead->expense_head_id,$datas['month'])}}
                            </p>
                        </td>
                    @endforeach

                </tr>
                <tr>
                    <td colspan="3" style="text-align: right"><b>अघिल्लो महिनासम्मको निकासा</b></td>
                    <td class="kalimati" style="text-align: right"><p style="width: 85px">{{$upToPreMonthTotalNikasa}}</p>
                    </td>
                    @foreach($budgetByExpenseHeads as $budgetExpenseHead)
                        <td class="kalimati"
                            style="vertical-align: top; padding: 0!important; margin: 0; text-align: center">
                            <p style="width: 150px">
                                {{$budgetExpenseHead->upToPreMonthNikasa($datas['office_id'],$datas['budget_sub_head'],$budgetExpenseHead->expense_head_id,$datas['month']-1)}}
                            </p>
                        </td>
                    @endforeach
                </tr>
                <tr>
                    <td colspan="3" style="text-align: right"><b>जम्मा निकासा</b></td>
                    <td class="kalimati" style="text-align: right"><p style="width: 85px">{{$totalNikasa}}</p>
                    </td>

                    @foreach($budgetByExpenseHeads as $budgetExpenseHead)
                        <td class="kalimati"
                            style="vertical-align: top; padding: 0!important; margin: 0; text-align: center">
                            <p style="width: 150px">
                                {{$budgetExpenseHead->totalNikasa($datas['office_id'],$datas['budget_sub_head'],$budgetExpenseHead->expense_head_id,$datas['month'])}}
                            </p>
                        </td>
                    @endforeach

                </tr>
            </table>

            <table class="table" border="1">
                <tr>
                    <td rowspan="{{$v_count+1}}">
                        <p style="width: 40px">तृतीय खण्ड</p>
                    </td>
                    <td rowspan="{{$v_count+1}}"><p style="width: 45px">खर्च</p></td>

                </tr>
                @foreach ($thisMonthVouchers as $voucher)
                    @if($voucher->isExpense())
                        <tr>
                            <td style="text-align: center"><p style="width: 85px">{{$voucher->data_nepali}}</p></td>
                            <td class="kalimati" style="text-align: center"><p
                                        style="width: 35px">{{$voucher->jv_number}}</p></td>
                            {{--                        <td><p style="width: 60px">2</p></td>--}}
                            <td><p style="width: 237px">{{$voucher->short_narration}}</p></td>
                            <td class="kalimati" style="text-align: right"><p
                                        style="width: 85px">{{$voucher->thisTotalExp()}}</p></td>
                            @foreach($budgetByExpenseHeads as $budgetExpenseHead)
                                <td class="kalimati"
                                    style="vertical-align: top; padding: 0!important; margin: 0; text-align: right">
                                    <p style="width: 150px">
                                        {{$voucher->get_kharcha_by_budget_expense_head_id($budgetExpenseHead->expense_head_id)}}
                                    </p>
                                </td>

                            @endforeach
                        </tr>
                    @endif
                @endforeach
                <tr>
                    <td colspan="3" style="text-align: right"><b>यो महिनाको जम्मा खर्च</b></td>
                    <td class="kalimati" style="text-align: right"><p style="width: 85px">{{$thisMonthTotalExpense}}</p></td>
                    @foreach($budgetByExpenseHeads as $budgetExpenseHead)
                        <td class="kalimati"
                            style="vertical-align: top; padding: 0!important; margin: 0; text-align: right">
                            <p style="width: 150px">
                                {{$budgetExpenseHead->thisMonthTotalExpense($datas['office_id'],$datas['budget_sub_head'],$budgetExpenseHead->expense_head_id,$datas['month'])}}
                            </p>
                        </td>

                    @endforeach

                </tr>
                <tr>
                    <td colspan="3" style="text-align: right"><b>अधिल्लो महिनाकोसम्मको जम्मा खर्च</b></td>
                    <td class="kalimati" style="text-align: right"><p style="width: 85px">{{$upToPreMonthTotalExpense}}</p></td>
                    @foreach($budgetByExpenseHeads as $budgetExpenseHead)
                        <td class="kalimati"
                            style="vertical-align: top; padding: 0!important; margin: 0; text-align: right">
                            <p style="width: 150px">
                                {{$budgetExpenseHead->preMonthTotalExpense($datas['office_id'],$datas['budget_sub_head'],$budgetExpenseHead->expense_head_id,$datas['month']-1)}}
                            </p>
                        </td>

                    @endforeach

                </tr>
                <tr>
                    <td colspan="3" style="text-align: right"><b>जम्मा खर्च</b></td>
                    <td class="kalimati" style="text-align: right"><p style="width: 85px">{{$totalExpense}}</p></td>
                    @foreach($budgetByExpenseHeads as $budgetExpenseHead)
                        <td class="kalimati"
                            style="vertical-align: top; padding: 0!important; margin: 0; text-align: right">
                            <p style="width: 150px">
                                {{$budgetExpenseHead->totalExpense($datas['office_id'],$datas['budget_sub_head'],$budgetExpenseHead->expense_head_id,$datas['month'])}}
                            </p>
                        </td>

                    @endforeach

                </tr>
                <tr>
                    <td colspan="3" style="text-align: right"><b>बाकीँ बजेट</b></td>
                    <td class="kalimati" style="text-align: right">{{$remainBudget}}</td>
                    @foreach($budgetByExpenseHeads as $budgetExpenseHead)
                        <td class="kalimati"
                            style="vertical-align: top; padding: 0!important; margin: 0; text-align: right">
                            <p style="width: 150px">
                                {{$budgetExpenseHead->totalRemainBudget($datas['office_id'],$datas['budget_sub_head'],$budgetExpenseHead->expense_head_id,$datas['month'])}}
                            </p>
                        </td>

                    @endforeach
                </tr>
            </table>
        </div>
    </section>
    <!-- /.content -->
</div>
</div>
<script>
    $("#downloadMe").click(function (e) {
        let allTable = $('div[id$=tableHeadingWrapper]').html() + $('div[id$=tableWrapper]').html();
        console.log(allTable);
        window.open('data:application/vnd.ms-excel,' + encodeURIComponent(allTable));
        e.preventDefault();
    });

    let changeToNepali = function (text) {
        let numbers = text.split('');
        let nepaliNo = '';
        $.each(numbers, function (key, value) {
            if (value) {
                if (value == 1)
                    nepaliNo += "१";
                else if (value == 2)

                    nepaliNo += "२";
                else if (value == 3)

                    nepaliNo += "३";
                else if (value == 4)

                    nepaliNo += "४";
                else if (value == 5)

                    nepaliNo += "५";
                else if (value == 6)

                    nepaliNo += "६";
                else if (value == 7)

                    nepaliNo += "७";
                else if (value == 8)

                    nepaliNo += "८";
                else if (value == 9)

                    nepaliNo += "९";
                else if (value == 0)

                    nepaliNo += "०";
                else if (value == ',')

                    nepaliNo += ",";
                else if (value == '.')

                    nepaliNo += ".";
                else if (value == '/')

                    nepaliNo += "/";
            }
        });
        console.log(nepaliNo);
        return nepaliNo;
    };

    // let change_all_to_nepali_number = function(){
    $('.e-n-t-n-n').each(function () {
        let nepaliNo = changeToNepali($(this).text());
        let nepaliVal = changeToNepali($(this).val());

        $(this).text(nepaliNo);
        $(this).val(nepaliVal);
    });
    // }


</script>

<script>
    $(document).ready(function () {

        let month = $('span.month').text();
        // alert(month);

    })
</script>

