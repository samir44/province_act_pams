@extends('frontend.layouts.app')
@section('title')
    Upload Excel
@stop
@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Upload Excel

            </h1>
            <ul class="breadcrumb">
                <li> <a type="button" href="{{route('program')}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> सुची हेर्ने</a></li>
            </ul>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <form action="{{route('excel.store')}}" method="post" id="programCreateForm">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="excel">Excel: </label>
                            <input type="file" class="form-control" id="excel" placeholder="Excel" name="excel">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary" id="btnProgramSave" style="margin-top: 18px;">थप</button>
                        </div>
                    </form>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>

@endsection
@section('scripts')

@stop