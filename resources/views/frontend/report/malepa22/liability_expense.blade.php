<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<link rel="stylesheet" href="{{asset('css/styles.css')}}">

<style>
  table{
    border-collapse: collapse;
    font-size: 13px;
  }

  /*th{*/
  /*  font-weight: 200;*/
  /*}*/
</style>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <table class="kalimati" width="99%" >
        <tr>
          <img src="{{ asset('img/nepal-govt-logo.png')}}" style="position: absolute;left: 20px;height: 80px;width:100px;
" alt="">
          <td colspan="6" style="text-align: center">प्रदेश सरकार</td>
        </tr>
        <tr>
          <td colspan="6" style="text-align: center">{{Auth::user()->office->province->name}}</td>
        </tr>
        <tr>
          <td colspan="6" style="text-align: center">{{Auth::user()->office->ministry->name}}</td>
        </tr>
        <tr>
          <td colspan="6" style="text-align: center">{{Auth::user()->office->name}}</td>
        </tr>
        <tr>
          <td colspan="6" style="text-align: center"><b>{{Auth::user()->office->district->name}}</b></td>
        </tr>
        <tr>
          <td colspan="4" style="padding-left:670px; padding-top: 17px;">
            <b>{{$expense_head_sirsak}}</b>
          </td>

          <td style="text-align: right;">म.ले.प.फा.नं. २२</td>
        </tr>
        <tr>
          <td>
            <br>
            <br>

          </td>
        </tr>
        <tr style="float: left">
          <td>आ.व. : {{$data['fiscal_year']}}</td>
          <td >खाता :{{$expense_head_sirsak}} ({{$expense_head_code}})</td>

        </tr>

      </table>

    </section>

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-body">
          <table class="table" id="liability_report_table" width="100%" border="1" style="background-color:#dbdbdb; font-size: 12px">
            <thead>
            <tr>
              <th>सि.न.</th>
              <th>भौचर न.</th>
              <th>मिति</th>
              <th>कारोवारको व्यहोरा</th>
              <th>हिसाब नं.</th>
              <th>डेबिट</th>
              <th>क्रेडिट</th>
              <th>बाँकी</th>
            </tr>
            </thead>
            <tbody>
              @foreach($liabilityLists as $index=>$liabilityList)
              <tr class="kalimati" style="background-color: white;">
                <td style="text-align: center">{{++$index}}</td>
                <td style="text-align: center"><u><a href="{{route('voucher.view',$liabilityList->voucher->id)}}" target="_blank">{{$liabilityList->voucher->jv_number}}</a></u></td>
                <td style="text-align: center">{{$liabilityList->voucher->data_nepali}}</td>
                <td style="text-align: center">{{$liabilityList->voucher->short_narration}}</td>
                <td style="text-align: center"></td>

                <td class="dr-amount" style="text-align: center">
                  @if($liabilityList->dr_or_cr == 1)

                        {{$liabilityList->dr_amount}}

                  @endif
                </td>
                <td class="cr-amount" style="text-align: center">
                    @if($liabilityList->dr_or_cr == '2')

                        {{$liabilityList->cr_amount}}

                    @endif
                 </td>
                <td class="remain" style="text-align: center"></td>
              </tr>
              @endforeach
            <tr class="kalimati">
              <td colspan="5"  style="text-align: right">जम्मा</td>
              <td style="text-align: center">{{$dr_amount_total}}</td>
              <td style="text-align: center">{{$cr_amount_total}}</td>
              <td style="text-align: center">

                 {{$remain_total}}
              </td>

            </tr>
            </tbody>
          </table>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>

{{--  Dr and Cr खो हिसाब काताब  --}}
<script>
  $(document).ready(function () {

    let trs = $('#liability_report_table').find('tr').not(':last');
    let drEng = 0;
    let crEng = 0;
    let remain = 0;

    $.each(trs, function () {

      let drAmount = ($.trim($(this).find('td.dr-amount').text()));
      let crAmount = ($.trim($(this).find('td.cr-amount').text()));

      if(drAmount){

        remain = parseFloat(drEng) + parseFloat(drAmount);
        drEng +=  parseFloat(current_dr_amount_eng);

        $(this).find('td.remain').text(remain)


      }
      if(crAmount) {

        remain = remain - parseFloat(crAmount);
        if(remain < 0 ){
            // remain = (-1) * remain;
            // remain = parseFloat(remain)  + parseFloat(crAmount);
          crEng +=  parseFloat(crAmount);
          $(this).find('td.remain').text('(' + (-1) * remain+ ')')
        }

      }

      })

  })
</script>

{{--change to nep --}}
<script>
  let changeToNepali = function (text) {
    (text = $.trim(text));
    let numbers = text.split('');
    let nepaliNo = '';
    $.each(numbers, function (key, value) {
      if (value) {
        if(value == 1)
          nepaliNo+="१";
        else if(value == 2)

          nepaliNo+="२";
        else if(value == 3)

          nepaliNo+="३";
        else if(value == 4)

          nepaliNo+="४";
        else if(value == 5)

          nepaliNo+="५";
        else if(value == 6)

          nepaliNo+="६";
        else if(value == 7)

          nepaliNo+="७";
        else if(value == 8)

          nepaliNo+="८";
        else if(value == 9)

          nepaliNo+="९";
        else if(value == 0)

          nepaliNo+="०";
        else if(value == ',')

          nepaliNo+=",";
        else if(value == '/')

          nepaliNo+="/";
        else if(value == '.')

          nepaliNo+=".";
        else if(value == '_')

          nepaliNo+="_";
      }
    });
    console.log(nepaliNo);
    return nepaliNo;
  };

  $('.e-n-t-n-n').each(function () {
    let nepaliNo = changeToNepali($(this).text());
    $(this).text(nepaliNo);
  });


</script>


{{--nep to english--}}
<script>
  function convertNepaliToEnglish(input) {
    console.log(input);
    var charArray = input.split('.');
    charArray = charArray[0].split('');
    // console.log('Test',charArray[0]);
    var engDate = '';
    $.each(charArray, function (key,value) {

      switch (value) {
        case '१':
          engDate += '1'
          break
        case '२':
          engDate += '2'
          break
        case '३':
          engDate += '3'
          break
        case '४':
          engDate += '4'
          break
        case '५':
          engDate += '5'
          break
        case '६':
          engDate += '6'
          break
        case '०':
          engDate += '0'
          break
        case '७':
          engDate += '7'
          break
        case '८':
          engDate += '8'
          break
        case '९':
          engDate += '9'
          break

        case '-':
          engDate += '-'
          break
      }
      //console.log(engDate)
    })

    return engDate

  }
</script>
