<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<link rel="stylesheet" href="{{asset('css/styles.css')}}">

<style>

  table{
    border-collapse: collapse;
  }

  /*th{*/
  /*  font-weight: 200;*/
  /*}*/
</style>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <table width="99%" class="kalimati">
        <tr>
          <img src="{{ asset('img/nepal-govt-logo.png')}}" style="position: absolute;left: 20px;height: 80px;width:100px;
" alt="">
          <td colspan="6" style="text-align: center">प्रदेश सरकार</td>
        </tr>
        <tr>
          <td colspan="6" style="text-align: center">{{Auth::user()->office->province->name}}</td>
        </tr>
        <tr>
          <td colspan="6" style="text-align: center">{{Auth::user()->office->ministry->name}}</td>
        </tr>
        <tr>
          <td colspan="6" style="text-align: center">{{Auth::user()->office->name}}</td>
        </tr>
        <tr>
          <td colspan="6" style="text-align: center"><b>{{Auth::user()->office->district->name}}</b></td>
        </tr>
        <tr>
          <td colspan="4" style="padding-left:670px;">
            <b>खाता</b>
          </td>
          <td style="text-align: right;">म.ले.प.फा.नं. २२</td>
        </tr>
        <tr>
          <td>
            <br>
            <br>
          </td>
        </tr>
        <tr style="float: left">
          <td>आर्थीक वर्ष :{{$data['fiscal_year']}}</td>
          <td >खाता :{{$party_name}}</td>

        </tr>

      </table>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-body">
          <table class="" width="100%" border="1" style="background-color:#dbdbdb; font-size: 12px">
            <thead>
            <tr class="kalimati">
              <th>सि.न.</th>
              <th>भौचर न.</th>
              <th>मिति</th>
              <th>कारोवारको व्यहोरा</th>
              <th>बैकं खाा‍ता न.</th>
              <th>चेक न.</th>
              <th>जारी मिति</th>
              <th>बिल न.</th>
              <th>भुक्तानी रकम</th>
              <th>अग्रिम कर कट्टी रकम</th>
              <th>भ्याट रकम</th>
              <th>कैफियत</th>
            </tr>
            </thead>
            <tbody>
              @foreach($preBhuktaniList as $index=>$preBhuktani)
              <tr class="kalimati" style="background-color: white;">
                <td style="text-align: center">{{++$index}}</td>
                <td style="text-align: center"><u><a href="{{route('voucher.view',$preBhuktani->voucher->id)}}" target="_blank">{{$preBhuktani->voucher->jv_number}}</a></u></td>
                <td  style="text-align: center" >{{$preBhuktani->voucher->data_nepali}}</td>
                <td style="text-align: center">{{$preBhuktani->voucher->short_narration}}</td>
                <td style="text-align: center">
                  @if($preBhuktani->party and $preBhuktani->party->party_khata_number)
                    {{$preBhuktani->party->party_khata_number}}
                    @endif
                </td>
                <td style="text-align: center">

                </td>
                <td> </td>
                <td> </td>
                <td style="text-align: center">{{$preBhuktani->amount}}</td>
                <td style="text-align: center">{{$preBhuktani->advance_vat_deduction}}</td>
                <td style="text-align: center">{{$preBhuktani->vat_amount}}</td>
                <td style="text-align: center"></td>
              </tr>
              @endforeach
            <tr class="kalimati">
              <td colspan="8"  style="text-align: right">जम्मा</td>
              <td style="text-align: center">{{$total_bhuktani}}</td>
              <td style="text-align: center">{{$total_advance_vat_deduction}}</td>
              <td style="text-align: center">{{$total_vat_amount}}</td>
              <td></td>
            </tr>
            </tbody>
          </table>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>



<script>
  let changeToNepali = function (text) {
    let numbers = text.split('');
    let nepaliNo = '';
    $.each(numbers, function (key, value) {
      if (value) {
        if(value == 1)
          nepaliNo+="१";
        else if(value == 2)

          nepaliNo+="२";
        else if(value == 3)

          nepaliNo+="३";
        else if(value == 4)

          nepaliNo+="४";
        else if(value == 5)

          nepaliNo+="५";
        else if(value == 6)

          nepaliNo+="६";
        else if(value == 7)

          nepaliNo+="७";
        else if(value == 8)

          nepaliNo+="८";
        else if(value == 9)

          nepaliNo+="९";
        else if(value == 0)

          nepaliNo+="०";
        else if(value == ',')

          nepaliNo+=",";
        else if(value == '/')

          nepaliNo+="/";
        else if(value == '.')

          nepaliNo+=".";
      }
    });
    console.log(nepaliNo);
    return nepaliNo;
  };

  $('.e-n-t-n-n').each(function () {
    let nepaliNo = changeToNepali($(this).text());
    $(this).text(nepaliNo);
  });


</script>