<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<link rel="stylesheet" href="{{asset('css/styles.css')}}">

<style>
  table{
    border-collapse: collapse;
  }

  /*th{*/
  /*  font-weight: 200;*/
  /*}*/
</style>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <table class="kalimati" width="99%" >
        <tr>
          <img src="{{ asset('img/nepal-govt-logo.png')}}" style="position: absolute;left: 20px;height: 80px;width:100px;
" alt="">
          <td colspan="6" style="text-align: center">प्रदेश सरकार</td>
        </tr>
        <tr>
          <td colspan="6" style="text-align: center">{{Auth::user()->office->province->name}}</td>
        </tr>
        <tr>
          <td colspan="6" style="text-align: center">{{Auth::user()->office->ministry->name}}</td>
        </tr>
        <tr>
          <td colspan="6" style="text-align: center">{{Auth::user()->office->name}}</td>
        </tr>
        <tr>
          <td colspan="6" style="text-align: center"><b>{{Auth::user()->office->district->name}}</b></td>
        </tr>
        <tr>
            <td colspan="4" style="padding-left:670px; padding-top: 17px;">
                <b>{{$party_name}} पेश्की खाता</b>
            </td>

            <td style="text-align: right;">म.ले.प.फा.नं. २२</td>
        </tr>
        <tr>
          <td>
            <br>
            <br>

          </td>
        </tr>
        <tr style="float: left">
          <td>आ.व. : {{$data['fiscal_year']}}</td>
          <td >खाता :{{$party_name}}</td>

        </tr>

      </table>

    </section>

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-body">
          <table class="kalimati" width="100%" border="1" style="background-color:#dbdbdb; font-size: 12px" id="peski_table">
            <thead>
            <tr>
              <th>सि.न.</th>
              <th>भौचर न.</th>
              <th>मिति</th>
              <th>कारोवारको व्यहोरा</th>
              <th>हिसाब नं.</th>
              <th>डेबिट</th>
              <th>क्रेडिट</th>
              <th>बाँकी</th>
            </tr>
            </thead>
            <tbody>
              @foreach($peskiDetails as $index=>$peski)
              <tr style="background-color: white;">
                <td style="text-align: center">{{++$index}}</td>
                <td style="text-align: center"><u><a href="{{route('voucher.view',$peski->voucher->id)}}" target="_blank">{{$peski->voucher->jv_number}}</a></u></td>
                <td style="text-align: center">{{$peski->voucher->data_nepali}}</td>
                <td style="text-align: center">{{$peski->voucher->short_narration}}</td>
                <td style="text-align: center">
                  {{$peski->expense_head->expense_head_code}}


                </td>

                <td class="dr-amount" style="text-align: center">
                  @if($peski->dr_or_cr == 1 and $peski->ledger_type_id == 4)
                    {{$peski->dr_amount}}
                  @endif
                </td>
                <td class="cr-amount" style="text-align: center">
                  @if($peski ->dr_or_cr == 2 and $peski->ledger_type_id == 5)
                    {{$peski->cr_amount}}
                  @endif
                 </td>
                <td class="remain" style="text-align: center"></td>

              </tr>
              @endforeach
            <tr>
              <td colspan="5"  style="text-align: right">जम्मा</td>
              <td style="text-align: center">{{$dr_amount_total}}</td>
              <td style="text-align: center">{{$cr_amount_total}}</td>
              <td style="text-align: center">{{$total_remain}}</td>

            </tr>
            </tbody>
          </table>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>

{{--Dr and Cr amount को हिसाब किताब--}}
<script>
  $(document).ready(function () {

    let drAmount = 0;
    let trs = $('#peski_table tbody').find('tr').not(':last');
    let drEng = 0;
    let crEng = 0;
    let remain = 0;
    $.each(trs, function () {

      let drAmount = ($.trim($(this).find('td.dr-amount').text()));
      let crAmount = ($.trim($(this).find('td.cr-amount').text()));

      if(drAmount){

        remain = parseFloat(drEng) + parseFloat(drAmount);
        drEng +=  parseFloat(drAmount);
        $(this).find('td.remain').text(remain)


      }
      if(crAmount){

        remain = parseFloat(remain) - parseFloat(crAmount);
        crEng +=  parseFloat(crAmount);
        $(this).find('td.remain span').text(remain)

      }

    })

  })
</script>

{{--Eng to nepali --}}
<script>
  let changeToNepali = function (text) {
    let numbers = text.toString().split('');
    let nepaliNo = '';
    $.each(numbers, function (key, value) {
      if (value) {
        if(value == 1)
          nepaliNo+="१";
        else if(value == 2)

          nepaliNo+="२";
        else if(value == 3)

          nepaliNo+="३";
        else if(value == 4)

          nepaliNo+="४";
        else if(value == 5)

          nepaliNo+="५";
        else if(value == 6)

          nepaliNo+="६";
        else if(value == 7)

          nepaliNo+="७";
        else if(value == 8)

          nepaliNo+="८";
        else if(value == 9)

          nepaliNo+="९";
        else if(value == 0)

          nepaliNo+="०";
        else if(value == ',')

          nepaliNo+=",";
        else if(value == '/')

          nepaliNo+="/";
        else if(value == '.')

          nepaliNo+=".";
        else if(value == '_')

          nepaliNo+="_";
      }
    });
    console.log(nepaliNo);
    return nepaliNo;
  };

  $('.e-n-t-n-n').each(function () {
    let nepaliNo = changeToNepali($(this).text());
    $(this).text(nepaliNo);
  });


</script>

{{--nep to english--}}
<script>
  function convertNepaliToEnglish(input) {
    console.log(input);
    var charArray = input.split('.');
    charArray = charArray[0].split('');
    // console.log('Test',charArray[0]);
    var engDate = '';
    $.each(charArray, function (key,value) {

      switch (value) {
        case '१':
          engDate += '1'
          break
        case '२':
          engDate += '2'
          break
        case '३':
          engDate += '3'
          break
        case '४':
          engDate += '4'
          break
        case '५':
          engDate += '5'
          break
        case '६':
          engDate += '6'
          break
        case '०':
          engDate += '0'
          break
        case '७':
          engDate += '7'
          break
        case '८':
          engDate += '8'
          break
        case '९':
          engDate += '9'
          break

        case '-':
          engDate += '-'
          break
      }
      //console.log(engDate)
    })

    return engDate

  }
</script>