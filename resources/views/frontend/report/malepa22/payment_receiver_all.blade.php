  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <table width="99%" >
        <tr>
          <td colspan="6" style="text-align: center"><b>प्रदेश सरकार</b></td>
        </tr>
        <tr>
          <td colspan="6" style="text-align: center">{{Auth::user()->office->province->name}}</td>
        </tr>
        <tr>
          <td colspan="6" style="text-align: center">{{Auth::user()->office->ministry->name}}</td>
        </tr>
        <tr>
          <td colspan="6" style="text-align: center"><b>{{Auth::user()->office->name}}</b></td>
        </tr>
        <tr>
          <td colspan="4" style="padding-left:647px;">
            <b>बैङ्क नगदी किताब</b>
          </td>

          <td style="text-align: right;">म.ले.प.फा.नं. २२</td>
        </tr>
        <tr>
          <td>
            <br>
            <br>

          </td>
        </tr>
        <tr>
          <td>आर्थीक वर्ष : २०७६/७७</td>
          <td >महिना :</td>
          <td>बजेट उप शीर्षक : </td>
        </tr>

      </table>

    </section>

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-body">
          <table class="table" width="100%" border="1">
            <thead>
            <tr>
              <th rowspan="2">सि.न.</th>
              <th rowspan="2">प्रापक प्रकार</th>
              <th rowspan="2">भुक्तानि पउने</th>
              <th rowspan="2">भुक्तानी रकम</th>
              <th colspan="4">कैफियत</th>

            </tr>

            </thead>
          </table>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
