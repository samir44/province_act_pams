<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<link rel="stylesheet" href="{{asset('css/styles.css')}}">

<style>
    table {
        border-collapse: collapse;
    }

    /*th{*/
    /*  font-weight: 200;*/
    /*}*/
</style>
<style type="text/css" media="print"> @page {
        size: landscape;
    } </style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <table width="99%" style="border-collapse: collapse; font-size:13px">
            <tr>
                <td colspan="6" style="text-align: center">
                    <img src="{{ asset('img/nepal-govt-logo.png')}}" style="position: absolute;left: 20px;height: 80px;width:100px;
" alt="">
{{--                    <b>प्रदेश सरकार</b>--}}
                </td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center">{{Auth::user()->office->province->name}}</td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center">{{Auth::user()->office->ministry->name}}</td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center">
                    @if(Auth::user()->office->department)
                        {{Auth::user()->office->department->name}}</td>
                @endif
                </td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center"><b>{{Auth::user()->office->name}}</b></td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center"><b>{{Auth::user()->office->district->name}}</b></td>
            </tr>
            <tr>
                <td colspan="6">
                    <div style="width: 100%; text-align: center">
                        <b>बैङ्क नगदी किताब</b><br>
                        <b><span class="e-n-t-n-n">{{$year}}</span> साल {{$month_name}} महिना</b>
                    </div>
                    <div style="float: right; margin-top: -20px">
                        म.ले.प.फा.नं. २०९
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <br>
                    <br>

                </td>
            </tr>
            <tr>
                <td class="kalimati">बजेट उपशीर्षक : {{$program['name']}}- {{$program['program_code']}}
                </td>
            </tr>

        </table>

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="panel panel-primary">
            <div class="panel-body">

                <table class="table" id="malepa-five-table" width="100%" border="1"
                       style="background-color:#dbdbdb; font-size: 12px">
                    <thead>
                    <tr>
                        <th rowspan="2">मिति</th>
                        <th rowspan="2">भौ.न.</th>
                        <th rowspan="2">विवरण</th>
                        <th colspan="2">नगद मौज्दात</th>
                        <th colspan="4">बैङ्क मौज्दात</th>
                        <th colspan="2">बजेट खर्च</th>
                        <th colspan="2">चालु आर्थिक वर्षको पेश्की</th>
                        <th colspan="2">गत आवको वर्षको पेश्की</th>
                        <th colspan="2">विविध</th>
                        <th colspan="2">कैफियत</th>
                    </tr>
                    <tr>
                        <td style="text-align: center">डे</td>
                        <td style="text-align: center">क्रे</td>
                        <td style="text-align: center">डे</td>
                        <td style="text-align: center">क्रे</td>
                        <td style="text-align: center">चेक न</td>
                        <td style="text-align: center">बकी</td>
                        <td style="text-align: center">खर्च शीर्षक</td>
                        <td style="text-align: center">रकम</td>
                        <td style="text-align: center">दिइएको</td>
                        <td style="text-align: center">फर्छौट</td>
                        <td style="text-align: center">जिम्मेवारी सारिएको</td>
                        <td style="text-align: center">फर्छौट</td>
                        <td style="text-align: center">डे</td>
                        <td style="text-align: center">क्रे</td>
                        <td colspan="2"></td>
                    </tr>
                    </thead>
                    <tbody>
                    @if($voucherList->count())
                        @foreach($voucherList as $voucher)
                            <tr style="background-color: white;">
                                <td>{{$voucher->data_nepali}}</td>
                                <td><span class="e-n-t-n-n" style="display: block; text-align: center">{{$voucher->jv_number}}</span></td>
                                <td>{{$voucher->short_narration}}</td>
                                <td></td>
                                <td></td>
                                <td style="text-align: right">
                                    @if($voucher->nikasa_check())
                                        <span class="e-n-t-n-n">@moneyFormat($voucher->payement_amount)</span>
                                    @endif
                                </td>
                                <td  style="text-align:right">
                                    @if(($voucher->check_payment()))
                                        <span class="e-n-t-n-n">@moneyFormat($voucher->payement_amount)</span>
                                    @endif
                                </td>
                                <td></td>
                                <td></td>
                                <td class="kalimati" style="text-align:left; font-size: 11px">{{$voucher->get_expense_heads()}}</td>
                                <td class="cr-amount" style="text-align:right">
                                    <span class="e-n-t-n-n">@moneyFormat($voucher->calculate_total_expense())</span>
                                </td>
                                <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($voucher->peski())</span></td>
                                <td class=""><span class="e-n-t-n-n">@moneyFormat($voucher->getPeskiClearance())</span></td>
                                <td></td>
                                <td></td>
                                <td class="dr-liability" style="text-align:right">
                                    <span class="e-n-t-n-n">@moneyFormat($voucher->calculate_total_dr_liabilities())</span>
                                </td>
                                <td class="cr-liability" style="text-align:right">
                                    @if($voucher->nikasa_check())
                                        <span class="e-n-t-n-n">@moneyFormat($voucher->payement_amount)</span>
                                    @else
                                        <span class="e-n-t-n-n">@moneyFormat($voucher->calculate_total_cr_liabilities())</span>
                                        @endif
                                </td>
                                <td></td>


                            </tr>
                        @endforeach
                    @endif
                    <tr>
                        <td colspan="3" style="text-align: right">यो महिनाको जम्मा</td>
                        <td></td>
                        <td></td>
                        <td style="text-align: right">
                            <span class="e-n-t-n-n">@moneyFormat($thisMonthTotal['nikasa'])</span>
                        </td>
                        <td class="bank-cr-amount" id="this_month_total" style="text-align:right">
                            <span class="e-n-t-n-n">@moneyFormat($thisMonthTotal['cr_tsa'])</span></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td style="text-align:right"><span class="e-n-t-n-n">@moneyFormat($thisMonthTotal['expense'])</span>
                        </td>
                        <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($thisMonthTotal['peski'])</span></td>
                        <td></td>
                        <td></td>
                        <td></td>

                        <td class="liability-dr" id="liability-dr" style="text-align:right"><span class="e-n-t-n-n">@moneyFormat($thisMonthTotal['dr_liability'])</span>
                        </td>

                        <td class="liability-cr" id="liability-cr" style="text-align:right"><span class="e-n-t-n-n">@moneyFormat($thisMonthTotal['cr_liability'])</span>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="3" style="text-align: right">अघिल्लो महिनासम्मको जम्मा</td>
                        <td></td>
                        <td></td>
                        <td style="text-align: right">
                            <span class="e-n-t-n-n">@moneyFormat($preMonthTotal['nikasa'])</span></td>
                        </td>
                        <td class="pre_month_total" id="pre_month_total" style="text-align:right">
                            <span class="e-n-t-n-n">@moneyFormat($preMonthTotal['cr_tsa'])</span></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td style="text-align:right"><span class="e-n-t-n-n">@moneyFormat($preMonthTotal['expense'])</span></td>
                        <td style="text-align:right"><span class="e-n-t-n-n">@moneyFormat($preMonthTotal['peski'])</span></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td style="text-align:right"><span class="e-n-t-n-n">@moneyFormat($preMonthTotal['dr_liability'])</span>
                        </td>
                        <td style="text-align:right"><span class="e-n-t-n-n">@moneyFormat($preMonthTotal['cr_liability'])</span>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="3" style="text-align: right">कुल जम्मा</td>
                        <td></td>
                        <td></td>
                        <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($upTothisMonthTotal['nikasa'])</span></td>
                        <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($upTothisMonthTotal['cr_tsa'])</span>
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($upTothisMonthTotal['expense'])</span>
                        </td>
                        <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($upTothisMonthTotal['peski'])</span></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($upTothisMonthTotal['dr_liability'])</span>
                        </td>
                        <td style="text-align: right"><span class="e-n-t-n-n">@moneyFormat($upTothisMonthTotal['cr_liability'])</span>
                        </td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
                <table width="99%" style="font-size: 13px">
                    <tr>
                        <br>
                        <br>
                        <br>
                        <br>
                    </tr>
                    <tr>
                        <td style="padding-left: 52px;">तयार गर्ने :
                            @if($voucher_signature and $voucher_signature->karmachari_prepare_by)
                                {{$voucher_signature->karmachari_prepare_by->name_nepali}}
                            @endif
                        </td>
                        <td>पेश गर्ने :
                            @if($voucher_signature and $voucher_signature->karmachari_submit_by)
                                {{$voucher_signature->karmachari_submit_by->name_nepali}}
                            @endif
                        </td>
                        <td>सदर गर्ने :
                            @if($voucher_signature and $voucher_signature->karmachari_approved_by)
                                {{$voucher_signature->karmachari_approved_by->name_nepali}}</td>
                        @endif
                    </tr>
                    <tr>
                        <td style="padding-left: 52px;">पद :
                            @if($voucher_signature and $voucher_signature->karmachari_prepare_by->get_pad)
                                {{$voucher_signature->karmachari_prepare_by->get_pad->name}}
                            @endif
                        </td>
                        <td>पद :
                            @if($voucher_signature and $voucher_signature->karmachari_submit_by->get_pad)
                                {{$voucher_signature->karmachari_submit_by->get_pad->name}}
                            @endif
                        </td>
                        <td>पद :
                            @if($voucher_signature and $voucher_signature->karmachari_approved_by->get_pad)
                                {{$voucher_signature->karmachari_approved_by->get_pad->name}}
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left: 52px;">मिति :</td>
                        <td>मिति :</td>
                        <td>मिति :</td>
                    </tr>

                </table>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<script>
    let changeToNepali = function (text) {
        let numbers = text.split('');
        let nepaliNo = '';
        $.each(numbers, function (key, value) {
            if (value) {
                if (value == 1)
                    nepaliNo += "१";
                else if (value == 2)

                    nepaliNo += "२";
                else if (value == 3)

                    nepaliNo += "३";
                else if (value == 4)

                    nepaliNo += "४";
                else if (value == 5)

                    nepaliNo += "५";
                else if (value == 6)

                    nepaliNo += "६";
                else if (value == 7)

                    nepaliNo += "७";
                else if (value == 8)

                    nepaliNo += "८";
                else if (value == 9)

                    nepaliNo += "९";
                else if (value == 0)

                    nepaliNo += "०";
                else if (value == ',')

                    nepaliNo += ",";
                else if (value == '.')

                    nepaliNo += ".";
                else if (value == '')

                    nepaliNo += " ";
            }
        });
        // console.log(nepaliNo);
        return nepaliNo;
    };

    let change_all_to_nepali_number = function () {
        $('.e-n-t-n-n').each(function () {
            let nepaliNo = changeToNepali($(this).text());
            let nepaliVal = changeToNepali($(this).val());

            $(this).text(nepaliNo);
            $(this).val(nepaliVal);
        });
    }
</script>

<script>
    $(document).ready(function () {
        change_all_to_nepali_number();
    })
</script>


<script>
    $(document).ready(function () {
        let current_payment = $('#bhuktani_amount').val();
        let trs = $('#malepa-five-table tbody').find('tr');
        let crAmount = 0;
        let dr_liability = 0;
        let cr_liability = 0;
        $.each(trs, function (key, tr) {

            let bank_remain = $.trim($(this).find('td.bank-cr-amount').text());
            let advanceGIven = $.trim($(this).find('td.advance_given').text());


        });

        // alert(cr_liability);
        // $('.total_advance_given').text(advanceGIven);
        // $('#cr_bank_two').text(crAmount);
        // $('td#liability-dr span').text(dr_liability);
        // $('td#liability-cr span').text(cr_liability);
        // change_all_to_nepali_number();


    })
</script>


<script>
    function convertNepaliToEnglish(input) {
        console.log(input);
        var charArray = input.split('.');
        charArray = charArray[0].split('');
        // console.log('Test',charArray[0]);
        var engDate = '';
        $.each(charArray, function (key, value) {

            switch (value) {
                case '१':
                    engDate += '1'
                    break
                case '२':
                    engDate += '2'
                    break
                case '३':
                    engDate += '3'
                    break
                case '४':
                    engDate += '4'
                    break
                case '५':
                    engDate += '5'
                    break
                case '६':
                    engDate += '6'
                    break
                case '०':
                    engDate += '0'
                    break
                case '७':
                    engDate += '7'
                    break
                case '८':
                    engDate += '8'
                    break
                case '९':
                    engDate += '9'
                    break

                case '-':
                    engDate += '-'
                    break
            }
            //console.log(engDate)
        })

        return engDate

    }
</script>

