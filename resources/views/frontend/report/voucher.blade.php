@extends('frontend.layouts.app')

@section('content')

  <style>
      tr:hover {
        background-color:  #e1e1e1;;
      }
  </style>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        भौचर खोज्ने
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-body">
          <form action="{{route('report.voucher')}}" method="post">
            {{csrf_field()}}
          <table>
            <tr>
              <td>
                बजेट उपशिर्षक
              </td>
              <td>
                <select name="budget_sub_head" class="form-control" required>
                  <option  value="" selected>.........................</option>
                  @foreach($budgetSubHeads as $budgetSubHead)
                  <option value="{{$budgetSubHead->id}}">{{$budgetSubHead->name}} | {{$budgetSubHead->program_code}}</option>
                  @endforeach
                </select>
              </td>
              <td>भौचर किसिम</td>
              <td>
                <select name="voucher_type" class="form-control">
                  <option>खर्च विवध</option>
                  <option>पेश्की</option>
                  <option>पेश्की फर्छ्यौट</option>
                  <option>गत विगत आव को पेश्की</option>
                  <option>गत विगत आव को पेश्की फर्छ्यौट</option>
                </select>
              </td>
              <td>भौचर न.</td>
              <td>
                <input type="number" class="form-control" name="voucher_number">
              </td>
              <td>
                <input type="submit" value="खोज">
              </td>
            </tr>
          </table>
          </form>
        </div>

      </div>

    </section>

    <!-- /.content -->
  </div>
  <table class="table" border="1" id="voucher-table" style="margin-bottom: 46px;">
    <thead>
    <td>SN</td>
    <td>बजेट उपशीर्षक</td>
    <td>भौचर न.</td>
    <td>मिति</td>
    <td>विवरण</td>
    <td>कारोवार रकम</td>
    <td>एक्सन</td>
    </thead>
    <tbody>

    @if($vouchersList)
        @foreach($vouchersList as $index=>$voucher)
          <tr>
            <td class="kalimati">{{++$index}}</td>
            <td>{{$voucher->budget_sub_head->name}}</td>
            <td class="kalimati">{{$voucher->jv_number}}</td>
            <td>{{$voucher->data_nepali}}</td>
            <td>{{$voucher->short_narration}}</td>
            <td class="kalimati" style="text-align: right">{{number_format($voucher->payement_amount,2, '.', ',')}}</td>
            <td style="text-align: center"><a href="{{route('voucher.view',['id'=>$voucher->id])}}" target="_blank">View</a></td>
          </tr>
        @endforeach
    @endif
    </tbody>
  </table>
@endsection

@section('scripts')
{{--  <script>--}}
{{--        let trs = $('#voucher-table tbody').find('#voucher-show');--}}
{{--  </script>--}}
@endsection


