<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<link rel="stylesheet" href="{{asset('css/styles.css')}}">
<style>
    table {
        border-collapse: collapse;

    }

    .accept-table {

    }

    /*th{*/
    /*  font-weight: 200;*/
    /*}*/
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <table width="99%" style="font-size:13px" class="">
            <tr>
                <img src="{{ asset('img/nepal-govt-logo.png')}}" style="position: absolute;left: 20px;height: 80px;width:100px;
" alt="">
{{--                <td colspan="6" style="text-align: center"><b>प्रदेश सरकार</b></td>--}}
            </tr>
            <tr>
                <td colspan="6" style="text-align: center">{{Auth::user()->office->province->name}}</td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center">{{Auth::user()->office->ministry->name}}</td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center">@If(Auth::user()->office->department){{Auth::user()->office->department->name}}@endif</td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center"><b>{{Auth::user()->office->name}}</b></td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center"><b>{{Auth::user()->office->district->name}}</b></td>
            </tr>
            <tr>
                <td colspan="6">
                    <div style="width: 100%; text-align: center">
                        <b>गोश्वारा धरौटी खाता</b>
                    </div>
                    <div style="float: right; margin-top: -20px">
                        म.ले.प.फा.नं. ६०२
                    </div>
                </td>
            </tr>

            <tr>
                <td colspan="4">आर्थिक वर्ष :<span class="e-n-t-n-n"></span></td>

            </tr>
            {{--            <tr>--}}
            {{--                <td colspan="4">बजेट उपशीर्षक : {{$voucher->program->name}}--}}
            {{--                    <span class="e-n-t-n-n">--}}
            {{--                        - {{$voucher->budget_sub_head->program_code}}--}}
            {{--                    </span></td>--}}
            {{--                <td style="text-align: right; padding-right: 1px;">मिति :{{$voucher->data_nepali}}</td>--}}
            {{--            </tr>--}}
        </table>

    </section>

    <section class="content">
        <div class="panel panel-primary">
            <div class="panel-body">

                <table border="1" width="100%" style="background-color:#dbdbdb; font-size: 12px" id="goswara_retention">
                    <thead>
                    <tr>
                        <th rowspan="2" width="47px">सि.नं.</th>
                        <th rowspan="2">मिति</th>
                        <th rowspan="2">भौचर नं.</th>
                        <th rowspan="2">जम्मा गर्ने</th>
                        <th rowspan="2">प्राप्त धरौटी</th>
                        <th colspan="3">फर्छ्यौट</th>
                        <th rowspan="2">मौज्दात</th>
                    </tr>
                    <tr>
                        <th> धरौटी फिर्ता</th>
                        <th> धरौटी सदरस्याहा</th>
                        <th>जम्मा</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($retentionVoucher as $index=>$details)
                        @if(!($details->dr_or_cr == 1 and $details->byahora == 3))
                            <tr style="background-color: white">

                                <td class="kalimati" style="text-align: center">{{$index}}</td>
                                <td class="kalimati" style="text-align: center">{{$details->date_nep}}</td>
                                <td class="kalimati"
                                    style="text-align: center">{{$details->retention_voucher->voucher_number}}</td>
                                <td>{{$details->advancePayment->name_nep}}</td>
                                <td class="kalimati"
                                    style="text-align: right">@if($details->dr_or_cr == 2 and $details->byahora == 9 and $details->hisab_number==395){{$details->amount}}@endif</td>
                                <td class="kalimati"
                                    style="text-align: right">@if($details->dr_or_cr == 1 and $details->byahora == 9 and $details->hisab_number==397){{$details->amount}}@endif</td>
                                <td class="kalimati"
                                    style="text-align: right">@if($details->dr_or_cr == 1 and $details->byahora == 9 and $details->hisab_number==398){{$details->amount}}@endif</td>
                                <td></td>
                                <td class="remain_retention kalimati"
                                    style="text-align: right">{{$details->getRemainRetention()}}</td>
                            </tr>
                        @endif
                    @endforeach
{{--                    <tr>--}}
{{--                        <td colspan="4" style="text-align: right">जम्मा</td>--}}
{{--                        <td class="kalimati" style="text-align: right">{{$totalRetentionIncome}}</td>--}}
{{--                        <td class="kalimati" style="text-align: right">{{$totalRetentionReturn}}</td>--}}
{{--                        <td class="kalimati" style="text-align: right">{{$totalRetentionCollaps}}</td>--}}
{{--                        <td></td>--}}
{{--                        <td class="kalimati" style="text-align: right"><span id="remain_retention"></span></td>--}}
{{--                    </tr>--}}
                    </tbody>
                </table>
            </div>
        </div>

    </section>

    <!-- /.content -->
</div>


<script>
    $(document).ready(function () {

        let trs = $('#goswara_retention tbody').find('tr').not(':last');
        let remain = 0;
        $.each(trs, function () {
            let remain_temp = $(this).find('td.remain_retention ').text();
            remain = parseFloat(remain) + parseFloat($.trim(remain_temp));

        });
        $('#remain_retention').text(remain);

    })
</script>
