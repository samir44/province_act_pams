
@extends('frontend.layouts.app')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>

        <small>म.ले.प.फा.नं. १७ :: आर्थिक विवरण</small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-body">
          <form action="{{route('report.malepa.seventeen')}}" method="post" target="_blank">
            {{csrf_field()}}
            <div class="form-group">
              <label for="name">कार्यालय : </label>
              <select name="office_id" class="form-control">
                <option value="{{Auth::user()->office_id}}">{{Auth::user()->office->name}}</option>
              </select>
            </div>
            <div class="form-group">
              <label for="name">आर्थिक वर्ष:</label>
              <select name="fiscal_year" class="form-control">
                <option value="{{$fiscalYear->year}}">{{$fiscalYear->year}}</option>
              </select>
            </div>
            <div class="form-group">
              <label>बजेट उप शिर्षक</label>
              <select name="program" class="form-control">
                <option value="1234">All</option>
                @foreach($programs as $program)
                  <option value="{{$program->id}}">{{$program->name}} | {{$program->program_code}}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <label>स्रोत</label>
              <select name="source_type" class="form-control" id="source_type" required>
                <option value="">...............</option>
                <option value="123">सबै</option>
                <option value="1">संघिय सरकार</option>
                <option value="2">प्रदेश सरकार</option>
                <option value="3">बैदेशिक श्रोत</option>
              </select>
            </div>
            <button type="submit" class="btn btn-default">Submit</button>
          </form>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>

@endsection