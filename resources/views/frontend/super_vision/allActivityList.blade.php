<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<link rel="stylesheet" href="{{asset('css/styles.css')}}">


<style>
    table {
        border-collapse: collapse;
    }

    /*th{*/
    /*  font-weight: 200;*/
    /*}*/
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <input type="button" style="position: absolute; top:20px;right: 20px" value="Export to Excel" id="downloadMe">
    <section class="content-header">
        <div id="tableHeadingWrapper">
            <table width="99%" id="headingTable" style="font-size:13px">
                <tr>
                    <img src="{{ asset('img/nepal-govt-logo.png')}}" style="position: absolute;left: 20px;height: 80px;width:100px;
" alt="">
                    {{--          <td colspan="6" style="text-align: center"><b>प्रदेश सरकार</b></td>--}}
                </tr>
                <tr>
                    <td colspan="6" style="text-align: center">{{Auth::user()->office->province->name}}</td>
                </tr>
                <tr>
                    <td colspan="6" style="text-align: center">{{Auth::user()->office->ministry->name}}</td>
                </tr>
                @if(Auth::user()->office->department)
                    <tr>
                        <td colspan="6" style="text-align: center">{{Auth::user()->office->department->name}}</td>
                    </tr>
                @endif
                <tr>
                    {{--          <td colspan="6" style="text-align: center"><b>{{$office[0]->name}}</b></td>--}}
                </tr>
                <tr>
                    <td colspan="6" style="text-align: center"><b>{{Auth::user()->office->district->name}}</b></td>
                </tr>
                <tr>
                    <td colspan="6">
                        <div style="width: 100%; text-align: center">
                            <b>कार्यक्रम / परियोजना अनुसार बजेट बिनियोजन</b><br>
                        </div>
                        <div style="float: right; margin-top: -20px">

                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br>
                        <br>

                    </td>
                </tr>
                <tr>


                </tr>

            </table>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="panel panel-primary">
            <div class="panel-body" id="tableWrapper">
                <table width="99%" id="contentTable" border="1" style="background-color:#dbdbdb; font-size: 12px">
                    <thead>
                    <tr>
                        <th rowspan="3" width="67px">सि.नं.</th>
                        <th rowspan="3" width="67px">कार्यालय</th>
                        <th rowspan="3" width="67px">बजेट उपशिर्षक</th>
                        <th rowspan="3" style="width: 28%;">कार्यक्रम/आयोजनाको नाम</th>
                        <th rowspan="3">खर्च शीर्षक</th>
                        <th rowspan="3" width="237px">स्रोत</th>
                        <th rowspan="3">लक्ष</th>
                        <th colspan="7">विनियोजन रु.</th>
                        <th colspan="4">खर्च</th>
                        <th rowspan="3">कैफियत</th>
                    </tr>

                    <tr>
                        <th colspan="2">प्रथम चौमासिक</th>
                        <th colspan="2">दोश्रो चौमासिक</th>
                        <th colspan="2">तेस्रो चौमासिक</th>
                        <th rowspan="2">जम्मा</th>
                        <th rowspan="2">प्रथम चौमासिक</th>
                        <th rowspan="2">दोश्रो चौमासिक</th>
                        <th rowspan="2">तेस्रो चौमासिक</th>
                        <th rowspan="3">जम्मा</th>

                    </tr>
                    <tr>
                        <td style="text-align: center">बजेट</td>
                        <td style="text-align: center">भार</td>
                        <td style="text-align: center">बजेट</td>
                        <td style="text-align: center">भार</td>
                        <td style="text-align: center">बजेट</td>
                        <td style="text-align: center">भार</td>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($offices as $index=>$office)
                        <?php
                        $i = 1;
                        ?>
                        @foreach($office->getBudgetSubHead($office->id) as $budgetSubHead)

                            @foreach($budgetSubHead->getActivities($budgetSubHead->id) as $activity)
                                <tr style="background-color: white">
                                    <td></td>
                                    <td>{{$office->name}} {{$office->office_code}}</td>
                                    <td>{{$budgetSubHead->name}} {{$budgetSubHead->program_code}}</td>
                                    <td>{{$activity->sub_activity}}</td>
                                    <td>{{$activity->expense_head_by_id->expense_head_code}}</td>
                                    <td>{{$activity->get_budget_source_level->name}}</td>
                                    <td></td>
                                    <td>{{$activity->first_quarter_budget}}</td>
                                    <td>{{$activity->first_chaimasik_bhar}}</td>
                                    <td>{{$activity->second_quarter_budget}}</td>
                                    <td>{{$activity->second_chaimasik_bhar}}</td>
                                    <td>{{$activity->third_quarter_budget}}</td>
                                    <td>{{$activity->third_chaimasik_bhar}}</td>
                                    <td>{{$activity->total_budget}}</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            @endforeach

                            <tr>
                                <td colspan="11" align="right">बजेट उपशिर्षक जम्मा</td>
                                <td></td>
                                <td></td>
                                <td>{{$budgetSubHead->getTotalBudgetByBudgetSubHead($budgetSubHead->id)}}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="11" align="right">कार्यालय जम्मा</td>
                            <td></td>
                            <td></td>
                            <td>{{$office->getTotalBudget($office->id)}}</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    @endforeach

                    <tr>
                        <td colspan="11" style="text-align: right"> कुल जम्मा</td>
                        <td></td>
                        <td></td>
                        <td>{{$ministrywise_total_budget}}</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </section>
    <!-- /.content -->
</div>


<script>
    let changeToNepali = function (text) {
        // alert(text);
        textTrim = $.trim(text)
        let numbers = textTrim.split('');
        let nepaliNo = '';
        $.each(numbers, function (key, value) {
            if (value) {
                if (!isNaN(value) && value != ' ') {
                    if (value == 1)
                        nepaliNo += "१";
                    else if (value == 2)

                        nepaliNo += "२";
                    else if (value == 3)

                        nepaliNo += "३";
                    else if (value == 4)

                        nepaliNo += "४";
                    else if (value == 5)

                        nepaliNo += "५";
                    else if (value == 6)

                        nepaliNo += "६";
                    else if (value == 7)

                        nepaliNo += "७";
                    else if (value == 8)

                        nepaliNo += "८";
                    else if (value == 9)

                        nepaliNo += "९";
                    else if (value == 0)

                        nepaliNo += "०";
                    else if (value == ',')

                        nepaliNo += ",";
                    else if (value == '.')

                        nepaliNo += ".";
                    else if (value == '/')

                        nepaliNo += "/";

                }
            }
        });
        console.log(nepaliNo);
        return nepaliNo;
    };


    $('.e-n-t-n-n').each(function () {
        let nepaliNo = changeToNepali($(this).text());
        let nepaliVal = changeToNepali($(this).val());

        $(this).text(nepaliNo);
        $(this).val(nepaliVal);
    });


</script>

<script>
    var tableToExcel = (function () {
        var uri = 'data:application/vnd.ms-excel;base64,'
            ,
            template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><meta charset="utf-8"/><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
            , base64 = function (s) {
                return window.btoa(unescape(encodeURIComponent(s)))
            }
            , format = function (s, c) {
                return s.replace(/{(\w+)}/g, function (m, p) {
                    return c[p];
                })
            };

        return function (table, name, filename) {
            var table1 = '';
            if (!table1.nodeType) table1 = document.getElementById('headingTable')
            if (!table.nodeType) table = document.getElementById(table)
            var bigTable = table1.innerHTML + table.innerHTML;
            var ctx = {worksheet: name || 'Worksheet', table: bigTable}

            document.getElementById("dlink").href = uri + base64(format(template, ctx));
            document.getElementById("dlink").download = filename;
            document.getElementById("dlink").click();

        }
    })();

    $("#downloadMe").click(function (e) {
        let allTable = $('div[id$=tableHeadingWrapper]').html() + $('div[id$=tableWrapper]').html();
        console.log(allTable);
        window.open('data:application/vnd.ms-excel,' + encodeURIComponent(allTable));
        e.preventDefault();
    });
</script>