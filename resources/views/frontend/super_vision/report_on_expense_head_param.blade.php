@extends('frontend.layouts.app')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                खर्च शीर्षकगत

            </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <form action="{{route('report.expense.head.wise')}}" method="post" target="_blank">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label>आर्थिक वर्ष</label>
                            <select class="form-control" name="fiscal_year">
                                <option>{{$fiscalYear['year']}}</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>माथिल्लो कार्यालय</label>
                            <select class="form-control" name="ministry" id="ministry" required>
                                <option value="{{Auth::user()->office->ministry_id}}">{{Auth::user()->office->name}}</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>मातहतका कार्यालय</label>
                            <select class="form-control" name="officeId" id="officeId" required>
                                <option value="">...............</option>
                                <option value="123">All</option>
                                @foreach($officeLists as $office)
                                    <option value="{{$office->id}}">{{$office->name}}
                                        | {{$office->district->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>बजेट उपशीर्षक</label>
                            <select class="form-control" id="budgetSUbHead" name="budgetSUbHead" required>
                                <option value="">...............................</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="form-control btn btn-primary" value="हेर्ने">
                        </div>
                    </form>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>

@endsection

@section('scripts')

    <script>
        $(document).on('change', '#officeId', function () {

            let office_id = $('#officeId').val();
            let url = '{{route('get.budget.sub.head',123)}}';
            url = url.replace('123', office_id);
            // alert(url);
            $.ajax({

                method: 'get',
                url: url,
                success: function (res) {
                    let data = $.parseJSON(res);
                    console.log(data);
                    let options = '<option value="">.......</option>';
                     options = '<option value="123">All</option>';

                    $.each(data, function () {

                        options += '<option value="' + this.id + '">' + this.name + ' | ' + this.program_code + '</option>'
                    });

                    $('#budgetSUbHead').html(options);
                }

            })
        })
    </script>

@endsection
