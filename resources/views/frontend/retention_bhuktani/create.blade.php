
@extends('frontend.layouts.app')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <form action="{{route('retention.bhuktani.store')}}" method="post" id="bhuktaniForm">
    <section class="content-header">
      <h1>

        <small><center>धरौटी भुक्तानी आदेश बनाउने</center></small>
      </h1>
{{--      @foreach($programs as $program)--}}
{{--        --}}
{{--      @endforeach  --}}

{{--      <h3><center>बजेट उपशिर्षक : {{$program->name}}</center></h3>--}}
      <input type="hidden" id="bs_roman" name="bs_roman">
{{--      <input type="hidden" value="{{$program->id}}" name="budget_sub_head">--}}
{{--      <h4><center>शिर्षक न  : {{$program->program_code}}</center></h4>--}}
{{--      <h4 align="right"> <u><a href="{{route('bhuktani.index',$program->id)}}" >back</a> </u></h4>--}}
    </section>

    <!-- Main content -->

    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-body">
            {{csrf_field()}}
            <table>
              <tr>
                <td>आदेश न:</td>
                <td class="kalimati">
                  <input type="text" name="adesh_number" class="form-control" value="{{$bhuktani_adesh_number}}" readonly>

                </td>
                <td>मिति</td>
                <td>
                  <input type="text" class="form-control" name="date" id="date" value="">
                </td>
              </tr>
            </table>
            <table class="table" border="1" >
              <thead>
              <tr style="background-color: #dbdbdb;">
                <td>भौचर न.</td>
                <td>मिति</td>
                <td>व्यहोरा</td>
                <td>रकम</td>
                <td>एक्सन</td>
              </tr>
              </thead>

              <tbody>
              @if($retentionVouchers->count() > 0)
              @foreach($retentionVouchers as $index=>$retentionVoucher)
                  <tr>
                    <td class="kalimati">{{$retentionVoucher->voucher_number}}</td>
                    <td class="kalimati">{{$retentionVoucher->date_nep}}</td>
                    <td>{{$retentionVoucher->short_narration}}</td>
                    <td class="kalimati">{{$retentionVoucher->amount}}</td>
                    <td>
                      <input type="checkbox" name="retentionBhuktani[]" class="bhuktani_check_box" value="{{$retentionVoucher->id}}" required>
                    </td>
                  </tr>
                @endforeach
                </tbody>
              <tr>
                <td colspan="6" style="text-align: center">
                  <button class="btn btn-primary" id="btnBhuktaniSubmit">Submit</button>
                </td>
              </tr>
              @else
                <td colspan="6" style="text-align: center">डाटा उपलब्ध छैन।</td>
            @endif

            </table>
        </div>
      </div>
    </section>
    <!-- /.content -->
    </form>
  </div>


@endsection

@section('scripts')

{{--validation--}}
<script>
  $(document).on('click','button#btnBhuktaniSubmit',function (e) {

    e.preventDefault();


  })
</script>
  {{--    Date Picker --}}
  <script>
    $("#date").nepaliDatePicker({
      dateFormat: "%y-%m-%d",
      closeOnDateSelect: true
    });

    var currentDate = new Date();
    var currentNepaliDate = calendarFunctions.getBsDateByAdDate(currentDate.getFullYear(), currentDate.getMonth() + 1, currentDate.getDate());
    var formatedNepaliDate = calendarFunctions.bsDateFormat("%y-%m-%d", currentNepaliDate.bsYear, currentNepaliDate.bsMonth, currentNepaliDate.bsDate);
    $("#date").val(formatedNepaliDate);
  </script>



  {{--Convert Nepali date to roman date --}}
  <script>
   function convertNepaliToEnglish(input) {
      // console.log(input);
      var charArray = input.split('');
      //console.log(charArray);
      var engDate = '';
      $.each(charArray, function (key, value) {
        switch (value) {
          case '१':
            engDate += '1'
            break
          case '२':
            engDate += '2'
            break
          case '३':
            engDate += '3'
            break
          case '४':
            engDate += '4'
            break
          case '५':
            engDate += '5'
            break
          case '६':
            engDate += '6'
            break
          case '०':
            engDate += '0'
            break
          case '७':
            engDate += '7'
            break
          case '८':
            engDate += '8'
            break
          case '९':
            engDate += '9'
            break

          case '-':
            engDate += '-'
            break
        }
        //console.log(engDate)
      })
      return engDate

    }
  </script>



  <script>
    $(document).ready(function () {
      $('#btnBhuktaniSubmit').click(function (e) {
        e.preventDefault();
        let nepali_date = $('#date').val();
        let bs_roman =  convertNepaliToEnglish(nepali_date);
       $('#bs_roman').val(bs_roman);
        if($('input.bhuktani_check_box:checked').length){

          $('#bhuktaniForm').submit();
        } else {

          alert("कृपया एक्सनको चेक बक्समा चेक लगाउनुहोस् !")
          $('.bhuktani_check_box').focus();
        }

      })
    });
  </script>


@endsection