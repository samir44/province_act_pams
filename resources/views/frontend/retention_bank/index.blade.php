
@extends('frontend.layouts.app')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       धरौटी बैङ्क विवरण

      </h1>
      <ul class="breadcrumb">
        <li> <a type="button" href="{{route('retention_bank_create')}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> नयां बनाउने</a></li>
      </ul>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-body">
          <table class="table table-bordered">
            <thead>
            <tr>
              <th>बैङ्क नाम</th>
              <th>ठगाना</th>
              <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($banks as $bank)
              <tr>
                <td>{{$bank->name}}</td>
                <td>{{$bank->address}}</td>

                <td>
                  <a type="button" href="{{route('retention_bank_edit',$bank->id)}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> Edit</a>
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>

@endsection