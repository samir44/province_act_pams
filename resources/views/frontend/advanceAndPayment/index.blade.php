
@extends('frontend.layouts.app')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>

        <small>पेश्की/भुक्तानी पाउने/धरौटी व्यक्तिगत खाता</small>
      </h1>
      <ul class="breadcrumb">
        <li> <a type="button" href="{{route('AdvanceAndPayment.create')}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> नयां बनाउने</a></li>
      </ul>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-body">

          <table class="table" style="background-color: #dbdbdb ;color: black;">
            <thead>
            <tr>
              <th>क्र.स.</th>
              <th>प्रकार</th>
              <th>नाम नेपाली</th>
              <th>पेयी कोड</th>
              <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $i =1;
            ?>
            @foreach($advanceAndPayments as $advanceAndPayment)
              @if($advanceAndPayment->party_type !=5)
              <tr style="background-color: white;">
                <td>{{$i++}}</td>
                <td>
                  @if($advanceAndPayment->get_party_type)
                  {{$advanceAndPayment->get_party_type->name}}
                  @endif
                </td>
                <td>{{$advanceAndPayment->name_nep}}</td>
                <td>{{$advanceAndPayment->payee_code}}</td>

                <td>
                  <a type="button" href="{{route('AdvanceAndPayment.edit',$advanceAndPayment->id)}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> Edit</a>
                  @if($advanceAndPayment->preBhktani()->count() == 0)
                      <a type="button" href="#" id="delete_advance_payment" data-advance-payment-id="{{$advanceAndPayment->id}}" class="btn btn-sm btn-danger"><i class="fa fa-pencil"></i> Delete</a>
                   @endif

                </td>
              </tr>
              @endif
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>

@endsection

@section('scripts')
  <script>
    $(document).on('click','#delete_advance_payment',function () {

      let advance_payment_id = $(this).attr('data-advance-payment-id');
      let url = '{{route('advance.payment.delete',123)}}';
      url = url.replace('123',advance_payment_id);
      swal({
        title: "Are you sure?",
        text: "Delete भए पछि Recovere हुदैन",
        icon: "warning",
        buttons: true,
        dangerMode: true,

      }).then((willDelete) => {

        if (willDelete) {

          $.ajax({

            url : url,
            method : 'get',
            success : function (res) {
              console.log($.parseJSON(res));
              if(res){
                swal("Bank has been deleted!", {
                  icon: "success",
                });

                location.reload();
              }

            }
          })

        } else {
          // swal("Your imaginary file is safe!");
        }
      });
    })
  </script>


@endsection