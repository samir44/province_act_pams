
@extends('frontend.layouts.app')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>

                <small>भौचर Edit गर्ने :: खोज  </small>
            </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            <div class="panel panel-primary">
                <div class="panel-body">
                   <form action="#" method="post">
                       <table width="100%">
                           <tr>
                               <td>
                                   <div class="form-group">
                                       <label>आर्थिक वर्ष : </label>
                                       <select name="fiscal_year" id="fiscal_year" class="form-control">
                                           <option value="{{$fiscalYear->year}}">{{$fiscalYear->year}}</option>
                                       </select>
                                   </div>

                               </td>
                               <td>
                                   <div class="form-group">
                                       <label>बजेट उपशिर्षक :</label>
                                       <select name="budget_sub_head" id="budget_sub_head" class="form-control">
                                           <option value="0">..............</option>
                                           @foreach($programs as $program)
                                               <option value="{{$program->id}}">{{$program->name}}</option>
                                           @endforeach
                                       </select>
                                   </div>
                               </td>
                           </tr>
                       </table>
                   </form>
                </div>
            </div>
            <table width="95%" border="1" id="voucher_list" class="table-striped">
                <thead>
                <th>Sn</th>
                <th>बजेट उप शीर्षक</th>
                <th>मिति</th>
                <th>भौचर नं.</th>
                <th>कारोबार रकम</th>
                <th>कारोबारको व्यहोरा</th>
                <th colspan="2">कार्य</th>
                </thead>
                <tbody>
                    <tr></tr>
                </tbody>
            </table>
        </section>
        <!-- /.content -->
    </div>

@endsection

@section('scripts')
{{--    बजेट उपशिर्षक click गरेर  भौचर देखाउने--}}
<script>
    $(document).ready(function () {
        $('#budget_sub_head').change(function () {

            let fiscal_year = $('#fiscal_year :selected').val();
            let budget_sub_head = $('#budget_sub_head :selected').val();
            let budget_sub_head_name = $('#budget_sub_head :selected').text();
            let url = '{{route('get_unapproved_voucher',123)}}';
            url = url.replace(123,budget_sub_head);
            // console.log(url);

            $.ajax({
                url : url,
                method : 'get',
                success : function (res) {
                    // console.log($.parseJSON(res));
                    $datas = $.parseJSON(res);
                    let i = 1;
                    let tr = '';
                    $.each($datas, function (key,value) {
                        let urlView = '{{route('voucher.view', '123')}}';
                        urlView = urlView.replace('123', this.id);

                        let url = "{{route('get_voucher_details',123)}}";
                        url = url.replace('123', this.id);
                        tr += "<tr>" +
                            "<td>" +
                            i +
                            "</td>" +

                            "<td class='activity' data-id=''>" +
                            budget_sub_head_name +
                            "</td>" +

                            "<td class='byahora'>" +
                            this.data_nepali +
                            "</td>" +

                            "<td class='details'>" +
                            this.jv_number +
                            "</td>" +

                            "<td class='drAmount'>" +
                            this.payement_amount +
                            "</td>" +

                            "<td class='drAmount'>" +
                            this.short_narration +
                            "</td>" +

                            "<td>" +
                            '<a href="'+url+'" class="voucher-edit" data-id="'+ this.id +'" target=" _blank">Edit</a> | <a href="'+urlView+'" target=" _blank">हेर्ने</a>' +
                            "</td>";
                        i = i + 1;
                    })
                    $('#voucher_list').find('tbody').last('tr').html(tr);
                    
                }

            })

        })
    })
</script>




@endsection