@extends('frontend.layouts.app')

@section('styles')
    <style>
        #budgetStoreCreateFrom label {
            display: block !important;
        }

        th {
            color: black;
            font-size: 14px !important;
        }

        .form-group {

            position: relative;
        }

        .validation-error {
            position: absolute;
            color: red;
            right: 4px;
            bottom: -23px;
            font-size: 10px;
        }
    </style>
@endsection
@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1 style="font-size: 25px;">

                <small>धरौटी भौचर प्रविस्टी</small>
            </h1>
            {{--            <ul class="breadcrumb">--}}
            {{--                <li><a type="button" href="{{route('admin.office')}}" class="btn btn-sm btn-primary"><i--}}
            {{--                                class="fa fa-pencil"></i> List</a></li>--}}
            {{--            </ul>--}}
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <div class="flash-message">
                        @if(session()->has('success'))
                            <div class="alert alert-success">
                                {{ session()->get('success') }}
                            </div>
                        @endif
                    </div>
                    <form action="{{route('retention.voucher.details.update',$retention_voucher_details_id)}}" method="post" id="">
                        {{csrf_field()}}
                        <div class="table-responsive">
                            <table class="table">
                                <tr>
                                    <td>
                                        <div class="form-group" style="width: 150px;">
                                            <label>डेविट/क्रेडिट</label>
                                            <select name="drOrCr" class="form-control" id="DrOrCr" required
                                                    style="height: 32px;">
                                                <option value="">..........</option>
                                                <option value="1" @if($retentionVoucherDetails->dr_or_cr == 1) selected @endif>डेबिट</option>
                                                <option value="2" @if($retentionVoucherDetails->dr_or_cr == 2) selected @endif>क्रेडिट</option>
                                            </select>
                                        </div>
                                        <div class="form-group" style="width:110px;">
                                            <label>व्यहोरा</label>
                                            <select name="byahora" class="form-control" id="byahora" required
                                                    style="height: 32px;">
                                                <option value="">.......</option>
                                                <option value="3" @if($retentionVoucherDetails->byahora == 3) selected @endif>एकल कोष खाता</option>
                                                <option value="4" @if($retentionVoucherDetails->byahora == 4) selected @endif>धरौटी</option>
                                            </select>
                                        </div>
                                        <div class="form-group" style="width:145px;">
                                            <label>हिसाब नं.</label>
                                            <select name="hisab_number" class="form-control select2" id="hisab_number" required style="height: 32px;">
                                                @foreach($hisabNumbers as $hisabNumber)
                                                    <option value="{{$hisabNumber->id}}" @if($hisabNumber->id == $retentionVoucherDetails->hisab_number)selected @endif>{{$hisabNumber->expense_head_sirsak}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group" style="width: 346px;">
                                            <label>बिबरण</label>
                                            <input type="text" class="form-control" name="details" id="details" value="{{$retentionVoucherDetails->details}}" required style="height: 32px;">
                                        </div>

                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <div class="form-group">
                                            <label>धरौटी अभिलेख</label>
                                            <select class="form-control" name="retention_record">
                                                <option value="">..............</option>
                                                @foreach($retentionRecords as $record)
                                                    <option value="{{$record->id}}" @if($record->id == $retentionVoucherDetails->retention_record) selected @endif>{{$record->purpose}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group" style="width: 150px;">
                                            <label>धरौटी प्रकार</label>
                                            <select name="retention_type" class="form-control" id="retention_type"
                                                    style="height: 32px;">
                                                <option value="">..............</option>
                                                <option value="1" @if($retentionVoucherDetails->retention_type ==1) selected @endif>जमानत</option>
                                                <option value="2" @if($retentionVoucherDetails->retention_type ==2) selected @endif>न्यायिक</option>
                                                <option value="3" @if($retentionVoucherDetails->retention_type ==3) selected @endif>विविध</option>
                                                <option value="4" @if($retentionVoucherDetails->retention_type ==4) selected @endif>लिलाम</option>
                                            </select>
                                        </div>
                                        <div class="form-group" style="width: 150px;">
                                            <label> धरौटी जम्मा गर्नेको प्रकार</label>
                                            <select name="party_type" class="form-control" id="party_type"
                                                    style="height: 32px;">
                                                <option value="">..............</option>
                                                <option value="1" @if($retentionVoucherDetails->depositor_type ==1) selected @endif>उपभोक्ता समिति</option>
                                                <option value="2" @if($retentionVoucherDetails->depositor_type ==2) selected @endif>ठेकेदार</option>
                                                <option value="3" @if($retentionVoucherDetails->depositor_type ==3) selected @endif>व्यक्तिगत</option>
                                                <option value="4" @if($retentionVoucherDetails->depositor_type ==4) selected @endif>संस्थागत</option>
                                            </select>
                                        </div>
                                        <div class="form-group" style="width: 130px">
                                            <label> धरौटी जम्मा गर्ने</label>
                                            <select name="party" id="party" class="form-control" style="height: 32px;">
                                                <option value="" selected>..............</option>
                                            </select>
                                        </div>
                                        <div class="form-group" style="width: 130px">
                                            <label>भौचर/नगदी रसिद नम्बर</label>
                                            <input type="number" name="bill_number" min="1" id="bill_number" value="{{$retentionVoucherDetails->bill_number}}" class="form-control" required style="height: 32px;">
                                        </div>
                                        <div class="form-group" style="width: 130px">
                                            <label>रकम</label>
                                            <input type="number" name="amount" min="1" id="amount" value="{{$retentionVoucherDetails->amount}}" class="form-control" required style="height: 32px;">
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary" id="voucher_submit" style="margin-top: 18px;">Update</button>
                                        </div>

                                    </td>
                                </tr>
                            </table>
                        </div>
                    </form>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
@endsection
@section('scripts')
    <script>
        let retentionVOucherDetailsEncode = '{!! json_encode($retentionVoucherDetails) !!}';
        let parseData = $($.parseJSON(retentionVOucherDetailsEncode));
        console.log(parseData[0].depositor);

    </script>

    <script>
        $(document).ready(function () {

            let party_type_id = $('#party_type :selected').val();
            getPartyByPartyType(party_type_id);

        })
    </script>

    {{-- get_expense_head_by_activity_id --}}
    <script>
        let get_expense_head_by_activity_id = function (main_activity_id) {

            let url = '{{route('get_expense_head_by_activity_id',123)}}';
            url = url.replace(123, main_activity_id);
            $.ajax({
                url: url,
                method: 'get',
                success: function (res) {
                    let data = $.parseJSON(res);
                    // console.log("test",data[0]);
                    let options = '<option selected>.........................</option>';

                    options += '<option value="' + data[0].expense_head_by_id.id + '" selected>' + data[0].expense_head_by_id.expense_head_code + ' | ' + data[0].expense_head_by_id.expense_head_sirsak + '</option>'
                    var selected_option = $('#byahora option:selected');
                    if (selected_option.length == 0 || selected_option.val() == 1 || selected_option.val() == 4) {

                        $('#hisab_number').html(options);
                        $('#details').val(data[0].expense_head_by_id.expense_head_sirsak);
                        $('input#details').closest('div').find('p.validation-error').remove();

                    }
                }
            })
        }
    </script>

    {{--Dr  Or Cr change हुदा--}}
    <script>
        $(document).ready(function () {
            $('#DrOrCr').change(function () {
                $('#byahora').val("");
                $('#hisab_number').html("");
                $('#details').val("");
            })
        })
    </script>

    {{--व्यहोरा change हुदा--}}
    <script>
        $(document).ready(function () {
            $('#byahora').change(function () {
                let byahora = $('#byahora :selected').val();
                get_hisab_number_by_byahora(byahora);

            })
        })

    </script>

    {{--on change party_type --}}
    <script>
        $(document).ready(function () {
            $('#party_type').change(function () {

                let party_type_id = $('#party_type :selected').val();
                getPartyByPartyType(party_type_id);

            })
        });
    </script>

    {{--get  party name --}}
    <script>
        let getPartyByPartyType = function (party_type_id, party_name = '') {
            let url = '{{route('get_party_name_by_party_type_and_office', 123)}}';
            url = url.replace(123, party_type_id);
            $.ajax({
                url: url,
                method: 'get',
                success: function (res) {

                    let option = '<option selected>....................</option>';
                    $.each($.parseJSON(res), function () {
                        // console.log(party_name,this.name_nep);
                        // console.log(this);

                        if (parseData[0].depositor == this.id) {
                            option += '<option value="' + this.id + '" selected>' + this.name_nep + '</option>'

                        } else {

                            option += '<option value="' + this.id + '">' + this.name_nep + '</option>'
                        }
                    })
                    $('#party').html(option);
                }
            })
        }
    </script>


    {{--Get हिसाब Number By व्यहोरा--}}
    <script>
        let get_hisab_number_by_byahora = function (byahora) {

            // $('#hisab_number').val('');
            let ledger_type = byahora;

            let url = '{{route('get_expense_head__by_ledger_type',123)}}';
            url = url.replace(123, ledger_type);

            $.ajax({
                url: url,
                method: 'get',
                success: function (res) {
                    console.log($.parseJSON(res));
                    let option = '<option value="">.........</option>';
                    let hisab_number = $.parseJSON(res);

                    if (res.length > 0) {

                        if (hisab_number.length > 1) {
                            $.each($.parseJSON(res), function () {

                                option += '<option value="' + this.id + '">' + this.expense_head_sirsak + '</option>'

                            })
                        } else {
                            option += '<option value="' + hisab_number[0].id + '" selected>' + hisab_number[0].expense_head_sirsak + '</option>'


                        }
                        $('#hisab_number').html(option);
                        getDetailsByHisabNumber();

                    } else {

                        $('#hisab_number').html(option);

                    }


                }
            })
        }
    </script>

    {{--hisab number change huda--}}
    <script>
        let getDetailsByHisabNumber = function () {

            let bibran = $('#hisab_number option:selected').text();
            // console.log('bibara check', bibran);
            $('input#details').val(bibran);
            $('input#details').closest('div').find('p.validation-error').remove();
        }
        $(document).ready(function () {
            $('#hisab_number').change(function () {
                getDetailsByHisabNumber();
            })
        })
    </script>


    {{--checkRemain funcion--}}

    <script>
        let checkRemain = () => {

            let $hiddenRemainingBudget = $('#hidden_remain_budget');
            let amount = $('input#amount').val();

            let mainActivityId = $('#main_activity_name').val();
            let $existingDebitTds = $('#voucher_table tbody td.activity[data-id="' + mainActivityId + '"]');

            let existingDebitTotal = 0;
            $.each($existingDebitTds, function () {
                if (!$(this).closest('tr').hasClass(editingSN)) {
                    existingDebitTotal += parseFloat($(this).closest('tr').find('td.drAmount').text())
                }
            });

            let remain = $hiddenRemainingBudget.val();
            // $hiddenRemainingBudget.val(remain-existingDebitTotal);
            // $('#remain_budget').text(remain-existingDebitTotal).addClass('e-n-t-n-n');
            // remain = $hiddenRemainingBudget.val();
            // alert(remain)
            if (parseFloat(remain) >= parseFloat(amount)) {

                remain = parseFloat(remain) - parseFloat(amount);

                expense = parseFloat(amount) + existingDebitTotal;
                $('#total_expense').text(expense).addClass('e-n-t-n-n');
                $('#hidden_expense').val(expense);
                $('#hidden_remain_budget').val(remain);
                $('#remain_budget').text(remain).addClass('e-n-t-n-n');

                return true;
            } else {

                alert('मौज्दात भन्दा बढि भयो। बाकी:' + remain);
                $('#amount').focus();
                $('#amount').val(remain);
                return false;
            }
        }
    </script>


    {{-- क्रे र एकल कोष खाता amount total गर्ने--}}
    <script>
        let get_cr_tsa = function () {
            let bhuktani_main_activity = $('#bhuktani_main_activity').val();

            let $mainActivityTds = $('#voucher_table tbody td.activity[data-id="' + bhuktani_main_activity + '"]');

            let cr_tsa = 0;
            let totalDebit = 0;
            let totalCredit = 0;
            $.each($mainActivityTds, function () {
                let $row = $(this).closest('tr');
                let drOrCr = $.trim($row.find('td.dr-cr').text());
                if (drOrCr === 'डेबिट') {
                    let rowDr = parseFloat($row.find('td.drAmount').text());
                    if (rowDr > 0) {
                        totalDebit += rowDr;
                    }
                } else if (drOrCr === 'क्रेडिट') {
                    if ($.trim($row.find('td.byahora').text()) !== "एकल कोष खाता") {
                        let rowCr = parseFloat($row.find('td.crAmount').text());
                        if (rowCr > 0) {
                            totalCredit += rowCr;
                        }
                    }
                }
            });
            cr_tsa = totalDebit - totalCredit;
            // let trs = $('#voucher_table').find('tr');
            // $.each(trs, function (key, tr) {
            //     // $.each($(tr).find('td'),function (key, td) {
            //     //
            //     // })
            //     if ($(tr).find('td.dr-cr').text() == 'क्रेडिट' && $(tr).find('td.byahora').text() == 'एकल कोष खाता') {
            //         cr_tsa += parseFloat($(tr).find('td.crAmount').text())
            //     }
            //
            // });
            return cr_tsa;

        }
    </script>

    {{--dr_cr_equal_check finction--}}
    <script>
        let dr_cr_equal_check = function () {


            $('#VoucherSave').prop('disabled', false);

            let option = '<option selected>...................</option>';
            $.each(activityForBhuktani, function () {
                option += '<option value="' + this.activity_id + '">' + this.activity_name + '</option>'
                $('#bhuktani_main_activity').html(option);
            })

        }
    </script>

    {{-- Dr and Cr total देखाउने function --}}
    <script>
        let dr_cr_total = function () {

            let trs = $('#voucher_table').find('tr');
            let DrAmount = 0;
            let CrAmount = 0;
            let drCrRemaining = 0;
            $.each(trs, function (key, tr) {
                if (!$(this).hasClass(editingSN)) {
                    let newDr = parseFloat($(this).find('td.drAmount').text());
                    if (newDr > 0) {
                        DrAmount += newDr;
                    }

                    let newCr = parseFloat($(this).find('td.crAmount').text());
                    if (newCr > 0) {
                        CrAmount += newCr;
                    }


                }

                // Total nikalne loop, kei error aako xaina aile samma vane delete handim, mathi ko le kaam garxa
                // $.each($(tr).find('td'), function (key_, td) {
                //
                //     if ($(td).prop('class') == 'drAmount') {
                //         let newDr = $(this).text();
                //         if (newDr) {
                //             DrAmount += parseFloat(newDr);
                //             $('#dr_amount').html(DrAmount);
                //
                //         }
                //     }
                //     if ($(td).prop('class') == 'crAmount') {
                //         let newCr = $(this).text();
                //         if (newCr) {
                //
                //             CrAmount += parseFloat(newCr);
                //             $('#dr_amount').html(CrAmount);
                //
                //         }
                //     }
                //
                // })
                drCrRemaining = DrAmount - CrAmount;
                if (CrAmount === DrAmount && CrAmount > 0) {


                    $('#VoucherSave').prop('disabled', false);

                    let option = '<option selected>...................</option>';
                    $.each(activityForBhuktani, function () {
                        option += '<option value="' + this.activity_id + '">' + this.activity_name + '</option>'
                        $('#bhuktani_main_activity').html(option);
                    })


                } else {
                    $('#VoucherSave').prop('disabled', "disabled");
                }
            });
            $('#dr_amount').text(DrAmount);
            $('#cr_amount').text(CrAmount);
            $('#amount_difference').text(drCrRemaining);
        }

    </script>

    {{--short narration key up --}}
    <script>
        $(document).ready(function () {
            $('#shortInfo').keyup(function () {
                let shortNarration = $('#shortInfo').val();
                $('#detailsInfo').val(shortNarration);
            })
        })
    </script>

    {{--VOucher End--}}


    {{--Bhuktani start--}}


    {{-- भौचर Save --}}



    {{-- Fixed --}}

    {{-- Validation   --}}
    <script>
        $('.form-control').change(function () {
            let val = $(this).val();
            console.log(val);
            if (val) {
                $(this).closest('div').find('p.validation-error').remove();
            }
        });

        function validation() {
            let flag = 1;
            // program name validation
            let programId = $('#program');
            if (!programId.val()) {
                if (programId.siblings('p').length == 0) {
                    programId.after('<p class="validation-error">छान्नुहोस!</p>')
                    programId.addClass("error");
                }
                programId.focus();
                flag = 0;
            } else {
                programId.siblings('p').remove()
            }
            // program name validation end

            //main_activity_name validation
            let mainactivitynameId = $('#main_activity_name');
            let DrOrCr = $('#DrOrCr').val();
            let ledgerType = $('#byahora').val();
            if (DrOrCr == 2 && ledgerType == 3) {
                mainactivitynameId.siblings('p').remove()
            } else {
                if (!mainactivitynameId.val()) {
                    if (mainactivitynameId.siblings('p').length == 0) {
                        mainactivitynameId.after('<p class="validation-error">छान्नुहोस!</p>')
                    }
                    mainactivitynameId.focus();
                    flag = 0;
                } else {
                    mainactivitynameId.siblings('p').remove()
                }
            }

            let partyTypeId = $('#party_type');
            if (ledgerType == 4) {

                // alert(partyTypeId.val());
                if (!partyTypeId.val()) {
                    if (partyTypeId.siblings('p').length == 0) {

                        partyTypeId.after('<p class="validation-error">छान्नुहोस!</p>')
                    }
                    partyTypeId.focus();
                    flag = 0;
                } else {
                    partyTypeId.siblings('p').remove()

                }
            }

            let partyName = $('#party_type');
            if (ledgerType == 4) {

                // alert(partyName.val());
                if (!partyName.val()) {
                    if (partyName.siblings('p').length == 0) {

                        partyName.after('<p class="validation-error">छान्नुहोस!</p>')
                    }
                    partyName.focus();
                    flag = 0;
                } else {
                    partyName.siblings('p').remove()

                }
            }


            //main_activity_name validation end
            //hisab number start
            let hisab_number = $('#hisab_number');
            if (!hisab_number.val()) {
                if (hisab_number.siblings('p').length == 0) {
                    hisab_number.after('<p class="validation-error">छान्नुहोस!</p>')
                }
                hisab_number.focus();
                flag = 0;
            } else {
                hisab_number.siblings('p').remove()
            }
            //details validation

            let detailsId = $('#details');
            if (!detailsId.val()) {
                if (detailsId.siblings('p').length == 0) {
                    detailsId.after('<p class="validation-error">लेख्नुहोस​!</p>')
                }
                detailsId.focus();
                flag = 0;
            } else {
                detailsId.siblings('p').remove()
            }

            //details validation end


            //amount validation

            let amountsId = $('#amount');

            if ((!amountsId.val() || parseInt(amountsId.val()) < 1) && amountsId.length > 0) {
                if (amountsId.siblings('p').length == 0) {
                    amountsId.after('<p class="validation-error">लेख्नुहोस​!</p>')
                }
                amountsId.focus();
                flag = 0;
            } else {
                amountsId.siblings('p').remove()
            }
            //amount validation end
            return flag;


        }

        function bhuktanivalidation() {
            let flag = 1;

            let mainActivityId = $('#bhuktani_main_activity');
            if (!mainActivityId.val()) {
                if (mainActivityId.siblings('p').length == 0) {
                    mainActivityId.after('<p class="validation-error">छान्नुहोस!</p>')
                }
                mainActivityId.focus();
                flag = 0;
            } else {
                mainActivityId.siblings('p').remove()
            }

            let partyTypeId = $('#bhuktani_party_type');
            if (!partyTypeId.val()) {
                if (partyTypeId.siblings('p').length == 0) {
                    partyTypeId.after('<p class="validation-error">छान्नुहोस!</p>')
                }
                partyTypeId.focus();
                flag = 0;
            } else {
                partyTypeId.siblings('p').remove()
            }

            let partyId = $('#bhuktani_party');
            if (!partyId.val()) {
                if (partyId.siblings('p').length == 0) {
                    partyId.after('<p class="validation-error">छान्नुहोस!</p>')
                }
                partyId.focus();
                flag = 0;
            } else {
                partyId.siblings('p').remove()
            }

            let bhuktaniAmount = $('#bhuktani_amount');
            if (!bhuktaniAmount.val()) {
                if (bhuktaniAmount.siblings('p').length == 0) {
                    bhuktaniAmount.after('<p class="validation-error">छान्नुहोस!</p>')
                }
                bhuktaniAmount.focus();
                flag = 0;
            } else {
                bhuktaniAmount.siblings('p').remove()
            }

            let advanceTaxDeduction = $('#advance_tax_deduction_amount');
            if (!advanceTaxDeduction.val()) {
                if (advanceTaxDeduction.siblings('p').length == 0) {
                    advanceTaxDeduction.after('<p class="validation-error">छान्नुहोस!</p>')
                }
                advanceTaxDeduction.focus();
                flag = 0;
            } else {
                advanceTaxDeduction.siblings('p').remove()
            }

            let vatAmount = $('#vat_amount');
            if (!vatAmount.val()) {
                if (vatAmount.siblings('p').length == 0) {
                    vatAmount.after('<p class="validation-error">छान्नुहोस!</p>')
                }
                vatAmount.focus();
                flag = 0;
            } else {
                vatAmount.siblings('p').remove()
            }

            let bankId = $('#bank');
            if (!bankId.val()) {
                if (bankId.siblings('p').length == 0) {
                    bankId.after('<p class="validation-error">छान्नुहोस!</p>')
                }
                bankId.focus();
                flag = 0;
            } else {
                bankId.siblings('p').remove()
            }

            let vat_bill_number_id = $('#vat_bill_number');
            if (!vat_bill_number_id.val()) {
                if (vat_bill_number_id.siblings('p').length == 0) {
                    vat_bill_number_id.after('<p class="validation-error">छान्नुहोस!</p>')
                }
                vat_bill_number_id.focus();
                flag = 0;
            } else {
                vat_bill_number_id.siblings('p').remove()
            }
            let pratibadhata_number_id = $('#pratibadhata_number');
            if (!pratibadhata_number_id.val()) {
                if (pratibadhata_number_id.siblings('p').length == 0) {
                    pratibadhata_number_id.after('<p class="validation-error">छान्नुहोस!</p>')
                }
                pratibadhata_number_id.focus();
                flag = 0;
            } else {
                pratibadhata_number_id.siblings('p').remove()
            }

            return flag;

        }


    </script>

    {{--Date Picker --}}
    <script>
        $("#date").nepaliDatePicker({
            dateFormat: "%y-%m-%d",
            closeOnDateSelect: true
        });

        var currentDate = new Date();
        var currentNepaliDate = calendarFunctions.getBsDateByAdDate(currentDate.getFullYear(), currentDate.getMonth() + 1, currentDate.getDate());
        var formatedNepaliDate = calendarFunctions.bsDateFormat("%y-%m-%d", currentNepaliDate.bsYear, currentNepaliDate.bsMonth, currentNepaliDate.bsDate);
        $("#date").val(formatedNepaliDate);
    </script>

    {{--Convert Nepali date to roman date --}}
    <script>
        function convertNepaliToEnglish(input) {
            // console.log(input);
            var charArray = input.split('');
            //console.log(charArray);
            var engDate = '';
            $.each(charArray, function (key, value) {
                switch (value) {
                    case '१':
                        engDate += '1'
                        break
                    case '२':
                        engDate += '2'
                        break
                    case '३':
                        engDate += '3'
                        break
                    case '४':
                        engDate += '4'
                        break
                    case '५':
                        engDate += '5'
                        break
                    case '६':
                        engDate += '6'
                        break
                    case '०':
                        engDate += '0'
                        break
                    case '७':
                        engDate += '7'
                        break
                    case '८':
                        engDate += '8'
                        break
                    case '९':
                        engDate += '9'
                        break

                    case '-':
                        engDate += '-'
                        break
                }
                //console.log(engDate)
            })
            return engDate

        }
    </script>

    {{--Clear Voucher and Bhuktani field when  Add button click--}}
    <script>
        function clearVoucherField() {

            // $('#main_activity_name').val('')
            $('#hisab_number').val('')
            $('#details').val('')
            $('#party_type').val('')
            $('#party-name').val('')
            $('#amount').val('')

        }

        function clearBhuktaniField() {
            // alert("blank here")
            // $('#main_activity_name').val('')
            $('select#bhuktani_main_activity').val('');
            $('#bhuktani_party_type').val('');
            $('#bhuktani_party').val('');
            $('#bhuktani_amount').val('');
            $('#advance_tax_deduction_amount').val('');
            $('#vat_amount').val('');
            $('#bank').val('');
            $('#vat_bill_number').val('');
            $('#pratibadhata_number').val('');

        }
    </script>

@endsection
