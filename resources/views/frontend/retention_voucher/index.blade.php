@extends('frontend.layouts.app')


@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <form action="{{route('retention.voucher.store')}}" method="post">
                {{csrf_field()}}
                <div class="form-group">
                    <label>कार्यालय</label>
                    <select class="form-control" name="office">
                        <option value="{{$office->id}}">{{$office->name}}</option>
                    </select>
                </div>
                <div class="form-group kalimati">
                    <label>आर्थीक वर्ष</label>
                    <select name="fiscal_year" class="form-control">
                        <option value="{{$fiscalYear->id}}">{{$fiscalYear->year}}</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>मिति</label>
                    <input type="text" class="form-control" name="date" id="date">
                </div>
                <div class="form-group">
                    <label>भौचर न.</label>
                    <input type="text" class="form-control kalimati" name="voucher_number" id="voucherNumber"
                           value="{{$retention_voucher_number}}" readonly>
                </div>
                <br>
                <div class="form-group">
                    <input type="submit" value="Save" class="btn btn-primary">
                </div>
            </form>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <table class="table table-bordered">
                        <thead>
                        <tr style="background-color: #bbbb">
                            <th>क्र.स.</th>
                            <th>मिति</th>
                            <th>भौचर न.</th>
                            <th>व्यहोरा</th>
                            <th>रकम</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($retentionVoucherList as $index=>$retentionVoucher)
                            <tr>
                                <td class="kalimati">{{++$index}}</td>
                                <td class="kalimati">{{$retentionVoucher->date_nep}}</td>
                                <td class="kalimati">{{$retentionVoucher->voucher_number}}</td>
                                <td>
                                    @if($retentionVoucher->short_narration)
                                        {{$retentionVoucher->short_narration}}
                                    @endif
                                </td>
                                <td class="kalimati">
                                    @if($retentionVoucher->amount)
                                        {{$retentionVoucher->amount}}
                                    @endif

                                </td>
                                <td>
                                    <a href="{{route('retention.voucher.view',$retentionVoucher->id)}}" target="_blank">हर्ने</a> |
                                    @if($retentionVoucher->status ==0)
                                    <a href="{{route('retention.voucher.details',$retentionVoucher->id)}}">Details</a> |
                                    <a href="{{route('retention.voucher.approved',$retentionVoucher->id)}}">स्विकृत</a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>

@endsection

@section('scripts')

    {{--  Date picker--}}
    <script>
        $("#date").nepaliDatePicker({
            dateFormat: "%y-%m-%d",
            closeOnDateSelect: true
        });

        var currentDate = new Date();
        var currentNepaliDate = calendarFunctions.getBsDateByAdDate(currentDate.getFullYear(), currentDate.getMonth() + 1, currentDate.getDate());
        var formatedNepaliDate = calendarFunctions.bsDateFormat("%y-%m-%d", currentNepaliDate.bsYear, currentNepaliDate.bsMonth, currentNepaliDate.bsDate);
        $("#date").val(formatedNepaliDate);

    </script>


    {{--Convert Nepali date to roman date --}}
    <script>
        function convertNepaliToEnglish(input) {
            // console.log(input);
            var charArray = input.split('');
            //console.log(charArray);
            var engDate = '';
            $.each(charArray, function (key, value) {
                switch (value) {
                    case '१':
                        engDate += '1'
                        break
                    case '२':
                        engDate += '2'
                        break
                    case '३':
                        engDate += '3'
                        break
                    case '४':
                        engDate += '4'
                        break
                    case '५':
                        engDate += '5'
                        break
                    case '६':
                        engDate += '6'
                        break
                    case '०':
                        engDate += '0'
                        break
                    case '७':
                        engDate += '7'
                        break
                    case '८':
                        engDate += '8'
                        break
                    case '९':
                        engDate += '9'
                        break

                    case '-':
                        engDate += '-'
                        break
                }
                //console.log(engDate)
            })
            return engDate

        }
    </script>


    {{--    VOucher submit button click हुदा--}}


@endsection