@extends('frontend.layouts.app')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">

            <div class="form-group">

            </div>
            <div class="form-group">

            </div>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>SN</th>
                            <th>Program</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($programs as $index=>$program)

                            <tr>
                                <td style="width: 100px;">{{++$index}}</td>
                                <td>{{$program->name}}</td>

                                <td><a href="{{route('retention.voucher.index',$program->id)}}">भित्र</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>

@endsection