
@extends('frontend.layouts.app')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Office
                <small>Control panel</small>
            </h1>
            <ul class="breadcrumb">
                <li> <a type="button" href="{{route('admin.office.create')}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> Create</a></li>
            </ul>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Province</th>
                            <th>MOF</th>
                            <th>Ministry</th>
                            <th>Department</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($offices as $office)
                            <tr>
                                <td>{{$office->name}}</td>
                                <td>{{$office->province->name}}</td>
                                <td>{{$office->mof->name}}</td>
                                <td>{{$office->ministry->name}}</td>
                                <td>{{$office->department->name}}</td>
                                <td>{{$office->status}}</td>
                                <td>
                                    <a type="button" href="" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> Edit</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>

@endsection