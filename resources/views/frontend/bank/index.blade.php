
@extends('frontend.layouts.app')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        बैङ्क विवरण

      </h1>
      <ul class="breadcrumb">
        <li> <a type="button" href="{{route('bank.create')}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> नयां बनाउने</a></li>
      </ul>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-body">
          <table class="table table-bordered">
            <thead>
            <tr>
              <th>क्र.स.</th>
              <th>बैङ्क नाम</th>
              <th>ठगाना</th>
              <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($banks as $index=>$bank)
              <tr>
                <td>{{++$index}}</td>
                <td>{{$bank->name}}</td>
                <td>{{$bank->address}}</td>

                <td>
                  <a type="button" href="{{route('bank.edit',$bank->id)}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> Edit</a>
                  <a type="button" href="#" id="delete_bank" data-bank-id="{{$bank->id}}" class="btn btn-sm btn-danger"><i class="fa fa-pencil"></i> Delete</a>

                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>

@endsection

@section('scripts')
  <script>
    $(document).on('click','#delete_bank',function () {

      let bank_id = $(this).attr('data-bank-id');
      let url = '{{route('bank.delete',123)}}';
      url = url.replace('123',bank_id);
      swal({
        title: "Are you sure?",
        text: "Delete भए पछि Recovere हुदैन",
        icon: "warning",
        buttons: true,
        dangerMode: true,

      }).then((willDelete) => {

        if (willDelete) {

          $.ajax({

            url : url,
            method : 'get',
            success : function (res) {
              console.log($.parseJSON(res));
              if(res){
                swal("Bank has been deleted!", {
                  icon: "success",
                });

                location.reload();
              }

            }
          })

        } else {
          // swal("Your imaginary file is safe!");
        }
      });
    })
  </script>


@endsection