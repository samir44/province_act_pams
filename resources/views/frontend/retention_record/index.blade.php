
@extends('frontend.layouts.app')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        धरौटी अभिलेख

      </h1>
      <ul class="breadcrumb">
        <li> <a type="button" href="{{route('retention.record.create')}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> नयां बनाउने</a></li>
      </ul>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-body">
          <table class="table table-bordered">
            <thead>
            <tr>
              <th>क्र.सं.</th>
              <th>प्रयोजन</th>
              <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($retentionRecords as $index=>$retentionRecord)
              <tr>

                  <td>{{++$index}}</td>
                  <td>{{$retentionRecord->purpose}}</td>
              <td>
                  <a type="button" href="{{route('retention.record.edit',$retentionRecord->id)}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> Edit</a>
                </td>

              </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>

@endsection