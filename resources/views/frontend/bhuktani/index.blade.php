
@extends('frontend.layouts.app')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h2 align="center">
        भुक्तानी आदेश

      </h2>
      <h3><center>बजेट उपशिर्षक : {{$programs->name}}</center></h3>
      <input type="hidden" id="bs-roman" name="bs-roman">
      <input type="hidden" value="{{$programs->id}}" name="budget_sub_head">
      <h4 class="kalimati"><center>शिर्षक न  : {{$programs->program_code}}</center></h4>
      <a type="button" href="{{route('bhuktani.create',$programs->id)}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> नयां</a>

    </section>

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-body">
          <table class="table table-bordered">
            <thead>
            <tr>
              <th>सि.न.</th>
              <th>मिति</th>
              <th>भुक्तानी आदेश न.</th>
              <th>रकम</th>
              <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($bhuktanies as $index=>$bhuktani)
              <tr>
                <td class="kalimati">{{++$index}}</td>
                <td>{{$bhuktani->date_nepali}}</td>
                <td class="kalimati">{{$bhuktani->adesh_number}}</td>
               <td class="kalimati">{{$bhuktani->amount}}</td>
               <td>
                   @if (count($bhuktanies) - $index == 0)
{{--                       <a type="button" href="{{route('bhuktani.create',$bhuktani->id)}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> Edit</a>--}}
                       <a type="button" href="{{route('bhuktani.delete',['id'=>$bhuktani,'program_id'=>$programs->id])}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> Delete</a>
                   @endif
               </td>
              </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>

@endsection