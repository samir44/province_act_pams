
@extends('frontend.layouts.app')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        बजेट उपशिर्षक विवरण

      </h1>
      <ul class="breadcrumb">
        <li> <a type="button" href="{{route('program.create')}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> नयां बनाउने</a></li>
      </ul>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-body">
          <table class="table table-bordered">
            <thead>
            <tr>
              <th>क्र.स.</th>
              <th>बजेट उपशिर्षक नाम</th>
              <th>बजेट उपशिर्षक कोड</th>
              <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($programes as $index=>$program)
              <tr>
                <td>{{++$index}}</td>
                <td>{{$program->name}}</td>
                <td>{{$program->program_code}}</td>

                <td>
                  <a type="button" href="{{route('program.edit',$program->id)}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> Edit</a>
                  @if($program->akhtiyari == null)
                    <a type="button" href="#" id="delete_program" data-program-id="{{$program->id}}" class="btn btn-sm btn-danger"><i class="fa fa-pencil"></i> Delete</a>
                  @endif
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>

@endsection

@section('scripts')
  <script>
    $(document).on('click','#delete_program',function () {

      let program_id = $(this).attr('data-program-id');
      let url = '{{route('program.delete',123)}}';
          url = url.replace('123',program_id);
      swal({
        title: "Are you sure?",
        text: "Delete भए पछि Recovere हुदैन",
        icon: "warning",
        buttons: true,
        dangerMode: true,

      }).then((willDelete) => {

        if (willDelete) {

          $.ajax({

            url : url,
            method : 'get',
            success : function (res) {
              console.log($.parseJSON(res));
              if(res){
                swal("Budget Sub Head has been deleted!", {
                  icon: "success",
                });

                location.reload();
              }

            }
          })

        } else {
          // swal("Your imaginary file is safe!");
        }
      });
    })
  </script>

@endsection