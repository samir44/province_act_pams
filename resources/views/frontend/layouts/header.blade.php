<div class="head-container">
    {{--    Header Start--}}
    <div class="top-nav">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">
                        <a href="{{url('/')}}"><img src="{{ asset('img/nepal-govt-logo.png')}}" height="25px"
                                                    width="25px" alt=""></a>
                        प्रदेश लेखा व्यवस्थापन प्रणाली, <b>{{Auth::user()->office->name}}
                            , {{Auth::user()->province->name}}
                            ,@if(Auth::user()->office->district){{Auth::user()->office->district->name}}@endif</b>
                    </a>
                </div>

                <ul class="nav navbar-nav navbar-right">

                </ul>
            </div>
        </nav>
    </div>
    <div class="menu-nav">
        <ul class="nav nav-tabs">
            @if(!auth()->user()->hasRole('ministry_super_vision'))
                <li class=""><a href="{{url('/')}}"><i class="fa fa-home"></i></a></li>
                <li class="@if(in_array(Route::current()->getName(),['program','bank','advance','karmachari','karmachari.create','program.edit','bank.edit','AdvanceAndPayment.create','AdvanceAndPayment.edit','karmachari.edit','program.create','voucher.signature'])) active @endif"
                    id="kharid_home"><a data-toggle="tab" class="active" href="#set_up">सेट अप</a></li>
                <li class="@if(in_array(Route::current()->getName(), ['budget.create','budget.create.continue','budget.edit','akhtiyari.param','edit.akhtiyari'])) active @endif">
                    <a data-toggle="tab" href="#budget">बजेट तर्जुमा </a></li>
                <li class="@if(in_array(Route::current()->getName(),['voucher','bhuktani','bhuktani.index','voucher_accept_create','bhuktani.create','voucher.list.edit','get_voucher_details'])) active @endif">
                    <a data-toggle="tab" href="#expense">बजेट खर्च</a></li>
                <li class="@if(in_array(Route::current()->getName(),['retention_bank','retention_bank_create','retention_bank_edit','retention.depositor','retention.depositor.create','retention.deposotor.edit','retention','retention.record.edit','retention.bank.guarantee','retention.bank.guarantee.create','retention.voucher.index','retention.voucher'])) active @endif">
                    <a data-toggle="tab" href="#deposit">धरौटी</a></li>
{{--                <li class="@if(in_array(Route::current()->getName(),['voucher','bhuktani','bhuktani.index','voucher_accept_create','bhuktani.create','voucher.list.edit','get_voucher_details','voucher.accept_create','voucher.unapprove.parameter','voucher.unapprove'])) active @endif">--}}
{{--                    <a data-toggle="tab" href="#expense">बजेट खर्च</a></li>--}}
{{--                <li class="@if(in_array(Route::current()->getName(),['retention_bank','retention_bank_create','retention_bank_edit','retention.depositor','retention.depositor.create','retention.deposotor.edit','retention','retention.bank.guarantee','retention.bank.guarantee.create','retention.voucher','retention.voucher.edit','retention.voucher.approve.param','retention.bhuktani'])) active @endif">--}}
{{--                    <a data-toggle="tab" href="#deposit">धरौटी</a></li>--}}
                <li class="@if(in_array(Route::current()->getName(),
            ['report.voucher','report.activity','report.bhuktani.adesh.param','report.malepa.five.param','report.malepa.thirteen.param',
            'report.malepa.seventeen.param','report.malepa.twentyTwo.param','report.malepa.fourteen.param','report.goswara.dharauti.khata.param',
            'report.byaktigat.dharauti.khata.param','report.dharauti.cash.book.param','report.retentio.bhuktani.adesh.param'])) active @endif">
                    <a  data-toggle="tab" href="#report">प्रतिवेदन </a></li>
            @endif
            @if(auth()->user()->hasRole('ministry_super_vision') or auth()->user()->hasRole('ministry'))
                <li class="@if(in_array(Route::current()->getName(), ['ministry.office.list','ministry.activity.list.parameter'])) active @endif">
                    <a data-toggle="tab" href="#superVision">अनुगमन</a></li>
            @endif
            {{--            <li> {{Route::current()->getName()}}</li>--}}
            <li class="pull-right dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="fa fa-cogs" aria-hidden="true"></i>
                    सेटिङ्स <span class="caret"></span></a>
                <ul class="dropdown-menu setting-dropdown">
                    @if(Auth::user()->roles->first()->name=='superadmin')
                        <li><a href="{{route('admin')}}" target="_blank">भित्र</a></li>
                    @endif
                    @if(Auth::user()->roles->first()->name=='ministry')
                        <li class=""><a href=""><i class="fa fa-dashboard"></i> See All Report </a></li>
                    @endif
                    <li>
                        <a href="{{route('logout')}}">
                            होम
                            बाहिर जानुहोस
                        </a>
                    </li>
                </ul>
            </li>
        </ul>

        <div class="tab-content">

            {{--सेट अप--}}
            <div id="set_up"
                 class="tab-pane fade @if(in_array(Route::current()->getName(),['program','bank','bank.create','advance','karmachari','karmachari.create','program.create','program.edit','bank.edit','AdvanceAndPayment.create','AdvanceAndPayment.edit','karmachari.edit','voucher.signature']))) active in @endif">
                <div class="sub-menu">
                    <a href="{{route('program')}}" class="add">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                        <span class="">बजेट उपशिर्षक</span>
                    </a>
                </div>
                <div class="sub-menu">
                    <a href="{{route('bank')}}" class="kharidAdeshSearch">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                        <span class="">बैकं</span>
                    </a>
                </div>
                <div class="sub-menu">
                    <a href="{{route('advance')}}" class="kharidAdeshSearch">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                        <span class="">पेश्की लेजर</span>
                    </a>
                </div>

                <div class="sub-menu">
                    <a href="{{route('karmachari')}}" class="kharidAdeshSearch">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                        <span class="">कर्मचारी</span>
                    </a>
                </div>
                <div class="sub-menu">
                    <a href="{{route('voucher.signature')}}" class="add">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                        <span class="">भौचर हस्ताक्षर कर्ता</span>
                    </a>

                </div>

            </div>


            {{--  बजेट  --}}
            <div id="budget"
                 class="tab-pane  fade @if(in_array(Route::current()->getName(), ['budget.create','budget.create.continue','budget.edit','akhtiyari.param','edit.akhtiyari'])) active in @endif">
                <div class="sub-menu">
                    <a href="{{route('akhtiyari.param')}}" class="add">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                        <span class="">अख्तियारी प्रविस्टी</span>
                    </a>

                </div>
                <div class="sub-menu">
                    <a href="{{route('budget.create')}}" class="add">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                        <span class="">विस्तृत बजेट प्रविस्टी</span>
                    </a>

                </div>
                <div class="sub-menu">
                    <a href="{{route('budget.edit')}}" class="add">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                        <span class="">बजेट संसोधन</span>
                    </a>

                </div>
            </div>

            {{--  खर्च भौचरर    --}}
            <div id="expense"
                 class="tab-pane fade @if(in_array(Route::current()->getName(),['voucher','bhuktani','voucher.accept_create','bhuktani.index','bhuktani.create','voucher.list.edit','get_voucher_details','voucher.unapprove.parameter','voucher.unapprove','grant.voucher.parameter'])) active in @endif">
                <div class="sub-menu">
                    <a href="{{route('voucher')}}" class="add">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                        <span class="">भौचर</span>
                    </a>
                </div>
                <div class="sub-menu">
                    <a href="{{route('voucher.accept_create')}}" class="magFaramSearch">
                        <i class="fa fa-search" aria-hidden="true"></i>
                        <span class="">भौचर स्विकृत</span>
                    </a>
                </div>
                <div class="sub-menu">
                    <a href="{{route('bhuktani')}}" class="magFaramSearch">
                        <i class="fa fa-search" aria-hidden="true"></i>
                        <span class="">भुक्तानि आदेश</span>
                    </a>
                </div>
                <div class="sub-menu">
                    <a href="{{route('voucher.list.edit')}}" class="magFaramSearch">
                        <i class="fa fa-search" aria-hidden="true"></i>
                        <span class="">भौचर संसोधन</span>
                    </a>
                </div>

                <div class="sub-menu">
                    <a href="{{route('voucher.unapprove.parameter')}}" class="add">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                        <span class="">भौचर अस्विकृत</span>
                    </a>
                </div>

                <div class="sub-menu">
                    <a href="{{route('grant.voucher.parameter')}}" class="add">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                        <span class="">निकासा भौचर</span>
                    </a>
                </div>
            </div>


            {{--            धरौटि--}}
            <div id="deposit"
                 class="tab-pane fade @if(in_array(Route::current()->getName(),['retention_bank','retention_bank_create','retention_bank_edit','retention.depositor','retention.depositor.create','retention.deposotor.edit','retention.record','retention.record.create','retention.record.edit','retention.bank.guarantee','retention.bank.guarantee.create','retention.voucher.parameter','retention.voucher.index','retention.voucher.details','retention.voucher.edit','retention.voucher.approve.param','retention.bhuktani'])) active in @endif">

                <div class="sub-menu">
                    <a href="{{route('retention_bank')}}" class="add">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                        <span class="">बैक्ङ खाता</span>
                    </a>
                </div>

                <div class="sub-menu">
                    <a href="{{route('retention.depositor')}}" class="add">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                        <span class="">धरौटि जम्मा गर्ने</span>
                    </a>

                </div>


                <div class="sub-menu">
                    <a href="{{route('retention.record')}}" class="add">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                        <span class="">धरौटी अभिलेख</span>
                    </a>

                </div>
                <div class="sub-menu">
                    <a href="{{route('retention.bank.guarantee')}}" class="add">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                        <span class="">बैक्ङ ग्यारेन्टी</span>
                    </a>

                </div>
                <div class="sub-menu">
                    <a href="{{route('retention.voucher.index')}}" class="add">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                        <span class="">धरौटी भौचर </span>
                    </a>

                </div>
                <div class="sub-menu">
                    <a href="{{route('retention.voucher.approve.param')}}" class="add">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                        <span class="">धरौटी भौचर स्विकृत</span>
                    </a>

                </div>

                <div class="sub-menu">
                    <a href="{{route('retention.bhuktani')}}" class="add">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                        <span class="">धरौटी भुक्तानी आदेश</span>
                    </a>

                </div>
            </div>


            {{--  report   --}}
            <div id="report" class="tab-pane fade
                @if(in_array(Route::current()->getName(),
                ['report.voucher','report.activity','report.malepa.five.param','report.malepa.thirteen.param','report.malepa.seventeen.param',
                'report.malepa.twentyTwo.param','report.bhuktani.adesh.param','report.malepa.five.param',
                'report.malepa.thirteen.param','report.malepa.seventeen.param','report.malepa.twentyTwo.param',
                'report.malepa.fourteen.param','report.goswara.dharauti.khata.param','report.byaktigat.dharauti.khata.param',
                'report.dharauti.cash.book.param','report.retention.bhuktani.adesh.param','report.budget.sheet.param']))active in
                @endif">

                <div class="sub-menu">
                    <a href="{{route('report.voucher')}}" class="add">
                        <span class="">गोश्वारा भौचर</span>
                    </a>

                </div>
                <div class="sub-menu">
                    <a href="{{route('report.activity')}}" class="add">
                        <span class="">कार्यक्रम</span>
                    </a>

                </div>
                <div class="sub-menu">
                    <a href="{{route('report.bhuktani.adesh.param')}}" class="add">
                        <span class="">खर्च भुक्तानी आदेश</span>
                    </a>
                </div>
                <div class="sub-menu">
                    <a href="{{route('report.retention.bhuktani.adesh.param')}}" class="add">
                        <span class="">धरौटी भुक्तानी आदेश</span>
                    </a>
                </div>
                <div class="sub-menu">
                    <a href="{{route('report.malepa.five.param')}}" class="add">
                        <span class="">म ले प फा न. २०९</span>
                    </a>

                </div>
                <div class="sub-menu">
                    <a href="{{route('report.malepa.thirteen.param')}}" class="magFaramSearch">
                        <span class="">म ले प फा न. २१०</span>
                    </a>
                </div>
                <div class="sub-menu">
                    <a href="{{route('report.malepa.fourteen.param')}}" class="magFaramSearch">
                        <span class="">म ले प फा न. २११ </span>
                    </a>
                </div>
                <div class="sub-menu">
                    <a href="{{route('report.malepa.seventeen.param')}}" class="add">
                        <span class="">आ.वि.</span>
                    </a>

                </div> <div class="sub-menu">
                    <a href="{{route('report.budget.sheet.param')}}" class="add">
                        <span class="">म ले प फा न. २०८</span>
                    </a>

                </div>
                <div class="sub-menu">
                    <a href="{{route('report.malepa.twentyTwo.param')}}" class="magFaramSearch">
                        <span class="">म ले प फा न. २०७</span>
                    </a>
                </div>
                <div class="sub-menu">
                    <a href="{{route('report.byaktigat.dharauti.khata.param')}}" class="magFaramSearch">
                        <span class="">म ले प फा न. ६०१</span>
                    </a>
                </div>
                <div class="sub-menu">
                    <a href="{{route('report.goswara.dharauti.khata.param')}}" class="magFaramSearch">
                        <span class="">म ले प फा न. ६०२</span>
                    </a>
                </div>
                <div class="sub-menu">
                    <a href="{{route('report.dharauti.cash.book.param')}}" class="magFaramSearch">
                        <span class="">म ले प फा न. ६०३</span>
                    </a>
                </div>

            </div>


            {{-- अनुगमन     --}}
            <div id="superVision"
                 class="tab-pane fade @if(in_array(Route::current()->getName(), ['ministry.office.list','ministry.activity.list.parameter','ministry.fatwari.parameter','report.on.source.ledger.parameter','report.expense.head.wise.expense.parameter','report.source.wise.annual.report.parameter'])) active in @endif">


                <div class="sub-menu">
                    <a href="{{route('ministry.office.list',Auth::user()->ministry_id)}}" class="add">
                        <span class="">कार्यालय</span>
                    </a>
                </div>
                <div class="sub-menu">
                    <a href="{{route('ministry.activity.list.parameter',Auth::user()->ministry_id)}}" class="add">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                        <span class="">कार्यक्रम अनुसार</span>
                    </a>
                </div>
                <div class="sub-menu">
                    <a href="{{route('ministry.fatwari.parameter',Auth::user()->ministry_id)}}" class="add">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                        <span class="">फांटवारी</span>
                    </a>
                </div>
                <div class="sub-menu">
                    <a href="{{route('report.on.source.ledger.parameter',Auth::user()->ministry_id)}}" class="add">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                        <span class="">स्रोत र खाता अनुसार</span>
                    </a>
                </div>
                {{--                <div class="sub-menu">--}}
                {{--                    <a href="{{route('report.expense.head.wise.annual.expense.parameter',Auth::user()->ministry_id)}}" class="add">--}}
                {{--                        <i class="fa fa-plus" aria-hidden="true"></i>--}}
                {{--                        <span class="">खर्च शीर्षकगत बजेट र खर्चको वित्तीय बिवरण</span>--}}
                {{--                    </a>--}}
                {{--                </div>--}}
                <div class="sub-menu">
                    <a href="{{route('report.expense.head.wise.expense.parameter',Auth::user()->ministry_id)}}"
                       class="add">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                        <span class="">खर्च शिर्षक अनुसार</span>
                    </a>
                </div>
                <div class="sub-menu">
                    <a href="{{route('report.source.wise.annual.report.parameter',Auth::user()->ministry_id)}}"
                       class="add">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                        <span class="">दातृ निकायगत बजेट र खर्चको आर्थिक बिवरण</span>
                    </a>
                </div>

            </div>

        </div>
    </div>


    <div class="menu-option-container">

    </div>
</div>

<div id="errorMessage" class="error" style="position: absolute;right: 40%;min-width: 300px;padding: 3px;">
    @if(session()->has('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div>
    @elseif(session()->has('error'))
        <div class="alert alert-danger">
            {{ session()->get('error') }}
        </div>
    @endif
</div>