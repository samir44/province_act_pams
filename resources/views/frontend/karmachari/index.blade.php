
@extends('frontend.layouts.app')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                कर्मचारी विवरण

            </h1>

            <ul class="breadcrumb">
                <li> <a type="button" href="{{route('karmachari.create')}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> नयां बनाउने</a></li>
            </ul>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <table class="table table-bordered">
                        <thead>
                        <tr style="background-color: #dbdbdb">
                            <th>क्र.स.</th>
                            <th>नाम नेपालीमा</th>
                            <th>पेयी कोड</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($karmacharis as $index=>$karmachari)

                            <tr>
                                <td>{{++ $index}}</td>
                                <td>{{$karmachari->name_nep}}</td>
                                <td>{{$karmachari->payee_code}}</td>

                                <td>
                                    <a type="button" href="{{route('karmachari.edit',$karmachari->karmachari_id)}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> Edit</a>
                                   @if($karmachari->getKarmachariInVoucherDetails->count() == 0)
                                    <a type="button" href="#" id="delete_karmachari" data-karmachari-id="{{$karmachari->karmachari_id}}" class="btn btn-sm btn-danger"><i class="fa fa-pencil"></i> Delete</a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('scripts')
    <script>
        $(document).on('click','#delete_karmachari',function () {

            let karmachari_id = $(this).attr('data-karmachari-id');
            let url = '{{route('karmachari.delete',123)}}';
            url = url.replace('123',karmachari_id);
            swal({
                title: "Are you sure?",
                text: "Delete भए पछि Recovere हुदैन",
                icon: "warning",
                buttons: true,
                dangerMode: true,

            }).then((willDelete) => {

                if (willDelete) {

                    $.ajax({

                        url : url,
                        method : 'get',
                        success : function (res) {
                            console.log($.parseJSON(res));
                            if(res){
                                swal("Bank has been deleted!", {
                                    icon: "success",
                                });

                                location.reload();
                            }

                        }
                    })

                } else {
                    // swal("Your imaginary file is safe!");
                }
            });
        })
    </script>


@endsection