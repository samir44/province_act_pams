
@extends('frontend.layouts.app')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        भौचर हस्ताक्षर कर्ता

      </h1>

    </section>
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-body">
          <form action="{{route('voucher.signature.create')}}" method="post">
            {{csrf_field()}}
            <div class="form-group">
              <label>तयार गर्ने</label>
              <select class="form-control" name="prepared_by">
                @foreach($karmacharies as $karmachari)
                  <option value="{{$karmachari->id}}">{{$karmachari->name_nepali}}</option>
                @endforeach
              </select>
            </div>

            <div class="form-group">
              <label>पेश गर्ने</label>
              <select class="form-control" name="submit_by">
                @foreach($karmacharies as $karmachari)
                  <option value="{{$karmachari->id}}">{{$karmachari->name_nepali}}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <label>सदर गर्ने</label>
              <select class="form-control" name="approved_by">
                @foreach($karmacharies as $karmachari)
                  <option value="{{$karmachari->id}}">{{$karmachari->name_nepali}}</option>
                @endforeach
              </select>
            </div>
           <div class="form-group">
             <br>
             <input type="submit" class="btn btn-primary" value="Save">
           </div>
          </form>
        </div>
      </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-body">
          <table class="table table-bordered" id="voucher_signature">
            <thead>
            <tr>
              <th>क्र.स.</th>
              <th>तयार गर्ने</th>
              <th>पेश गर्ने</th>
              <th>सदर गर्ने</th>
{{--              <th>Action</th>--}}
            </tr>
            </thead>
            <tbody>
              <tr></tr>
            </tbody>
          </table>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>

@endsection

@section('scripts')
  <script>
    $(document).on('click','#delete_bank',function () {

      let bank_id = $(this).attr('data-bank-id');
      let url = '{{route('bank.delete',123)}}';
      url = url.replace('123',bank_id);
      swal({
        title: "Are you sure?",
        text: "Delete भए पछि Recovere हुदैन",
        icon: "warning",
        buttons: true,
        dangerMode: true,

      }).then((willDelete) => {

        if (willDelete) {

          $.ajax({

            url : url,
            method : 'get',
            success : function (res) {
              console.log($.parseJSON(res));
              if(res){
                swal("Bank has been deleted!", {
                  icon: "success",
                });

                location.reload();
              }

            }
          })

        } else {
          // swal("Your imaginary file is safe!");
        }
      });
    })
  </script>

  <script>
    $(document).ready(function () {
        let office_id = '{{$office_id}}';
        let url = '{{route('voucher.signature.list',123)}}';
        url = url.replace(123,office_id);

      $.ajax({
          
        method : 'get',
        url : url,
        success : function (res) {

            let parseRes = $.parseJSON(res);
            // console.log(parseRes.length);
            // return false;
          console.log(parseRes.karmachari_approved_by.name_nepali);

            let i= 1;
            let tr = '';

              tr = "<tr style='background-color: white; color: black'>" +
                      "<td>" +
                      i +
                      "</td>" +

                      "<td class='activity' data-id=''>" +
                      parseRes.karmachari_prepare_by.name_nepali +
                      "</td>" +

                      "<td class='byahora'>" +
                      parseRes.karmachari_submit_by.name_nepali +
                      "</td>" +

                      "<td class='details'>" +
                      parseRes.karmachari_approved_by.name_nepali +
                      "</td>"


                      // "<td class='action'>" +
                      // '<a href="'+url+'"  class="edit-budget"  target="_blank"  data-index="' + this.id + '">Edit</a> | '
                      ;

            $('#voucher_signature tr:last').after(tr);
          }
      
      })
    })
  </script>


@endsection