@extends('frontend.layouts.app')

@section('styles')
    <style>
        #budgetStoreCreateFrom label {
            display: block !important;
        }

        th {
            color: black;
            font-size: 14px !important;
        }

        .form-group {

            position: relative;
        }

        .validation-error {
            position: absolute;
            color: red;
            right: 4px;
            bottom: -23px;
            font-size: 10px;
        }
    </style>
@endsection
@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1 style="font-size: 25px;">

                <small>भौचर प्रविस्टी</small>
            </h1>
            {{--            <ul class="breadcrumb">--}}
            {{--                <li><a type="button" href="{{route('admin.office')}}" class="btn btn-sm btn-primary"><i--}}
            {{--                                class="fa fa-pencil"></i> List</a></li>--}}
            {{--            </ul>--}}
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <div class="flash-message">
                        @if(session()->has('success'))
                            <div class="alert alert-success">
                                {{ session()->get('success') }}
                            </div>
                        @endif
                    </div>
                    <form action="{{route('voucher.store')}}" method="post" id="voucherStoreCreateFrom">
                        {{csrf_field()}}
                        <div class="table-responsive">
                            <table class="table">
                                <tr>
                                    <td colspan="6">
                                        <div class="form-group" style="width: 150px;">
                                            <label for="fiscal_year"> मिति:</label>
                                            <input type="text" class="form-control" name="date" id="date"
                                                   value="{{$voucher->data_nepali}}"
                                                   required style="height: 32px;">
                                        </div>

                                        <div class="form-group" style="width: 265px;">
                                            <label for="program">बजेट उपशिर्षक</label>
                                            <select name="program" class="form-control" id="program" required
                                                    style="height: 32px;">
                                                <option value="" selected>..................</option>
                                                @foreach($programs as $program)
                                                    <option value="{{$program->id}}">{{$program->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group" style="width: 346px;">
                                            <label for="mainActivityName">कार्यक्रम आयोजनाको नाम</label>
                                            <select name="main_activity_name" class="form-control select2"
                                                    id="main_activity_name" required style="height: 32px;">
                                            </select>
                                        </div>

                                    </td>
                                    <td rowspan="5">
                                        <table style="color: red;">
                                            <tr>
                                                <td style="inline-size: 377px;" class="voucher_number_show">भौचर
                                                    न.:
                                                {{--                                                    <span id="voucher_number"></span></td>--}}
                                                <td class="voucher_numbmer_td hidden">
                                                    <input type="hidden" id="hidden_voucher_numbmer"></input>
                                                </td>

                                            </tr>
                                            <tr>
                                                <td>बजेट :<span id="total_budget"></span></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>खर्च :<span id="total_expense"></span></td>
                                                <input type="hidden" id="hidden_expense">
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>पेश्की :</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>मौज्दात :<span id="remain_budget"></span></td>
                                                <input type="hidden" id="hidden_remain_budget">
                                                {{-- <td class="hidden" id="hidden_remain_budget"></td>--}}
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="form-group" style="width: 150px;">
                                            <label>डेविट/क्रेडिट</label>
                                            <select name="drOrCr" class="form-control" id="DrOrCr" required
                                                    style="height: 32px;">
                                                <option value="1">डेबिट</option>
                                                <option value="2">क्रेडिट</option>
                                            </select>
                                        </div>
                                        <div class="form-group" style="width:110px;">
                                            <label>व्यहोरा</label>
                                            <select name="byahora" class="form-control" id="byahora" required
                                                    style="height: 32px;">
                                                <option value="">.......</option>
                                                @foreach($ledgerTypes as $ledgerType)
                                                    <option value="{{$ledgerType->id}}">{{$ledgerType->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group" style="width:145px;">
                                            <label>हिसाब नं.</label>
                                            <select name="hisab_number" class="form-control select2" id="hisab_number"
                                                    required
                                                    style="height: 32px;">
                                                <option>...................</option>
                                            </select>
                                        </div>
                                        <div class="form-group" style="width: 346px;">
                                            <label>बिबरण</label>
                                            <input type="text" class="form-control" name="details" id="details" required
                                                   style="height: 32px;">
                                        </div>
                                        <div class="form-group"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="form-group" style="width: 150px;">
                                            <label>पेश्कि पाउने प्रकार</label>
                                            <select name="party_type" class="form-control" id="party_type" disabled
                                                    style="height: 32px;">
                                                <option value="">..............</option>
                                                <option value="1">उपभोक्ता समिति</option>
                                                <option value="2">ठेकेदार</option>
                                                <option value="3">व्यक्तिगत</option>
                                                <option value="4">संस्थागत</option>
                                                <option value="5">कर्मचारी</option>
                                            </select>
                                        </div>
                                        <div class="form-group" style="width: 130px">
                                            <label>पेश्कि पाउने</label>
                                            <select name="party-name" id="party-name" class="form-control select2"
                                                    style="height: 32px;">
                                                <option value="" selected>..............</option>

                                            </select>

                                        </div>
                                        <div class="form-group" style="width: 130px">
                                            <label>रकम</label>
                                            <input type="number" name="amount" min="1" id="amount" class="form-control"
                                                   required style="height: 32px;">
                                        </div>
                                        <div class="form-group">
                                            <button type="button" class="btn btn-primary" id="voucher_submit"
                                                    style="margin-top: 18px;">थप
                                            </button>
                                        </div>

                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div>
                            <table width="100%" border="1"
                                   style="background-color: #d3d3d3; margin-bottom: 20px; display: none"
                                   id="peskiVoucherTable">
                                <col width="">
                                <col width="">
                                <col width="">
                                <col width="">
                                <col width="200">
                                <thead>
                                <th></th>
                                <th>Date</th>
                                <th>Voucher No</th>
                                <th>Peski</th>
                                <th>Baki</th>
                                </thead>
                                <tbody>


                                </tbody>
                            </table>
                        </div>
                    </form>
                </div>
            </div>
        </section>
        <!-- /.content -->

        <section class="content">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <table class="table" id="voucher_table" border="1">
                        <thead>
                        <tr style="background-color:  #dbdbdb;">
                            <th>SN</th>
                            <th>डे|क्रे</th>
                            <th>कार्यक्रम|आयोजनाको नाम</th>
                            <th>व्यहोरा हिसाब न।</th>
                            <th>विवरण</th>
                            <th>डेबिट रकम</th>
                            <th>क्रेडिट रकम</th>
                            <th>प्रापक प्रकार</th>
                            <th>पेश्क पाउने</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr></tr>
                        @foreach($voucherDetailsLIst as $key=>$voucherDetail)

                            <tr class="{{++$key}} voucher-details existing" style="background-color: white;">
                                <td class="sn">{{$key}} </td>
                                <input type="hidden" class="voucher_details_id" id="voucher_details_id"
                                       value="{{$voucherDetail->id}}">

                                @if($voucherDetail->dr_or_cr == 1)
                                    <td class='dr-cr' data-dr-cr="{{$voucherDetail->dr_or_cr}}">डेबिट</td>
                                @else
                                    <td class='dr-cr' data-dr-cr="{{$voucherDetail->dr_or_cr}}">क्रेडिट</td>
                                @endif

                                <td class="activity"
                                    data-id="@if($voucherDetail->mainActivity){{$voucherDetail->main_activity_id}}@endif"
                                    data-activity-name="@if($voucherDetail->mainActivity){{$voucherDetail->mainActivity->sub_activity}}@endif">
                                    @if($voucherDetail->mainActivity){{$voucherDetail->mainActivity->sub_activity}}
                                    @endif
                                </td>
                                <td class="byahora" data-byahora="{{$voucherDetail->ledger_type_id}}">
                                    @if($voucherDetail->ledgerType){{$voucherDetail->ledgerType->name}}@endif</td>
                                <td class='hidden expense_head'
                                    data-expense-head_id="{{$voucherDetail->expense_head_id}}">{{$voucherDetail->expense_head_id}}</td>
                                <td class="details">{{$voucherDetail->bibaran}}</td>
                                <td class="drAmount">{{$voucherDetail->dr_amount}}</td>
                                <td class="crAmount">{{$voucherDetail->cr_amount}}</td>
                                <td class="partyType"
                                    data-party-type="{{$voucherDetail->party_type}}">{{$voucherDetail->party_type}}</td>
                                <td class="party"
                                    data-party-id="{{$voucherDetail->peski_paune_id}}">{{$voucherDetail->peski_paune_id}}</td>
                                <td class="hidden peski_type"
                                    data-peski-type="{{$voucherDetail->peski_type}}">{{$voucherDetail->peski_type}}</td>
                                <td class="edit-voucher-td">
                                    <a href="#" class="edit-voucher" data-sn="{{$key}}">Edit</a>
                                    | <a href="#" class="delete-row">Delete</a>
                                </td>
                                <td>
                                    <input type="hidden" name="details_id" id="details_id"
                                           value="{{$voucherDetail->voucher_details_id}}">
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <table id="dr_cr_total_table" width="100%" border="1">
                        <tr style="background-color: #dbdbdb">
                            <td style="text-align: right">
                                <span style="margin-right: 20px">जम्मा|</span>
                            </td>
                            <td id="dr_amount">0|</td>
                            <td id="cr_amount">0|</td>
                            <td id="amount_difference">0|</td>
                        </tr>
                    </table>
                    <table class="table">
                        <tr>
                            <td>
                                <div class="form-group">
                                    <label>कारोवारको संक्षिप्त व्यहोरा</label>
                                    <textarea class="form-control shortInfo" name="shortInfo" id="shortInfo"
                                              style="margin: 0px;width:361px;height: 35px; resize: none"></textarea>
                                </div>
                                <div class="form-group">
                                    <label>कारोवारको विस्तृत व्यहोरा</label>
                                    <textarea class="form-control detailsInfo" name="detailsInfo" id="detailsInfo"
                                              style="margin: 0px; width:758px;
    height: 35px; resize: none "></textarea>

                                </div>

                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <table class="table">
                        <tr style="background-color: #dbdbdb ;color: black;">
                            <td colspan="7">
                                भुक्तानी प्रायोजनको लागि
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="form-group">
                                    <label>कार्यक्रम/आयोजना/क्रियाकलापको नाम</label>
                                    <select class="form-control bhuktani_main_activity" id="bhuktani_main_activity"
                                            style="width: 290px;">
                                        <option value="">.........</option>
                                        {{--                                        <option value="11" data_test="1">test</option>--}}
                                    </select>

                                </div>
                                <div class="form-group">
                                    <label>प्रापक प्रकार/प्राप्तकर्ता</label>
                                    <select name="bhuktani_party_type" id="bhuktani_party_type" class="form-control">
                                        <option value="">..............</option>
                                        <option value=1>उपभोक्ता समिति</option>
                                        <option value="2">ठेकेदार</option>
                                        <option value="3">व्यक्तिगत</option>
                                        <option value="4">संस्थागत</option>
                                        <option value="5">कर्मचारी</option>
                                    </select>

                                </div>
                                <div class="form-group">
                                    <label>प्राप्त कर्ता</label>
                                    <select class="form-control bhuktani_party" id="bhuktani_party"
                                            style="width: 135px; padding-top: 10px">
                                        <option value="">.................</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label> भुक्तानी रकम</label>
                                    <input type="number" name="bhuktani_amount" min="1" id="bhuktani_amount"
                                           class="form-control" style="width: 114px">
                                </div>
                                <div class="form-group">
                                    <label>अग्रिम कर कट्टी रकम</label>
                                    <input type="number" name="advance_tax_deduction_amount" min="1"
                                           id="advance_tax_deduction_amount" class="form-control" style="width: 114px">

                                </div>
                                <div class="form-group">
                                    <label>भ्याट रकम</label>
                                    <input type="number" name="vat_amount" min="1" id="vat_amount" class="form-control"
                                           style="width: 114px">

                                </div>

                                <div class="form-group">
                                    <label>भ्याट बिल न.</label>
                                    <input type="number" name="vat_bill_number" min="1" id="vat_bill_number"
                                           class="vat_bill_number form-control" style="width: 114px">

                                </div>
                                <div class="form-group">
                                    <label>प्रतिबद्धता न.</label>
                                    <input type="number" name="pratibadhata_number" min="1" id="pratibadhata_number"
                                           class="pratibadhata_number form-control" style="width: 114px">

                                </div>
                                <div class="form-group">
                                    <label>चेक किसिम</label>
                                    <select class="form-control" name="cheque_type" id="chequeType">
                                        <option value="">.........</option>
                                        <option value="1">A/c payee</option>
                                        <option value="2">Barreer</option>
                                        <option value="3">Transfer</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <input type="button" class="btn btn-primary" name="buktaniButton" id="buktaniButton"
                                           style="margin-top: 18px;" value="थप"
                                           onclick="bhuktanivalidation()">
                                </div>
                            </td>
                        </tr>
                    </table>

                    <table class="table" id="bhuktani" border="1">
                        <thead>
                        <tr style="background-color:  #dbdbdb;">
                            <th>सि न</th>
                            <th>कार्यक्रम/आयोजना/क्रियाकलापको नाम</th>
                            <th>प्रापक प्रकार</th>
                            <th> प्राप्त कर्ता</th>
                            <th> भुक्तानी रकम</th>
                            <th>अग्रिम कर कट्टी रकम</th>
                            <th>भ्याट रकम</th>
                            {{--                            <th>बैङ्क</th>--}}
                            <th>भ्याट बिल न.</th>
                            <th>प्रतिबद्धता न.</th>
                            <th>चेक किसिम</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr></tr>
                        @foreach($preBhuktanies as $key=>$preBhuktani)
                            <tr class="{{++$key}}">
                                <td class="sn">{{$key}}</td>
                                <td class="hidden" data-voucher_details_id="{{$preBhuktani->id}}"></td>
                                <td class="bhuktani-main-activity"
                                    data-bhuktani_activity="{{$preBhuktani->main_activity_id}}">
                                    @if($preBhuktani->main_activity)
                                        {{$preBhuktani->main_activity->sub_activity}}
                                    @endif
                                </td>
                                <td class="bhuktani-party-type" data-bhuktani_party_type="{{$preBhuktani->party_type}}">
                                    @if($preBhuktani->partyko_type)
                                        {{$preBhuktani->partyko_type->name}}
                                    @endif
                                </td>

                                <td class="bhuktani_party " data-bhuktani_party="{{$preBhuktani->bhuktani_paaune}}">
                                    @if($preBhuktani->party)
                                        {{$preBhuktani->party->name_nep}}
                                    @endif
                                </td>
                                <td class="bhuktani_amount">{{$preBhuktani->amount}}</td>
                                <td class="advance_tax_deduction">{{$preBhuktani->advance_vat_deduction}}</td>
                                <td class="vat_amount">{{$preBhuktani->vat_amount}}</td>

                                <td class="vat_bill_number">{{$preBhuktani->vat_bill_number}}</td>
                                <td class="pratibaddhata_number">{{$preBhuktani->pratibadhhata_number}}</td>
                                <td class="cheque_type" data-cheque_type="{{$preBhuktani->cheque_type}}">
                                    @if($preBhuktani->cheque_type)
                                        @if($preBhuktani->cheque_type == "1")
                                            A/c Payee
                                        @endif
                                        @if($preBhuktani->cheque_type == "2")
                                            Barerrer
                                        @endif
                                        @if($preBhuktani->cheque_type == "3")
                                            Transfer
                                        @endif
                                    @endif
                                </td>
                                <td class="edit-voucher-td">
                                    <a href="#" class="edit-bhuktani-row" data-sn="{{$key}}">Edit</a> | <a href="#"
                                                                                                           class="delete-bhuktani">Delete</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <table class="table-striped" id="bhuktaniAmountTotal" width="100%" border="1">
                        <tr style="background-color: #dbdbdb">
                            <td style="text-align: right;width:769px;padding-right: 10px;">
                                <span>जम्मा</span>
                            </td>
                            <td id="bhuktaniTotal" style="padding-left: 11px;">
                                0
                            </td>
                        </tr>
                    </table>
                    <div class="submit-buttonss btn-group">
                        <button class="btn btn-primary" id="VoucherUpdate" onclick="voucherValidationAndSave()"
                                type="button" disabled>Update
                        </button>
                        <a type="button" class="btn btn-primary" href="{{route('voucher')}}">Clear</a>
                    </div>
                </div>
            </div>
        </section>
    </div>
    {{--    उपभोक्ता modal--}}
    <div class="modal fade party_modal" id="party_modal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">पेश्की भुक्तानी पाउने विवरण प्रविष्टी</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="party_type" style="display: block;">प्रकार</label>
                        <select name="party_type" id="moda_party_type" required class="form-control">
                            @foreach($party_types as $party_type)
                                @if($party_type->id != 5)
                                    <option value="{{$party_type->id}}">{{$party_type->name}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="name_nep">नाम नेपाली</label>
                        <input type="text" class="form-control" id="name_nep" placeholder="नाम नेपाली" name="name_nep"
                               required>
                    </div>
                    <div class="form-group">
                        <label for="name_eng">नाम अग्रेजी</label>
                        <input type="text" class="form-control" id="name_eng" placeholder="नाम अग्रेजी" name="name_eng"
                               required>
                    </div>
                    <div class="form-group">
                        <label for="citizen_number">नागरिकता न.</label>
                        <input type="text" class="form-control" id="citizen_number" placeholder="नागरिकता न."
                               name="citizen_number" required>
                    </div>
                    <div class="form-group">
                        <label for="vat_number">भ्याट/प्यान न.</label>
                        <input type="text" class="form-control" id="vat_number" placeholder="भ्याट/प्यान न."
                               name="vat_number">
                    </div>
                    <div class="form-group">
                        <label for="vat_office">स्थायी लेखा नम्वर जारी गर्ने कार्यालय र ठेगाना</label>
                        <select id="vat_office" class="form-control select2" name="vat_office" style="width: 202px;">
                            <option value="" selected="">.........</option>
                            @foreach($vat_offices as $vat_office)
                                <option value="{{$vat_office->id}}">{{$vat_office->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="mobile_number">मोबाइल न.</label>
                        <input type="text" class="form-control" id="mobile_number" placeholder="मोबाइल न."
                               name="mobile_number" required>
                    </div>
                    <div class="form-group">
                        <label for="payee_code">पेयी कोड न.</label>
                        <input type="text" class="form-control" id="payee_code" name="payee_code">
                    </div>
                    <div class="form-group">
                        <label for="bank">बैङ्क</label>
                        <select id="bank" name="bank" class="form-control select2" style="width: 202px;">
                            <option value="" selected="">.........</option>
                            @foreach($all_banks as $bank)
                                <option value="{{$bank->id}}">{{$bank->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>शाखा कार्यालय</label>
                        <input type="text" class="form-control" name="bank_address" id="bank_address" value="">
                    </div>

                    <div class="form-group">
                        <label>खाता न.</label>
                        <input type="number" class="form-control" name="khata_number" id="khata_number">
                    </div>
                    <div class="form-group">
                        <label for="is_advance">पेश्किमा देखाउने</label>
                        <select name="is_advance" id="is_advance" class="form-control" required>
                            <option value="0">हो</option>
                            <option value="2">होइन</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="is_bhuktani">भुक्तानीमा देखाउने</label>
                        <select name="is_bhuktani" id="is_bhuktani" required class="form-control">
                            <option value="0">हो</option>
                            <option value="2">होइन</option>

                        </select>
                    </div>
                    <div class="form-group">
                        <label for="is_dharauti">धरौटीमा देखाउने</label>
                        <select name="is_dharauti" id="is_dharauti" required class="form-control">
                            <option value="0">हो</option>
                            <option value="2">होइन</option>
                        </select>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"
                            id="party_modal_close">Close
                    </button>
                    <button type="button" class="btn btn-default" id="party_modal_submit">Add</button>
                </div>
            </div>

        </div>
    </div>


    {{--    कर्मचारी modal--}}
    <div class="modal fade karmachari_modal" id="karmachari_modal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Modal Header</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name_nepali">पुरा नाम नेपालीमा:</label>
                        <input type="text" class="form-control" id="name_nepali" placeholder="नाम नेपालीमा"
                               name="name_nepali" required>
                    </div>

                    <div class="form-group">
                        <label for="name_english">पुरा नाम अंग्रेजीमा:</label>
                        <input type="text" class="form-control" id="name_english" placeholder="नाम अंग्रेजीमा"
                               name="name_english" required>
                    </div>

                    <div class="form-group">
                        <label for="gender">लिङ्ग:</label>
                        <select class="form-control" name="gender" id="gender" required>
                            <option value="">....</option>
                            <option value="0">महिला</option>
                            <option value="1">पुरुष</option>
                            <option value="2">अन्य</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="date_of_birth">जन्म मिति :</label>
                        {{--              <input type="hidden" id="roman_date" name="dob_roman">--}}
                        <input type="text" class="form-control" id="date_of_birth" placeholder="Select Date"
                               name="date_of_birth" required>
                    </div>

                    <div class="form-group">
                        <label for="marital_status">वैवाहिक स्थिति:</label>
                        <select class="form-control" name="marital_status" id="marital_status" required>
                            <option value="">....</option>
                            <option value="0">एकल</option>
                            <option value="1">दम्पति</option>

                        </select>
                    </div>

                    <div class="form-group">
                        <label for="vat_pan">भ्याट/प्यान.</label>
                        <input type="number" class="form-control" id="vat_pan" placeholder="भ्याट/प्यान" name="vat_pan">
                    </div>
                    {{--            Personal Details End--}}
                    <div class="clearfix"></div>

                    <h3>सम्पर्क विवरण</h3>
                    <div class="form-group">
                        <label>प्रदेश</label>
                        <select class="form-control select2" id="province" name="province">
                            <option>.....................</option>
                            @foreach($provinces as $province)
                                <option value="{{$province->id}}">{{$province->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="district">जिल्ला :</label>
                        <select class="form-control select2" name="district" id="district" required
                                style="width: 120px;">
                            <option>..............</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="local_level">स्थानीय तह :</label>
                        <select class="form-control select2" name="local_level" id="local_level" required
                                style="width: 160px;">
                            <option value="">....</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="phone_number">फोन नं.:</label>
                        <input type="tel" class="form-control" id="phone_number" placeholder="फोन नं"
                               name="phone_number" required>
                    </div>

                    <div class="form-group">
                        <label for="mobile_number">मोबाइल:</label>
                        <input type="text" class="form-control mobile_number" id="mobile_number" placeholder="मोबाइल"
                               name="mobile_number" required>
                    </div>

                    <div class="form-group">
                        <label for="email_address">इमेल:</label>
                        <input type="email" class="form-control" id="email_address" placeholder="इमेल"
                               name="email_address" required>
                    </div>

                    {{--            Contact Details End--}}

                    <div class="clearfix"></div>

                    {{--            Organizational Details--}}
                    <h3>कार्यालयसम्बन्धी विवरण</h3>

                    <div class="form-group">
                        <label for="darbandi_srot">दरबन्दीको श्रोत:</label>
                        <select class="form-control" name="darbandi_srot_id" id="darbandi_srot_id" required>
                            <option value="">....</option>
                            @foreach($darbandisrots as $darbandisrot)
                                <option value="{{$darbandisrot->id}}">{{$darbandisrot->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="darbandi_type">दरबन्दीको प्रकार:</label>
                        <select class="form-control" name="darbandi_type_id" id="darbandi_type_id" required>
                            <option value="">....</option>
                            @foreach($darbanditypes as $darbanditype)
                                <option value="{{$darbanditype->id}}">{{$darbanditype->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="sheet_roll_no">सिटरोल नंबर.:</label>
                        <input type="number" class="form-control" id="sheet_roll_no" placeholder="सिटरोल नंबर"
                               name="sheet_roll_no" required>
                    </div>

                    <div class="form-group">
                        <label for="sewa">सेवा वर्ग:</label>
                        <select class="form-control" name="sewa_barga" id="sewa_barga" required>
                            <option value="">............</option>
                            <option value="1">निजामती सेवा</option>
                            <option value="2">अन्य</option>

                        </select>
                    </div>
                    <div class="form-group">
                        <label for="sewa">सेवा:</label>
                        <select class="form-control" name="sewa_id" id="sewa_id" required>
                            <option value="">....</option>
                            @foreach($sewas as $sewa)
                                <option value="{{$sewa->id}}">{{$sewa->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="samuha_id">समूह:</label>
                        <select class="form-control" name="samuha_id" id="samuha_id" required style="width: 219px;">
                            <option value="">....</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="taha">श्रेणी/तह:</label>
                        <select class="form-control" name="taha_id" id="taha_id" required style="width: 150px;">
                            <option value="">....</option>
                            @foreach($tahas as $taha)
                                <option value="{{$taha->id}}">{{$taha->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="pad">पद:</label>
                        <select class="form-control" name="pad_id" id="pad_id" required style="width: 117px;">
                            <option value="">....</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="office_head">कार्यालय प्रमुख:</label>
                        <select class="form-control" name="office_head" id="office_head" required>
                            <option value="2">होईन</option>
                            <option value="1">हो</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="acc_head">आर्थिक प्रमुख:</label>
                        <select class="form-control" name="acc_head" id="acc_head" required>
                            <option value="2">होईन</option>
                            <option value="1">हो</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="is_peski">पेश्किमा देखाउने:</label>
                        <select class="form-control" name="is_peski" id="is_peski" required>
                            <option value="1">हो</option>
                            <option value="2">होईन</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="is_payroll">तलवी देखउने:</label>
                        <select class="form-control" name="is_payroll" id="is_payroll" required>
                            <option value="2">होईन</option>
                            <option value="1">हो</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="can_accept">स्विकृत:</label>
                        <select class="form-control" name="can_accept" id="can_accept" required>
                            <option value="2">होईन</option>
                            <option value="1">हो</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="status">सक्रिय:</label>
                        <select class="form-control" name="status" id="status" required>
                            <option value="1">हो</option>
                            <option value="0">होईन</option>
                        </select>
                    </div>

                </div>
                <div class="clearfix"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"
                            id="karmachari_modal_close">Close
                    </button>
                    <button type="button" class="btn btn-default" id="karmachari_modal_submit">Add</button>
                </div>
            </div>

        </div>
    </div>
@endsection
@section('scripts')




    {{--    पेश्की पाउने change हुदा--}}

    <script>
        $(document).on('change', '#party-name', function () {

            let activity_id = $('select#main_activity_name').val();
            let party = $('select#party-name').val();
            let dr_cr = $('select#DrOrCr').val();
            let budget_sub_head_id = $('#program').val();
            if (dr_cr == 2) {
                if (activity_id) {
                    let url = '{{route('get.reamin.peski',['123','124','1212'])}}';
                    url = url.replace(123, activity_id);
                    url = url.replace(124, party);
                    url = url.replace(1212, budget_sub_head_id);
                    console.log(url);
                    $.ajax({

                        url: url,
                        method: 'get',
                        success: function (res) {

                            let peskiVouchers = $.parseJSON(res);
                            let $table = $('table#peskiVoucherTable');
                            if (peskiVouchers.length > 0) {
                                let $editingRow = $('#voucher_table').find('tr.' + editingSN);
                                let voucherDetailId = $editingRow.find('input[name="details_id"]').val();
                                $table.find('tbody').html('');
                                let remainFlag = 0;
                                $.each(peskiVouchers, function () {
                                    if (this.baki > 0) {
                                        remainFlag = 1;
                                        let tr = '<tr>';
                                        if (voucherDetailId == this.id) {

                                            tr += '<td style="text-align: center" class="voucher-detail-id"><input checked type="checkbox" class="peski-voucher-detail-id" value="' + this.id + '" name="peski_farsyot_voucher_id"></td>';
                                        } else {
                                            tr += '<td style="text-align: center" class="voucher-detail-id"><input  type="checkbox" class="peski-voucher-detail-id" value="' + this.id + '" name="peski_farsyot_voucher_id" disabled></td>';

                                        }
                                        tr += '<td class="date">';
                                        tr += this.date;
                                        tr += '</td><td class="voucher_no">';
                                        tr += this.voucher_no;
                                        tr += '</td><td class="peski">';
                                        tr += this.peski;
                                        tr += '</td><td class="baki">';
                                        tr += this.baki;
                                        tr += '</td></tr>';
                                        $table.find('tbody').append(tr)
                                    }
                                });
                                if ($('input.peski-voucher-detail-id:checked').length == 0) {
                                    $('input.peski-voucher-detail-id').prop('disabled', false);
                                }

                                if (remainFlag) {
                                    $table.show();
                                } else {
                                    alert("पेश्की बाकीँ छैन उउ!!")
                                    $table.hide();
                                }
                            } else {

                                alert("पेश्की बाकीँ छैन !!");
                                $table.hide();
                            }
                        }
                    })
                }
            }

        })
    </script>


    {{--party name click हुदा--}}
    <script>
        let adding_party_id = ''
        $(document).on('change', '#party-name, #bhuktani_party', function () {

            let party_type = '';
            let party_type_id = $(this).prop('id');

            if (party_type_id == "party-name") {

                party_type = $('#party_type').val();
            } else {

                party_type = $('#bhuktani_party_type').val();
                // alert(party_type);

            }

            if ($(this).val() == 'add') {


                if (party_type != 5) {

                    get_modal_show(this, party_type_id);

                } else {

                    get_karmachari_modal_show(this);
                }

            } else {
                // alert("name ho")
            }

        });

        let get_modal_show = function (this_, party_type_id) {

            if (party_type_id == "party-name") {

                party_type = $('select#party_type').val();

            } else {

                party_type = $('select#bhuktani_party_type').val();

            }

            $('#moda_party_type').val(party_type);
            adding_party_id = $(this_).prop('id');
            $('#party_modal').modal('show');

        };


        $('#party_modal_close').click(function () {
            $('select#party-name').val('');
            $('select#bhuktani_party').val('');
        });


        let get_karmachari_modal_show = function (this_) {
            adding_party_id = $(this_).prop('id');
            $('#karmachari_modal').modal('show');


        };

        $('#karmachari_modal_close').click(function () {
            $('select#party-name').val('');
        })
    </script>

    {{--  party  modal submit हुदा--}}

    <script>
        $(document).on('click', '#party_modal_submit', function () {

            let data = {};
            data['party_type'] = $('#moda_party_type').val();
            data['name_nep'] = $('#name_nep').val();
            data['name_eng'] = $('#name_eng').val();
            data['citizen_number'] = $('#citizen_number').val();
            data['vat_number'] = $('#vat_number').val();
            data['vat_office'] = $('#vat_office').val();
            data['mobile_number'] = $('#mobile_number').val();
            data['payee_code'] = $('#payee_code').val();
            data['bank'] = $('#bank').val();
            data['bank_address'] = $('#bank_address').val();
            data['khata_number'] = $('#khata_number').val();
            data['is_advance'] = $('#is_advance').val();
            data['is_bhuktani'] = $('#is_bhuktani').val();
            data['is_dharauti'] = $('#is_dharauti').val();
            data['_token'] = '{{csrf_token()}}';

            let url = '{{route('AdvanceAndPayment.store')}}';
            // console.log(data);
            $.ajax({

                method: 'post',
                url: url,
                data: data,
                success: function (res) {
                    // console.log($.parseJSON(res));
                    let options = '<option value="">......</option>';
                    let party = $.parseJSON(res);
                    // console.log(this);
                    options += '<option value="' + party.id + '" selected>' + party.name_nep + '</option>';
                    //console.log(options);

                    $('#' + adding_party_id).append(options);
                    $('#party_modal').modal('hide');

                }
            })
        })
    </script>


    {{--    Karmachari modal submit हुदा--}}

    {{--    modal submit हुदा--}}

    <script>
        $(document).on('click', '#karmachari_modal_submit', function () {


            let data = {};
            data['name_nepali'] = $('#name_nepali').val();
            data['name_english'] = $('#name_english').val();
            data['gender'] = $('#gender').val();
            data['date_of_birth'] = $('#date_of_birth').val();
            data['marital_status'] = $('#marital_status').val();
            data['bank'] = $('#bank').val();
            data['bank_address'] = $('#bank_address').val();
            data['khata_number'] = $('#khata_number').val();
            data['vat_pan'] = $('#vat_pan').val();
            data['province'] = $('#province').val();
            data['district'] = $('#district').val();
            data['local_level'] = $('#local_level').val();
            data['phone_number'] = $('#phone_number').val();
            data['mobile_number'] = $('.mobile_number').val();
            data['email_address'] = $('#email_address').val();
            data['payee_code'] = $('#payee_code').val();
            data['darbandi_srot_id'] = $('#darbandi_srot_id').val();
            data['darbandi_type_id'] = $('#darbandi_type_id').val();
            data['sheet_roll_no'] = $('#sheet_roll_no').val();
            data['sewa_barga'] = $('#sewa_barga').val();
            data['sewa_id'] = $('#sewa_id').val();
            data['samuha_id'] = $('#samuha_id').val();
            data['taha_id'] = $('#taha_id').val();
            data['pad_id'] = $('#pad_id').val();
            data['office_head'] = $('#office_head').val();
            data['acc_head'] = $('#acc_head').val();
            data['is_peski'] = $('#is_peski').val();
            data['is_payroll'] = $('#is_payroll').val();
            data['can_accept'] = $('#can_accept').val();
            data['status'] = $('#status').val();
            data['_token'] = '{{csrf_token()}}';

            let url = '{{route('karmachari.store')}}';
            // console.log(data);
            $.ajax({

                method: 'post',
                url: url,
                data: data,
                success: function (res) {
                    // console.log($.parseJSON(res));
                    let options = '<option value="">......</option>';

                    let party = $.parseJSON(res);
                    // console.log(this);
                    options += '<option value="' + party.id + '" selected>' + party.name_nep + '</option>';
                    //console.log(options);

                    $('#' + adding_party_id).append(options);
                    $('#karmachari_modal').modal('hide');

                }
            })
        })
    </script>

    {{--    modal end --}}






    {{--    page edit भएर आउदा  dr_cr_total function लाइ document ready हुदा बोलाको किनकि भौचर save page मा भौचर थप हुदा मात्र यो function call गरेको छ--}}
    <script>
        $(document).ready(function () {

            dr_cr_total();

        })
    </script>
    {{--    Edit बाट आएको data लाई show गर्ने--}}
    <script>
        $(document).ready(function () {
            $('#program').attr("disabled", 'disabled');
            let budget_sub_head = '{{$voucher->budget_sub_head_id}}';
            $("#program option").each(function () {

                if ($(this).val() == budget_sub_head) {
                    $(this).prop('selected', true);
                    get_main_activity_by_budget_sub_head($(this).val());
                    get_voucher_number(budget_sub_head);

                }
            });

            let shortNarration = '{{$voucher->short_narration}}';
            $('#shortInfo').val(shortNarration);

            let longNarration = '{{$voucher->long_narration}}';
            $('#detailsInfo').val(longNarration);

        })
    </script>

    {{--माथिको voucher table बाट कार्यक्रम ल्याउने--}}
    <script>
        $(document).ready(function () {
            getActivity();
        });
        let getActivity = function () {

            let trs = $('#voucher_table tbody').find('.voucher-details');
            // console.log(trs, "this is trs");
            let data = {};
            $.each(trs, function () {

                let activity_name = $(this).find('td.activity').attr('data-activity-name')
                let activity_id = $(this).find('td.activity').attr('data-id');
                data[activity_id] = activity_name;

            });
            // console.log(data,"test");
            let option = '<option value="" selected>.........</option>';
            $.each(data, function (key, val) {

                option += '<option value="' + key + '">' + val + '</option>'
            });
            // console.log(option);
            $('#bhuktani_main_activity').html(option);
            // console.log(data);
        };


    </script>



    {{-- बजेट उपशिर्षक change हुदा  कार्यक्रम आउने  Event--}}
    <script>
        $(document).ready(function () {
            $('#program').change(function () {
                let program_code = $('#program').val();
                get_main_activity_by_budget_sub_head(program_code);

            })
        })

    </script>

    {{-- बजेट उपशिर्षक change हुदा कार्यक्रम आउने  function--}}
    <script>
        let get_main_activity_by_budget_sub_head = function (program_code, activity_id = '') {
            let url = '{{route('get_main_activity_by_budget_sub_head',123)}}';
            url = url.replace(123, program_code);
            $.ajax({
                url: url,
                method: 'get',
                success: function (res) {
                    let option = '<option value="" selected>.......</option>';
                    let activities = $.parseJSON(res);
                    // console.log(activities);
                    if (res) {
                        $.each(activities, function () {
                            if (this.id == activity_id) {
                                option += '<option value="' + this.id + '" selected>' + this.expense_head_by_id.expense_head_code + ' | ' + this.sub_activity + ' |रु. ' + this.total_budget + '</option>'
                                get_expense_head_by_activity_id(this.id);
                            } else {
                                option += '<option value="' + this.id + '">' + this.expense_head_by_id.expense_head_code + ' | ' + this.sub_activity + ' | ' + this.total_budget + '</option>'
                            }
                        })
                    }
                    $('#main_activity_name').html(option).change();

                }
            })

        }
    </script>

    {{-- बजेट उपशिर्षक click गर्दा भौचर न. आउने Event--}}
    <script>
        $(document).ready(function () {
            $('#program').change(function () {
                let budget_sub_head_id = $('#program').val();
                get_voucher_number(budget_sub_head_id);
            })
        })
    </script>

    {{--  बजेट उपशिर्षक click गर्दा भौचर न. आउने Function--}}
    <script>
        let get_voucher_number = function (budget_sub_head_id, vn) {

            let voucher_number = 0;
            let url = '{{route('get_voucher_by_budget_sub_head_and_office',123)}}';
            url = url.replace(123, budget_sub_head_id);
            $.ajax({
                url: url,
                method: 'get',
                success: function (res) {
                    let nepaliVal = changeToNepali(res);
                    // console.log(res);
                    // console.log(nepaliVal);
                    $('#voucher_number').text(nepaliVal);
                    $('#hidden_voucher_numbmer').val(res);
                }
            });
        }
    </script>

    {{-- कार्यक्रम change हुदा Event--}}
    <script>
        $(document).ready(function () {
            $('#main_activity_name').change(function () {

                let main_activity_id = $('#main_activity_name').val();
                let byahora = $('#byahora').val();
                if (byahora.length < 1) {

                    $("#byahora").val("1").change();
                }

                if (main_activity_id != '') {

                    get_expense_head_by_activity_id(main_activity_id);

                } else {
                    if (byahora == 1 || byahora == 4) {
                        $('#hisab_number').html("")
                        $('#details').val('')
                    } else {

                        // alert("test what");

                    }

                }
            })
        })
    </script>

    {{--कार्यक्रम change हुदा बजेट र खर्च देखिने Event--}}
    <script>
        let global_remain_budget = 0;
        $(document).ready(function () {
            $('#main_activity_name').change(function () {
                let main_activity_id = $('#main_activity_name').val();
                let byahora = $('#byahora').val();
                if (byahora.length < 1) {

                    $("#byahora").val("1").change();
                }

                if (main_activity_id != '') {

                    get_expense_head_by_activity_id(main_activity_id);

                } else {
                    if (byahora == 1 || byahora == 4) {
                        $('#hisab_number').html("")
                        $('#details').val('')
                    } else {

                        // alert("test what");

                    }

                }
                // let main_activity_id = $('#main_activity_name').val();
                get_budget_by_main_Activity(main_activity_id);
                // get_expense_by_main_activity(main_activity_id);
            })
        });
    </script>

    {{--  कार्यक्रम change हुदा बजेट र खर्च देखिने Function  --}}
    {{--        yesle table ma herere kati kharcha xa tya vani hisab garxa, edit ko,, purano , naya sab lai herxa --}}

    <script>
        let adjustAmountsInPreview = () => {
            let main_activity_id = $('#main_activity_name').val();
            let $existingDebitTds = $('#voucher_table tbody td.activity[data-id="' + main_activity_id + '"]');
            let expenseInTable = 0;

            $.each($existingDebitTds, function () {

                if ($(this).closest('tr').find('td.dr-cr').attr('data-dr-cr') == 1) {
                    let oldValue = parseFloat($(this).closest('tr').find('td.drAmount').attr('data-old_value'));
                    let newValue = parseFloat($(this).closest('tr').find('td.drAmount').text());
                    if ($(this).closest('tr').hasClass('existing')) {

                        if ($(this).closest('tr').hasClass(editingSN)) {
                            if (oldValue)
                                expenseInTable += oldValue;
                            else
                                expenseInTable += newValue
                        } else {
                            if (oldValue)
                                expenseInTable += oldValue - newValue
                        }
                    } else {
                        if (!$(this).closest('tr').hasClass(editingSN)) {
                            expenseInTable -= newValue
                        }
                    }
                }
            });
            // alert(expenseInTable);
            return expenseInTable;
        };
        let get_budget_by_main_Activity = function () {
            let main_activity_id = $('#main_activity_name').val();
            let url = '{{route('get_budget_and_expe_by_activity',123)}}';
            url = url.replace(123, main_activity_id);
            $.ajax({
                url: url,
                method: 'get',
                success: function (result) {
                    let data = $.parseJSON(result);
                    let $existingDebitTds = $('#voucher_table tbody td.activity[data-id="' + main_activity_id + '"]');

                    let existingExpenseDebitTotal = 0;
                    let remainingBudget = data['remain'];
                    let totalExpense = data['expense'];
                    // alert(editingSN);

                    // $.each($existingDebitTds, function () {
                    //     if ($(this).closest('tr').hasClass('existing')) {
                    //         if ($(this).closest('tr').hasClass(editingSN)) {
                    //             let oldValue = parseFloat($(this).closest('tr').find('td.drAmount').attr('data-old_value'));
                    //             if(oldValue){
                    //                 existingExpenseDebitTotal = oldValue
                    //             }else {
                    //                 existingExpenseDebitTotal = parseFloat($(this).closest('tr').find('td.drAmount').text())
                    //             }
                    //             console.log('Old Value =', oldValue)
                    //
                    //         }
                    //
                    //         else{
                    //             let oldValue = parseFloat($(this).closest('tr').find('td.drAmount').attr('data-old_value'));
                    //             if(oldValue){
                    //                 existingExpenseDebitTotal += parseFloat($(this).closest('tr').find('td.drAmount').text()) - oldValue
                    //             }else{
                    //                 existingExpenseDebitTotal += parseFloat($(this).closest('tr').find('td.drAmount').text())
                    //             }
                    //
                    //         }
                    //         totalExpense = parseFloat(data['expense']) - existingExpenseDebitTotal;
                    //         remainingBudget = parseFloat(data['remain']) + existingExpenseDebitTotal;
                    //     } else {
                    //         existingExpenseDebitTotal = 0;
                    //         if (!$(this).closest('tr').hasClass(editingSN)) {
                    //
                    //             existingExpenseDebitTotal += parseFloat($(this).closest('tr').find('td.drAmount').text());
                    //         }
                    //         // console.log(existingExpenseDebitTotal,data['expense'])
                    //         totalExpense = parseFloat(data['expense']) + existingExpenseDebitTotal;
                    //         remainingBudget = parseFloat(data['remain']) - existingExpenseDebitTotal;
                    //     }
                    // });

                    let expenseInTable = adjustAmountsInPreview();
                    totalExpense = parseFloat(data['expense']) - expenseInTable;
                    remainingBudget = parseFloat(data['remain']) + expenseInTable;
                    // alert(expenseInTable);
                    $('#total_budget').text(parseFloat(data['budget'])).addClass('e-n-t-n-n');
                    $('#total_expense').text(totalExpense).addClass('e-n-t-n-n');
                    $('#hidden_expense').val(totalExpense);
                    $('#remain_budget').text(remainingBudget).addClass('e-n-t-n-n');
                    $('#hidden_remain_budget').val(remainingBudget);


                    global_remain_budget = data['remain'];
                }
            })
        };
    </script>

    {{-- get_expense_head_by_activity_id --}}
    <script>
        let get_expense_head_by_activity_id = function (main_activity_id) {

            let url = '{{route('get_expense_head_by_activity_id',123)}}';
            url = url.replace(123, main_activity_id);
            $.ajax({
                url: url,
                method: 'get',
                success: function (res) {
                    let data = $.parseJSON(res);
                    // console.log("test",data[0]);
                    let options = '<option value="" selected>.........................</option>';

                    options += '<option value="' + data[0].expense_head_by_id.id + '" selected>' + data[0].expense_head_by_id.expense_head_code + ' | ' + data[0].expense_head_by_id.expense_head_sirsak + '</option>'
                    var selected_option = $('#byahora option:selected');
                    if (selected_option.length == 0 || selected_option.val() == 1 || selected_option.val() == 4) {

                        $('#hisab_number').html(options);
                        $('#details').val(data[0].expense_head_by_id.expense_head_sirsak);
                        $('input#details').closest('div').find('p.validation-error').remove();

                    }
                }
            })
        }
    </script>

    {{--Dr  Or Cr change हुदा--}}
    <script>
        $(document).ready(function () {
            $('#DrOrCr').change(function () {
                $('#byahora').val("");
                $('#hisab_number').val("");
                $('#details').val("");
            })
        })
    </script>

    {{--व्यहोरा change हुदा--}}
    <script>
        $(document).ready(function () {
            $('#byahora').change(function () {

                $('#main_activity_name').focus();
                let main_activity_id = $('#main_activity_name').val();
                let byahora = $('#byahora :selected').val();
                let DrOrCr = $('#DrOrCr').val();
                if (byahora == 4) {

                    $('#party_type').prop('disabled', false);
                    get_expense_head_by_activity_id(main_activity_id);

                } else if (byahora == 1) {

                    get_expense_head_by_activity_id(main_activity_id);
                    $('#party_type').attr("disabled", true);

                } else if (DrOrCr == 2 && byahora == 3) {


                    $('#party_type').attr("disabled", "disabled");
                    get_hisab_number_by_byahora(byahora);

                } else if (byahora == 9 || byahora == 7) {

                    $('#party_type').attr("disabled", false);
                    get_hisab_number_by_byahora(byahora);

                } else {

                    $('#party_type').attr("disabled", "disabled");
                    get_hisab_number_by_byahora(byahora);
                }
            })
        })

    </script>

    {{--on change party_type for Vocuher --}}
    <script>
        $(document).ready(function () {
            $('#party_type').change(function () {

                let party_type_id = $('#party_type :selected').val();
                // if(party_type_id == 5){
                //
                //     getKarmachari();
                // } else {
                getPartyByPartyType(party_type_id);
                // }


            })
        });
        let getKarmachari = function () {

            let url = '{{route('get.karmachari')}}';
            $.ajax({

                url: url,
                method: 'get',
                success: function (res) {
                    // console.log($.parseJSON(res));
                    let option = '<option value="" selected>....................</option>';
                    $.each($.parseJSON(res), function () {
                        option += '<option value="' + this.id + '">' + this.name_nepali + '</option>'

                    });
                    $('#party-name').html(option);
                }
            });
        }
    </script>

    {{--get  party name for voucher--}}
    <script>
        let getPartyByPartyType = function (party_type_id, party_id) {
            let url = '{{route('get_party_name_by_party_type_and_office', 123)}}';
            url = url.replace(123, party_type_id);
            $.ajax({
                url: url,
                method: 'get',
                success: function (res) {
                    // console.log($.parseJSON(res));
                    let option = '<option value="" selected>....................</option>';
                    option += '<option value="add" style="font-style: italic; color: blue; text-decoration: underline">Add New</option>';

                    $.each($.parseJSON(res), function () {
                        // console.log(party_name,this.name_nep);
                        if (party_id == this.id) {
                            option += '<option value="' + this.id + '" selected>' + this.name_nep + '</option>'

                        } else {

                            option += '<option value="' + this.id + '">' + this.name_nep + '</option>'
                        }
                    })
                    $('#party-name').html(option).change();
                }
            })
        }
    </script>

    {{--    Peski voucher options --}}
    <script>
        $(document).on('change', 'input.peski-voucher-detail-id', function () {
            let $tr = $(this).closest('tr');
            let baki = $.trim($tr.find('td.baki').text());
            if (this.checked) {
                $('input#amount').val(baki);
                $tr.find('td.baki').text(0);

                let test = $(this).val();
                let $editingRow = $('#voucher_table').find('tr.' + editingSN);


                let voucherDetailsId = $.trim(test);

                $editingRow.find('#details_id').val(voucherDetailsId);

                $('input.peski-voucher-detail-id').prop('disabled', true);
                $(this).prop('disabled', false);

            } else {
                $tr.find('td.baki').text($('input#amount').val());
                $('input#amount').val('');
                $('input#details_id').val('');
                $('input.peski-voucher-detail-id').prop('disabled', false);
            }
        });
    </script>

    {{--Get हिसाब Number By व्यहोरा--}}
    <script>
        let get_hisab_number_by_byahora = function (byahora) {

            // $('#hisab_number').val('');
            let ledger_type = byahora;

//बजेट खर्च र पेश्कि हुदा
            if (ledger_type == 1 || ledger_type == 4) {
                // alert(ledger_type);
                let main_activity_id = $('#main_activity_name').val();

                get_expense_head_by_activity_id(main_activity_id);
            }

            let url = '{{route('get_expense_head__by_ledger_type',123)}}';
            url = url.replace(123, ledger_type);

            $.ajax({
                url: url,
                method: 'get',
                success: function (res) {
                    // console.log($.parseJSON(res));
                    let option = '<option value="">.........</option>';
                    let hisab_number = $.parseJSON(res);
                    console.log(expenseHeadId);
                    if (hisab_number.length > 1) {

                        $.each($.parseJSON(res), function () {
                            // console.log(this.id == '186');
                            if (this.id == expenseHeadId)
                                option += '<option value="' + this.id + '" selected>' + this.expense_head_sirsak + '</option>'
                            else
                                option += '<option value="' + this.id + '">' + this.expense_head_sirsak + '</option>'

                        })
                    } else {
                        option += '<option value="' + hisab_number[0].id + '" selected>' + hisab_number[0].expense_head_sirsak + '</option>'


                    }

                    $('#hisab_number').html(option);
                    getDetailsByHisabNumber();

                }
            })
        }
    </script>

    {{--hisab number change huda--}}
    <script>
        let getDetailsByHisabNumber = function () {

            let bibran = $('#hisab_number option:selected').text();
            // console.log('bibara check', bibran);
            $('input#details').val(bibran);
            $('input#details').closest('div').find('p.validation-error').remove();
        }
        $(document).ready(function () {
            $('#hisab_number').change(function () {
                getDetailsByHisabNumber();
            })
        })
    </script>

    {{-- भौचर थप गर्दा हुने  --}}
    <script>
        let getSerialNumber = function () {
            return $('#voucher_table tbody').find('tr').length;
        };

        let getSerialNumberIf = function () {
            if (editingSN) {
                return editingSN;
            }
            return getSerialNumber();
        };
        let expense = 0;
        let activityForBhuktani = [];
        let voucherDetails = [];
        let crBankAmount = 0;
        let Dr = 0;
        let Cr = 0;
        let paymentAmount = 0;
        $('#voucher_submit').click(function (e) {
            if (validation()) {
                e.preventDefault();
                let crAmount = 0;
                let drAmount = 0;
                let date = $('input#date').val();
                let budget_sub_head_text = $('#program option:selected').text();
                let budget_sub_head_id = $('#program :selected').val();
                let activity_name = $('#main_activity_name option:selected').text();
                activity_name = activity_name.replace(/\d+/g, '').replace(/\|/g, '');
                let activity_id = $('#main_activity_name :selected').val();
                let dr_or_cr_name = $('select#DrOrCr option:selected').text();
                let drOrCr = $('select#DrOrCr').val();
                let byahora_text = $('#byahora option:selected').text();
                let ledger_type_id = $('#byahora').val(); //व्यहोराको field
                let hisab_number = $('select#hisab_number').val();
                let details = $('#details').val();
                let party_type_id = $('select#party_type').val();
                let party_type = $('#party_type option:selected').text();
                let party_name = $('#party-name option:selected').text();
                let party_id = $('select#party-name').val();
                let amount = $('input#amount').val();
                let nepali_date = $('#date').val();
                let nepali_date_in_english = convertNepaliToEnglish(nepali_date);
                let voucherDetailsId = $('#voucher_details_id').val();


                if ((parseInt(drOrCr) == 1 && ledger_type_id == 1) || (parseInt(drOrCr) == 1 && ledger_type_id == 4)) {
                    if (checkRemain()) {

                        //Voucher थप  गर्दा तल Voucher List को  डे र क्रे को Total आउने
                        if (drOrCr == '1') {

                            drAmount = $('#amount').val();
                            Dr = parseInt(Dr) + parseInt(drAmount);
                            $('#dr_amount').html(Dr);
                        }
                        if (drOrCr == '2') {

                            crAmount = $('#amount').val();
                            Cr = parseInt(Cr) + parseInt(crAmount);
                            $('#cr_amount').html(Cr);

                        }

                        //भुक्तानी आदेश मा रकम बाडफाडलाई  validation गर्ने रकम , यो रकम भन्दा बढि भुक्तानि गर्न रोक्ने गरि।
                        if (drOrCr == '2' && ledger_type_id == 3) {

                            paymentAmount = parseFloat(paymentAmount) + parseFloat(amount);
                        }

                        //भुक्तानि मा कार्यक्रम देखाउन लाइ
                        let obj = activityForBhuktani.find(o => o.activity_id === activity_id);
                        if (!obj) {
                            activityForBhuktani.push({
                                activity_name: activity_name,
                                activity_id: activity_id,
                            })
                        }

                        // console.log(activityForBhuktani[0]);
                        if (editingSN) {

                            editVoucherDetails = [];
                            editVoucherDetails['drOrCr'] = drOrCr;
                            editVoucherDetails['budget_sub_head'] = budget_sub_head_id;
                            editVoucherDetails['activity_id'] = activity_id;
                            editVoucherDetails['ledger_type_id'] = ledger_type_id;
                            editVoucherDetails['hisab_number'] = hisab_number;
                            editVoucherDetails['amount'] = amount;
                            editVoucherDetails['date_nepali'] = nepali_date;
                            editVoucherDetails['date_english'] = nepali_date_in_english;
                            editVoucherDetails['bibran'] = details;
                            editVoucherDetails['party_id'] = party_id;
                            // editVoucherDetails['peski_type'] = peski_type;

                            voucherDetails[editingSN - 1] = editVoucherDetails;
                            // console.log(voucherDetails);

                        } else {

                            voucherDetails.push({
                                drOrCr: drOrCr,
                                budget_sub_head: budget_sub_head_id,
                                activity_id: activity_id,
                                ledger_type_id: ledger_type_id,
                                hisab_number: hisab_number,
                                amount: amount,
                                date_nepali: nepali_date,
                                date_english: nepali_date_in_english,
                                bibran: details,
                                party_type: party_type_id,
                                party_id: party_id,
                                // peski_type : peski_type
                            });
                        }
                        let $editingRow = $('#voucher_table').find('tr.' + editingSN);
                        let tr = '';
                        if ($editingRow.hasClass('existing')) {
                            tr = "<tr class='" + getSerialNumberIf() + "  existing voucher-details' style='background-color: white;'>";
                        } else {
                            tr = "<tr class='" + getSerialNumberIf() + " existing voucher-details' style='background-color: white;'>";
                        }

                        tr += "<td class='sn'>" + getSerialNumberIf() +
                            "</td>" +
                            "<td class='dr-cr' data-dr-cr='" + drOrCr + "'>" +
                            dr_or_cr_name +
                            "</td>" +

                            "<td class='activity' data-id='" + activity_id + "' data-activity-name='" + activity_name + "'>" +
                            activity_name +
                            "</td>" +

                            "<td class='byahora' data-byahora='" + ledger_type_id + "'>" +
                            byahora_text +
                            "</td>" +

                            "<td class='hidden expense_head' data-expense-head_id='" + hisab_number + "'>" +
                            byahora_text +
                            "</td>" +

                            "<td class='details'>" +
                            details +
                            "</td>";

                        if ($editingRow.hasClass('existing')) {
                            let oldDebit = parseInt($.trim($editingRow.find('td.drAmount').text()));
                            if ($editingRow.find('td.drAmount').attr('data-old_value'))
                                oldDebit = $editingRow.find('td.drAmount').attr('data-old_value')
                            tr += "<td class='drAmount' data-old_value='" + oldDebit + "'>";

                        } else {
                            tr += "<td class='drAmount'>";
                        }
                        tr += drAmount +
                            "</td>" +

                            "<td class='crAmount'>" +
                            crAmount +
                            "</td>" +
                            "<td class='partyType' data-party-type='" + party_type_id + "'>" +
                            party_type +
                            "</td>" +
                            "<td class='party' data-party-id='" + party_id + "'>" +
                            party_name +
                            "</td>" +

                            "<td class='edit-voucher-td'>" +
                            '<a href="#" class="edit-voucher" data-sn="' + getSerialNumberIf() + '">Edit</a> | <a href="#" class="delete-row">Delete</a>' +
                            "</td>";
                        // $('#voucher_table').find('tbody').first('tr').append(tr);
                        if (editingSN) {
                            $(tr).css('background-color', '#fff');

                            $(tr).find('td.sn').text(editingSN);


                            $editingRow.replaceWith(tr);
                        } else {

                            $("#voucher_table tr:last").after(tr);
                        }
                        editingSN = 0;
                        dr_cr_total();

                        //एक पटक थप गरेपछि बजेट उपशि्रषक disabled हुने गरि
                        $('#program').prop("disabled", 'disabled');
                        // if($(tr).hasClass('existing')){
                        //     $(tr).removeClass('existing');
                        // }else{
                        //     $('select#main_activity_name').change();
                        // }
                        clearVoucherField();
                    }
                } else {

                    //Voucher थप  गर्दा तल Voucher List को  डे र क्रे को Total आउने
                    if (drOrCr == '1') {

                        drAmount = $('#amount').val();
                        Dr = parseInt(Dr) + parseInt($('#amount').val());
                        $('#dr_amount').html(Dr);
                    }
                    if (drOrCr == '2') {

                        crAmount = $('#amount').val();
                        Cr = parseInt(Cr) + parseInt($('#amount').val());
                        $('#cr_amount').html(Cr);

                    }

                    //भुक्तानी आदेश मा रकम बाडफाडलाई  validation गर्ने रकम , यो रकम भन्दा बढि भुक्तानि गर्न रोक्ने गरि।
                    if (drOrCr == '2' && ledger_type_id == 3) {

                        paymentAmount = parseFloat(paymentAmount) + parseFloat(amount);

                    }

                    //भुक्तानि मा कार्यक्रम देखाउन लाइ
                    let obj = activityForBhuktani.find(o => o.activity_id === activity_id);
                    if (!obj) {
                        activityForBhuktani.push({
                            activity_name: activity_name,
                            activity_id: activity_id,
                        })
                    }
                    // console.log(activityForBhuktani[0]);
                    let td_td = '';
                    if (editingSN) {

                        let $editingRow = $('#voucher_table').find('tr.' + editingSN);
                        td_td = $editingRow.find('input#details_id').val();
                        td_td = '<input type="hidden" name="details_id" id="details_id" value="' + td_td + '">';

                        editVoucherDetails = [];
                        editVoucherDetails['drOrCr'] = drOrCr;
                        editVoucherDetails['budget_sub_head'] = budget_sub_head_id;
                        editVoucherDetails['activity_id'] = activity_id;
                        editVoucherDetails['ledger_type_id'] = ledger_type_id;
                        editVoucherDetails['hisab_number'] = hisab_number;
                        editVoucherDetails['amount'] = amount;
                        editVoucherDetails['date_nepali'] = nepali_date;
                        editVoucherDetails['date_english'] = nepali_date_in_english;
                        editVoucherDetails['bibran'] = details;
                        editVoucherDetails['party_type'] = party_type_id;
                        editVoucherDetails['party_id'] = party_id;
                        editVoucherDetails['voucher_details_id'] = voucherDetailsId;

                        voucherDetails[editingSN - 1] = editVoucherDetails;
                        // console.log("this is editSn",voucherDetails);

                    } else {

                        voucherDetails.push({
                            drOrCr: drOrCr,
                            budget_sub_head: budget_sub_head_id,
                            activity_id: activity_id,
                            ledger_type_id: ledger_type_id,
                            hisab_number: hisab_number,
                            amount: amount,
                            date_nepali: nepali_date,
                            date_english: nepali_date_in_english,
                            bibran: details,
                            party_type: party_type_id,
                            party_id: party_id,
                            voucher_details_id: voucherDetailsId
                        });
                        // console.log(voucherDetails);
                    }

                    let tr = "<tr class='" + getSerialNumberIf() + " voucher-details' style='background-color: white;'>" +
                        "<td class='sn'>" + getSerialNumberIf() +
                        "</td>" +
                        "<td class='dr-cr' data-dr-cr='" + drOrCr + "'>" +
                        dr_or_cr_name +
                        "</td>" +

                        "<td class='activity' data-id='" + activity_id + "' data-activity-name='" + activity_name + "'>" +
                        activity_name +
                        "</td>" +

                        "<td class='byahora' data-byahora='" + ledger_type_id + "'>" +
                        byahora_text +
                        "</td>" +

                        "<td class='hidden expense_head' data-expense-head_id='" + hisab_number + "'>" +
                        byahora_text +
                        "</td>" +

                        "<td class='details'>" +
                        details +
                        "</td>" +

                        "<td class='drAmount'>" +
                        drAmount +
                        "</td>" +

                        "<td class='crAmount'>" +
                        crAmount +
                        "</td>" +
                        "<td class='partyType' data-party-type='" + party_type_id + "'>" +
                        party_type +
                        "</td>" +
                        "<td class='party' data-party-id='" + party_id + "'>" +
                        party_name +
                        "</td>" +

                        "<td class='edit-voucher-td'>" +
                        '<a href="#" class="edit-voucher" data-sn="' + getSerialNumberIf() + '">Edit</a> | <a href="#" class="delete-row">Delete</a>' +
                        "</td>";

                    // $('#voucher_table').find('tbody').first('tr').append(tr);
                    if (editingSN) {
                        $(tr).css('background-color', '#fff');
                        $(tr).find('td.sn').text(editingSN);
                        $('#voucher_table').find('tr.' + editingSN).replaceWith(tr);
                        $("#voucher_table tr:last").append('<td class="voucher_details hidden">' + td_td + '</td>');
                    } else {

                        $("#voucher_table tr:last").after(tr);

                    }
                    editingSN = 0;
                    dr_cr_total();

                    //एक पटक थप गरेपछि बजेट उपशि्रषक disabled हुने गरि
                    $('#program').prop("disabled", 'disabled');
                    clearVoucherField();
                }

            } else {
                // alert('validation failed');
            }
            getActivity();
            if ($('input.peski-voucher-detail-id').length > 0) {
                let baki = $.trim($('input.peski-voucher-detail-id:checked').closest('tr').find('td.baki').text());
                if (parseInt(baki) == 0) {
                    $('input.peski-voucher-detail-id:checked').closest('tr').remove();
                }
                $('input.peski-voucher-detail-id').prop('disabled', false);
            }
        })
    </script>

    {{--checkRemain funcion--}}

    <script>
        let checkRemain = () => {

            let $hiddenRemainingBudget = $('#hidden_remain_budget');
            let amount = $('input#amount').val();
            let mainActivityId = $('#main_activity_name').val();
            let $existingDebitTds = $('#voucher_table tbody td.activity[data-id="' + mainActivityId + '"]');
            let existingDebitTotal = 0;
            $.each($existingDebitTds, function () {
                if (!$(this).closest('tr').hasClass(editingSN)) {
                    existingDebitTotal += parseFloat($(this).closest('tr').find('td.drAmount').text())
                }
            });
            let remain = $hiddenRemainingBudget.val();
            let previuosExpense = parseFloat($('input#hidden_expense').val());
            if (parseFloat(remain) >= parseFloat(amount)) {

                remain = parseFloat(remain) - parseFloat(amount);
                expense = parseFloat(amount) + previuosExpense;
                $('#total_expense').text(expense.toFixed(2)).addClass('e-n-t-n-n');
                $('#hidden_expense').val(expense.toFixed(2));
                $('#hidden_remain_budget').val(remain.toFixed());
                $('#remain_budget').text(remain).addClass('e-n-t-n-n');


                return true;
            } else {

                alert('मौज्दात भन्दा बढि भयो। बाकी:' + remain);
                $('#amount').focus();
                $('#amount').val(remain.toFixed(2));
                return false;
            }
        }
    </script>


    {{--  भौचर update हुने  --}}
    <script>
        let voucherUpdate = function () {
            // if (get_bhuktani_amount()) {
            voucherDetails = [];
            preBhuktani = [];
            let voucher_id = '{{$voucher->id}}';
            let token = '{{csrf_token()}}';
            let nepali_date = $('#date').val();
            let nepali_date_in_english = convertNepaliToEnglish(nepali_date);
            let budget_sub_head_id = $('#program :selected').val();
            let voucher_number = $('#hidden_voucher_numbmer').val();
            let narration_short = $('#shortInfo').val();
            let narration_long = $('#detailsInfo').val();
            let tsa_amount = get_cr_tsa();
            let payment_amount = tsa_amount;

            let voucher = [];
            voucher = {
                id: voucher_id,
                voucher_number: voucher_number,
                budget_sub_head_id: budget_sub_head_id,
                payment_amount: payment_amount,
                narration_short: narration_short,
                narration_long: narration_long,
                date_nepali: nepali_date,
                date_english: nepali_date_in_english,
            };

            //Get all VOucher Detials
            let trs = $('#voucher_table tbody').find('tr').not(':first');
            $.each(trs, function () {
                let drOrCr = $(this).find('td.dr-cr').data('dr-cr');
                let budget_sub_head = budget_sub_head_id;
                let activity_id = $(this).find('td.activity').data('id');
                let ledger_type_id = $(this).find('td.byahora').data('byahora');
                let hisab_number = $(this).find('td.expense_head').attr('data-expense-head_id');
                let bibran = $(this).find('td.details').text();
                let dr_amount = $(this).find('td.drAmount').text();
                let cr_amount = $(this).find('td.crAmount').text();

                let voucher_details_id = $(this).find('td').find('input[name="details_id"]').val();

                let amount = 0;
                if (dr_amount > cr_amount) {
                    amount = dr_amount;
                } else {

                    amount = cr_amount;

                }
                let party_type = $(this).find('td.partyType').attr('data-party-type');
                let party_id = $(this).find('td.party').attr('data-party-id');
                // let peski_type = $(this).find('td.peski_type').attr('data-peski-type');
                let date_nepali = nepali_date;
                let date_english = nepali_date_in_english;
                voucherDetails.push({

                    voucher_id: voucher_id,
                    drOrCr: drOrCr,
                    budget_sub_head: budget_sub_head,
                    activity_id: activity_id,
                    ledger_type_id: ledger_type_id,
                    hisab_number: hisab_number,
                    amount: amount,
                    date_nepali: nepali_date,
                    date_english: nepali_date_in_english,
                    bibran: bibran,
                    party_type: party_type,
                    party_id: party_id,
                    voucher_details_id: voucher_details_id,
                });


            });

            // console.log(voucherDetails);
            // return false;

            //Get all Bhuktani for update
            let bhuktani_trs = $('#bhuktani tbody').find('tr').not(':first');
            $.each(bhuktani_trs, function () {

                let budget_sub_head_id = $('#program').val();
                let main_activity_id = $(this).find('td.bhuktani-main-activity').attr('data-bhuktani_activity');
                let bhuktani_party_type = $(this).find('td.bhuktani-party-type').attr('data-bhuktani_party_type');
                let bhuktani_party_id = $(this).find('td.bhuktani_party').attr('data-bhuktani_party');
                let bhuktani_amount = $(this).find('td.bhuktani_amount').text();
                let advance_tax_deduction = $(this).find('td.advance_tax_deduction').text();
                let vat_amount = $(this).find('td.vat_amount').text();
                let vat_bill_number = $(this).find('td.vat_bill_number').text();
                let pratibaddhata_number = $(this).find('td.pratibaddhata_number').text();
                let cheque_type = $(this).find('td.cheque_type').attr('data-cheque_type');
                preBhuktani.push({
                    voucher_id: voucher_id,
                    budget_sub_head_id: budget_sub_head_id,
                    main_activity_id: main_activity_id,
                    bhuktani_party_type: bhuktani_party_type,
                    bhuktani_party_id: bhuktani_party_id,
                    bhuktani_amount: bhuktani_amount,
                    date_nepali: nepali_date,
                    date_english: nepali_date_in_english,
                    advance_tax_deduction: advance_tax_deduction,
                    vat_amount: vat_amount,
                    vat_bill_number: vat_bill_number,
                    pratibaddhata_number: pratibaddhata_number,
                    cheque_type: cheque_type
                })
            });

            // console.log(voucherDetails);
            // return false;

            $.ajax({
                method: 'post',
                url: '{{route('voucher.update')}}',
                data: {
                    voucher_id: voucher_id,
                    voucher: voucher,
                    voucherDetails: voucherDetails,
                    preBhuktani: preBhuktani,
                    _token: token,
                },
                success: function (resp) {
                    // console.log(resp);
                    if (resp == '"success"') {
                        swal({
                            title: "Good job!",
                            text: "You clicked the button!",
                            icon: "success",
                        });
                        location.reload();
                    } else {
                        alert("failed");

                    }
                }
            })
            // } else {
            //
            //     alert("भुक्तानि रकम र एकल कोष खाता रकम मिलेन");
            // }
        }

    </script>

    <script>
        $(document).on('click', '#VoucherUpdate', function () {
            if (voucherValidationAndSave()) {
                voucherUpdate();

            }

        });
    </script>
    {{--voucher details ma peski prakar save huna lai--}}
    <script>
        // let peski_type=0;
        // let get_peski_type = function(){
        //     let party_type = $('select#party_type').val();
        //
        //     if(party_type == 5){
        //
        //         return peski_type = 1;
        //     }
        //     else if(party_type == ''){
        //
        //         return 0;
        //     } else {
        //         return  peski_type = 2;
        //
        //     }
        // };
        // $(document).ready(function () {
        //     $('#party_type').change(function () {
        //         get_peski_type();
        //     })
        // })
    </script>

    {{-- भौचर एडिट गर्न--}}
    <script>
        let editingSN;
        let expenseHeadId = 0;
        $(document).on('click', '.edit-voucher', function () {

            editingSN = parseInt($(this).attr('data-sn'));
            //click गरेको row को tds तानेको।
            let tds = $(this).parents('tr').find('td');
            $(this).parents('tbody').find('tr').css('background-color', '#FFF');
            $(this).parents('tr').css('background-color', '#FFF1CC');
            let item = [];
            let program_id = $('#program').val();
            $.each(tds, function () {

                let className = $(this).prop('class');
                if (className == 'dr-cr') {

                    item['dr-cr'] = $(this).attr('data-dr-cr')

                }

                if (className == 'activity') {
                    item['activity'] = $(this).html();

                    item['activity_id'] = $(this).attr('data-id');
                    item['byahora_id'] = $('.byahora').attr('data-byahora');

                    // बजेट खर्च वा पेश्कि छ भने
                    if (item['byahora_id'] == 1 || item['byahora_id'] == 4 || item['byahora_id'] == 9) {

                        get_expense_head_by_activity_id(item['activity_id']);
                    }
                }

                if (className == 'byahora') {

                    item['byahora'] = $(this).html();
                    item['byahora_id'] = $(this).attr('data-byahora');

                    // व्यहोरामा दायित्व र एकल कोष खाता छ भने।
                    if (item['byahora_id'] == 2 || item['byahora_id'] == 3) {
                        get_hisab_number_by_byahora(item['byahora_id']);

                    }
                }
                if ($(this).hasClass('expense_head')) {

                    expenseHeadId = item['expense_head_id'] = $(this).attr('data-expense-head_id');


                }

                if (className == 'details') {
                    item['details'] = $(this).html();
                }

                if (className == 'crAmount' || className == 'drAmount') {
                    let test = parseInt($(this).html());
                    if (test > 0) {
                        item['amount'] = $(this).html();
                    }
                }

                if (className == 'partyType') {

                    item['partyTypeId'] = $(this).attr('data-party-type');
                    item['partyType'] = $(this).html();

                    $('#party_type').prop("disabled", false);
                }
                if (className == 'party') {


                    item['partyId'] = $(this).attr('data-party-id');
                    item['party'] = $(this).html();

                }

            });


            $('select#DrOrCr').val(item['dr-cr']);
            // console.log(item)
            // $.each(drOrCrOption, function () {
            //     if ($(this).val() == item['dr-cr']) {
            //         $(this).prop('selected', true);
            //     }
            // });


            $budget_sub_head = $('#program :selected').val();
            // $('#main_activity_name').val(item['activity']);

            let activity_option = $('#main_activity_name').find('option');
            $.each(activity_option, function () {

                if ($(this).val() == item['activity_id']) {
                    $(this).prop('selected', true).parent('select').change();
                }
            });
            //व्यहोराको option लेको।
            let byahoraOption = $('#byahora').find('option');
            $.each(byahoraOption, function () {

                if ($(this).val() == item['byahora_id']) {

                    $(this).prop('selected', true).change();
                    let byahora = $('#byahora :selected').val();
                }

            });

            //कार्यक्रमको option लेको।
            let activityOption = $('#main_activity_name').find('option');
            $.each(activityOption, function () {

                if (this.text == item['activity']) {
                    $(this).prop('selected', true);
                }
            });


            let partyTypeOption = $('#party_type').find('option');
            $.each(partyTypeOption, function () {

                if ($(this).val() == item['partyTypeId']) {

                    $(this).prop('selected', true);
                    // alert(item['partyId']);
                    getPartyByPartyType($(this).val(), item['partyId']);

                }
            });

            // let partyOption = $('#party-name').find('option');
            // $.each(partyOption, function () {
            //
            //     if ($(this).val() == item['partyId']) {
            //
            //         $(this).prop('selected', true);
            //         alert(item['partyId']);
            //         // getPartyByPartyType($(this).val(), item['partyId']);
            //
            //     }
            // });
            $('#details').val(item['details']).change();
            $('#amount').val(item['amount']);
            // $(e.target).parents('tr').remove();

            let totalExpense = parseFloat($('#hidden_expense').val()) - parseFloat(item['amount']);
            let remainBudget = parseFloat($('#hidden_remain_budget').val()) + parseFloat(item['amount']);

            dr_cr_total();
            dr_cr_equal_check();
            return false;

        });
    </script>

    {{--थप गरेको भौचर delete  गर्ने--}}
    <script>
        let resetSn = function (tabelId) {

            let $trs = $('#' + tabelId + ' tbody').find('tr');
            $.each($trs, function (key, value) {
                // console.log(key, value);
                $(value).removeClass();
                $(value).find('td.sn').html(key);
                $(value).find('td.edit-voucher-td').find('a').first().attr('data-sn', key);
                $(value).attr('class', key);
            });
            $('select#main_activity_name').change();
        };


        $(document).on('click', '.delete-row', function () {

            if ($(this).closest('tr').hasClass('existing')) {

                let voucher_details_id = $(this).closest('tr').find('.voucher_details_id').val();
                // alert(voucher_details_id);
                let url = '{{route('delete.voucher.details',123)}}';
                url = url.replace(123, voucher_details_id);
                // console.log(url);
                swal({
                    title: "Are you sure?",
                    text: "Once deleted, you will not be able to recover!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {

                        $.ajax({

                            url: url,
                            method: 'get',
                            success: function (res) {
                                // console.log($.parseJSON(res));
                                if (res) {
                                    swal("Poof! Your imaginary file has been deleted!", {
                                        icon: "success",
                                    });
                                    location.reload();

                                }
                            }
                        })

                    } else {
                        swal("Your imaginary file is safe!");
                    }
                });

            } else {
                let comfirm_result = confirm("are you sure. It will delete the item");
                if (comfirm_result == true) {
                    $(this).closest('tr').remove();
                    resetSn('voucher_table');
                    dr_cr_total();
                    // get_budget_by_main_Activity()
                    return false;
                }
            }


        })

    </script>

    {{-- क्रे र एकल कोष खाता amount total गर्ने--}}
    <script>
        let get_cr_tsa = function () {
            let cr_tsa = 0;

            let trs = $('#voucher_table').find('tr');
            $.each(trs, function (key, tr) {

                if ($(tr).find('td.dr-cr').attr('data-dr-cr') == 2 && $(tr).find('td.byahora').attr('data-byahora') == 3 || $(tr).find('td.dr-cr').attr('data-dr-cr') == 1 && $(tr).find('td.byahora').attr('data-byahora') == 3) {
                    cr_tsa += parseFloat($(tr).find('td.crAmount').text())
                }

                if ($(tr).find('td.dr-cr').attr('data-dr-cr') == 1 && $(tr).find('td.byahora').attr('data-byahora') == 3) {
                    cr_tsa += parseFloat($(tr).find('td.drAmount').text())
                }

            });

            return cr_tsa;

        }
    </script>

    <script>
        $(document).ready(function () {
            get_bhuktani_amount();

        });


        let get_bhuktani_amount = () => {

            let bhuktani_amount = 0;
            let total_tsa = get_cr_tsa();
            let trs = $('#bhuktani tbody').find('tr').not(':first');
            if (total_tsa && trs.length > 0) {

                $.each(trs, function (key, tr) {
                    let bhuktani_amount_temp = parseFloat($(tr).find('td.bhuktani_amount').text());
                    if (!isNaN(bhuktani_amount_temp)) {
                        bhuktani_amount += parseFloat($(tr).find('td.bhuktani_amount').text());
                    }

                });
                return total_tsa === bhuktani_amount;
            }
            return true;
        }
    </script>

    {{--dr_cr_equal_check finction--}}
    <script>
        let dr_cr_equal_check = function () {

            $('#VoucherUpdate').prop('disabled', false);

            let option = '<option value="" selected>...................</option>';
            $.each(activityForBhuktani, function () {
                option += '<option value="' + this.activity_id + '">' + this.activity_name + '</option>'
                // $('#bhuktani_main_activity').html(option);
            })

        }
    </script>

    {{-- Dr and Cr total देखाउने function --}}
    <script>
        let dr_cr_total = function () {

            let trs = $('#voucher_table').find('tr');
            let DrAmount = 0;
            let CrAmount = 0;
            let drCrRemaining = 0;
            $.each(trs, function (key, tr) {
                if (!$(this).hasClass(editingSN)) {
                    let newDr = parseFloat($(this).find('td.drAmount').text());
                    if (newDr > 0) {
                        DrAmount += newDr;
                    }

                    let newCr = parseFloat($(this).find('td.crAmount').text());
                    if (newCr > 0) {
                        CrAmount += newCr;
                    }


                }

                // Total nikalne loop, kei error aako xaina aile samma vane delete handim, mathi ko le kaam garxa
                // $.each($(tr).find('td'), function (key_, td) {
                //
                //     if ($(td).prop('class') == 'drAmount') {
                //         let newDr = $(this).text();
                //         if (newDr) {
                //             DrAmount += parseFloat(newDr);
                //             $('#dr_amount').html(DrAmount);
                //
                //         }
                //     }
                //     if ($(td).prop('class') == 'crAmount') {
                //         let newCr = $(this).text();
                //         if (newCr) {
                //
                //             CrAmount += parseFloat(newCr);
                //             $('#dr_amount').html(CrAmount);
                //
                //         }
                //     }
                //
                // })
                drCrRemaining = parseFloat(DrAmount) - parseFloat(CrAmount);
                // console.log("dr_cr_check",CrAmount,DrAmount);
                if (CrAmount.toFixed(2) === DrAmount.toFixed(2) && CrAmount.toFixed(2) > 0) {

                    $('#VoucherUpdate').prop('disabled', false);
                    getActivity();
                    // let option = '<option  value="" selected>...................</option>';
                    // $.each(activityForBhuktani, function () {
                    //     option += '<option value="' + this.activity_id + '">' + this.activity_name + '</option>'
                    //     $('#bhuktani_main_activity').html(option);
                    // })


                } else {

                    // $('#VoucherUpdate').prop('disabled', "disabled");
                }
            });
            $('#dr_amount').text(DrAmount.toFixed(2));
            $('#cr_amount').text(CrAmount.toFixed(2));
            $('#amount_difference').text(drCrRemaining.toFixed(2));
        }

    </script>

    {{--short narration key up --}}
    <script>
        $(document).ready(function () {
            $('#shortInfo').keyup(function () {
                let shortNarration = $('#shortInfo').val();
                $('#detailsInfo').val(shortNarration);
            })
        })
    </script>

    {{--VOucher End--}}


    {{--Bhuktani start--}}

    {{--on change party_type evvent --}}
    <script>
        $(document).ready(function () {
            $('#bhuktani_party_type').change(function () {

                let party_type_id = $('#bhuktani_party_type :selected').val();
                // if(party_type_id == 5){
                //     // alert("test");
                //     getKarmachariForBhuktani();
                // } else {
                getBhuktaniPartyByPartyType(party_type_id);
                // }
            })
        });

        let getKarmachariForBhuktani = function () {

            let url = '{{route('get.karmachari')}}';
            $.ajax({
                url: url,
                method: 'get',
                success: function (res) {
                    // console.log($.parseJSON(res));
                    let option = '<option value="" selected>....................</option>';
                    $.each($.parseJSON(res), function () {

                        option += '<option value="' + this.id + '">' + this.name_nepali + '</option>'

                    });
                    $('#bhuktani_party').html(option);
                }
            });
        }

    </script>

    {{--on change party_type function --}}
    <script>
        let getBhuktaniPartyByPartyType = function (party_type_id, party_name = '') {

            let url = '{{route('get_party_name_by_party_type_and_office', 123)}}';
            url = url.replace(123, party_type_id);
            $.ajax({
                url: url,
                method: 'get',
                success: function (res) {
                    // console.log($.parseJSON(res));
                    let option = '<option value="" selected>....................</option>';
                    option += '<option value="add" style="font-style: italic; color: blue; text-decoration: underline">Add New</option>';
                    $.each($.parseJSON(res), function () {
                        // console.log(party_name,this.name_nep);
                        if ($.trim(party_name) == $.trim(this.name_nep)) {
                            option += '<option value="' + this.id + '" selected>' + this.name_nep + '</option>'

                        } else {
                            option += '<option value="' + this.id + '">' + this.name_nep + '</option>'

                        }

                    });
                    $('#bhuktani_party').html(option);
                }
            })
        }
    </script>

    {{-- भुक्तानी थप हुदा --}}
    <script>

        let getBhuktaniSn = function () {
            return $('#bhuktani tbody').find('tr').length;
        };
        let getBhktaniSerialNumberIf = function () {
            if (editingBhuktaniSN) {
                return editingBhuktaniSN;
            }
            return getBhuktaniSn();
        };
        let preBhuktani = [];
        $remaining = 0;
        $(document).on('click', '#buktaniButton', function () {
            if (bhuktanivalidation()) {

                let nepali_date = $('#date').val();
                let nepali_date_in_english = convertNepaliToEnglish(nepali_date);
                let main_activity_name = $('#bhuktani_main_activity :selected').text();
                let main_activity_id = $('#bhuktani_main_activity').val();
                let party_type_id = $('#bhuktani_party_type').val();
                let party_type_text = $('#bhuktani_party_type :selected').text();
                let bhuktani_party_text = $('#bhuktani_party :selected').text();
                let bhuktani_party_id = $('#bhuktani_party :selected').val();
                let bhuktani_amount = $('#bhuktani_amount').val();
                let advance_tax_deduction_amount = $('#advance_tax_deduction_amount').val();
                let vat_amount = $('#vat_amount').val();
                let vat_bill_number = $('input#vat_bill_number').val();
                let pratibadhata_number = $('input#pratibadhata_number').val();
                let budget_sub_head_id = $('#program :selected').val();
                let chequeTypeName = $('#chequeType :selected').text();
                let chequeTypeId = $('select#chequeType').val();

                // if (bhuktani_total_payment_check()) {
                var tr = "<tr class='" + getBhktaniSerialNumberIf() + "' style='background-color: white;'>" +
                    "<td class='sn'>" + getBhktaniSerialNumberIf() +

                    "</td>" +

                    "<td class='bhuktani-main-activity' data-bhuktani_activity='" + main_activity_id + "'>" +
                    main_activity_name +
                    "</td>" +

                    "<td class='bhuktani-party-type' data-bhuktani_party_type='" + party_type_id + "'>" +
                    party_type_text +
                    "</td>" +

                    "<td class='bhuktani_party' data-bhuktani_party='" + bhuktani_party_id + "'>" +
                    bhuktani_party_text +
                    "</td>" +

                    "<td class='bhuktani_amount' >" +
                    bhuktani_amount +
                    "</td>" +

                    "<td class='advance_tax_deduction' >" +
                    advance_tax_deduction_amount +
                    "</td>" +
                    "<td class='vat_amount'>" +
                    vat_amount +
                    "</td>" +

                    "<td class='vat_bill_number'>" +
                    vat_bill_number +
                    "</td>" +

                    "<td class='pratibaddhata_number'>" +
                    pratibadhata_number +
                    "</td>" +

                    "<td class='cheque_type' data-cheque_type='" + chequeTypeId + "'>" +
                    chequeTypeName +
                    "</td>" +

                    "<td class='edit-voucher-td'>" +
                    '<a href="#" class="edit-bhuktani-row" data-sn="' + getBhktaniSerialNumberIf() + '">Edit</a> | <a href="#" class="delete-bhuktani">Delete</a>' +
                    "</td>";

                if (editingBhuktaniSN) {
                    $(tr).css('background-color', '#fff');
                    $(tr).find('td.sn').text(editingBhuktaniSN);
                    $('#bhuktani').find('tr.' + editingBhuktaniSN).replaceWith(tr);

                    preBhuktaniEdit = [];
                    preBhuktaniEdit['budget_sub_head_id'] = budget_sub_head_id;
                    preBhuktaniEdit['main_activity_id'] = main_activity_id;
                    preBhuktaniEdit['bhuktani_party_type'] = party_type_id;
                    preBhuktaniEdit['bhuktani_party_id'] = bhuktani_party_id;
                    preBhuktaniEdit['bhuktani_amount'] = bhuktani_amount;
                    preBhuktaniEdit['date_nepali'] = nepali_date;
                    preBhuktaniEdit['date_english'] = nepali_date_in_english;
                    preBhuktaniEdit['advance_tax_deduction'] = advance_tax_deduction_amount;
                    preBhuktaniEdit['vat_amount'] = vat_amount;
                    preBhuktaniEdit['vat_bill_number'] = vat_bill_number;
                    preBhuktaniEdit['pratibadhata_number'] = pratibadhata_number;
                    preBhuktaniEdit['chequeTypeId'] = chequeTypeId;
                    preBhuktani[editingBhuktaniSN - 1] = preBhuktaniEdit;

                } else {

                    preBhuktani.push({
                        budget_sub_head_id: budget_sub_head_id,
                        main_activity_id: main_activity_id,
                        bhuktani_party_type: party_type_id,
                        bhuktani_party_id: bhuktani_party_id,
                        bhuktani_amount: bhuktani_amount,
                        date_nepali: nepali_date,
                        date_english: nepali_date_in_english,
                        advance_tax_deduction: advance_tax_deduction_amount,
                        vat_amount: vat_amount,
                        vat_bill_number: vat_bill_number,
                        pratibadhata_number: pratibadhata_number,
                        chequeTypeId: chequeTypeId
                    });

                    $('#bhuktani').find('tbody').last('tr').append(tr);
                }
                editingBhuktaniSN = 0;
                bhuktani_total();
                $('#bhuktani_amount').val(" ")
                $('#advance_tax_deduction_amount').val(" ")
                $('#vat_amount').val("")

                clearBhuktaniField();

                // } else {
                //
                //     alert("भुक्तानि रकम भन्दा बढी भयो");
                //     $('#bhuktani_amount').val("");
                //     $('#bhuktani_amount').focus();
                //
                // }

            }


        })

    </script>

    {{--get_bhuktani_peski_type Function--}}
    <script>
        // let bhuktani_peski_type=0;
        // let get_bhuktani_peski_type = function(){
        //     let bhuktani_party_type = $('select#bhuktani_party_type').val();
        //
        //     if(bhuktani_party_type == 5){
        //
        //         return bhuktani_peski_type = 1;
        //     }
        //     else if(bhuktani_party_type == ''){
        //
        //         return bhuktani_peski_type = 0;
        //
        //     } else {
        //         return  bhuktani_peski_type = 2;
        //
        //     }
        // };
        // $(document).ready(function () {
        //     $('#bhuktani_party_type').change(function () {
        //
        //         get_bhuktani_peski_type();
        //     })
        // })
    </script>

    {{-- थप गरेको भुक्तानि एडिट गर्न --}}
    <script>
        let editingBhuktaniSN;
        $(document).on('click', '.edit-bhuktani-row', function () {
            editingBhuktaniSN = parseInt($(this).attr('data-sn'));
            // console.log(editingBhuktaniSN);
            $(this).parents('tr').css('background-color', '#FFF1CC');
            let bhuktaniTds = $(this).parents('tr').find('td');
            let item = [];
            $.each(bhuktaniTds, function () {

                let className = $(this).prop('class');
                if (className == 'bhuktani-main-activity') {

                    item['bhuktani_main_activity'] = $(this).html();
                    item['data_bhuktani_activity'] = $(this).attr('data-bhuktani_activity');

                }
                if (className == 'bhuktani-party-type') {

                    item['bhuktani_party_type'] = $(this).html();
                    item['data_bhuktani_party_type'] = $(this).attr('data-bhuktani_party_type');

                }
                if (className == 'bhuktani_party') {

                    item['bhuktani_party'] = $(this).html();
                    item['data_bhuktani_party'] = $(this).attr('data-bhuktani_party');
                }
                if (className == 'bhuktani_amount') {

                    item['bhuktani_amount'] = $(this).html();
                }
                if (className == 'advance_tax_deduction') {

                    item['advance_tax_deduction'] = $(this).html();
                }
                if (className == 'vat_amount') {

                    item['vat_amount'] = $(this).html();
                }
                if (className == 'vat_bill_number') {

                    item['vat_bill_number'] = $(this).html();
                }

                if (className == 'vat_number') {

                    item['vat_bill_number'] = $(this).html();

                }
                if (className == 'pratibaddhata_number') {
                    item['pratibaddhata_number'] = $(this).html();
                }
                if (className == 'cheque_type') {

                    item['cheque_type'] = $(this).attr('data-cheque_type');

                }
            });

            // console.log(item);
            let activityOption = $('#bhuktani_main_activity').find('option');
            $.each(activityOption, function () {
                // console.log($(this).val(), item['data_bhuktani_activity'])
                if ($(this).val() == item['data_bhuktani_activity']) {
                    $(this).prop('selected', true);
                }
            });

            let partyTypeOption = $('#bhuktani_party_type').find('option');
            $.each(partyTypeOption, function () {
                if ($(this).val() == item['data_bhuktani_party_type']) {

                    $(this).prop('selected', true);
                    getBhuktaniPartyByPartyType($(this).val(), item['bhuktani_party']);

                }
            });
            let partyOption = $('#bhuktani_party').find('option');
            $.each(partyOption, function () {

                if ($(this).val() == item['data_bhuktani_party']) {

                    $(this).prop('selected', true);

                }
            });

            let chequeTypeOptions = $('#chequeType').find('option');
            $.each(chequeTypeOptions, function () {
                if ($(this).val() == item['cheque_type']) {

                    $(this).prop('selected', true);
                }
            })

            $('#bhuktani_amount').val(item['bhuktani_amount']);
            $('#advance_tax_deduction_amount').val(item['advance_tax_deduction']);
            $('#vat_amount').val(item['vat_amount']);
            $('#vat_bill_number').val(item['vat_bill_number']);
            $('#pratibadhata_number').val(item['pratibaddhata_number']);

            return false;

        })
    </script>


    {{--थप गरेको भुक्तानि Delete गर्न--}}
    <script>
        $(document).on('click', '.delete-bhuktani', function () {
            $(this).closest('tr').remove();
            resetSn('bhuktani');
            bhuktani_total();
            return false;

        })
    </script>


    {{--Bhuktani amount total calculation--}}
    <script>
        let bhuktani_total = function () {

            let trs = $('#bhuktani').find('tr');
            let bhuktaniAmount = 0;
            $.each(trs, function (key, tr) {
                $.each($(tr).find('td'), function (key_, td) {

                    if ($(td).prop('class') == 'bhuktani_amount') {

                        let newAmount = $(this).text();
                        if (newAmount) {
                            bhuktaniAmount += parseFloat(newAmount);

                        }
                    }
                })
            });
            $('#bhuktaniTotal').text(bhuktaniAmount);
        }
    </script>


    {{--Function for take total in bhuktani table and compare with cr. bank value--}}
    <script>
        let bhuktani_total_payment_check = function () {

            let current_payment = $('#bhuktani_amount').val();
            let bhuktani_main_activity = $('#bhuktani_main_activity').val();

            let $bhuktaniTds = $('table#bhuktani tbody td.bhuktani-main-activity[data-bhuktani_activity="' + bhuktani_main_activity + '"]');
            let trs = $('#bhuktani').find('tr');
            let bhuktaniAmount = 0;

            $.each($bhuktaniTds, function () {
                let $row = $(this).closest('tr');
                let bhuktaniAmt = parseFloat($row.find('td.bhuktani_amount').text());
                if (bhuktaniAmt > 0) {
                    bhuktaniAmount += bhuktaniAmt;
                }
            });
            // $.each(trs, function (key, tr) {
            //     $.each($(tr).find('td'), function (key_, td) {
            //
            //         if ($(td).prop('class') == 'bhuktani_amount') {
            //
            //             let newAmount = $(this).text();
            //             if (newAmount) {
            //                 bhuktaniAmount += parseFloat(newAmount);
            //             }
            //         }
            //     })
            // });

            let totalBhuktani;
            let previous_value = 0;
            if (editingBhuktaniSN) {
                previous_value = $('table#bhuktani').find('tr' + '.' + editingBhuktaniSN).find('td.bhuktani_amount').text();
                totalBhuktani = parseFloat(bhuktaniAmount) + parseFloat(current_payment) - previous_value;
            } else {
                totalBhuktani = parseFloat(bhuktaniAmount) + parseFloat(current_payment);
            }
            let tsa_amount = get_cr_tsa();
            // console.log('TSA=====================', tsa_amount, totalBhuktani);
            return totalBhuktani <= parseFloat(tsa_amount);
        }
    </script>

    {{--Bhuktani end--}}



    {{-- भौचर Validation हुने --}}
    <script>
        function voucherValidationAndSave() {
            let flag = 1;

            let shortInfo = $('#shortInfo');
            if (!shortInfo.val()) {
                if (shortInfo.siblings('p').length == 0) {
                    shortInfo.after('<p class="validation-error">संक्षिप्त व्यहोरा लेख्नुहोस्!</p>')
                }
                shortInfo.focus();
                flag = 0;
            } else {
                shortInfo.siblings('p').remove()
            }

            let detailsInfo = $('#detailsInfo');
            if (!detailsInfo.val()) {
                if (detailsInfo.siblings('p').length == 0) {
                    detailsInfo.after('<p class="validation-error">विस्तृित व्यहोरा लेख्नुहोस्!</p>')
                }
                detailsInfo.focus();
                flag = 0;
            } else {
                detailsInfo.siblings('p').remove()
            }
            return flag;
        }
    </script>



    {{-- Fixed --}}

    {{-- Validation   --}}
    <script>
        $('.form-control').change(function () {
            let val = $(this).val();
            // console.log(val);
            if (val) {
                $(this).closest('div').find('p.validation-error').remove();
            }
        });

        function validation() {
            let flag = 1;
            // program name validation
            let programId = $('#program');
            if (!programId.val()) {
                if (programId.siblings('p').length == 0) {
                    programId.after('<p class="validation-error">छान्नुहोस!</p>')
                    programId.addClass("error");
                }
                programId.focus();
                flag = 0;
            } else {
                programId.siblings('p').remove()
            }
            // program name validation end

            //main_activity_name validation
            let mainactivitynameId = $('#main_activity_name');
            let DrOrCr = $('#DrOrCr').val();
            let ledgerType = $('#byahora').val();
            if (DrOrCr == 2 && ledgerType == 3 || DrOrCr == 1 && ledgerType == 3 || DrOrCr == 2 && ledgerType == 8 || DrOrCr == 1 && ledgerType == 2 || DrOrCr == 2 && ledgerType == 2 || DrOrCr == 1 && ledgerType == 7) {
                mainactivitynameId.siblings('p').remove()
            } else {
                if (!mainactivitynameId.val()) {
                    if (mainactivitynameId.siblings('p').length == 0) {
                        mainactivitynameId.after('<p class="validation-error">छान्नुहोस!</p>')
                    }
                    mainactivitynameId.focus();
                    flag = 0;
                } else {
                    mainactivitynameId.siblings('p').remove()
                }
            }

            let partyTypeId = $('#party_type');
            if (ledgerType == 4) {

                // alert(partyTypeId.val());
                if (!partyTypeId.val()) {
                    if (partyTypeId.siblings('p').length == 0) {

                        partyTypeId.after('<p class="validation-error">छान्नुहोस!</p>')
                    }
                    partyTypeId.focus();
                    flag = 0;
                } else {
                    partyTypeId.siblings('p').remove()

                }
            }

            let partyName = $('#party_type');
            if (ledgerType == 4) {

                // alert(partyName.val());
                if (!partyName.val()) {
                    if (partyName.siblings('p').length == 0) {

                        partyName.after('<p class="validation-error">छान्नुहोस!</p>')
                    }
                    partyName.focus();
                    flag = 0;
                } else {
                    partyName.siblings('p').remove()

                }
            }


            //main_activity_name validation end
            //hisab number start
            let hisab_number = $('#hisab_number');
            if (!hisab_number.val()) {
                if (hisab_number.siblings('p').length == 0) {
                    hisab_number.after('<p class="validation-error">छान्नुहोस!</p>')
                }
                hisab_number.focus();
                flag = 0;
            } else {
                hisab_number.siblings('p').remove()
            }
            //details validation

            let detailsId = $('#details');
            if (!detailsId.val()) {
                if (detailsId.siblings('p').length == 0) {
                    detailsId.after('<p class="validation-error">लेख्नुहोस​!</p>')
                }
                detailsId.focus();
                flag = 0;
            } else {
                detailsId.siblings('p').remove()
            }

            //details validation end


            //amount validation

            let amountsId = $('#amount');

            if ((!amountsId.val() || parseInt(amountsId.val()) < 1) && amountsId.length > 0) {
                if (amountsId.siblings('p').length == 0) {
                    amountsId.after('<p class="validation-error">लेख्नुहोस​!</p>')
                }
                amountsId.focus();
                flag = 0;
            } else {
                amountsId.siblings('p').remove()
            }
            //amount validation end
            return flag;


        }

        function bhuktanivalidation() {
            let flag = 1;

            let mainActivityId = $('#bhuktani_main_activity');
            if (!mainActivityId.val()) {
                if (mainActivityId.siblings('p').length == 0) {
                    mainActivityId.after('<p class="validation-error">छान्नुहोस!</p>')
                }
                mainActivityId.focus();
                flag = 0;
            } else {
                mainActivityId.siblings('p').remove()
            }

            let partyTypeId = $('#bhuktani_party_type');
            if (!partyTypeId.val()) {
                if (partyTypeId.siblings('p').length == 0) {
                    partyTypeId.after('<p class="validation-error">छान्नुहोस!</p>')
                }
                partyTypeId.focus();
                flag = 0;
            } else {
                partyTypeId.siblings('p').remove()
            }

            let partyId = $('#bhuktani_party');
            if (!partyId.val()) {
                if (partyId.siblings('p').length == 0) {
                    partyId.after('<p class="validation-error">छान्नुहोस!</p>')
                }
                partyId.focus();
                flag = 0;
            } else {
                partyId.siblings('p').remove()
            }

            let bhuktaniAmount = $('#bhuktani_amount');
            if (!bhuktaniAmount.val()) {
                if (bhuktaniAmount.siblings('p').length == 0) {
                    bhuktaniAmount.after('<p class="validation-error">छान्नुहोस!</p>')
                }
                bhuktaniAmount.focus();
                flag = 0;
            } else {
                bhuktaniAmount.siblings('p').remove()
            }

            // let advanceTaxDeduction = $('#advance_tax_deduction_amount');
            // if (!advanceTaxDeduction.val()) {
            //     if (advanceTaxDeduction.siblings('p').length == 0) {
            //         advanceTaxDeduction.after('<p class="validation-error">छान्नुहोस!</p>')
            //     }
            //     advanceTaxDeduction.focus();
            //     flag = 0;
            // } else {
            //     advanceTaxDeduction.siblings('p').remove()
            // }
            //
            // let vatAmount = $('#vat_amount');
            // if (!vatAmount.val()) {
            //     if (vatAmount.siblings('p').length == 0) {
            //         vatAmount.after('<p class="validation-error">छान्नुहोस!</p>')
            //     }
            //     vatAmount.focus();
            //     flag = 0;
            // } else {
            //     vatAmount.siblings('p').remove()
            // }

            // let bankId = $('#bank');
            // if (!bankId.val()) {
            //     if (bankId.siblings('p').length == 0) {
            //         bankId.after('<p class="validation-error">छान्नुहोस!</p>')
            //     }
            //     bankId.focus();
            //     flag = 0;
            // } else {
            //     bankId.siblings('p').remove()
            // }

            // let vat_bill_number_id = $('#vat_bill_number');
            // if (!vat_bill_number_id.val()) {
            //     if (vat_bill_number_id.siblings('p').length == 0) {
            //         vat_bill_number_id.after('<p class="validation-error">छान्नुहोस!</p>')
            //     }
            //     vat_bill_number_id.focus();
            //     flag = 0;
            // } else {
            //     vat_bill_number_id.siblings('p').remove()
            // }
            // let pratibadhata_number_id = $('#pratibadhata_number');
            // if (!pratibadhata_number_id.val()) {
            //     if (pratibadhata_number_id.siblings('p').length == 0) {
            //         pratibadhata_number_id.after('<p class="validation-error">छान्नुहोस!</p>')
            //     }
            //     pratibadhata_number_id.focus();
            //     flag = 0;
            // } else {
            //     pratibadhata_number_id.siblings('p').remove()
            // }

            return flag;

        }


    </script>

    {{--Date Picker --}}
    <script>
        $("#date").nepaliDatePicker({
            dateFormat: "%y-%m-%d",
            closeOnDateSelect: true
        });

        var currentDate = new Date();
        var currentNepaliDate = calendarFunctions.getBsDateByAdDate(currentDate.getFullYear(), currentDate.getMonth() + 1, currentDate.getDate());
        var formatedNepaliDate = calendarFunctions.bsDateFormat("%y-%m-%d", currentNepaliDate.bsYear, currentNepaliDate.bsMonth, currentNepaliDate.bsDate);
        // $("#date").val(formatedNepaliDate);
    </script>

    {{--Convert Nepali date to roman date --}}
    <script>
        function convertNepaliToEnglish(input) {
            // console.log(input);
            var charArray = input.split('');
            //console.log(charArray);
            var engDate = '';
            $.each(charArray, function (key, value) {
                switch (value) {
                    case '१':
                        engDate += '1'
                        break
                    case '२':
                        engDate += '2'
                        break
                    case '३':
                        engDate += '3'
                        break
                    case '४':
                        engDate += '4'
                        break
                    case '५':
                        engDate += '5'
                        break
                    case '६':
                        engDate += '6'
                        break
                    case '०':
                        engDate += '0'
                        break
                    case '७':
                        engDate += '7'
                        break
                    case '८':
                        engDate += '8'
                        break
                    case '९':
                        engDate += '9'
                        break

                    case '-':
                        engDate += '-'
                        break
                }
                //console.log(engDate)
            })
            return engDate

        }
    </script>

    {{--Clear Voucher and Bhuktani field when  Add button click--}}
    <script>
        function clearVoucherField() {

            $('#main_activity_name').val(null).trigger('change');
            $('#hisab_number').val('')
            $('#details').val('')
            $('#party_type').val('')
            $('#party-name').val('')
            $('#amount').val('')

        }

        function clearBhuktaniField() {
            // alert("blank here")
            // $('#main_activity_name').val('')
            $('select#bhuktani_main_activity').val('');
            $('#bhuktani_party_type').val('');
            $('#bhuktani_party').val('');
            $('#bhuktani_amount').val('');
            $('#advance_tax_deduction_amount').val('');
            $('#vat_amount').val('');
            $('#bank').val('');
            $('#vat_bill_number').val('');
            $('#pratibadhata_number').val('');

        }
    </script>

    {{-- Ask before Reload --}}
    {{--<script>--}}
    {{--    window.onbeforeunload = function (e) {--}}
    {{--        e = e || window.event;--}}

    {{--        // For IE and Firefox prior to version 4--}}
    {{--        if (e && editingSN) {--}}
    {{--            e.returnValue = 'Sure?';--}}
    {{--        }--}}

    {{--        // For Safari--}}
    {{--        return 'Sure?';--}}
    {{--    };--}}
    {{--</script>--}}
@endsection
