
@extends('frontend.layouts.app')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h2>
        बजेट उपशीर्षक छान्नुहोस्
      </h2>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-body">
          <table class="table table-bordered">
            <thead>
            <tr>
              <th>सि.न.</th>
              <th>बजेट उपशिर्षक</th>
              {{--              <th>Action</th>--}}
            </tr>
            </thead>
            <tbody>
            @foreach($programs as $index=>$program)
              <tr>
                <td>{{++$index}}</td>
                <td> <u><a href="{{route('grant.voucher',$program->id)}}" >{{$program->name}} | {{$program->program_code}}</a></u></td>


                {{--                <td>--}}
                {{--                  <a type="button" href="{{route('bhuktani.create',$program->id)}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> भित्र</a>--}}
                {{--                </td>--}}
              </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>

@endsection