
@extends('frontend.layouts.app')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        भौचर अस्विकृत गर्ने।

      </h1>

    </section>
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-body">
          <form action="{{route('voucher.unapprove')}}" method="post">
            {{csrf_field()}}
            <div class="form-group">
              <label>कार्यालय</label>
              <select class="form-control" name="office_id">

                  <option value="{{Auth::user()->office->id}}">{{Auth::user()->office->name}}</option>

              </select>
            </div>

            <div class="form-group">
              <label>बजेट उपशिर्षक</label>
              <select class="form-control" name="budget_sub_head">
                @foreach($programs as $program)
                  <option value="{{$program->id}}">{{$program->name}} | {{$program->program_code}}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <label>भौचर न.</label>
             <input type="number" name="voucher_number" class="form-control" required>
            </div>
           <div class="form-group">
             <br>
             <input type="submit" class="btn btn-primary" value="अस्विकृत">
           </div>
          </form>
        </div>
      </div>
    </section>


  </div>

@endsection

@section('scripts')


@endsection