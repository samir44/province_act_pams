
@extends('backend.layouts.app')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tahas
        <small>Control panel</small>
      </h1>
      <ul class="breadcrumb">
        <li> <a type="button" href="{{route('admin.tahas')}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> List</a></li>
      </ul>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-body">
          <form action="{{route('admin.tahas.store')}}" method="post">
            {{csrf_field()}}
            <div class="form-group">
              <label for="name">सेवा:</label>
              <select name="sewa_id" id="sewa_id" class="form-control">
                @foreach($sewas as $sewa)
                  <option value="{{$sewa->id}}">{{$sewa->name}}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <label for="name">Name:</label>
              <input type="text" class="form-control" id="name" placeholder="Taha Name" name="name">
            </div>
            <button type="submit" class="btn btn-default">Submit</button>
          </form>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>

@endsection