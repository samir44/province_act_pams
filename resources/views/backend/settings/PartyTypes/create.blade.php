@extends('backend.layouts.app')
@section('title')
  Party Types Create
@stop
@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Party Types
        <small>Control panel</small>
      </h1>
      <ul class="breadcrumb">
        <li> <a type="button" href="{{route('partytypes')}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> List</a></li>
      </ul>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-body">
          <form action="{{route('partytypes.store')}}" method="post" id="programCreateForm">
            {{csrf_field()}}

            <div class="form-group">
              <label for="name">Name:</label>
              <input type="text" class="form-control" id="name" placeholder="Party Type" name="name">
            </div>

            <button type="submit" class="btn btn-default">Submit</button>
          </form>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>

@endsection
@section('scripts')
  <script>
    $("#programCreateForm").validate({
      rules: {
        name: {
          required: true,
        },
        program_code: {
          required: true,
        }
      },
      messages: {
        name: "Name Field is Required"
      }
    });
  </script>
@stop