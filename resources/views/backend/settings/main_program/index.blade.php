
@extends('backend.layouts.app')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Main Program
        <small>Control panel</small>
      </h1>
      <ul class="breadcrumb">
        <li> <a type="button" href="{{route('admin.main.program.create')}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> Create</a></li>
      </ul>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-body">
          <table class="table table-bordered">
            <thead>
            <tr>
              <th>Area Name</th>
              <th>Sub Area Name</th>
              <th>Main Program Name</th>
              <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($mainPrograms as $mainProgram)
              <tr>
                <td>{{$mainProgram->area->name}}</td>
                <td>{{$mainProgram->sub_area->name}}</td>
                <td>{{$mainProgram->name}}</td>
                <td>
                  <a type="button" href="{{route('admin.main.program.create')}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> Edit</a>
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>

@endsection