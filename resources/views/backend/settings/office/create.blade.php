@extends('backend.layouts.app')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Office
                <small>Control panel</small>
            </h1>
            <ul class="breadcrumb">
                <li><a type="button" href="{{route('admin.office')}}" class="btn btn-sm btn-primary"><i
                                class="fa fa-pencil"></i> List</a></li>
            </ul>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <form action="{{route('admin.office.store')}}" method="post">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="name">Name:</label>
                            <input type="text" class="form-control" id="name" placeholder="Office" name="name">
                        </div>
                        <div class="form-group">
                            <label for="office_code">Office Code:</label>
                            <input type="text" class="form-control" id="office_code" placeholder="office code" name="office_code">
                        </div>
                        <div class="form-group">
                            <label for="province">Province</label>
                            <select name="province_id" class="form-control">
                                <option>.................</option>
                                @foreach($provinces as $province)
                                    <option value="{{$province->id}}">{{$province->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="mof">MOF</label>
                            <select name="mof_id" class="form-control" id="mof">
                                <option>.................</option>
                                @foreach($mofs as $mof)
                                    <option value="{{$mof->id}}">{{$mof->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="ministry">Ministry</label>
                            <select name="ministry_id" class="form-control" id="ministry">
                                <option>.................</option>
                                @foreach($ministries as $ministry)
                                    <option value="{{$ministry->id}}">{{$ministry->name}} | {{$ministry->province->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="department">Department</label>
                            <select name="department_id" class="form-control" id="department">
                                <option>.................</option>
                                @foreach($departments as $department)
                                    <option value="{{$department->id}}">{{$department->name}} | {{$department->province->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>District</label>
                            <select class="form-control select2" name="district_id">
                                @foreach($districts as $district)
                                    <option value="{{$district->id}}">{{$district->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <button type="submit" class="btn btn-default">Submit</button>
                    </form>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>

@endsection

@section('scripts')

@endsection
