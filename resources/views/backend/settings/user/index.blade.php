@extends('backend.layouts.app')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                User
                <small>Control panel</small>
            </h1>
            <ul class="breadcrumb">
                <li><a type="button" href="{{route('admin.user.create')}}" class="btn btn-sm btn-primary"><i
                                class="fa fa-pencil"></i> Create</a></li>
            </ul>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Province</th>
                            <th>MOF</th>
                            <th>Ministry</th>
                            <th>Department</th>
                            <th>Office</th>
                            <th>Role</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>{{$user->name}}</td>
                                <td>{{$user->username}}</td>
                                <td>{{$user->email}}</td>
                                <td>
                                    @if($user->province)
                                        {{$user->province->name}}
                                    @endif
                                </td>
                                <td>
                                    @if($user->mof)
                                        {{$user->mof->name}}
                                    @endif
                                </td>
                                <td>
                                    @if($user->ministry)
                                        {{$user->ministry->name}}
                                    @endif
                                </td>
                                <td>
                                    @if($user->department)
                                        {{$user->department->name}}
                                    @endif
                                </td>
                                <td>
                                    @if($user->office)
                                        {{$user->office->name}}
                                    @endif
                                </td>
                                <td>
                                    @if($user->roles->count())
                                    {{$user->roles[0]->name}}
                                    @endif
                                </td>
                                <td>{{$user->status}}</td>
                                <td>
                                    <a type="button" href="" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i>
                                        Edit</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>

@endsection