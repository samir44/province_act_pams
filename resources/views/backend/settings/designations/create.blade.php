
@extends('backend.layouts.app')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Designations
        <small>Control panel</small>
      </h1>
      <ul class="breadcrumb">
        <li> <a type="button" href="{{route('admin.designations')}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> List</a></li>
      </ul>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-body">
          <form action="{{route('admin.designations.store')}}" method="post">
            {{csrf_field()}}
            <div class="form-group">
              <label for="name">पद:</label>
              <input type="text" class="form-control" id="name" placeholder="Sewa Name" name="name">
            </div>

            <div class="form-group">
              <label for="name">तह:</label>
              <select name="taha_id" id="taha_id" class="form-control">
                <option value="">.....</option>
                @foreach($tahas as $taha)
                <option value="{{$taha->id}}">{{$taha->name}}</option>
                @endforeach
              </select>
            </div>


            <button type="submit" class="btn btn-default">Submit</button>
          </form>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>

@endsection