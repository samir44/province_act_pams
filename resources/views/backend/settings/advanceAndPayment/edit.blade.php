
@extends('backend.layouts.app')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        पेश्की/भुक्तानी पाउने
        <small>Control panel</small>
      </h1>
      <ul class="breadcrumb">
        <li> <a type="button" href="{{route('admin.advance')}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> List</a></li>
      </ul>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-body">
          <form action="{{route('admin.AdvanceAndPayment.update',$advanceandpayment->id)}}" method="post">
            {{csrf_field()}}
            <div class="form-group">
              <label for="party_type">प्रकार</label>
              <select name="party_type" required>
                <option value="उपभोक्ता समिति" @if ($advanceandpayment->party_type == "उपभोक्ता समिति" ) selected @endif>उपभोक्ता समिति</option>
                <option value="ठेकेदार" @if ($advanceandpayment->party_type == "ठेकेदार" ) selected @endif>ठेकेदार</option>
                <option value="्यक्तिगत" @if ($advanceandpayment->party_type == "्यक्तिगत" ) selected @endif>व्यक्तिगत</option>
                <option value="संस्थागत" @if ($advanceandpayment->party_type == "संस्थागत" ) selected @endif>संस्थागत</option>
              </select>
            </div>
            <div class="form-group">
              <label for="name_nep">नाम नेपाली</label>
              <input type="text" class="form-control" id="name_nep" placeholder="Name" name="name_nep" value="{{$advanceandpayment->name_nep}}" required>
            </div>
            <div class="form-group">
              <label for="name_eng">नाम अग्रेजी</label>
              <input type="text" class="form-control" id="name_eng" placeholder="name_eng" name="name_eng" value="{{$advanceandpayment->name_eng}}" required>
            </div>
            <div class="form-group">
              <label for="citizen_number">नागरिकता न.</label>
              <input type="text" class="form-control" id="citizen_number" placeholder="citizen_number" name="citizen_number" value="{{$advanceandpayment->citizen_number}}" required>
            </div>
            <div class="form-group">
              <label for="vat_number">भ्याट/प्यान न.</label>
              <input type="text" class="form-control" id="vat_number" placeholder="vat_number" name="vat_number" value="{{$advanceandpayment->vat_pan_number}}" required>
            </div>
            <div class="form-group">
              <label for="mobile_number">मोबाइल न.</label>
              <input type="text" class="form-control" id="mobile_number" placeholder="vat_number" name="mobile_number" value="{{$advanceandpayment->phone_number}}" required>
            </div>
            <div class="form-group">
              <label for="is_advance">पेश्किमा देखाउने</label>
              <select name="is_advance" required>
                <option value="0" @if ($advanceandpayment->is_advance == 0) selected @endif>हो</option>
                <option value="2" @if ($advanceandpayment->is_advance == 2) selected @endif>होइन</option>
              </select>
            </div>
            <div class="form-group">
              <label for="is_bhuktani">भुक्तानीमा देखाउने</label>
              <select name="is_bhuktani" required>
                <option value="0" @if ($advanceandpayment->is_bhuktani == 0) selected @endif>हो</option>
                <option value="2" @if ($advanceandpayment->is_bhuktani == 2) selected @endif>होइन</option>

              </select>
            </div>
            <div class="form-group">
              <label for="is_dharauti">धरौटीमा देखाउने</label>
              <select name="is_dharauti" required>
                <option value="0" @if ($advanceandpayment->is_dharauti == 0) selected @endif>हो</option>
                <option value="2" @if ($advanceandpayment->is_dharauti == 2) selected @endif>होइन</option>
              </select>
            </div>

            <button type="submit" class="btn btn-default">Submit</button>
          </form>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>

@endsection