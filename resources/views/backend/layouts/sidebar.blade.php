 <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ asset('admin/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{Auth::user()->name}}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
          </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>

            <li class=""><a href="{{route('admin')}}"><i class="fa fa-dashboard"></i> Dashboard </a></li>

  @if(Auth::user()->roles->first()->name=='superadmin')
        <li class="treeview @if(strpos($_SERVER['REQUEST_URI'], 'setting')) active @endif">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Settings</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="@if(strpos($_SERVER['REQUEST_URI'], 'province')) active @endif"><a href="{{route('admin.province')}}"><i class="fa fa-circle-o"></i> Province</a></li>
            <li class="@if(strpos($_SERVER['REQUEST_URI'], 'mof')) active @endif"><a href="{{route('admin.mof')}}"><i class="fa fa-circle-o"></i> MOF</a></li>
            <li class="@if(strpos($_SERVER['REQUEST_URI'], 'ministry')) active @endif"><a href="{{route('admin.ministry')}}"><i class="fa fa-circle-o"></i> Ministry</a></li>
            <li class="@if(strpos($_SERVER['REQUEST_URI'], 'department')) active @endif"><a href="{{route('admin.department')}}"><i class="fa fa-circle-o"></i> Department</a></li>
            <li class="@if(strpos($_SERVER['REQUEST_URI'], 'office')) active @endif"><a href="{{route('admin.office')}}"><i class="fa fa-circle-o"></i> Office</a></li>
            <li class="@if(strpos($_SERVER['REQUEST_URI'], 'role')) active @endif"><a href="{{route('admin.role')}}"><i class="fa fa-circle-o"></i> Role</a></li>
            <li class="@if(strpos($_SERVER['REQUEST_URI'], 'user')) active @endif"><a href="{{route('admin.user')}}"><i class="fa fa-circle-o"></i> User</a></li>

          </ul>
        </li>
    @endif
        @if(Auth::user()->roles->first()->name=='superadmin')
          <li class="treeview @if(strpos($_SERVER['REQUEST_URI'], 'setting')) active @endif">
            <a href="#">
              <i class="fa fa-files-o"></i>
              <span>Settings</span>
              <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
              <li class="@if(strpos($_SERVER['REQUEST_URI'], 'program')) active @endif"><a href="{{route('admin.area')}}"><i class="fa fa-paw"></i> क्षेत्र </a></li>
              <li class="@if(strpos($_SERVER['REQUEST_URI'], 'program')) active @endif"><a href="{{route('admin.sub.area')}}"><i class="fa fa-paw"></i> उप-क्षेत्र </a></li>
              <li class="@if(strpos($_SERVER['REQUEST_URI'], 'program')) active @endif"><a href="{{route('admin.main.program')}}"><i class="fa fa-paw"></i> मुख्य कार्यक्रम </a></li>
              <li class="@if(strpos($_SERVER['REQUEST_URI'], 'program')) active @endif"><a href="{{route('admin.darbandisrot')}}"><i class="fa fa-paw"></i>Darbandi ko Srot</a></li>
              <li class="@if(strpos($_SERVER['REQUEST_URI'], 'program')) active @endif"><a href="{{route('admin.darbanditype')}}"><i class="fa fa-paw"></i>Darbandi Type</a></li>
              <li class="@if(strpos($_SERVER['REQUEST_URI'], 'program')) active @endif"><a href="{{route('admin.sewas')}}"><i class="fa fa-paw"></i>Sewa</a></li>
              <li class="@if(strpos($_SERVER['REQUEST_URI'], 'program')) active @endif"><a href="{{route('samuha')}}"><i class="fa fa-paw"></i>समूह</a></li>
              <li class="@if(strpos($_SERVER['REQUEST_URI'], 'program')) active @endif"><a href="{{route('admin.designations')}}"><i class="fa fa-paw"></i>Designation</a></li>
              <li class="@if(strpos($_SERVER['REQUEST_URI'], 'program')) active @endif"><a href="{{route('admin.tahas')}}"><i class="fa fa-paw"></i>Taha</a></li>
              <li class="@if(strpos($_SERVER['REQUEST_URI'], 'program')) active @endif"><a href="{{route('admin.sourcetypes')}}"><i class="fa fa-paw"></i>स्रोत प्रकार</a></li>
              <li class="@if(strpos($_SERVER['REQUEST_URI'], 'program')) active @endif"><a href="{{route('admin.budget.source')}}"><i class="fa fa-paw"></i>बजेट स्रोत/तह</a></li>
              <li class="@if(strpos($_SERVER['REQUEST_URI'], 'program')) active @endif"><a href="{{route('admin.source')}}"><i class="fa fa-paw"></i>स्रोत</a></li>
              <li class="@if(strpos($_SERVER['REQUEST_URI'], 'program')) active @endif"><a href="{{route('admin.mediums')}}"><i class="fa fa-paw"></i>प्राप्तिको विधि</a></li>
              <li class="@if(strpos($_SERVER['REQUEST_URI'], 'program')) active @endif"><a href="{{route('partytypes')}}"><i class="fa fa-paw"></i>Party Types</a></li>


            </ul>
          </li>
        @endif
        <li class="treeview @if(strpos($_SERVER['REQUEST_URI'], 'setting')) active @endif">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>आधारभुत प्रविशटि</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
             <li class="@if(strpos($_SERVER['REQUEST_URI'], 'program')) active @endif"><a href="{{route('admin.program')}}"><i class="fa fa-paw"></i> बजेट उपशिर्षक </a></li>
            <li class="@if(strpos($_SERVER['REQUEST_URI'], 'program')) active @endif"><a href="{{route('admin.bank')}}"><i class="fa fa-paw"></i>बैकँ नाम र खाता नम्बर</a></li>
            <li class="@if(strpos($_SERVER['REQUEST_URI'], 'program')) active @endif"><a href="{{route('admin.advance')}}"><i class="fa fa-paw"></i>पेश्की लेजर</a></li>
            <li class="@if(strpos($_SERVER['REQUEST_URI'], 'program')) active @endif"><a href="{{route('admin.program')}}"><i class="fa fa-paw"></i>लेजर छनौट</a></li>
{{--            <li class="@if(strpos($_SERVER['REQUEST_URI'], 'program')) active @endif"><a href="{{route('admin.budget.create')}}"><i class="fa fa-paw"></i>वार्षिक बजेट प्रविश्टि</a></li>--}}
{{--            <li class="@if(strpos($_SERVER['REQUEST_URI'], 'program')) active @endif"><a href="{{route('admin.program')}}"><i class="fa fa-paw"></i>सहायक लेजर</a></li>--}}

          </ul>
        </li>

       </ul>
    </section>
    <!-- /.sidebar -->
  </aside>